<head>
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/termos.css" >
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/theme-animate.css" >
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <script src="<?php echo base_url(); ?>style/ckeditor/ckeditor.js"></script>
    <link type="text/css" href="<?php echo base_url(); ?>style/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">


    <script>
        function sair_modal(){
            $('#preto').css('display', 'none');
            $('#preto').css('opacity', '0');
            $('#ver_edit').css('display', 'none');
            $('#ver_cidade').css('display', 'none');


        }
        function abrir_modal(){
            $('#preto').animate({'opacity':'.60'}, 100, 'linear');
            $('#preto').css('display', 'block');
            $('#ver_edit').css('display', 'table');
        }
        function abrir_cidade(status,nome,id){
            sair_modal();


            $('#preto').animate({'opacity':'.60'}, 100, 'linear');
            $('#preto').css('display', 'block');
            $('#ver_cidade').css('display', 'table');
            $("#nome_da_cidade").html('');
            var clone = nome;
            $('#nome_da_cidade').append(clone);

            $("#status_final").html('');
            if(status == 1){
                var texto = 'Desativar';
            }
            else {
                var texto = 'Ativar';
            }
            var clone = texto;
            $('#status_final').append(clone);


            $("#local_input").html('');
            if(status == 1){
                var texto = 'Desativar';
            }
            else {
                var texto = 'Ativar';
            }

            var clone = '<input type="hidden" name="status" value="'+status+'"><input type="hidden" name="cidade" value="'+nome+'"><input type="hidden" name="id" value="'+id+'">';

            $('#local_input').append(clone);

        }
    </script>


</head>





<div id="ver_cidade" style="position: fixed;width: 400px;z-index: 9999;margin-top: 80px;display: none;box-shadow: 0 0 2px #292929;background: #fff;padding: 30px;left: 50%;margin-left: -200px">
    <form action="<?php echo base_url(); ?>controller_adm/atulizar_cidade" method="POST">
        <p style="font-size: 20px;">Realmente deseja <span id="status_final">desativar</span></p>
        <p style="font-size: 20px;" id="nome_da_cidade"> </p>
        <button style="width: 49%;margin-top: 15px;background: #666;border: none;padding: 15px 0px;color: #fff" class="botao"><span>Sim</span></button>

        <span id="local_input"></span>

        <div onclick="sair_modal()" id="bot_cancel" style="width: 49%;float: right;text-align: center;margin-left: 1%;margin-top: 15px;background: #666;border: none;padding: 16px 0px;color: #fff"><span>Não</span></div>
    </form>
</div>



<div>


    <div id="ver_edit" style="position: fixed;width: 400px;z-index: 9999;margin-top: 80px;display: none;box-shadow: 0 0 2px #292929;background: #fff;padding: 30px;left: 50%;margin-left: -200px">
       <h3 style="text-align: center">Nova cidade</h3>
        <form action="<?php echo base_url(); ?>controller_adm/cidade" method="POST">
            <input type="text" style="width: 100%;padding: 10px 15px;border: none;box-shadow: 1px 1px 4px #ccc" name="cidade" placeholder="Nome da cidade">
            <button style="width: 100%;margin-top: 15px;background: #666;border: none;padding: 15px 0px;color: #fff" class="botao"><span>Enviar</span></button>
        </form>
    </div>


            <h1 class="fade_1">  <i class="fas fa-1x  fa-building lado_topo" data-wow-duration="2s" data-wow-delay="0.3s" > </i>  <i  style="margin-left: -20px" class="fas fa-2x  fa-building lado_topo" data-wow-duration="2s" data-wow-delay="0.9s" > </i>  <i class="fas fa-1x  fa-building lado_topo" style="margin-left: -20px" data-wow-duration="2s" data-wow-delay="0.6s" > </i>  &nbsp;&nbsp;Cidades</h1>
    <div class="lado_direito" style="height: 2px;background: #f8d509;margin-top: 30px;overflow-x: hidden"></div>
    <div class="col-lg-12 col-md-12 col-sm-12"   style="padding: 0;margin-top: 20px;">


        <!--CADASTRAR-->
        <div class="col-lg-3" style="padding: 0;padding-right: 6px;">
            <div class="col-lg-12 botao" onclick="abrir_modal()" style="background: #666;border: 4px solid #fff;box-shadow: 0 0 4px #ccc;color: #fff ;padding: 20px 10px;margin-bottom: 10px;text-align: left;" >
                <div class="col-lg-8" style="z-index: 9999;position: relative">
                    <i class="fas fa-1x  fa-plus lado_topo" data-wow-duration="2s" data-wow-delay="0.3s" > </i>&nbsp; Nova Cidade
                </div>
            </div>
        </div>
        <div class="col-lg-12">&nbsp;</div>




        <?php
        foreach ($dados_iniciais['dados'] as $cidades_atuacao):
            extract($cidades_atuacao);
            ?>
            <div class="col-lg-3" style="padding: 0;padding-right: 6px;">



                <?php
                if($cidades_atuacao['ativa_cidades_atuacao'] == '1'){
                    echo '<div class="col-lg-12 botao" onclick="status('.$cidades_atuacao['ativa_cidades_atuacao'].',\''.$cidades_atuacao['nome_cidades_atuacao'].'\','.$cidades_atuacao['id_cidades_atuacao'].')" style="background: #666;border: 4px solid #fff;box-shadow: 0 0 4px #ccc;color: #fff ;padding: 20px 10px;margin-bottom: 10px;text-align: left;" >';
                }
                else{
                    echo '<div class="col-lg-12 botao" onclick="status('.$cidades_atuacao['ativa_cidades_atuacao'].',\''.$cidades_atuacao['nome_cidades_atuacao'].'\','.$cidades_atuacao['id_cidades_atuacao'].')" style="background: #292929;border: 4px solid #fff;box-shadow: 0 0 4px #ccc;color: #fff ;padding: 20px 10px;margin-bottom: 10px;text-align: left;" >';
                }
                ?>

                    <div class="col-lg-9" style="z-index: 9999;position: relative">
                        <i class="fas fa-1x  fa-building lado_topo" data-wow-duration="2s" data-wow-delay="0.3s" > </i> &nbsp;<?php echo $cidades_atuacao['nome_cidades_atuacao']; ?>
                    </div>
                    <div class="col-lg-3" style="margin-left: -20px">
                        <p style="z-index: 9999;position: relative">
                            <?php
                            if($cidades_atuacao['ativa_cidades_atuacao'] == '1'){
                                echo 'Ativo';
                            }
                            else{
                                echo 'Desativado';
                            }
                            ?>
                            <script>
                                function status(status,nome,id) {
                                    abrir_cidade(status,nome,id);
                                }
                            </script>

                        </p>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>


    </div>
</div>
<style>
    .botao{
        margin-top: 1px;
        visibility: visible;
        animation-duration: 0.3s;
        animation-delay: 0s;
        animation-name: fadeIn;
    }
    .botao:hover{
        cursor: pointer;
        background: #000;
        color: #fff;
        transition-duration: 0.3s;
        transition-timing-function: ease-in;
        transition-property: all;
        visibility: visible;
        animation-duration: 0.3s;
        animation-delay: 0s;
        animation-name: fadeIn;
    }

    #bot_cancel{
        animation-duration: 0.3s;
        animation-delay: 0s;
        animation-name: fadeInUp;
    }
    #bot_cancel:hover{
        opacity: .60;
        cursor: pointer;
    }
</style>
