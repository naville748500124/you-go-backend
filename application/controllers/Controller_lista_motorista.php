<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_lista_motorista extends MY_Controller {
        public function __construct(){
            parent::__construct();
            $this->load->model('model_motorista');
        }

    function listar(){
        $listar =  $this->input->get('id_motorista');
        $dados_motorista['motorista'] = $this->model_motorista->listar_motorista($listar);
        $this->load->view('estrutura/header');
        $this->load->view('motorista/lista_motorista',$dados_motorista);
        $this->load->view('estrutura/footer');
    }


}