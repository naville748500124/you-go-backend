<head>
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/cupons.css" >
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/theme-animate.css" >
    <link type="text/css" href="<?php echo base_url(); ?>style/css/bootstrap.min.css" rel="stylesheet" />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <meta name="theme-color" content="#292929">
    <style type="text/css">
        .ui-datepicker {
            width: 15.3em !important;
            visibility: fadeinDwon;
        }
        #ui-datepicker-div{
            z-index: 999 !important;
            position: relative;
        }
        .ui-datepicker-header{
            background: #f8d509 !important;
            border-color: #f8d509 !important;
            color: #000 !important;
        }
        .ui-widget-content {
            border: 1px solid #dddddd;
            background: #292929;
            color: #fff;
        }
        .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
            border: 1px solid #fff;
            background: #292929;
            font-weight: bold;
            color: #fff;
        }
         .ui-state-default:hover {
            background: #fff;
            font-weight: bold;
            color: #000;
        }


    </style>


</head>



<script>
    function sair_modal(){
        $('#preto').css('display', 'none');
        $('#preto').css('opacity', '0');
        $('#box').css('display', 'none');


    }
    function abrir_modal(){
        $('#preto').animate({'opacity':'.60'}, 1800, 'linear');
        $('#modal_info').animate({'opacity':'.60'}, 2500, 'linear');
        $('#preto').css('display', 'block');
        $('#box').css('display', 'table');
    }
    $(document).ready(function(){
        timer = setInterval(updateDiv,10);
        function updateDiv(){
            var campo = document.getElementById('percentagem').value;

            if(campo == ""){
                $('#fixo').css('display','inline');
                $('#fixo').css('opacity','1');
                document.getElementById('fixo').removeAttribute('disabled',true);
            }
            else{
                $('#fixo').css('opacity','.40');
                document.getElementById('fixo').setAttribute('disabled',true);
            }


            var campo_fixo = document.getElementById('fixo').value;
            if(campo_fixo == ""){
                $('#percentagem').css('display','inline');
                $('#percentagem').css('opacity','1');
                document.getElementById('percentagem').removeAttribute('disabled',true);
            }
            else{
                $('#percentagem').css('opacity','.40');
                document.getElementById('percentagem').setAttribute('disabled',true);

            }

        }

    });
</script>


<div id="box" class="lado_topo">

    <svg  id="svg_linha" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="600" height="400">
        <rect x="0" class="logo" y="0" width="600" height="400" fill="none" stroke="#000" stroke-width="3" />
    </svg>
    <div style="z-index: 999;margin:  0 auto;margin-top: -380px;width: 80%;" class="fade_1">

        <form class="fade_1 center-block" action="<?php echo base_url();?>controller_motorista/cupons" method="post">

            <h3 style="color: #fff" class="fade_1"> <i class="fa fa-1x fa-plus lado_topo" data-wow-duration="2s" data-wow-delay="0.0s" > </i>    Novo Cupom</h3>

            <input type="text" class="form_modal fade_1"  placeholder="Nome" name="nome_cupom" required>

            <input type="number" min="1" max="100" class="form_modal fade_1" placeholder="Valor de desconto em %"  style="width: 47.5%" name="valor_porcentagem_cupom" id="percentagem" value="">
            <span style="color: #fff;margin-top: -10px;margin-bottom: -10px">Ou</span>
            <input type="text" class="form_modal fade_1" placeholder="Valor de desconto em R$" style="width: 47.5%"  name="valor_fixo_cupom"  id="fixo" value="" onKeyPress="return(moeda(this,'.',',',event))"  >

            <input type="text" class="form_modal fade_1" placeholder="Quantidade de vezes"  name="quantidade_cupom" required>
            <input type="text" class="mascara_data form_modal fade_1" placeholder="Início" name="data_inicio_cupom" style="width: 50%" required>
            <input type="text" class="mascara_data form_modal fade_1" placeholder="Validade" style="width: 49%" name="data_fim_cupom" required>
            <button  type="submit" class="botao lado_baixo botao_envia_pagamento"  onclick="abrir_modal()" ><span>Salvar</span></button>
        </form>
    </div>

</div>

<div>
    <h1 class="fade_1"><i class="fa fa-2x fa-ticket lado_topo" data-wow-duration="2s" data-wow-delay="0.0s" > </i>  Cadastro de Cupons</h1>
    <div class="lado_direito" style="width: 100%;height: 2px;background: #f8d509;margin-top: 30px;overflow-x: hidden"></div>
    <button  type="submit" class="botao lado_baixo botao_envia_pagamento" onclick="abrir_modal()" ><span><i class="fa fa-1x fa-plus" > </i> Novo cupom</span></button>
</div>



<div class="lado_direito" style="width: 100%;height: 2px;background: transparent;margin-top: 40px;margin-bottom: 10px"></div>

<table class="table table-bordered table-hover" align="center">
    <thead style="background: #f1f1f1">
    <tr>
        <th>Código</th>
        <th>Nome</th>
        <th>Valor de desconto</th>
        <th>Ínicio</th>
        <th>Termino</th>
        <th>Quantidade de vezes</th>
        <th class="no-filter" style="text-align: center">Status</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach ($dados_iniciais['dados'] as $cupons){
        extract($cupons);
        $data_inicio_certa = explode('-',$data_inicio_cupom);
        $data_termino_certa = explode('-',$data_termino_cupom);
        ?>

        <tr style="text-align: center">
            <td><p style="margin-top: 10px"><?php echo $codigo_cupom; ?></p></td>
            <td><p style="margin-top: 10px"><?php echo $nome_cupom; ?></p></td>
            

            <td><p style="margin-top: 10px">
                <?php if($tipo_desconto_cupom == 1){
                     echo $valor_desconto_cupom.'%';
                 }
                else{ 
                    echo number_format($valor_desconto_cupom,2,',','.');
                     } ?> 
            </p></td>


            <td><p style="margin-top: 10px"><?php echo $data_inicio_certa[2]; ?>/<?php echo $data_inicio_certa[1]; ?>/<?php echo $data_inicio_certa[0]; ?></p></td>
            <td><p style="margin-top: 10px"><?php echo $data_termino_certa[2]; ?>/<?php echo $data_termino_certa[1]; ?>/<?php echo $data_termino_certa[0]; ?></p></td>
            <td><p style="margin-top: 10px"><?php echo $quantidade_cupom; ?></p></td>
           

            <?php if($ativo_cupom == 1){
                     echo '<td><a href="'.base_url().'controller_motorista/cupons_ativar?id='.$id_cupom.'&status='.$ativo_cupom.'"   class="btn btn-danger" style="background-color: #292929;border-color: #000;" >';
                 }
                else{ 
              echo '<td><a href="'.base_url().'controller_motorista/cupons_ativar?id='.$id_cupom.'&status='.$ativo_cupom.'"  class="btn btn-danger" style="background: #f8d509;border-color: #FCCD08;color: #000" >';
                 } ?>
               <?php if($ativo_cupom == 0){ ?>
               Ativar
               <?php }else{ ?>
               Desativar
               <?php } ?>
           </a></td>
        </tr>

    <?php } ?>
    </tbody>
</table>

 <script language="javascript">
        function moeda(a, e, r, t) {
            var n = ""
                , h = j = 0
                , u = tamanho2 = 0
                , l = ajd2 = ""
                , o = window.Event ? t.which : t.keyCode;
            if (13 == o || 8 == o)
                return !0;
            if (n = String.fromCharCode(o),
                -1 == "0123456789".indexOf(n))
                return !1;
            for (u = a.value.length,
                     h = 0; h < u && ("0" == a.value.charAt(h) || a.value.charAt(h) == r); h++)
                ;
            for (l = ""; h < u; h++)
                -1 != "0123456789".indexOf(a.value.charAt(h)) && (l += a.value.charAt(h));
            if (l += n,
                0 == (u = l.length) && (a.value = ""),
                1 == u && (a.value = "0" + r + "0" + l),
                2 == u && (a.value = "0" + r + l),
                u > 2) {
                for (ajd2 = "",
                         j = 0,
                         h = u - 3; h >= 0; h--)
                    3 == j && (ajd2 += e,
                        j = 0),
                        ajd2 += l.charAt(h),
                        j++;
                for (a.value = "",
                         tamanho2 = ajd2.length,
                         h = tamanho2 - 1; h >= 0; h--)
                    a.value += ajd2.charAt(h);
                a.value += r + l.substr(u - 2, u)
            }
            return !1
        }
    </script>
