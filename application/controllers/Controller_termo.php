<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_termo extends MY_Controller {
        public function __construct(){
            parent::__construct();
            $this->load->model('model_termo');
        }

    function termo(){
        $termo =  $this->input->post('termos');
        $this->model_termo->termo($termo);
        redirect('main/redirecionar/23');
    }
    function termo_motorista(){
        $termo =  $this->input->post('termos_mot');
        $this->model_termo->termo_motorista($termo);
        redirect('main/redirecionar/23');
    }

    ######################################################
    //Termo de uso
    ######################################################
    public function termos_de_uso(){
      $termo['termo'] = $this->model_termo->termosDeUso();
      $this->load->view('termo/termo_app',$termo);

    }
    public function termos_de_uso_motorista(){
        $termo['termo'] = $this->model_termo->termosDeUso_mot();
        $this->load->view('termo/termo_app_motorista',$termo);
    }

}

