<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_tarifas extends MY_Controller {


    function __construct() {
        parent::__construct();
        $this->load->model('model_tarifas');
    }

    function  gerais(){
        $tarifa1_valor = $this->input->post('tarifa1_valor');
        $tarifa1_inicio = $this->input->post('tarifa1_inicio');
        $tarifa1_fim = $this->input->post('tarifa1_fim');
        $tarifa1_min = $this->input->post('tarifa1_min');
        $usar_tarifa = $this->input->post('usar_tarifa');

        $tarifa2_valor = $this->input->post('tarifa2_valor');
        $tarifa2_inicio = $this->input->post('tarifa2_inicio');
        $tarifa2_fim = $this->input->post('tarifa2_fim');
        $tarifa2_min = $this->input->post('tarifa2_min');
        $usar_tarifa2 = $this->input->post('usar_tarifa2');

        $tempo_cancelamento = $this->input->post('tempo_cancelamento');
        $tempo_busca = $this->input->post('tempo_busca');

        $tempo_espera = $this->input->post('tempo_espera');
        $km_max = $this->input->post('km_max');
        $cancelamento = $this->input->post('cancelamento');
        $yougo = $this->input->post('yougo');

            $tirar = array(' ','.','R$');
             $tarifa1_valor =  str_replace($tirar,'',$tarifa1_valor);
             $tarifa1_valor =  str_replace(',','.',$tarifa1_valor);

            $tarifa1_min =  str_replace($tirar,'',$tarifa1_min);
            $tarifa1_min =  str_replace(',','.',$tarifa1_min);

            $tarifa2_valor =  str_replace($tirar,'',$tarifa2_valor);
            $tarifa2_valor =  str_replace(',','.',$tarifa2_valor);

            $tarifa2_min =  str_replace($tirar,'',$tarifa2_min);
            $tarifa2_min =  str_replace(',','.',$tarifa2_min);

            $cancelamento =  str_replace($tirar,'',$cancelamento);
            $cancelamento =  str_replace(',','.',$cancelamento);



        if(empty($usar_tarifa[0])){
            $usar_tarifa_valor = 0;
        }
        else{
            $usar_tarifa_valor = 1;
        }
        if(empty($usar_tarifa2[0])){
            $usar_tarifa_valor2 = 0;
        }
        else{
            $usar_tarifa_valor2 = 1;
        }



        $dados_tarifa1 = array(
            'valor_tarifa' => $tarifa1_valor,
            'valor_tarifa_minima' => $tarifa1_min,
            'usar_tarifa_minima' => $usar_tarifa_valor,
            'hora_inicio' => $tarifa1_inicio,
            'hora_fim' => $tarifa1_fim
        );

        $dados_tarifa2 = array(
            'valor_tarifa' => $tarifa2_valor,
            'valor_tarifa_minima' => $tarifa2_min,
            'usar_tarifa_minima' => $usar_tarifa_valor2,
            'hora_inicio' => $tarifa2_inicio,
            'hora_fim' => $tarifa2_fim
        );

        $config = array(
          'tempo_espera' =>  $tempo_espera,
          'km_maximo' =>  $km_max,
          'taxa_cancelamento' =>  $cancelamento,
          'taxa_yougo' =>  $yougo,
           'tempo_cancelamento' => $tempo_cancelamento,
           'tempo_busca' => $tempo_busca    
        );



        $this->model_tarifas->atualizar_tarifa1($dados_tarifa1);
        $this->model_tarifas->atualizar_tarifa2($dados_tarifa2);
        $this->model_tarifas->configuracoes($config);

        redirect('/main/redirecionar/25');

    }
    function  nova_taxa(){
        $tarifa1_valor = $this->input->post('tarifa1_valor');

	   $fk_cidade = $this->input->post('fk_cidade');
       $nome_tarifa = $this->input->post('nome_tarifa');
	   $valor_tarifa = $this->input->post('valor_tarifa');
	   $valor_fixo = $this->input->post('valor_fixo');
	   $status_tarifa = $this->input->post('status_tarifa');


        $tirar = array(' ','.','R$');
        $valor_tarifa =  str_replace($tirar,'',$valor_tarifa);
        $valor_tarifa =  str_replace(',','.',$valor_tarifa);


	   if($status_tarifa[0] == 'on'){
           $status_bol = '1';
       }
       else{
           $status_bol = '0';
       }


	   if($valor_fixo[0] == 'on'){
           $valor_fixo_bol = '0';
       }
       else{
           $valor_fixo_bol = '1';
       }




	   $nova = array(
	   'fk_cidade'  => $fk_cidade ,
 	   'nome_tarifa'  => $nome_tarifa ,
 	   'valor_tarifa' => $valor_tarifa,
 	   'valor_fixo' => $valor_fixo_bol,
 	   'status_tarifa' => $status_bol
 	   );

        $this->model_tarifas->tarifa_cidade($nova);
        redirect('/main/redirecionar/25');

    }

    function atualizar_taxa(){

        $id_taxa = $this->input->post('id_tarifa_atualizar');

        $caidade_atualizar = $this->input->post('caidade_atualizar');
        $nome_tarifa_atualizar = $this->input->post('nome_tarifa_atualizar');
        $valor_tarifa_atualizar = $this->input->post('valor_tarifa_atualizar');




        $tirar = array(' ','.','R$');
        $valor_tarifa_atualizar =  str_replace($tirar,'',$valor_tarifa_atualizar);
        $valor_tarifa_atualizar =  str_replace(',','.',$valor_tarifa_atualizar);


        $status_atulizar = $this->input->post('status_atualizar');
        $valor_fixo_atulizar = $this->input->post('valor_fixo_atulizar');


        if($status_atulizar == 'on'){
            $status_bol = '1';
        }
        else{
            $status_bol = '0';
        }


        if($valor_fixo_atulizar == 'on'){
            $valor_fixo_bol = '0';
        }
        else{
            $valor_fixo_bol = '1';
        }




        $atulizar_tarifa_cidades = array(
            'fk_cidade'  => $caidade_atualizar,
            'nome_tarifa'  => $nome_tarifa_atualizar ,
            'valor_tarifa' => $valor_tarifa_atualizar,
            'valor_fixo' => $valor_fixo_bol,
            'status_tarifa' => $status_bol
        );



        $this->model_tarifas->atulizar_tarifa_cidades($atulizar_tarifa_cidades,$id_taxa);
        redirect('/main/redirecionar/25');


    }

}
