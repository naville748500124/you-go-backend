<head>
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/cupons.css" >
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/theme-animate.css" >
    <meta name="theme-color" content="#292929">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">

</head>

<div>
    <h1 class="fade_1"><i class="fas fa-phone  fa-2x lado_topo" data-wow-duration="2s" data-wow-delay="0.0s" > </i> Contatos</h1>
    <div class="lado_direito" style="width: 100%;height: 2px;background: #f8d509;margin-top: 30px;overflow-x: hidden"></div>
</div>
<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" style="padding:0;">
    <p style="margin-bottom: -10px;margin-top: 40px;font-size: 18px;font-weight: bold">Motorista</p>
    <form action="<?= base_url(); ?>controller_adm/atualizar_contato" method="post">

        <input type="text" name="tel_empresa" value="<?= $dados_iniciais['tel_empresa']; ?>"    style="border:none;border-bottom: 1px solid #292929;background: none;padding: 5px 13px;width: 100%;margin-top: 20px;">
        <input type="text" name="email_empresa" value="<?= $dados_iniciais['email_empresa']; ?>"  style="border:none;border-bottom: 1px solid #292929;background: none;padding: 5px 13px;width: 100%;margin-top: 20px;">

        <p style="margin-bottom: -10px;margin-top: 40px;font-size: 18px;font-weight: bold">Passageiro</p>

        <input type="text" name="tel_empresa_passageiro" value="<?= $dados_iniciais['tel_empresa_passageiro']; ?>"    style="border:none;border-bottom: 1px solid #292929;background: none;padding: 5px 13px;width: 100%;margin-top: 20px;">
        <input type="text" name="email_empresa_passageiro" value="<?= $dados_iniciais['email_empresa_passageiro']; ?>"  style="border:none;border-bottom: 1px solid #292929;background: none;padding: 5px 13px;width: 100%;margin-top: 20px;">

</div>
<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="padding:0;"></div>
<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" style="padding:0;">
    <p style="margin-bottom: -10px;margin-top: 40px;font-size: 18px;font-weight: bold">Endereço</p>
    <input type="text" name="cep_empresa" value="<?= $dados_iniciais['cep_empresa']; ?>"  id="cep"  onblur="pesquisacep(this.value);" maxlength="9"       placeholder="CEP" style="border:none;border-bottom: 1px solid #292929;background: none;padding: 5px 13px;width: 100%;margin-top: 20px;">


        <script>
            $(document).ready(function () {
                $('#uf').val('<?= $dados_iniciais['uf_empresa']; ?>').trigger('change');
            });
        </script>

        <select name="uf_empresa" id="uf"  >
            <option value="AC" selected>AC</option>
            <option value="AL">AL</option>
            <option value="AP">AP</option>
            <option value="AM">AM</option>
            <option value="BA">BA</option>
            <option value="CE">CE</option>
            <option value="DF">DF</option>
            <option value="ES">ES</option>
            <option value="GO">GO</option>
            <option value="MA">MA</option>
            <option value="MT">MT</option>
            <option value="MS">MS</option>
            <option value="MG">MG</option>
            <option value="PA">PA</option>
            <option value="PB">PB</option>
            <option value="PR">PR</option>
            <option value="PE">PE</option>
            <option value="PI">PI</option>
            <option value="RJ">RJ</option>
            <option value="RN">RN</option>
            <option value="RS">RS</option>
            <option value="RO">RO</option>
            <option value="RR">RR</option>
            <option value="SC">SC</option>
            <option value="SP">SP</option>
            <option value="SE">SE</option>
            <option value="TO">TO</option>
        </select>

    <div id="mensagem_cep"></div>


    <input type="text" name="cidade_empresa" value="<?= $dados_iniciais['cidade_empresa']; ?>"  id="cidade" placeholder="Cidade" style="border:none;border-bottom: 1px solid #292929;background: none;padding: 5px 13px;width: 100%;margin-top: 20px;">
    <input type="text" name="bairro_empresa" value="<?= $dados_iniciais['bairro_empresa']; ?>"  id="bairro" placeholder="Bairro" style="border:none;border-bottom: 1px solid #292929;background: none;padding: 5px 13px;width: 100%;margin-top: 20px;">
    <input type="text" name="rua_empresa" value="<?= $dados_iniciais['rua_empresa']; ?>"        id="rua" placeholder="Rua" style="border:none;border-bottom: 1px solid #292929;background: none;padding: 5px 13px;width: 49%;margin-top: 20px;margin-right: 1%">
    <input type="text" name="numero_empresa" value="<?= $dados_iniciais['numero_empresa']; ?>"  id="numero" placeholder="Numero" style="border:none;border-bottom: 1px solid #292929;background: none;padding: 5px 13px;width: 49%;margin-top: 20px;">




        <p style="margin-bottom: -10px;margin-top: 40px;font-size: 18px;font-weight: bold">Link Youtube Passageiro</p>

        <input type="text" name="link_youtube" value="<?= $dados_iniciais['link_youtube']; ?>"    style="border:none;border-bottom: 1px solid #292929;background: none;padding: 5px 13px;width: 100%;margin-top: 20px;">

<p style="margin-bottom: -10px;margin-top: 40px;font-size: 18px;font-weight: bold">Link Youtube Motorista</p>
        <input type="text" name="link_youtube_motorista" value="<?= $dados_iniciais['link_youtube_motorista']; ?>"    style="border:none;border-bottom: 1px solid #292929;background: none;padding: 5px 13px;width: 100%;margin-top: 20px;">


</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0;">
    <button  type="submit" class="botao lado_baixo botao_envia_pagamento" ><span>Salvar</span></button>
    </form>
</div>

<!--<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" style="padding:0;">
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <svg id="svg_linha" class="center-block" height="380" viewBox="0 0 1000 1000" style="padding: 15px" x="0" y="0" enable-background="new 0 0 147 147" xml:space="preserve">
        <path  class="logo_y"  fill="#ffffff" stroke="#000"  stroke-width="3" stroke-miterlimit="10"  d="M439.5 66.6c-54.7.9-103.5 3.3-150 7.4l-23 2-4.3 4.7c-13.5 15-40.6 58-57.4 91-11.7 22.8-11.9 23.3-10.4 26.1.8 1.6 2.3 3.2 3.2 3.6.9.3 21.5.2 45.8-.4 24.2-.5 57.2-1.2 73.1-1.5 16-.2 47.9-.9 71-1.5 54.3-1.3 155.2-1.3 210.5 0 23.9.6 56.3 1.3 72 1.5 15.7.3 48.5 1 73 1.6l44.5 1 2.3-2.7c2.9-3.3 2.8-3.8-4.2-18.4-8-16.6-17.6-34.1-28.4-51.5-14-22.5-36-53.5-38-53.5-.4 0-10.6-.9-22.7-2-30.1-2.6-64.9-4.8-96.5-6-28.1-1-130.3-1.9-160.5-1.4zM121.6 148.6c-13 2-25.5 5.3-30.2 8-5.9 3.3-7.2 8.4-5 18.6 2.4 10.4 5 15.3 10.2 18.9 7.9 5.4 13.9 7.4 28.4 9.3 11.9 1.7 17.8 6.2 24.6 19.1 1 1.7 2.3 2 10.3 2.3l9.1.3-.1-20.8c0-26.7-1.3-49.3-2.8-52.2-2.2-4.1-6.8-5.1-21.8-5-7.6.1-17.8.7-22.7 1.5zM825.2 148.4c-6.1 2.8-6.4 5-7 42.8l-.5 33.8h18.2l3.2-6.1c5.2-9.7 11.7-14.1 22.7-15.5 13.5-1.6 24.9-5.8 30.6-11.1 3.8-3.5 6.3-9.1 7.7-16.7 1.4-8.5.7-14.1-2.2-17.2-2.7-2.8-13.9-6.5-27.4-9-12.8-2.3-41-2.9-45.3-1zM218 212.5c0 .9 20.1 31.6 29.3 44.8 8.4 12 16.7 19.7 21.4 19.7 3.2 0 2.9-1.9-1.6-8.6-12.9-19.3-49.1-60.5-49.1-55.9zM749.3 215c-6.4 5.8-17 17.9-30.5 34.9-12.8 16-17.8 23.4-17.8 26.1 0 2 4.8.9 8.8-2 5.6-4 11-11 25.9-33.5 6.9-10.5 13.4-20.1 14.3-21.5 4.9-7.2 4.6-8.9-.7-4zM862 292.9c-30 11.4-61.4 26.1-79.9 37.6-32.4 20-80.8 64.9-87.8 81.3-1.9 4.5-.9 5.5 4.3 4.7 7.5-1.1 36.3-11.7 56.5-20.8 32.8-14.8 47.7-21.6 57.2-26.1 48.8-23.2 63-30.7 65.6-35 4.5-7.3 7.2-26 5.1-35.9-1.2-6.1-4.2-10.8-6.8-10.6-.9.1-7.3 2.2-14.2 4.8zM107.6 293c-3.3 3.9-3.9 7.8-3.4 20.3.3 9 .8 11 3.6 17l3.3 6.7 31.7 15.6c17.4 8.5 32.5 15.9 33.4 16.4.9.5 6.5 3.1 12.5 5.8 5.9 2.8 14 6.4 17.8 8.2 12.2 5.6 14.7 6.7 28 12.5 22.4 9.9 48.4 19.5 52.6 19.5 5.5 0 2.2-7.1-10-21.3-19.2-22.2-54.7-51.9-79.1-65.9-20.8-12-79.7-37.8-86.1-37.8-.9 0-2.9 1.4-4.3 3zM291 363.9c0 .6.4 1.3 1 1.6 3.7 2.3 24.2 32.3 30.1 44.1 5.2 10.3 6 15.6 3 18.8-1.3 1.3-1.9 2.6-1.4 2.9.4.2 7.3 2.1 15.3 4.1 8 2.1 19.5 5.1 25.5 6.7 34.9 9.2 83.7 18.2 113.5 20.9 31.6 2.9 84.4-5.2 159.4-24.6 30.7-7.9 28.7-7.2 25.9-9.1-6.7-4.5 1.1-22.5 21.9-50.3 5.1-6.9 9.9-13.4 10.7-14.4 1.8-2.5 2.3-2.6-19.9 5.9-29.1 11.2-61.7 21.3-88.5 27.5-4.9 1.1-9.9 2.3-11 2.6-1.1.2-6.9 1.3-13 2.4-6 1.1-13 2.4-15.5 2.9-9.6 2-37.7 4.2-54.2 4.1-16.5 0-41.2-1.9-52.2-4.1-2.8-.6-6-1.2-7.1-1.4-31-5.2-82.9-19.9-128.6-36.6-14.6-5.3-14.9-5.4-14.9-4zM246.2 574.7l.3 5.8h513l.3-5.8.3-5.7H245.9l.3 5.7zM37.1 648.9c-1 .6 1.8 4.4 11.1 14.7 16.1 18 59.3 66.8 67.7 76.4 3.6 4.1 6.8 7.7 7.1 8 .3.3 2.2 2.4 4.3 4.8l3.7 4.2v59h53v-29.4c0-28.7 0-29.4 2.2-32.2 1.2-1.6 2.5-3.1 2.8-3.4.3-.3 2.7-3.2 5.5-6.5 2.7-3.3 5.9-7.1 7.2-8.5 18.3-20.7 72.9-86.6 72.2-87.1-.6-.3-15.3-.7-32.8-.8l-31.8-.2-15.4 19.8c-8.5 10.9-20.1 26-25.9 33.5-5.8 7.6-10.8 13.7-11.1 13.8-.5 0-24.3-28.9-37.4-45.4-1.6-2.1-3.7-4.7-4.7-5.8-.9-1-3.1-3.8-5-6-1.8-2.3-4.3-5.3-5.5-6.7l-2.1-2.6-31.9-.2c-17.5-.2-32.4.1-33.2.6zM656 648.7c-14.4 1-28.3 2.7-34.9 4.4-19.5 5-26.7 16.1-29.8 45.9-1 9.9-1 58.3 0 68.5 2.4 23 9.1 36.3 21.2 41.6 6.4 2.8 20.3 5.6 30.3 6 15.1.7 98.4.4 105.7-.3 27.2-2.8 39.2-11.5 43.4-31.8 1.1-5.5 3.1-34.1 3.1-45.8V731H685v20H772.3l-.7 10.8c-2 30.9-4.9 32.6-57.6 33.8-29.9.7-73.4-.9-82.4-3-12-2.8-15.2-7.9-17.3-27.1-.9-8.4-1-62.3-.1-68 3-19.7 6.5-23.5 23.8-26.6 10.4-1.9 96.6-2 109.5-.1 16.9 2.5 21 6.5 22.2 22.1l.6 8.2 11.5-.3 11.6-.3-.2-6.5c-.6-15.3-4-23.8-13-32.1-9.7-9-17.9-11.2-49.2-12.8-14.9-.8-65.7-1.1-75-.4z"/>
        <path class="logo_y" fill="#ffffff" stroke="#000" stroke-width="3" stroke-miterlimit="10"   d="M277.1 695.1c-5.1.4-10.9 1.1-13 1.5-19.2 4-28.6 10.9-34.2 25.1-2.1 5.3-2.3 7.8-2.7 28.7-.6 33.8 1.6 45 10.5 54 11.6 11.8 24.4 14.3 71.8 14.4 47.7.2 63.4-2.7 73.7-13.5 8.8-9.2 11.6-22.3 11.3-51.3-.4-25.4-2-33.6-8.2-42.2-6.7-9.3-20.8-15-41.3-16.7-13.1-1.2-53.8-1.2-67.9 0zm59.5 35.3c7.8 2.4 11 6.3 12.5 15 1.5 9.1.6 23.3-2 29-3.6 8.3-12.3 10.8-37.1 10.8-24.5-.1-34.6-3.5-37.1-12.5-1.5-5.4-1.5-25 0-30.4 1.6-5.9 5.3-9.9 10.9-11.7 7.1-2.3 9.2-2.4 29.2-2.1 13.7.2 19.8.7 23.6 1.9zM410.4 698.5c-1.1 2.7.3 76.6 1.5 83.4a43.8 43.8 0 0 0 11.7 22.4c10.4 10.5 25.1 14.6 52.1 14.5 28.3-.1 39.7-3.8 48.6-15.6l4.2-5.6.3 9.2.3 9.2H571l-.2-59.3-.3-59.2-21.6-.3-21.6-.2-.6 33.2c-.4 18.3-1.1 35.1-1.6 37.2-1.3 5.4-5.9 11.5-10.1 13.7-5.4 2.8-13.3 4-26.2 4-14.9.1-21.9-1.3-26.4-5.3-6.9-6.1-6.8-5.4-7.4-45.8l-.5-36.5-21.8-.3c-19-.2-21.8 0-22.3 1.3zM860.2 699c-20 1.7-29 4.9-36.6 13-6.2 6.7-9.2 13.1-11.5 25.1-3.7 19.3-1.7 44.6 4.5 57.1 6.4 12.7 17.9 19.5 36.9 21.7 11.5 1.4 57.5 1.4 68.5.1 29.4-3.6 41.5-20.6 41.5-58-.1-22.7-4.3-36.8-13.7-46.3-6.1-6.1-13.4-9.5-24.6-11.3-10.6-1.7-50.8-2.6-65-1.4zm33.1 16.1c34.9.3 44.8 5.2 48.7 24.4 1.4 6.5 1.7 26.4.6 33.5-1.2 7.7-3.7 14-6.9 17.7-7 8-17.3 10.2-47.4 10.1-45.5-.1-55.2-5.4-57.3-31.7-1.3-16.2.1-31.9 3.6-38.5 3.6-7 7.7-10.3 15.4-12.6 5.6-1.7 23.4-4.1 25.1-3.4.3.2 8.5.4 18.2.5zM223 871.2c-.1 1 0 48.1 0 51.1 0 .5 1.5.7 3.3.5l3.2-.3.3-22.8c.2-21.4.3-22.7 2.1-22.7 1 0 2.1.6 2.4 1.4.3.8 3 5.2 6 9.8 3 4.5 9.3 14.3 14 21.5 7.8 12 8.9 13.3 11.5 13.3 2.7 0 3.7-1.2 10.8-12.3 4.3-6.7 10.7-16.7 14.2-22.2 6.5-10.2 8.8-12.8 10.3-11.9.5.3.9 10.9.9 23.5v23l3.3-.3 3.2-.3v-52l-7.2-.3c-7.2-.3-7.3-.3-9.2 2.8-10.9 18-25.8 40.9-26.4 40.7-.4-.1-7-9.9-14.6-21.7l-13.8-21.5-7.2-.3c-5.3-.2-7.1 0-7.1 1zM359 871c-4.2 1-7.5 4.7-8.9 9.8-.6 2-1.1 9.4-1.1 16.4 0 14.6 1.3 19.5 6.4 23.1 3 2.1 4.1 2.2 24.9 2.4 34.1.4 35.2-.4 35.2-26.2 0-13-.3-15.5-2-19-3.3-6.5-5.1-7-29.5-7.2-11.8-.1-23.1.2-25 .7zm45.2 6.8c1.4.6 2.9 1.6 3.3 2.3.4.6.7 7.6.7 15.6-.1 17.1-.8 18.8-7.4 19.8-2.4.4-12.3.5-22.1.3-17-.3-18-.4-19.7-2.5-3.7-4.6-3.7-29 0-33.6 1.3-1.6 3.4-2.3 7.6-2.8 8.2-1 34.3-.3 37.6.9zM456 896.5v26.6l25.1-.1c33.5-.2 35.9-1.2 35.9-15 0-6.9-1.3-10.4-4.6-11.9l-2.4-1 2.5-2.7c2.1-2.3 2.5-3.7 2.5-8.8 0-6.9-1.4-9.7-6-12.1-2.3-1.2-7.7-1.5-28-1.5h-25v26.5zm50.1-18.8c.6.6 1.4 3.1 1.6 5.6.4 3.6.1 4.9-1.6 6.6-2 2-3.1 2.1-22.6 2.1H463v-16.2l21 .4c14.1.2 21.3.7 22.1 1.5zm-.6 22.3c3.2 1.2 4.8 5.3 4 10.2-1 5.9-2.3 6.2-25.5 6.3h-20.5l-.3-8.8-.3-8.7h20c11 0 21.1.4 22.6 1zM558 896.5v26.6l3.3-.3 3.2-.3v-52l-3.2-.3-3.3-.3v26.6zM606 896.4V923h50v-3c0-1.7-.5-3-1.2-3s-10.2-.2-21.1-.3l-19.9-.2-.1-23-.2-23-3.7-.3-3.8-.3v26.5zM693 896.5V923h52v-6l-22.2-.2-22.3-.3-.3-8.7-.3-8.8 21.3-.2 21.3-.3.3-3.3.3-3.2H700v-16h45v-6h-52v26.5z"/>
    </svg>
</div>
-->
<style>



    .select2-container--default .select2-selection--single{
        background: transparent !important;
        background-color: transparent !important;
        border: none;
        border-bottom: 1px solid #000;
        border-radius: 0;
        margin-top: 10px;
    }
    input[type='search'] {
        padding: 2px 10px;
        border-radius: 0px;
        background: 0;
        border: none;
        color: #fff !important;
        border-bottom: 1px solid #000;
        visibility: visible;
        animation-duration: 0.5s;
        animation-delay: 0s;
        animation-name: fadeInUp;
        margin-bottom: 10px;
    }
    .select2-dropdown {
        background-color: #292929;
        color: #fff;
        border: 1px solid #000;
        border-radius: 4px;
        box-sizing: border-box;
        display: block;
        position: absolute;
        left: -100000px;
        width: 100%;
        z-index: 1051;
    }
    .select2-container--default .select2-results__option--highlighted[aria-selected] {
        background-color: #f8d509;
        color: #fff;
    }
    .select2-container--default .select2-results__option[aria-selected=true] {
        background-color: #171515;
        color: #f8d509;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        color: #000 !important;
    }


    .botao_alt {
        opacity: .80;
        background: #292929;
        border: 2px solid #292929;
        margin-top: 10px;
        padding: 8px 45px;
        color: #f8d509;
        position: relative;
        overflow: hidden;
        cursor: pointer;
    }

    .botao_alt:hover {
        opacity: 1;
        transition-duration: 0.3s;
        transition-timing-function: ease-in;
        transition-property: all;
    }
    .botao_alt span {
        position: relative;
        z-index: 100;
        font-size: 16px;
    }
    .botao_alt:before, .botao_alt:after {
        content: '';
        position: absolute;
        display: block;
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
    }
    .botao_alt:before {
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0 , 0);
        background-color: #000;
        color: #000;
        border: 1px solid #000;
        -webkit-transition: -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19), -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
    }
    .botao_alt:after {
        background-color: #000;
        color: #000;
        border: 1px solid #000;
        -webkit-transform: translate3d(100%, 0, 0);
        transform: translate3d(100%, 0, 0);
        -webkit-transition: -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62), -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
    }
    .botao_alt:hover:before {
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
    }
    .botao_alt:hover:after {
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
    }


    #svg_linha{
        width: 100%;
    }
    @media screen and (max-width: 768px) and (min-width: 500px){
        #svg_linha{
            width: 60%;
            margin: 0 auto;
            margin-left: 20%;
            margin-top: -20px;

        }
    }
    @media screen and (max-width: 500px){
        #svg_linha{
            width: 60%;
            margin: 0 auto;
            margin-left: 20%;
            margin-top: -90px;

        }
    }
    svg path, svg rect {
        fill: transparent !important;
    }
    .logo_y {
        stroke-dasharray: 2000;
        stroke-dashoffset: 2000;

        animation:
            logo_yStroke 1.75s linear forwards,
            logo_yStrokeFill 0.75s cubic-bezier(0.445, 0.050, 0.550, 0.950) 1.80s forwards;
    }

    @keyframes logo_yStroke {
        to {
            stroke-dashoffset: 0;
        }
    }

    @keyframes logo_yStrokeFill {
        to {
            stroke:  #000;
            fill: #000;
        }
    }

</style>



<script type="text/javascript">


    function limpa_formulario_cep() {
        //Limpa valores do formulário de cep.
        document.getElementById('rua').value=("");
        document.getElementById('bairro').value=("");
        document.getElementById('cidade').value=("");
        document.getElementById('uf').value=("");
    }

    function meu_callback(conteudo) {
        if (!("erro" in conteudo)) {
            //Atualiza os campos com os valores.
            document.getElementById('rua').value=(conteudo.logradouro);
            document.getElementById('bairro').value=(conteudo.bairro);
            document.getElementById('cidade').value=(conteudo.localidade);
            $('#uf').val(conteudo.uf).trigger('change');
        } //end if.
        else {
            //CEP não Encontrado.
            limpa_formulario_cep();
            /*$('#mensagem_cep').html('<p style="text-align: center;color: #ff0000;width: 80%;margin: 0 auto;margin-top: 5px;font-size: 13px">CEP não encontrado, preencha manualmente os dados de endereço.</p>');*/
        }
    }

    function pesquisacep() {
        $('#mensagem_cep').html('');
        var valor =  document.getElementById('cep').value;
        //Nova variável "cep" somente com dígitos.
        var cep = valor.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;



            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                document.getElementById('rua').value="";
                document.getElementById('bairro').value="";
                document.getElementById('uf').value="";
                document.getElementById('cidade').value="";

                //Cria um elemento javascript.
                var script = document.createElement('script');

                //Sincroniza com o callback.
                script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

                //Insere script no documento e carrega o conteúdo.
                document.body.appendChild(script);

            } //end if.
            else {
                //cep é inválido.
                $('#mensagem_cep').html('<p style="text-align: center;color: #ff0000;width: 80%;margin: 0 auto;margin-top: 5px;font-size: 13px"> Ocorreu um erro, seu CEP é invalido</p>');
                limpa_formulario_cep();
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            $('#mensagem_cep').html('<p style="text-align: center;color: #ff0000;width: 80%;margin: 0 auto;margin-top: 5px;font-size: 13px"> Digite um CEP</p>');
            limpa_formulario_cep();
        }


    };

</script>
