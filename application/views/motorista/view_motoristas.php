<head>
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/cupons.css" >
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/motoristas_todos.css" >
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/theme-animate.css" >
    <link type="text/css" href="<?php echo base_url(); ?>style/css/bootstrap.min.css" rel="stylesheet" />
    <script src="<?php echo base_url(); ?>style/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>style/js/script_efeito.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <script>
        $(document).ready(function(){
           var nome = document.getElementById('nome').value;
           console.log(nome);
        });
    </script>

</head>
<body>
<div style="overflow-x: hidden">
    <h1 class="fade_1"><i class="fa fa-cab fa-2x lado_topo" data-wow-duration="2s" data-wow-delay="0.0s" > </i>  &nbsp;&nbsp;Lista de motoristas</h1>
    <div class="lado_direito tm30 wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.3s" style="width: 100%;height: 2px;background: #f8d509;margin-top: 30px"></div>
</div>

<div class="lado_direito" style="width: 100%;height: 2px;background: transparent;margin-top: 40px;margin-bottom: 0px"></div>


<style>
    .form-control{
        background: transparent;
        border: none;
        box-shadow: none;
        border-radius: 0;
        border-bottom: 1px solid #000;
        visibility: visible;
        animation-duration: 1s;
        animation-delay: 0s;
        animation-name: fadeInDown;
    }
    .input-group-addon{
        background: transparent;
        border: none;
        box-shadow: none;
        border-radius: 0;
        border-bottom: 1px solid #000;
        visibility: visible;
        animation-duration: 1s;
        animation-delay: 0s;
        animation-name: fadeInDown;
    }

    .form-control:focus{
        border: none;
        border-bottom: 1px solid #ccc;
        box-shadow: none;
        visibility: visible;
        animation-duration: 0.3s;
        animation-delay: 0s;
        animation-name: fadeInUp;
    }

    .fa-search:hover{
        color: #f8d509;
        cursor: pointer;
    }

</style>




        <table class="table table-bordered table-hover" align="center">
            <thead style="background: #f1f1f1">
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Telefone</th>
                <th>CPF</th>
                <th>Status</th>
                <th class="no-filter" style="text-align: center;padding: 0 80px">Ver</th>
            </tr>
            </thead>
            <tbody>

            <?php
            $cont = 0.3;
            foreach ($dados_iniciais as $motoristas) {
                ?>
                <tr >
                    <td><p style="margin-top: 15px;text-align: center"><?php echo $motoristas['id_usuario']; ?></p></td>
                    <td><p style="margin-top: 15px;text-align: center"><?php echo $motoristas['nome_usuario']; ?></p></td>
                    <td><p style="margin-top: 15px;text-align: center"><?php echo $motoristas['email_usuario']; ?> </p></td>
                    <td class="mascara_cel" style="text-align: center;padding-top: 20px;"><p style="margin-top: 15px;text-align: center"><?php echo $motoristas['celular_usuario']; ?></p></td>
                    <td class="mascara_cpf" style="text-align: center;padding-top: 20px;"><p style="margin-top: 15px;text-align: center"><?php echo $motoristas['cpf_usuario']; ?></p></td>


                    <td>
                        <p style="margin-top: 15px;text-align: center">
                        <?php if($motoristas['ativo_usuario'] == 1){ ?>
                            <span style="color: #7da957">Ativado</span>
                        <?php }else{ ?>
                            <?php if($motoristas['bloqueado'] == 1){ ?>
                                <span style="color: #db2d49">Bloqueado</span>
                            <?php } else { ?>
                                <span style="color: #db2d49">Inativo</span>
                            <?php } ?>
                        <?php } ?>
                        </p>
                    </td>

                    <td><center><a href="<?php echo base_url()?>main/redirecionar/28/<?php echo $motoristas['id_usuario']; ?> "><button  type="submit" class="botao_alt"  style="margin-top: -0px;border-radius: 5px"><span>Ver mais</span></button></a> </center></td>
                </tr>
            <?php  } ?>
            </tbody>
        </table>






<style>
    input[type='search']{
        color: #000 !important;
    }
    .botao_alt {
        opacity: .80;
        background: #292929;
        border: 2px solid #292929;
        margin-top: 10px;
        padding: 8px 45px;
        color: #f8d509;
        position: relative;
        overflow: hidden;
        cursor: pointer;
    }

    .botao_alt:hover {
        opacity: 1;
        transition-duration: 0.3s;
        transition-timing-function: ease-in;
        transition-property: all;
    }
    .botao_alt span {
        position: relative;
        z-index: 100;
        font-size: 16px;
    }
    .botao_alt:before, .botao_alt:after {
        content: '';
        position: absolute;
        display: block;
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
    }
    .botao_alt:before {
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0 , 0);
        background-color: #000;
        color: #000;
        border: 1px solid #000;
        -webkit-transition: -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19), -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
    }
    .botao_alt:after {
        background-color: #000;
        color: #000;
        border: 1px solid #000;
        -webkit-transform: translate3d(100%, 0, 0);
        transform: translate3d(100%, 0, 0);
        -webkit-transition: -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62), -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
    }
    .botao_alt:hover:before {
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
    }
    .botao_alt:hover:after {
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
    }
</style>
<!--
<ul class="pagination tm30 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s">
    <li><a href="#">«</a></li>
    <li><a href="#">1</a></li>
    <li><a class="active" href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li><a href="#">6</a></li>
    <li><a href="#">7</a></li>
    <li><a href="#">»</a></li>
</ul>
-->












