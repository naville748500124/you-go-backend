<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_motorista extends MY_Model {

		private $regras = array (

			'nome_usuario' => 'required',
			'email_usuario' => 'required',
			'telefone_usuario' => 'required',
			'senha_usuario' => 'required',
			'facebook_usuario' => 'max_length[40]',
			'rg_usuario' => 'required',
			'cpf_usuario' => 'required',
			'celular_usuario' => 'required',
			'logradouro_usuario' => 'required',
			'cidade_usuario' => 'required',
			'fk_uf_usuario' => 'required',
			'bairro_usuario' => 'required',
			'cep_usuario' => 'required',
			'num_residencia_usuario' => 'required',
			'complemento_usuario' => 'max_length[200]',
			'fk_genero' => 'required',
			'fk_cidade_ativa_motorista' => 'required',
			'fk_modelo_carro_motorista' => 'required',
			'cor_carro_motorista' => 'required',
			'placa_carro_motorista' => 'required|is_unique[cad_detalhes_motorista.placa_carro_motorista]',
			'ativo_usuario' => 'required',
			'fk_grupo_usuario'	=> 'required',
			'login_usuario'	=> 'required'


		);

		private $regras_bancos = array (

			'fk_usuario' 		 => 'required',
			'fk_banco_motorista' => 'required',
			'agencia_motorista'  => 'required',
			'conta_motorista'    => 'required',
			'digito_motorista'   => 'required'

		);

		private $nomes = array (

			'nome_usuario' => 'Nome',
			'email_usuario' => 'E-mail',
			'telefone_usuario' => 'Telefone',
			'senha_usuario' => 'Senha',
			'facebook_usuario' => 'Facebook ID',
			'rg_usuario' => 'RG',
			'cpf_usuario' => 'CPF',
			'celular_usuario' => 'Celular',
			'logradouro_usuario' => 'Logradouro',
			'cidade_usuario' => 'Cidade',
			'fk_uf_usuario' => 'UF',
			'bairro_usuario' => 'Bairro',
			'cep_usuario' => 'CEP',
			'num_residencia_usuario' => 'Número',
			'complemento_usuario' => 'Complemento',
			'fk_genero' => 'Gênero',
			'fk_cidade_ativa_motorista' => 'Cidade atuação',
			'fk_modelo_carro_motorista' => 'Modelo Veículo',
			'cor_carro_motorista' => 'Cor Veículo',
			'placa_carro_motorista' => 'Placa Veículo',
			'ativo_usuario' => 'Status',
			'fk_grupo_usuario'	=> 'Grupo',
			'login_usuario'	=> 'Login'

		);

		private $nomes_bancos = array (

			'fk_usuario' 		 => 'Fk Usuário',
 			'fk_banco_motorista' => 'Banco',
			'agencia_motorista'  => 'Agência',
			'conta_motorista'    => 'Conta',
			'digito_motorista'   => 'Dígito'

		);


		public function regras(){
			return $this->regras;
		}

		public function regras_bancos(){
			return $this->regras_bancos;
		}

		public function nomes(){
			return $this->nomes;
		}

		public function nomes_bancos(){
			return $this->nomes_bancos;
		}

		public function view_motoristas($where = null){

		/*	var_dump($where);
			die();*/

			if(count($where) > 0){

				$filtro = array('fk_grupo_usuario' => 2,'email_usuario' => $where[0],'ativo_usuario' => $where[1]);

				$this->db->get_where('seg_usuarios',$filtro)->result_array();
				/*echo $this->db->last_query();
				die();*/

			} else {

				return $this->db->get_where('seg_usuarios',array('fk_grupo_usuario' => 2))->result_array();
				// echo $this->db->last_query();
				// die();

			}

		}
        public function editar_motorista($where){
		    $this->db->join('cad_detalhes_motorista','id_usuario = fk_usuario');

		    $dado['dados'] = $this->db->get_where('seg_usuarios',array('id_usuario' => $where[0]))->row_array();
            $dado['cidades'] = $this->db->get('cad_cidades_atuacao')->result_array();

            $dado['bancos'] = $this->db->query('SELECT ( SELECT nome_item_grupo FROM cad_item_grupo WHERE id_item_grupo = fk_banco_motorista ) AS banco  FROM cad_detalhes_motorista WHERE fk_usuario = '.$where[0])->row_array();

            $dado['ufs'] = $this->db->query('SELECT * from cad_item_grupo WHERE id_item_grupo < 28')->result_array();



		    $dado['montadora'] = $this->db->get('cad_montadoras')->result_array();

		    $dado['modelo'] = $this->db->get('cad_modelos')->result_array();


		    $dado['relatorio'] = $this->db->query("
		    					SELECT
		    						(SELECT Concat('R$ ', Replace(Round(ifnull(Sum((SELECT valor_corrida 
                                        FROM   view_custo_final_motorista cvfm 
                                        WHERE  cvfm.id_corrida = 
                                               cad_corridas.id_corrida)),0), 2), 
					                      '.', ',')) AS valor_corrida 
										FROM   cad_corridas 
										WHERE  fk_status_corrida = 86 
										       AND fk_motorista = {$where[0]} 
										       AND data_corrida >= DATE_SUB(curdate(), interval 1 day)) as dia,

									(SELECT Concat('R$ ', Replace(Round(ifnull(Sum((SELECT valor_corrida 
                                        FROM   view_custo_final_motorista cvfm 
                                        WHERE  cvfm.id_corrida = 
                                               cad_corridas.id_corrida)),0), 2), 
					                      '.', ',')) AS valor_corrida 
										FROM   cad_corridas 
										WHERE  fk_status_corrida = 86 
										       AND fk_motorista = {$where[0]} 
										       AND data_corrida >= DATE_SUB(curdate(), interval 1 week)) as semana,

									(SELECT Concat('R$ ', Replace(Round(ifnull(Sum((SELECT valor_corrida 
                                        FROM   view_custo_final_motorista cvfm 
                                        WHERE  cvfm.id_corrida = 
                                               cad_corridas.id_corrida)),0), 2), 
					                      '.', ',')) AS valor_corrida 
										FROM   cad_corridas 
										WHERE  fk_status_corrida = 86 
										       AND fk_motorista = {$where[0]} 
										       AND data_corrida >= DATE_SUB(curdate(), interval 1 month)) as mes")
		    							->row_array();


		    return $dado;
        }

        public function desativar($valor,$mensagem,$bloqueado){
            $this->db->where('id_usuario', $valor);
            if($bloqueado){
            	return $this->db->update('seg_usuarios',array('ativo_usuario' => '0','bloqueado' => $bloqueado, 'motivo_inativacao' => $mensagem.' Bloqueado em '.date('d/m/Y H:i:s')));	
            } else {
            	return $this->db->update('seg_usuarios',array('ativo_usuario' => '0', 'motivo_inativacao' => $mensagem.' Inativo em '.date('d/m/Y H:i:s')));
            }
            

        }
        public function ativar_painel($valor){
            $this->db->where('id_usuario', $valor);
            return $this->db->update('seg_usuarios',array('ativo_usuario' => '1', 'motivo_inativacao' => null));

        }


        /*Parte do perfil que o usuário acessa via painel*/
        public function view_perfil_motorista(){
            $this->db->join('cad_detalhes_motorista','id_usuario = fk_usuario');


            $dado['dados'] = $this->db->get_where('seg_usuarios',array('id_usuario' => $this->session->userdata('usuario')))->row_array();
            $dado['cidades'] = $this->db->get('cad_cidades_atuacao')->result_array();

						$dado['ufs'] = $this->db->query('SELECT * from cad_item_grupo WHERE id_item_grupo < 28')->result_array();

            $dado['montadora'] = $this->db->get('cad_montadoras')->result_array();

            $dado['modelo'] = $this->db->get('cad_modelos')->result_array();
            $id =  $this->session->userdata('usuario');
            $dado['relatorio'] = $this->db->query("
		    					SELECT
		    						Concat('R$ ', Replace(Round(ifnull((SELECT (SELECT Sum(valor_corrida) + 0
                                        FROM   view_custo_final_motorista cvfm 
                                        WHERE  cvfm.id_corrida = 
                                               cad_corridas.id_corrida) 
										FROM   cad_corridas 
										WHERE  fk_status_corrida = 86 
										       AND fk_motorista = {$id} 
										       AND data_corrida >= DATE_SUB(curdate(), interval 1 day) limit 1) ,0), 2), 
					                      '.', ',')) as dia,

									Concat('R$ ', Replace(Round(ifnull((SELECT (SELECT Sum(valor_corrida) + 0
                                        FROM   view_custo_final_motorista cvfm 
                                        WHERE  cvfm.id_corrida = 
                                               cad_corridas.id_corrida) 
										FROM   cad_corridas 
										WHERE  fk_status_corrida = 86 
										       AND fk_motorista = {$id} 
										       AND data_corrida >= DATE_SUB(curdate(), interval 1 week) limit 1) ,0), 2), 
					                      '.', ',')) as semana,

									Concat('R$ ', Replace(Round(ifnull((SELECT (SELECT Sum(valor_corrida) + 0
                                        FROM   view_custo_final_motorista cvfm 
                                        WHERE  cvfm.id_corrida = 
                                               cad_corridas.id_corrida) 
										FROM   cad_corridas 
										WHERE  fk_status_corrida = 86 
										       AND fk_motorista = {$id} 
										       AND data_corrida >= DATE_SUB(curdate(), interval 1 month) limit 1) ,0), 2), 
					                      '.', ',')) as mes")
                ->row();

            return $dado;
        }

        public function view_ver_corridas($where){

            if(empty($where[0])){
                $where  = "where fk_motorista = ".$this->session->userdata("usuario");
            }

        	switch ($where[0]) {
        		case '5':
                    $where  = "where fk_motorista = ".$this->session->userdata("usuario");
        			$where .= " and data_corrida >= DATE(NOW()) - INTERVAL 5 DAY";
        			break;
        		case '15':
                    $where  = "where fk_motorista = ".$this->session->userdata("usuario");
        			$where .= " and data_corrida >= DATE(NOW()) - INTERVAL 15 DAY";
        			break;
        		case '30':
                    $where  = "where fk_motorista = ".$this->session->userdata("usuario");
        			$where .= " and data_corrida >= DATE(NOW()) - INTERVAL 30 DAY";
        			break;
        		default:
                    $where  = "where fk_motorista = ".$this->session->userdata("usuario");
        			$where .= "";
        			break;
        	}


            $dado['dados'] = $this->db->query("SELECT 
            									pagamento_motorista,
            									id_corrida,
            									km_corrida,
            									fk_passageiro,
            									taxa_yougo,
            									endereco_origem,
            									endereco_destino,
            									placa_carro_motorista,
            									data_corrida,
            									fk_status_corrida,
            									round(valor_corrida,2) as valor_corrida 
            										from cad_corridas 
            											 
            										{$where}
            													ORDER BY id_corrida DESC")->result_array();


           return $dado;
        }

        public function view_cidades($where){
            $this->db->order_by("nome_cidades_atuacao", "asc");
            $dado['dados'] = $this->db->get('cad_cidades_atuacao')->result_array();
            return $dado;
        }


        public function cupons($dados){
            $this->db->insert('cad_cupons',$dados);
            $id = $this->db->insert_id();
            $codigo = base64_encode($id);
            $cod = explode('=', $codigo);
            $this->db->where('id_cupom',$id);
            return $this->db->update('cad_cupons',array('codigo_cupom' => $cod[0]));
        }

        public function atualizar_cupom($id,$status_novo){
            $this->db->where('id_cupom',$id);
            return $this->db->update('cad_cupons',$status_novo);
        }


    }
