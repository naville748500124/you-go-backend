

function dataTableLoad(){
    console.log("Load DataTable");
	
    $('<div style="max-width: 100%; overflow: scroll;">').insertBefore('.table');
    $("</div>").insertAfter('.table');

    if(!$('table').hasClass('semDataTable')){

        // Adicionando um campos com input para as buscas indivíduais.
         $('table thead th').each( function () {
                var title = $(this).text();
                if (title != '' && !$(this).hasClass('no-filter') && !$(this).hasClass('status_corrida')) {
                    $(this).html(title+'<br><div class="form-group has-feedback"><input type="text" class="form-control" placeholder="'+title+'" alt="'+title+'" title="'+title+'"/><i class="glyphicon glyphicon-search form-control-feedback"></i></div>' );    
                } else if($(this).hasClass('status_corrida')){
                    $(this).html(title+'<br><div class="form-group has-feedback"><input type="hidden" class="form-control" id="filtro_status_corrida" placeholder="'+title+'" alt="'+title+'" title="'+title+'" value=""/><select id="filtro_status"><option>Buscando Motorista</option><option>Aguardando Motorista</option><option>Em Andamento</option><option>Corrida Finalizada</option><option>Cancelado pelo passageiro</option><option>Cancelado pelo motorista</option></select></div>' );    
                }
            } );

            var table = $('table').DataTable({
                "ordering": false,
                "oLanguage": {
                    "sLengthMenu": "Mostrar _MENU_ registros por página",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sInfo": "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    "sInfoEmpty": "Mostrando 0 / 0 de 0 registros",
                    "sInfoFiltered": "(filtrado de _MAX_ registros)",
                    "sSearch": "Pesquisa Geral: ",
                    "oPaginate": {
                        "sFirst": "Início",
                        "sPrevious": "Anterior",
                        "sNext": "Próximo",
                        "sLast": "Último"
                    }
                }
            });
                     
         
            //Aplicando a busca nos campos
            table.columns().every( function () {
                var that = this;
         
                $( 'input', this.header() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );

    }

}

$(document).ready(function(){
    dataTableLoad();
});