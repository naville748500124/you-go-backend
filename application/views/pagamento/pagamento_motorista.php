<head>
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/cupons.css" >
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/theme-animate.css" >
    <link type="text/css" href="<?php echo base_url(); ?>style/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <meta name="theme-color" content="#292929">
</head>



<div>
    <h1 class="fade_1"> <i class="fas fa-donate lado_topo" style="font-size: 13px;"></i> <i class="fas fa-donate lado_topo"  ></i> <i class="fas fa-donate fa-2x lado_topo" data-wow-duration="2s" data-wow-delay="0.0s" > </i> Pagamentos</h1>

    <div class="lado_direito" style="width: 100%;height: 2px;background: #f8d509;margin-top: 30px;overflow-x: hidden"></div>
</div>

<div class="lado_direito" style="width: 100%;height: 2px;background: transparent;margin-top: 40px;margin-bottom: 10px"></div>


<table class="table table-bordered table-hover" align="center">
    <thead style="background: #f1f1f1">
    <tr>
        <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">ID</td>
        <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Motorista</td>
        <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">R$</td>

        <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Banco</td>
        <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Agencia</td>
        <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Conta</td>
        <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Tipo</td>

        <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Dias sem receber</td>

        <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Pagar</td>

    </tr>
    <tr>
        <th> </th>
        <th> </th>
        <th> </th>
        <th> </th>
        <th> </th>
        <th> </th>
        <th> </th>
        <th> </th>
        <th> </th>
    </tr>
    </thead>
    <tbody>

    <?php
    foreach ($dados_iniciais['dados'] as $dados){ ?>
        <tr>
            <td><p style="margin-top: 15px;margin-left: 5px;text-align: center"><?php echo $dados['id_corrida']; ?></td></p>
            <td><p style="margin-top: 15px;margin-left: 5px;text-align: center"><?php echo $dados['nome_usuario']; ?></td></p>
            <td><p style="margin-top: 15px;margin-left: 5px;text-align: center">R$ <?php echo $dados['valor_corrida']; ?></td></p>
            <td><p style="margin-top: 15px;margin-left: 5px;text-align: center"><?php echo $dados['nome_item_grupo']; ?></td></p><!--Banco-->
            <td><p style="margin-top: 15px;margin-left: 5px;text-align: center"><?php echo $dados['agencia_motorista']; ?></td></p><!--Agencia-->
            <td><p style="margin-top: 15px;margin-left: 5px;text-align: center"><?php echo $dados['conta_motorista']; ?> - <?php echo $dados['digito_motorista']; ?></td></p> <!--Conta-->

            <?php if($dados['tipo_conta'] == 1){ ?>
                <td><p style="margin-top: 15px;margin-left: 5px;text-align: center">Corrente</p></td>
            <?php } else { ?>
                <td><p style="margin-top: 15px;margin-left: 5px;text-align: center">Poupança</p></td>  
            <?php } ?>
            
            <td><p style="margin-top: 15px;margin-left: 5px;text-align: center"><?php echo $dados['dias_receber']; ?></p></td>

            <td>
                <?php
                if($dados['pagamento_motorista'] == 0 or $dados['pagamento_motorista'] == null ){
                   ?>
                    <center><a href="<?php echo base_url()?>controller_adm/atulizar_pagamento_motorista?id=<?php echo $dados['id_corrida']; ?>&tipo=1"><button  type="submit" class="botao_alt"  style="margin-top: -0px;border-radius: 5px"><span style="white-space: nowrap">Pagar</span></button></a> </center>
                    <?php
                }else{
                    ?>
                    <center><a href="<?php echo base_url()?>controller_adm/atulizar_pagamento_motorista?id=<?php echo $dados['id_corrida']; ?>&tipo=0"><button  type="submit" class="btn btn-danger"  style="margin-top: -0px;border-radius: 5px"><span style="white-space: nowrap">Rever Pagamento</span></button></a> </center>
                <?php }

                ?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>




<style>

    .botao_alt {
        opacity: .80;
        background: #292929;
        border: 2px solid #292929;
        margin-top: 10px;
        padding: 8px 45px;
        color: #f8d509;
        position: relative;
        overflow: hidden;
        cursor: pointer;
    }

    .botao_alt:hover {
        opacity: 1;
        transition-duration: 0.3s;
        transition-timing-function: ease-in;
        transition-property: all;
    }
    .botao_alt span {
        position: relative;
        z-index: 100;
        font-size: 16px;
    }
    .botao_alt:before, .botao_alt:after {
        content: '';
        position: absolute;
        display: block;
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
    }
    .botao_alt:before {
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0 , 0);
        background-color: #000;
        color: #000;
        border: 1px solid #000;
        -webkit-transition: -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19), -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
    }
    .botao_alt:after {
        background-color: #000;
        color: #000;
        border: 1px solid #000;
        -webkit-transform: translate3d(100%, 0, 0);
        transform: translate3d(100%, 0, 0);
        -webkit-transition: -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62), -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
    }
    .botao_alt:hover:before {
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
    }
    .botao_alt:hover:after {
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
    }
</style>