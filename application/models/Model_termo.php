<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_termo extends MY_Model {

		public function get_menus($where = "") {

			return $this->db->query('select titulo_menu as titulo, id_menu as id, menu_acima as anterior from seg_menu where menu_acima '.$where.' order by posicao_menu')->result();

		}

		public function get_lista_aplicacoes($id = null,$grupo = null){

			return $this->db->query('select sa.* from seg_aplicacao sa
									inner join seg_aplicacoes_menu as sam on sam.fk_aplicacao = id_aplicacao
									inner join seg_menu on fk_menu = id_menu
									inner join seg_aplicacoes_grupos as sag on sag.fk_aplicacao = id_aplicacao
									where menu_acima = '.$id.' and fk_grupo = '.$grupo)->result();

		}

		public function get_link($id = null,$grupo = null) { //VALIDAR ACESSO DO USUÁRIO********

			$consulta = $this->db->query('select 
											m.titulo_menu,
											a.link_aplicacao,
											a.id_aplicacao
											from seg_aplicacoes_menu am
											inner join seg_menu m on m.id_menu = am.fk_menu
											inner join seg_aplicacao a on a.id_aplicacao = am.fk_aplicacao
											inner join seg_aplicacoes_grupos as sag on sag.fk_aplicacao = id_aplicacao
											where fk_menu = '.$id.' and fk_grupo = '.$grupo.'
											group by titulo_menu, link_aplicacao, id_aplicacao');

			if($consulta->num_rows() > 0) { // não tem sub-menu, é um link direto

				return $consulta;

			} else { //Tem sub-menu, deve chamar o else e iniciar a recursividade, se necessário.

				return false;

			}

		}

		/*Criar*/

		public function listar(){

			return array('models' => $this->db->get('seg_models')->result(),
						 'controllers' => $this->db->get('seg_controllers')->result(),
						 'menus' => $this->db->get('seg_menu')->result());

		}

		public function criarMc($model,$controller){

			$this->db->insert('seg_models',$model);
			echo $this->db->last_query().';<br>';

			$controller['fk_model'] = $this->db->insert_id();

			$this->db->insert('seg_controllers',$controller);
			echo $this->db->last_query().';<br>';

		}

		public function criarMenu($menu){

			$menu['id_menu'] = $this->db->query('select max(id_menu)+1 as proximo from seg_menu where id_menu < 1000')->row()->proximo;

			$this->db->insert('seg_menu',$menu);
			echo $this->db->last_query().';<br>';

			return true;

		}

		public function criarAplicacao($aplicacao,$menu){

			$this->db->insert('seg_aplicacao',$aplicacao);
			$id = $this->db->insert_id();

			echo $this->db->last_query().';<br>';

			if ($menu != "") {
				$this->db->query('insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (
																'.$id.',
																'.$menu.');');
				echo $this->db->last_query().'<br>';
			}

			$this->db->query('insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(
																'.$id.',1);');

			echo $this->db->last_query().'<br>';

			return true;

		}







		/*##### Parte do jean #####*/
        public function termo($termo){

		    $termo_array = array (
		    'termo_de_uso' => $termo
		    );
            $this->db->update('cad_configuracoes',$termo_array);
        }
        public function termo_motorista($termo){

		    $termo_array = array (
		    'termo_de_uso_motorista' => $termo
		    );
            $this->db->update('cad_configuracoes',$termo_array);
        }

        public function view_termo(){
            return $this->db->get('cad_configuracoes')->result_array();


        }

        public function termosDeUso(){

			return $this->db->query("select termo_de_uso from cad_configuracoes")->row()->termo_de_uso;

		}

        public function termosDeUso_mot(){

		    return	$this->db->query("select termo_de_uso_motorista from cad_configuracoes")->row_array();


		}











	}