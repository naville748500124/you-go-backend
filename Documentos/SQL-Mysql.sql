/*
Observações.
	Comentários: Os campos em tabelas nas quais os usuários podem alterar seu valor devem ser comentados,
	pois o comentário será usado no relatório de edições realizadas, para melhor identificar o campo que recebeu
	a alteração.

	Alterar:
		Em Drop, e Create o nome do schema.
		Em view_relatorio_edicoes alterar o nome do schema
*/

-- http://devnaville-br2.16mb.com/yougo

-- DROP DATABASE yougo;
-- CREATE SCHEMA yougo CHARACTER SET utf8 COLLATE utf8_general_ci; /*ALTERAR NO VIEW_ALIAS ANTES DE USAR.*/
-- use yougo;

-- http://devnavillehomologacao.16mb.com/yougo

DROP DATABASE db_yougo;
CREATE SCHEMA db_yougo CHARACTER SET utf8 COLLATE utf8_general_ci; /*ALTERAR NO VIEW_ALIAS ANTES DE USAR.*/
use db_yougo;

CREATE TABLE cad_montadoras (
	id_montadora INT NOT NULL AUTO_INCREMENT,
	montadora varchar(45) NOT NULL,
	CONSTRAINT cad_montadora_pk PRIMARY KEY (id_montadora)
);

CREATE TABLE cad_modelos (
	id_modelo INT NOT NULL AUTO_INCREMENT,
	fk_montadora INT NOT NULL,
	modelo varchar(100) NOT NULL,
	CONSTRAINT cad_modelo_pk PRIMARY KEY (id_modelo),
	CONSTRAINT cad_modelo_cad_montadora_fk FOREIGN KEY (fk_montadora) REFERENCES cad_montadoras(id_montadora)
);

/*GRUPO DE ITENS*/
CREATE TABLE IF NOT EXISTS cad_grupos(

	id_grupo int not null AUTO_INCREMENT,
	nome_grupo text,

	PRIMARY KEY (id_grupo)

);

insert into cad_grupos (id_grupo,nome_grupo)
		values
			(1,'Estados'),
			(2,'Bancos'),
			(3,'Gênero'),
			(4,'Gênero');

CREATE TABLE IF NOT EXISTS cad_item_grupo(

	id_item_grupo int not null AUTO_INCREMENT,
	fk_grupo int,
	nome_item_grupo text,
	sigla_item_grupo text,

	PRIMARY KEY (id_item_grupo),
	FOREIGN KEY (fk_grupo) REFERENCES cad_grupos (id_grupo)

);

insert into cad_item_grupo (id_item_grupo,fk_grupo,nome_item_grupo,sigla_item_grupo)
		values

			(1,1,'','AC'), /*Estados*/
			(2,1,'','AL'),
			(3,1,'','AP'),
			(4,1,'','AM'),
			(5,1,'','BA'),
			(6,1,'','CE'),
			(7,1,'','DF'),
			(8,1,'','ES'),
			(9,1,'','GO'),
			(10,1,'','MA'),
			(11,1,'','MT'),
			(12,1,'','MS'),
			(13,1,'','MG'),
			(14,1,'','PA'),
			(15,1,'','PB'),
			(16,1,'','PR'),
			(17,1,'','PE'),
			(18,1,'','PI'),
			(19,1,'','RJ'),
			(20,1,'','RN'),
			(21,1,'','RS'),
			(22,1,'','RO'),
			(23,1,'','RR'),
			(24,1,'','SC'),
			(25,1,'','SP'),
			(26,1,'','SE'),
			(27,1,'','TO'),

			/*Lista Completa Bancos*/
			(28,2,'Banco do Brasil','BB'),
			(29,2,'Banco Central do Brasil','BACEN, BCB ou BC'),
			(30,2,'Banco da Amazônia','BASA'),
			(31,2,'Banco do Nordeste do Brasil','BNB'),
			(32,2,'Banco Nacional de Desenvolvimento Econômico e Social','BNDES'),
			(33,2,'Caixa Econômica Federal','CEF'),
			(34,2,'Banco Regional de Desenvolvimento do Extremo Sul','BRDE'),
			(35,2,'Banco de Desenvolvimento de Minas Gerais','BDMG'),
			(36,2,'Banco de Brasília','BRB'),
			(37,2,'Banco do Estado de Sergipe','Banese'),
			(38,2,'Banco do Estado do Espírito Santo','Banestes'),
			(39,2,'Banco do Estado do Pará','Banpará'),
			(40,2,'Banco do Estado do Rio Grande do Sul','Banrisul'),
			(41,2,'Banco ABN Amro S.A.','ABN'),
			(42,2,'Banco Alfa','Alfa'),
			(43,2,'Banco Banif','Banif'),
			(44,2,'Banco BBM','BBM'),
			(45,2,'Banco BMG','BMG'),
			(46,2,'Banco Bonsucesso','Bonsucesso'),
			(47,2,'Banco BTG Pactual','BTG'),
			(48,2,'Banco Cacique','Cacique'),
			(49,2,'Banco Caixa Geral - Brasil','BCG-Brasil'),
			(50,2,'Banco Citibank','Citibank'),
			(51,2,'Banco Credibel','Credibel'),
			(52,2,'Banco Credit Suisse',''),
			(53,2,'Banco Daycoval','Daycoval'),
			(54,2,'Banco Fator','Fator'),
			(55,2,'Banco Fibra','Fibra'),
			(56,2,'Banco Gerador','Gerador'),
			(57,2,'Banco Guanabara','Guanabara'),
			(58,2,'Banco Industrial do Brasil','BI'),
			(59,2,'Banco Industrial e Comercial','BICBANCO'),
			(60,2,'Banco Indusval','BI&P'),
			(61,2,'Banco Intermedium','Intermedium'),
			(62,2,'Banco Itaú BBA','Itaú BBA'),
			(63,2,'Banco ItaúBank','ItaúBank'),
			(64,2,'Banco Mercantil do Brasil','BMB'),
			(65,2,'Banco Modal','Modal'),
			(66,2,'Banco Morada','Morada'),
			(67,2,'Banco Pan','Pan'),
			(68,2,'Banco Paulista','Paulista'),
			(69,2,'Banco Pine','Pine'),
			(70,2,'Banco Renner','Renner'),
			(71,2,'Banco Ribeirão Preto','BRP'),
			(72,2,'Banco Safra','Safra'),
			(73,2,'Banco Santander','Santander'),
			(74,2,'Banco Sofisa','Sofisa'),
			(75,2,'Banco Topázio','Topázio'),
			(76,2,'Banco Votorantim','BV'),
			(77,2,'Bradesco','Bradesco'),
			(78,2,'Itaú Unibanco','Itaú'),
			(79,2,'Banco Cooperativo do Brasil','BANCOOB'),
			(80,2,'Banco Cooperativo Sicredi','BANSICREDI'),

			/*Gêneros*/
			(81,3,'Feminino','F'),
			(82,3,'Masculino','M'),

			/*Status*/
			(83,4,'Buscando Motorista','BM'),
			(84,4,'Aguardando Motorista','AM'),
			(85,4,'Em Andamento','EA'),
			(86,4,'Corrida Finalizada','CF'),
			(87,4,'Cancelado pelo passageiro','CC'),
			(88,4,'Cancelado pelo motorista','CM');

/*Cidades em que o APP irá funcionar*/
CREATE TABLE IF NOT EXISTS cad_cidades_atuacao(

	id_cidades_atuacao    int not null AUTO_INCREMENT,
	nome_cidades_atuacao  text,
	ativa_cidades_atuacao boolean DEFAULT false,

	PRIMARY KEY (id_cidades_atuacao)

);

CREATE TABLE IF NOT EXISTS cad_dados_empresa (

	id_dados_empresa  int not null AUTO_INCREMENT,
	tel_empresa		  text,
	email_empresa	  text,
	tel_empresa_passageiro		  text,
	email_empresa_passageiro	  text,
	cep_empresa text,
	uf_empresa text,
	cidade_empresa text,
	bairro_empresa text,
	rua_empresa text,
	numero_empresa text,
	link_youtube text,
	link_youtube_motorista text,

	PRIMARY KEY (id_dados_empresa)

);

insert into cad_dados_empresa (tel_empresa,email_empresa,tel_empresa_passageiro,email_empresa_passageiro,link_youtube,link_youtube_motorista) values ('11987654321','email@yougo.com','11987654321','email@yougo.com','link','link');

insert into cad_cidades_atuacao (nome_cidades_atuacao,ativa_cidades_atuacao)
	values  ('Bauru',true),
			('Jaú',true),
			('Birigui',true),
			('Botucatu',true),
			('Marília',true),
			('Ourinhos',true),
			('Assis',true),
			('Presidente prudente',true),
			('Araçatuba',true),
			('Franca',true),
			('Barretos',true),
			('Araraquara',true),
			('São Carlos',true),
			('Rio Claro',true),
			('Limeira',true),
			('Indaiatuba',true),
			('Jundiaí',true),
			('Itu',true),
			('Tatuí',true),
			('Itapetininga',true),
			('Guarulhos',true);
/*Cidades em que o APP irá funcionar*/

/*Segurança Usuários*/
CREATE TABLE IF NOT EXISTS seg_usuarios(

	id_usuario int not null AUTO_INCREMENT comment "ID Usuário",
	nome_usuario character varying(100) NOT NULL comment "Nome Completo",
	email_usuario character varying(40) NOT NULL comment "E-mail Usuário",
	telefone_usuario character varying(11) NOT NULL comment "Telefone Usuário",
	login_usuario character varying(40) NOT NULL comment "Login Usuário",
	senha_usuario character varying(40) NOT NULL comment "Senha Usuário",
	ativo_usuario boolean NOT NULL comment "Status Usuário",
	fk_grupo_usuario int not null comment "Grupo Usuário",
	usuario_criou_usuario int comment "Usuário que criou",
	criacao_usuario timestamp DEFAULT CURRENT_TIMESTAMP comment "Data criação",
	facebook_usuario character varying(255) comment "ID Facebook",

	rg_usuario  character varying(10)  comment "RG Usuário",
	cpf_usuario character varying(11)  comment "CPF Usuário",
	celular_usuario character varying(11)  comment "Celular Usuário",

	logradouro_usuario text  comment "Logradouro Usuário",
	cidade_usuario text  comment "Cidade Usuário",
	fk_uf_usuario int  comment "UF Usuário",
	bairro_usuario text  comment "Bairro Usuário",
	cep_usuario character varying(8)  comment "CEP Usuário",
	complemento_usuario text  comment "Complemento Usuário",
	num_residencia_usuario text  comment "Número Residencia",

	fk_genero int comment "Gênero Usuário",

	ativado_sms boolean DEFAULT false comment "Ativado por SMS",
	ativado_email boolean DEFAULT false comment "Ativado por E-Mail",

	bloqueado boolean DEFAULT false,
	motivo_inativacao text,

	PRIMARY KEY (id_usuario),
	UNIQUE (login_usuario,fk_grupo_usuario),
	UNIQUE (email_usuario,fk_grupo_usuario),
	UNIQUE (cpf_usuario,fk_grupo_usuario),
	FOREIGN KEY (usuario_criou_usuario) REFERENCES seg_usuarios (id_usuario),
	FOREIGN KEY (fk_uf_usuario) 		REFERENCES cad_item_grupo (id_item_grupo), /*Grupo 1*/
	FOREIGN KEY (fk_genero) 		REFERENCES cad_item_grupo (id_item_grupo)     /*Grupo 3*/


);

CREATE TABLE IF NOT EXISTS cad_detalhes_motorista(

	id_motorista int not null AUTO_INCREMENT comment "ID Motorista",
	fk_usuario int not null comment "FK Usuário",
	fk_cidade_ativa_motorista int  comment "Vinculo Cidade",
	/*Banco*/
	fk_banco_motorista int comment "Banco",
	agencia_motorista character varying(8) comment "Agencia",
	conta_motorista character varying(16) comment "Conta",
	digito_motorista character varying(1) comment "Dígito Conta",
	tipo_conta int,
	/*Carro*/
	fk_modelo_carro_motorista int,
	cor_carro_motorista text,
	placa_carro_motorista character varying(8),

	motorista_online boolean,
	latitude_atual float(10,6),
	longitude_atual float(10,6),
	data_atualizacao_deslocamento timestamp,

	PRIMARY KEY (id_motorista),
	UNIQUE (placa_carro_motorista),
	FOREIGN KEY (fk_usuario) 					REFERENCES seg_usuarios (id_usuario),
	FOREIGN KEY (fk_banco_motorista) 			REFERENCES cad_item_grupo (id_item_grupo), /*Grupo 2*/
	FOREIGN KEY (fk_cidade_ativa_motorista) 	REFERENCES cad_cidades_atuacao (id_cidades_atuacao),
	FOREIGN KEY (fk_modelo_carro_motorista) 	REFERENCES cad_modelos (id_modelo)


);


CREATE TABLE IF NOT EXISTS elo_pagamento_cliente (

	id_pagamento int not null AUTO_INCREMENT,
	fk_usuario   int not null,

	nome_cartao text,
	numero_cartao character varying(19),
	data_vencimento_cartao date,
	cvv_cartao character varying(4),
	data_nascimento date not null comment "Data de nascimento",
	cartao_ativo boolean DEFAULT true, /*Para ocultar o cartao caso ele seja 'deletado'*/
	hash_pagamento text,
	token_pagamento text,
	code_pag_seguro text,
	status_pag_seguro int,

	PRIMARY KEY (id_pagamento),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario)

);


/*Segurança Grupos*/
CREATE TABLE IF NOT EXISTS seg_grupos(

	id_grupo int not null AUTO_INCREMENT comment "ID Grupo",
	nome_grupo character varying(100) NOT NULL comment "Nome Grupo",
	descricao_grupo text NOT NULL comment "Descrição Grupo",
	usuario_criou_grupo int comment "Usuário que criou",
	ativo_grupo boolean NOT NULL comment "Status Grupo",
	criacao_grupo timestamp DEFAULT CURRENT_TIMESTAMP comment "Data Criação",

	PRIMARY KEY (id_grupo),
	CONSTRAINT unique_nome_grupo UNIQUE (nome_grupo),
	FOREIGN KEY (usuario_criou_grupo) REFERENCES seg_usuarios (id_usuario)

);

ALTER TABLE seg_usuarios
ADD FOREIGN KEY (fk_grupo_usuario) REFERENCES seg_grupos(id_grupo);

/*Segurança, cadastro dos models*/
CREATE TABLE IF NOT EXISTS seg_models(

	id_model int not null AUTO_INCREMENT,
	link_model character varying(100) not null,
	descricao_model text not null,

	CONSTRAINT pk_model PRIMARY KEY (id_model)

);

CREATE TABLE IF NOT EXISTS seg_log_acesso(

	id_log_acesso int NOT NULL AUTO_INCREMENT,
	fk_usuario int,
	data_log_acesso timestamp DEFAULT CURRENT_TIMESTAMP,
	ip_usuario_acesso character varying(16),
	acesso boolean,
	maquina_usuario_acesso text,

	PRIMARY KEY (id_log_acesso),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario)

);

CREATE TABLE IF NOT EXISTS seg_log_erro(

	id_log_erro int NOT NULL AUTO_INCREMENT,
	fk_usuario int,
	data_log_erro timestamp DEFAULT CURRENT_TIMESTAMP,
	cod text, /*Text para casos onde o código seja texto ao invés de número*/
	erro text,
	query text,
	erro_feedback text, /*Relatado pelo usuário*/
	funcao text,
	maquina_usuario_erro text,

	PRIMARY KEY (id_log_erro),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario)

);

/*Segurança, cadastro dos controllers*/
CREATE TABLE IF NOT EXISTS seg_controllers(

	id_controller int not null AUTO_INCREMENT,
	link_controller character varying(100) not null,
	descricao_controller text not null,
	fk_model int,

	PRIMARY KEY (id_controller),
	FOREIGN KEY (fk_model) REFERENCES seg_models (id_model)

);

/*Segurança menu, gerá um menu no aplicativo, as telas devem ser vinculadas*/
CREATE TABLE IF NOT EXISTS seg_menu(

	id_menu int not null AUTO_INCREMENT,
	titulo_menu character varying(100) NOT NULL,
	descricao_menu text NOT NULL,
	menu_acima int,
	posicao_menu int,

	PRIMARY KEY (id_menu),
	FOREIGN KEY (menu_acima) REFERENCES seg_menu (id_menu)

);

/*Segurança, aplicações do menu.*/
CREATE TABLE IF NOT EXISTS seg_aplicacao(

	id_aplicacao int not null AUTO_INCREMENT,
	link_aplicacao character varying(100) NOT NULL,
	titulo_aplicacao character varying(100) NOT NULL,
	descricao_aplicacao text NOT NULL,
	fk_controller int,

	PRIMARY KEY (id_aplicacao),
    FOREIGN KEY (fk_controller) REFERENCES seg_controllers (id_controller)

);

/*Gera um Log dos acessos do usuário.*/
CREATE TABLE IF NOT EXISTS seg_log_navegacao(

	id_log_navegacao int NOT NULL AUTO_INCREMENT,
	fk_usuario int,
	data_log_navegacao timestamp DEFAULT CURRENT_TIMESTAMP,
	permissao boolean,
	fk_aplicacao int,
	parametros text,

	PRIMARY KEY (id_log_navegacao),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario),
	FOREIGN KEY (fk_aplicacao) REFERENCES seg_aplicacao (id_aplicacao)

);

CREATE TABLE IF NOT EXISTS seg_log_edicao(

	id_log_edicao int NOT NULL AUTO_INCREMENT,
	fk_usuario int,
	data_log_edicao timestamp DEFAULT CURRENT_TIMESTAMP,
	original_edicao text,
	novo_edicao text,
	campo_edicao text,
	tabela_edicao text,
	fk_aplicacao int,
	id_edicao int,


	PRIMARY KEY (id_log_edicao),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario),
	FOREIGN KEY (fk_aplicacao) REFERENCES seg_aplicacao (id_aplicacao)

);

/*Vincula as aplicações ao menu*/
CREATE TABLE IF NOT EXISTS seg_aplicacoes_menu(

	id_aplicacoes_menu int not null AUTO_INCREMENT,
	fk_aplicacao int NOT NULL,
	fk_menu int NOT NULL,

	PRIMARY KEY (id_aplicacoes_menu),
	FOREIGN KEY (fk_aplicacao) REFERENCES seg_aplicacao (id_aplicacao),
    FOREIGN KEY (fk_menu) REFERENCES seg_menu (id_menu)

);

/*Segurança, Grupo aplicação*/
CREATE TABLE IF NOT EXISTS seg_aplicacoes_grupos(

	id_aplicacoes_grupos int not null AUTO_INCREMENT,
	fk_grupo int NOT NULL,
	fk_aplicacao int NOT NULL,

	PRIMARY KEY (id_aplicacoes_grupos),
	FOREIGN KEY (fk_aplicacao) REFERENCES seg_aplicacao (id_aplicacao),
    FOREIGN KEY (fk_grupo) REFERENCES seg_grupos (id_grupo)

);

/*Cadastro dos controllers e models*/
insert into seg_models (id_model,link_model,descricao_model) values
	(1,'Model_menu','Responsável pelos menus.');
insert into seg_models (id_model,link_model,descricao_model) values
	(2,'Model_seguranca','Responsável pelo acesso as sistema e controle de perfils.');
insert into seg_models (id_model,link_model,descricao_model) values
	(3,'Model_usuarios','Responsável pelo gerênciamento dos usuários.');
insert into seg_models (id_model,link_model,descricao_model) values
	(4,'Model_grupos','Responsável pelo gerênciamento dos grupos.');


insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values
	(1,'Main','Responsável pelo gerênciamento do acesso ao sistema.',2);
insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values
	(2,'Controller_usuarios','Responsável pelo gerênciamento dos usuários.',3);
insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values
	(3,'Controller_grupos','Responsável pelo gerênciamento dos grupos.',4);


/*INSERTS PARA USUÁRIO DE TESTES*/
insert into seg_grupos(id_grupo,nome_grupo,descricao_grupo,ativo_grupo) values (1,'Administradores','Administradores do sistema',true);

insert into seg_usuarios (id_usuario,nome_usuario,email_usuario,telefone_usuario,login_usuario,senha_usuario,ativo_usuario,fk_grupo_usuario)
values (1,'Usuário Administrador','megamil3d@gmail.com','11962782329','admin','40bd001563085fc35165329ea1ff5c5ecbdbbeef',TRUE,1);

/*Definindo os menus.*/
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu)
	values (1,'Segurança','Segurança, Perfils e Grupos',null,1000);	 /*1000 para Garantir que será o útima menu*/
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu)
	values (2,'Editar Perfil','Editar Perfil',1,null);
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu)
	values (3,'Usuários','Lista de usuários',1,null);
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu)
	values (4,'Grupos','Lista de grupos',1,null);

/*Difinindo aplicações e links*/
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller)
	values (1,'seguranca/view_editar_perfil','Editar Perfil','Editar Perfil',2);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller)
	values (2,'seguranca/view_usuarios','Listar Usuários','Listar Usuários',2);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller)
	values (3,'seguranca/view_grupos','Listar Grupos','Listar Grupos',3);

/*Aplicações sem menu*/
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller)
	values (4,'seguranca/view_editar_grupo','Editar Grupo','Editar Grupo',3);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller)
	values (5,'seguranca/view_editar_usuario','Editar Usuário','Editar Usuários',2);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller)
	values (6,'seguranca/view_novo_grupo','Novo Grupo','Novo Grupo',3);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller)
	values (7,'seguranca/view_novo_usuario','Novo Usuário','Novo Usuários',2);

/*Indicando quais aplicações estão ligadas a quais menus.*/
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (1,2);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (2,3);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (3,4);

/*Dando permissões para o grupo administrador.*/
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(1,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(2,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(3,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(4,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(5,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(6,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(7,1);

/*Notificações*/
INSERT INTO seg_models (id_model,link_model, descricao_model) VALUES (5,'Model_notificacoes', 'Model Notificações');
INSERT INTO seg_controllers (id_controller,link_controller, descricao_controller, fk_model) VALUES (4,'Controller_notificacoes', 'Controller Notificações', 5);

INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES (NULL, 999, 'Notificações', 'Listas de Notificações', 5);
INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES (1, NULL, 'Enviar Notificação', 'Lista de suas notificações podendo enviar uma nova.', 6);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (8,'notificacoes/view_notificacoes', 'Notificações', 'Notificações', 4);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (8,5);
/*insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(8,1);*/

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (9,'notificacoes/view_hist_notificacoes', 'Minhas Notificações', 'Minhas Notificações', 4);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (9,6);
/*insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(9,1);*/

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (10,'notificacoes/view_notificar', 'Nova Notificação', 'Nova Notificação', 4);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(10,1);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (11,'notificacoes/view_editar_notificacao', 'Editar Notificação', 'Editar Notificação', 4);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(11,1);

/*Relatórios*/
INSERT INTO seg_models (id_model,link_model, descricao_model) VALUES (6,'Model_relatorios', 'Model Relatórios');
INSERT INTO seg_controllers (id_controller,link_controller, descricao_controller, fk_model) VALUES (5,'Controller_relatorios', 'Relatórios', 6);

INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (NULL, 'Relatórios', 'Relatórios', 7, 998);
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (7, 'Acessos', 'Relatório de Acessos', 8, null);
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (7, 'Navegação', 'Relatório de Navegação', 9, null);
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (7, 'Edições', 'Relatório de Edições', 10, null);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (12,'relatorios/view_relatorio_acesso', 'Acessos', 'Relatório dos Acessos', 5);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (12,8);
/*insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(12,1);*/

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (13,'relatorios/view_relatorio_navegacao', 'Navegação', 'Relatório de Navegação', 5);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (13,9);
/*insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(13,1);*/

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (14,'relatorios/view_relatorio_edicoes', 'Edições', 'Relatório de Edições', 5);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (14,10);
/*insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(14,1);*/

CREATE TABLE IF NOT EXISTS cad_notificacao (

	id_notificacao int not null AUTO_INCREMENT,
	fk_usuario int,
	data_notificacao timestamp DEFAULT CURRENT_TIMESTAMP,
	titulo_notificacao character varying(20),
	notificacao text,
	data_limite_notificacao date,

	PRIMARY KEY (id_notificacao),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario)

);

CREATE TABLE IF NOT EXISTS cad_hist_notificacao (

	id_hist_notificacao int not null AUTO_INCREMENT,
	fk_usuario_destino int,
	fk_notificacao int,
	data_envio_notificacao timestamp DEFAULT CURRENT_TIMESTAMP,
	data_leitura timestamp DEFAULT 0,
	notificacao_enviada boolean DEFAULT false, /*Quando o navegador/mobile receber já marca para não enviar novamente.*/

	PRIMARY KEY (id_hist_notificacao),
	FOREIGN KEY (fk_notificacao) REFERENCES cad_notificacao (id_notificacao),
	FOREIGN KEY (fk_usuario_destino) REFERENCES seg_usuarios (id_usuario)

);


/*Views*/
-- Por não trazer os comentários das views não pode ajudar.
-- create or replace view view_alias as
-- 	SELECT TABLE_NAME as tabela,
-- 		   COLUMN_KEY as chave,
-- 		   COLUMN_NAME as coluna,
-- 		   DATA_TYPE as tipo,
-- 		   CHARACTER_MAXIMUM_LENGTH as tamanho,
-- 		   COLUMN_TYPE as coluna_detalhes,
-- 		   COLUMN_COMMENT as comentario
-- 	FROM INFORMATION_SCHEMA.COLUMNS
-- 	WHERE TABLE_SCHEMA = 'db_yougo';

/*Tabela que irá dar detalhes dos campos da view*/
CREATE TABLE IF NOT EXISTS cad_detalhes_views (

	id_detalhes_views  int not null AUTO_INCREMENT,
	nome_view text,
	nome_campo text,
	tipo_campo text,
	descricao_campo text,
	visivel boolean DEFAULT true, /*Existem campos que não devem ser listados, mas existem para serem usados de filtro*/

	PRIMARY KEY (id_detalhes_views)

);
/***************************************************************************************************/
create or replace view view_relatorio_acessos as
	SELECT
		id_log_acesso as id,
		id_usuario,
		nome_usuario as usuario,
		data_log_acesso as data_acesso,
		date_format(data_log_acesso,'%d/%m/%Y as  %H:%i:%s') as data_acesso_formatado,
		ip_usuario_acesso as ip,
		maquina_usuario_acesso as maquina,
		acesso,

		case acesso
			when 1 then 'Entrou'
            when 0 then 'Saiu' end as acessou

		FROM seg_log_acesso
		inner join seg_usuarios on id_usuario = fk_usuario;

insert into cad_detalhes_views (nome_view,nome_campo,tipo_campo,descricao_campo,visivel) values
	("view_relatorio_acessos","id","int","ID",true),
	("view_relatorio_acessos","usuario","text","Nome Usuário",true),
	("view_relatorio_acessos","data_acesso_formatado","timestamp","Data Acesso",true),
	("view_relatorio_acessos","ip","text","IP",true),
	("view_relatorio_acessos","maquina","text","Maquina / Navegador",true),
	("view_relatorio_acessos","acessou","text","Login / Logoff",true),
	-- Campos usados somente para o where.
	("view_relatorio_acessos","id_usuario","int","Nome Usuário",false),
	("view_relatorio_acessos","data_acesso","timestamp","Data Acesso",false),
	("view_relatorio_acessos","acesso","int","Login / Logoff",false);

/***************************************************************************************************/

create or replace view view_relatorio_navegacao as
	select
    id_log_navegacao as id,
    fk_usuario,
    nome_usuario,
    data_log_navegacao as data_log,
    date_format(data_log_navegacao,'%d/%m/%Y as  %H:%i:%s') as data_log_formatado,
    permissao,
    case permissao
			when 1 then 'Sim'
            when 0 then 'Não' end as com_permissao,
	fk_aplicacao,
    descricao_aplicacao,
    parametros

    from seg_log_navegacao
    inner join seg_usuarios on fk_usuario = id_usuario
    inner join seg_aplicacao on id_aplicacao = fk_aplicacao;

insert into cad_detalhes_views (nome_view,nome_campo,tipo_campo,descricao_campo,visivel) values
	("view_relatorio_navegacao","id","int","ID",true),
	("view_relatorio_navegacao","nome_usuario","text","Nome Usuário",true),
	("view_relatorio_navegacao","data_log_formatado","timestamp","Data Acesso",true),
	("view_relatorio_navegacao","com_permissao","timestamp","Tem acesso?",true),
	("view_relatorio_navegacao","descricao_aplicacao","text","Aplicação",true),
	("view_relatorio_navegacao","parametros","text","Parametros Usados",true),

	("view_relatorio_navegacao","fk_usuario","int","Nome Usuário",false),
	("view_relatorio_navegacao","data_log","timestamp","Data Acesso",false),
	("view_relatorio_navegacao","permissao","tinyint","Tem acesso?",false),
	("view_relatorio_navegacao","fk_aplicacao","int","Aplicação",false);
/***************************************************************************************************/
create or replace view view_relatorio_edicoes as
	select
    id_log_edicao as id,
    fk_usuario,
    nome_usuario,
    data_log_edicao as data_log,
    date_format(data_log_edicao,'%d/%m/%Y as  %H:%i:%s') as data_log_formatado,
    original_edicao,
    novo_edicao,
    fk_aplicacao,
    descricao_aplicacao,
    id_edicao,
    (SELECT  COLUMN_COMMENT as comentario
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_SCHEMA = 'db_yougo' AND TABLE_NAME = tabela_edicao AND COLUMN_NAME = campo_edicao) as campo

    from seg_log_edicao
    inner join seg_usuarios on fk_usuario = id_usuario
    inner join seg_aplicacao on id_aplicacao = fk_aplicacao;

insert into cad_detalhes_views (nome_view,nome_campo,tipo_campo,descricao_campo,visivel) values
	("view_relatorio_edicoes","id","int","ID",true),
	("view_relatorio_edicoes","nome_usuario","text","Nome Usuário",true),
	("view_relatorio_edicoes","data_log_formatado","timestamp","Data Edição",true),
	("view_relatorio_edicoes","original_edicao","text","Valor Original",true),
	("view_relatorio_edicoes","novo_edicao","text","Novo Valor",true),
	("view_relatorio_edicoes","descricao_aplicacao","text","Titulo Aplicação",true),
	("view_relatorio_edicoes","id_edicao","int","Filtro Edição",true),
	("view_relatorio_edicoes","campo","text","Campo",true),

	("view_relatorio_edicoes","fk_usuario","int","Nome Usuário",false),
	("view_relatorio_edicoes","data_log","timestamp","Data Edição",false),
	("view_relatorio_edicoes","fk_aplicacao","int","Titulo Aplicação",false);
/***************************************************************************************************/
/*View de listas*/
create or replace view view_lista_grupos as
	select
    id_grupo as id,
    nome_grupo,
    descricao_grupo,
    usuario_criou_grupo as usuario,
    nome_usuario,
    ativo_grupo,
    case ativo_grupo
			when 1 then 'Ativo'
            when 0 then 'Inativo' end as ativo,
    criacao_grupo,
    date_format(criacao_grupo,'%d/%m/%Y as  %H:%i:%s') as criacao_grupo_formatado,
    (select count(*) from seg_usuarios where fk_grupo_usuario = id_grupo and ativo_usuario = true) as usuarios_ativos

    from seg_grupos
    left join seg_usuarios on id_usuario = usuario_criou_grupo;

insert into cad_detalhes_views (nome_view,nome_campo,tipo_campo,descricao_campo,visivel) values
	("view_lista_grupos","id","int","ID",true),
	("view_lista_grupos","nome_grupo","text","Nome Grupo",true),
	("view_lista_grupos","descricao_grupo","text","Descrição Grupo",true),
	("view_lista_grupos","nome_usuario","text","Criado por",true),
	("view_lista_grupos","criacao_grupo_formatado","timestamp","Criado Em",true),
	("view_lista_grupos","usuarios_ativos","int","Usuários Ativos",true),
	("view_lista_grupos","ativo","tinyint","Status Grupo",true),

	("view_lista_grupos","usuario","int","Criado por",false),
	("view_lista_grupos","ativo_grupo","tinyint","Status Grupo",false),
	("view_lista_grupos","criacao_grupo","timestamp","Criado Em",false);
/***************************************************************************************************/
create or replace view view_lista_usuarios as
	select
    id_usuario as id,
    nome_usuario,
    email_usuario,
    telefone_usuario,
    login_usuario,
    ativo_usuario,
    case ativo_usuario
			when 1 then 'Sim'
            when 0 then 'Não' end as ativo,
    fk_grupo_usuario,
    nome_grupo,
    usuario_criou_usuario usuario,
    (select nome_usuario from seg_usuarios su0 where su0.id_usuario = su1.usuario_criou_usuario) as usuario_criou,
    criacao_usuario,
    date_format(criacao_usuario,'%d/%m/%Y as  %H:%i:%s') as criacao_usuario_formatado

    from seg_usuarios su1
    inner join seg_grupos on id_grupo = fk_grupo_usuario;

insert into cad_detalhes_views (nome_view,nome_campo,tipo_campo,descricao_campo,visivel) values
	("view_lista_usuarios","id","int","ID",true),
	("view_lista_usuarios","nome_usuario","text","Usuário",true),
	("view_lista_usuarios","email_usuario","text","E-mail",true),
	("view_lista_usuarios","telefone_usuario","text","Telefone",true),
	("view_lista_usuarios","login_usuario","text","Login",true),
	("view_lista_usuarios","ativo","text","Status Usuário",true),
	("view_lista_usuarios","nome_grupo","text","Grupo",true),
	("view_lista_usuarios","usuario_criou","text","Criado por",true),
	("view_lista_usuarios","criacao_usuario_formatado","timestamp","Criado em",true),

	("view_lista_usuarios","ativo_usuario","tinyint","Status Usuário",false),
	("view_lista_usuarios","fk_grupo_usuario","int","Grupo",false),
	("view_lista_usuarios","usuario","int","Criado por",false),
	("view_lista_usuarios","criacao_usuario","timestamp","Criado em",false);
/***************************************************************************************************/
/*Notificações PUSH, Tokens.*/
CREATE TABLE IF NOT EXISTS cad_tokens(

	id_token int not null auto_increment,
	token character varying(300),
	fk_usuario int,
	aparelho int, /*'MOTORISTA_ANDROID' = 1 'MOTORISTA_IOS' = 2 'PASSAGEIRO_ANDROID' = 3 'PASSAGEIRO_IOS' = 4*/
	data_registro timestamp DEFAULT CURRENT_TIMESTAMP,

	PRIMARY KEY (id_token),
	FOREIGN KEY (fk_usuario)
        REFERENCES seg_usuarios(id_usuario)

);

/***************************************************************************************************/
/*Modelos*/
insert into cad_montadoras values(1,'CHEVROLET');
insert into cad_montadoras values(2,'VOLKSWAGEN');
insert into cad_montadoras values(3,'FIAT');
insert into cad_montadoras values(4,'MERCEDES-BENZ');
insert into cad_montadoras values(5,'CITROEN');
insert into cad_montadoras values(6,'CHANA');
insert into cad_montadoras values(7,'HONDA');
insert into cad_montadoras values(8,'SUBARU');
insert into cad_montadoras values(10,'FERRARI');
insert into cad_montadoras values(11,'BUGATTI');
insert into cad_montadoras values(12,'LAMBORGHINI');
insert into cad_montadoras values(13,'FORD');
insert into cad_montadoras values(14,'HYUNDAI');
insert into cad_montadoras values(15,'JAC');
insert into cad_montadoras values(16,'KIA');
insert into cad_montadoras values(17,'GURGEL');
insert into cad_montadoras values(18,'DODGE');
insert into cad_montadoras values(19,'CHRYSLER');
insert into cad_montadoras values(20,'BENTLEY');
insert into cad_montadoras values(21,'SSANGYONG');
insert into cad_montadoras values(22,'PEUGEOT');
insert into cad_montadoras values(23,'TOYOTA');
insert into cad_montadoras values(24,'RENAULT');
insert into cad_montadoras values(25,'ACURA');
insert into cad_montadoras values(26,'ADAMO');
insert into cad_montadoras values(27,'AGRALE');
insert into cad_montadoras values(28,'ALFA ROMEO');
insert into cad_montadoras values(29,'AMERICAR');
insert into cad_montadoras values(31,'ASTON MARTIN');
insert into cad_montadoras values(32,'AUDI');
insert into cad_montadoras values(34,'BEACH');
insert into cad_montadoras values(35,'BIANCO');
insert into cad_montadoras values(36,'BMW');
insert into cad_montadoras values(37,'BORGWARD');
insert into cad_montadoras values(38,'BRILLIANCE');
insert into cad_montadoras values(41,'BUICK');
insert into cad_montadoras values(42,'CBT');
insert into cad_montadoras values(43,'NISSAN');
insert into cad_montadoras values(44,'CHAMONIX');
insert into cad_montadoras values(46,'CHEDA');
insert into cad_montadoras values(47,'CHERY');
insert into cad_montadoras values(48,'CORD');
insert into cad_montadoras values(49,'COYOTE');
insert into cad_montadoras values(50,'CROSS LANDER');
insert into cad_montadoras values(51,'DAEWOO');
insert into cad_montadoras values(52,'DAIHATSU');
insert into cad_montadoras values(53,'VOLVO');
insert into cad_montadoras values(54,'DE SOTO');
insert into cad_montadoras values(55,'DETOMAZO');
insert into cad_montadoras values(56,'DELOREAN');
insert into cad_montadoras values(57,'DKW-VEMAG');
insert into cad_montadoras values(59,'SUZUKI');
insert into cad_montadoras values(60,'EAGLE');
insert into cad_montadoras values(61,'EFFA');
insert into cad_montadoras values(63,'ENGESA');
insert into cad_montadoras values(64,'ENVEMO');
insert into cad_montadoras values(65,'FARUS');
insert into cad_montadoras values(66,'FERCAR');
insert into cad_montadoras values(68,'FNM');
insert into cad_montadoras values(69,'PONTIAC');
insert into cad_montadoras values(70,'PORSCHE');
insert into cad_montadoras values(72,'GEO');
insert into cad_montadoras values(74,'GRANCAR');
insert into cad_montadoras values(75,'GREAT WALL');
insert into cad_montadoras values(76,'HAFEI');
insert into cad_montadoras values(78,'HOFSTETTER');
insert into cad_montadoras values(79,'HUDSON');
insert into cad_montadoras values(80,'HUMMER');
insert into cad_montadoras values(82,'INFINITI');
insert into cad_montadoras values(83,'INTERNATIONAL');
insert into cad_montadoras values(86,'JAGUAR');
insert into cad_montadoras values(87,'JEEP');
insert into cad_montadoras values(88,'JINBEI');
insert into cad_montadoras values(89,'JPX');
insert into cad_montadoras values(90,'KAISER');
insert into cad_montadoras values(91,'KOENIGSEGG');
insert into cad_montadoras values(92,'LAUTOMOBILE');
insert into cad_montadoras values(93,'LAUTOCRAFT');
insert into cad_montadoras values(94,'LADA');
insert into cad_montadoras values(95,'LANCIA');
insert into cad_montadoras values(96,'LAND ROVER');
insert into cad_montadoras values(97,'LEXUS');
insert into cad_montadoras values(98,'LIFAN');
insert into cad_montadoras values(99,'LINCOLN');
insert into cad_montadoras values(100,'LOBINI');
insert into cad_montadoras values(101,'LOTUS');
insert into cad_montadoras values(102,'MAHINDRA');
insert into cad_montadoras values(104,'MASERATI');
insert into cad_montadoras values(106,'MATRA');
insert into cad_montadoras values(107,'MAYBACH');
insert into cad_montadoras values(108,'MAZDA');
insert into cad_montadoras values(109,'MENON');
insert into cad_montadoras values(110,'MERCURY');
insert into cad_montadoras values(111,'MITSUBISHI');
insert into cad_montadoras values(112,'MG');
insert into cad_montadoras values(113,'MINI');
insert into cad_montadoras values(114,'MIURA');
insert into cad_montadoras values(115,'MORRIS');
insert into cad_montadoras values(116,'MP LAFER');
insert into cad_montadoras values(117,'MPLM');
insert into cad_montadoras values(118,'NEWTRACK');
insert into cad_montadoras values(119,'NISSIN');
insert into cad_montadoras values(120,'OLDSMOBILE');
insert into cad_montadoras values(121,'PAG');
insert into cad_montadoras values(122,'PAGANI');
insert into cad_montadoras values(123,'PLYMOUTH');
insert into cad_montadoras values(124,'PUMA');
insert into cad_montadoras values(125,'RENO');
insert into cad_montadoras values(126,'REVA-I');
insert into cad_montadoras values(127,'ROLLS-ROYCE');
insert into cad_montadoras values(129,'ROMI');
insert into cad_montadoras values(130,'SEAT');
insert into cad_montadoras values(131,'UTILITARIOS AGRICOLAS');
insert into cad_montadoras values(132,'SHINERAY');
insert into cad_montadoras values(137,'SAAB');
insert into cad_montadoras values(139,'SHORT');
insert into cad_montadoras values(141,'SIMCA');
insert into cad_montadoras values(142,'SMART');
insert into cad_montadoras values(143,'SPYKER');
insert into cad_montadoras values(144,'STANDARD');
insert into cad_montadoras values(145,'STUDEBAKER');
insert into cad_montadoras values(146,'TAC');
insert into cad_montadoras values(147,'TANGER');
insert into cad_montadoras values(148,'TRIUMPH');
insert into cad_montadoras values(149,'TROLLER');
insert into cad_montadoras values(150,'UNIMOG');
insert into cad_montadoras values(154,'WIESMANN');
insert into cad_montadoras values(155,'CADILLAC');
insert into cad_montadoras values(156,'AM GEN');
insert into cad_montadoras values(157,'BUGGY');
insert into cad_montadoras values(158,'WILLYS OVERLAND');
insert into cad_montadoras values(161,'KASEA');
insert into cad_montadoras values(162,'SATURN');
insert into cad_montadoras values(163,'SWELL MINI');
insert into cad_montadoras values(175,'SKODA');
insert into cad_montadoras values(239,'KARMANN GHIA');
insert into cad_montadoras values(254,'KART');
insert into cad_montadoras values(258,'HANOMAG');
insert into cad_montadoras values(261,'OUTROS');
insert into cad_montadoras values(265,'HILLMAN');
insert into cad_montadoras values(288,'HRG');
insert into cad_montadoras values(295,'GAIOLA');
insert into cad_montadoras values(338,'TATA');
insert into cad_montadoras values(341,'DITALLY');
insert into cad_montadoras values(345,'RELY');
insert into cad_montadoras values(346,'MCLAREN');
insert into cad_montadoras values(534,'GEELY');



insert into cad_modelos values(1,25,'INTEGRA');
insert into cad_modelos values(2,25,'LEGEND');
insert into cad_modelos values(3,25,'NSX');
insert into cad_modelos values(4,27,'MARRUA');
insert into cad_modelos values(5,28,'145');
insert into cad_modelos values(6,28,'147');
insert into cad_modelos values(7,28,'155');
insert into cad_modelos values(8,28,'156');
insert into cad_modelos values(9,28,'164');
insert into cad_modelos values(10,28,'166');
insert into cad_modelos values(11,28,'2300');
insert into cad_modelos values(12,28,'SPIDER');
insert into cad_modelos values(13,156,'HUMMER');
insert into cad_modelos values(14,16,'AM-825');
insert into cad_modelos values(15,16,'HI-TOPIC');
insert into cad_modelos values(16,16,'ROCSTA');
insert into cad_modelos values(17,16,'TOPIC');
insert into cad_modelos values(18,16,'TOWNER');
insert into cad_modelos values(19,32,'100');
insert into cad_modelos values(20,32,'80');
insert into cad_modelos values(21,32,'A1');
insert into cad_modelos values(22,32,'A3');
insert into cad_modelos values(23,32,'A4 SEDAN');
insert into cad_modelos values(24,32,'A5 COUPE');
insert into cad_modelos values(25,32,'A6 SEDAN');
insert into cad_modelos values(26,32,'A7');
insert into cad_modelos values(27,32,'A8');
insert into cad_modelos values(30,32,'Q3');
insert into cad_modelos values(31,32,'Q5');
insert into cad_modelos values(32,32,'Q7');
insert into cad_modelos values(33,32,'R8');
insert into cad_modelos values(34,32,'RS3');
insert into cad_modelos values(35,32,'RS4');
insert into cad_modelos values(36,32,'RS5');
insert into cad_modelos values(37,32,'RS6');
insert into cad_modelos values(38,32,'S3');
insert into cad_modelos values(39,32,'S4 SEDAN');
insert into cad_modelos values(40,32,'S6 SEDAN');
insert into cad_modelos values(41,32,'S8');
insert into cad_modelos values(42,32,'TT');
insert into cad_modelos values(43,32,'TTS');
insert into cad_modelos values(86,157,'BUGGY');
insert into cad_modelos values(87,155,'DEVILLE');
insert into cad_modelos values(88,155,'ELDORADO');
insert into cad_modelos values(89,155,'SEVILLE');
insert into cad_modelos values(90,42,'JAVALI');
insert into cad_modelos values(92,6,'MINI STAR FAMILY');
insert into cad_modelos values(93,6,'MINI STAR UTILITY');
insert into cad_modelos values(95,47,'CIELO');
insert into cad_modelos values(96,47,'FACE');
insert into cad_modelos values(97,47,'QQ');
insert into cad_modelos values(98,47,'S-18');
insert into cad_modelos values(99,47,'TIGGO');
insert into cad_modelos values(100,19,'300C');
insert into cad_modelos values(101,19,'CARAVAN');
insert into cad_modelos values(102,19,'CIRRUS');
insert into cad_modelos values(103,19,'GRAN CARAVAN');
insert into cad_modelos values(104,19,'LE BARON');
insert into cad_modelos values(105,19,'NEON');
insert into cad_modelos values(106,19,'PT CRUISER');
insert into cad_modelos values(107,19,'SEBRING');
insert into cad_modelos values(108,19,'STRATUS');
insert into cad_modelos values(109,19,'TOWN E COUNTRY');
insert into cad_modelos values(110,19,'VISION');
insert into cad_modelos values(111,5,'AIRCROSS');
insert into cad_modelos values(112,5,'AX');
insert into cad_modelos values(113,5,'BERLINGO');
insert into cad_modelos values(114,5,'BX');
insert into cad_modelos values(115,5,'C3');
insert into cad_modelos values(116,5,'C4');
insert into cad_modelos values(117,5,'C5');
insert into cad_modelos values(118,5,'C6');
insert into cad_modelos values(119,5,'C8');
insert into cad_modelos values(120,5,'DS3');
insert into cad_modelos values(121,5,'EVASION');
insert into cad_modelos values(122,5,'JUMPER');
insert into cad_modelos values(123,5,'XANTIA');
insert into cad_modelos values(124,5,'XM');
insert into cad_modelos values(125,5,'XSARA');
insert into cad_modelos values(126,5,'ZX');
insert into cad_modelos values(127,50,'CL-244');
insert into cad_modelos values(128,50,'CL-330');
insert into cad_modelos values(129,51,'ESPERO');
insert into cad_modelos values(130,51,'LANOS');
insert into cad_modelos values(131,51,'LEGANZA');
insert into cad_modelos values(132,51,'NUBIRA');
insert into cad_modelos values(133,51,'PRINCE');
insert into cad_modelos values(134,51,'RACER');
insert into cad_modelos values(135,51,'SUPER SALON');
insert into cad_modelos values(136,51,'TICO');
insert into cad_modelos values(137,52,'APPLAUSE');
insert into cad_modelos values(138,52,'CHARADE');
insert into cad_modelos values(139,52,'CUORE');
insert into cad_modelos values(140,52,'FEROZA');
insert into cad_modelos values(141,52,'GRAN MOVE');
insert into cad_modelos values(142,52,'MOVE VAN');
insert into cad_modelos values(143,52,'TERIOS');
insert into cad_modelos values(144,18,'AVENGER');
insert into cad_modelos values(145,18,'DAKOTA');
insert into cad_modelos values(146,18,'JOURNEY');
insert into cad_modelos values(147,18,'RAM');
insert into cad_modelos values(148,18,'STEALTH');
insert into cad_modelos values(151,61,'M-100');
insert into cad_modelos values(152,61,'PLUTUS');
insert into cad_modelos values(153,61,'START');
insert into cad_modelos values(155,61,'ULC');
insert into cad_modelos values(158,63,'ENGESA');
insert into cad_modelos values(159,64,'CAMPER');
insert into cad_modelos values(160,64,'MASTER');
insert into cad_modelos values(161,10,'348');
insert into cad_modelos values(162,10,'355');
insert into cad_modelos values(163,10,'360');
insert into cad_modelos values(164,10,'456');
insert into cad_modelos values(165,10,'550');
insert into cad_modelos values(166,10,'575M');
insert into cad_modelos values(167,10,'612');
insert into cad_modelos values(168,10,'CALIFORNIA');
insert into cad_modelos values(169,10,'F430');
insert into cad_modelos values(170,10,'F599');
insert into cad_modelos values(171,3,'147');
insert into cad_modelos values(174,3,'500');
insert into cad_modelos values(175,3,'BRAVA');
insert into cad_modelos values(176,3,'BRAVO');
insert into cad_modelos values(178,3,'COUPE');
insert into cad_modelos values(179,3,'DOBLO');
insert into cad_modelos values(180,3,'DUCATO CARGO');
insert into cad_modelos values(181,3,'DUNA');
insert into cad_modelos values(182,3,'ELBA');
insert into cad_modelos values(183,3,'FIORINO');
insert into cad_modelos values(184,3,'FREEMONT');
insert into cad_modelos values(185,3,'GRAND SIENA');
insert into cad_modelos values(186,3,'IDEA');
insert into cad_modelos values(187,3,'LINEA');
insert into cad_modelos values(188,3,'MAREA');
insert into cad_modelos values(189,3,'OGGI');
insert into cad_modelos values(190,3,'PALIO');
insert into cad_modelos values(191,3,'PANORAMA');
insert into cad_modelos values(192,3,'PREMIO');
insert into cad_modelos values(193,3,'PUNTO');
insert into cad_modelos values(194,3,'SIENA');
insert into cad_modelos values(195,3,'STILO');
insert into cad_modelos values(196,3,'STRADA');
insert into cad_modelos values(197,3,'TEMPRA');
insert into cad_modelos values(198,3,'TIPO');
insert into cad_modelos values(199,3,'UNO');
insert into cad_modelos values(201,13,'AEROSTAR');
insert into cad_modelos values(202,13,'ASPIRE');
insert into cad_modelos values(203,13,'BELINA');
insert into cad_modelos values(204,13,'CLUB WAGON');
insert into cad_modelos values(205,13,'CONTOUR');
insert into cad_modelos values(206,13,'CORCEL II');
insert into cad_modelos values(207,13,'COURIER');
insert into cad_modelos values(208,13,'CROWN VICTORIA');
insert into cad_modelos values(209,13,'DEL REY');
insert into cad_modelos values(210,13,'ECOSPORT');
insert into cad_modelos values(211,13,'EDGE');
insert into cad_modelos values(212,13,'ESCORT');
insert into cad_modelos values(213,13,'EXPEDITION');
insert into cad_modelos values(214,13,'EXPLORER');
insert into cad_modelos values(215,13,'F-100');
insert into cad_modelos values(216,13,'F-1000');
insert into cad_modelos values(217,13,'F-150');
insert into cad_modelos values(218,13,'F-250');
insert into cad_modelos values(219,13,'FIESTA');
insert into cad_modelos values(220,13,'FOCUS');
insert into cad_modelos values(221,13,'FURGLAINE');
insert into cad_modelos values(222,13,'FUSION');
insert into cad_modelos values(223,13,'IBIZA');
insert into cad_modelos values(224,13,'KA');
insert into cad_modelos values(225,13,'MONDEO');
insert into cad_modelos values(226,13,'MUSTANG');
insert into cad_modelos values(227,13,'PAMPA');
insert into cad_modelos values(228,13,'PROBE');
insert into cad_modelos values(229,13,'RANGER');
insert into cad_modelos values(230,13,'VERSAILLES ROYALE');
insert into cad_modelos values(231,13,'TAURUS');
insert into cad_modelos values(232,13,'THUNDERBIRD');
insert into cad_modelos values(233,13,'TRANSIT');
insert into cad_modelos values(234,13,'VERONA');
insert into cad_modelos values(235,13,'VERSAILLES');
insert into cad_modelos values(236,13,'WINDSTAR');
insert into cad_modelos values(238,1,'A-10');
insert into cad_modelos values(239,1,'A-20');
insert into cad_modelos values(240,1,'AGILE');
insert into cad_modelos values(241,1,'ASTRA');
insert into cad_modelos values(242,1,'BLAZER');
insert into cad_modelos values(243,1,'BONANZA');
insert into cad_modelos values(245,1,'C-10');
insert into cad_modelos values(246,1,'C-20');
insert into cad_modelos values(247,1,'CALIBRA');
insert into cad_modelos values(248,1,'CAMARO');
insert into cad_modelos values(249,1,'CAPRICE');
insert into cad_modelos values(250,1,'CAPTIVA');
insert into cad_modelos values(251,1,'CARAVAN');
insert into cad_modelos values(252,1,'CAVALIER');
insert into cad_modelos values(253,1,'CELTA');
insert into cad_modelos values(255,1,'CHEVY');
insert into cad_modelos values(256,1,'CHEYNNE');
insert into cad_modelos values(258,1,'COBALT');
insert into cad_modelos values(259,1,'CORSA');
insert into cad_modelos values(262,1,'CORVETTE');
insert into cad_modelos values(263,1,'CRUZE');
insert into cad_modelos values(264,1,'D-10');
insert into cad_modelos values(265,1,'D-20');
insert into cad_modelos values(266,1,'IPANEMA');
insert into cad_modelos values(267,1,'KADETT');
insert into cad_modelos values(268,1,'LUMINA');
insert into cad_modelos values(269,1,'MALIBU');
insert into cad_modelos values(271,1,'MERIVA');
insert into cad_modelos values(272,1,'MONTANA');
insert into cad_modelos values(274,1,'OMEGA');
insert into cad_modelos values(275,1,'OPALA');
insert into cad_modelos values(276,1,'PRISMA');
insert into cad_modelos values(277,1,'S10');
insert into cad_modelos values(280,1,'SILVERADO');
insert into cad_modelos values(281,1,'SONIC');
insert into cad_modelos values(282,1,'SONOMA');
insert into cad_modelos values(283,1,'SPACEVAN');
insert into cad_modelos values(284,1,'SS10');
insert into cad_modelos values(285,1,'SUBURBAN');
insert into cad_modelos values(287,1,'SYCLONE');
insert into cad_modelos values(288,1,'TIGRA');
insert into cad_modelos values(289,1,'TRACKER');
insert into cad_modelos values(290,1,'TRAFIC');
insert into cad_modelos values(291,1,'VECTRA');
insert into cad_modelos values(292,1,'VERANEIO');
insert into cad_modelos values(293,1,'ZAFIRA');
insert into cad_modelos values(294,75,'HOVER');
insert into cad_modelos values(295,17,'BR-800');
insert into cad_modelos values(296,17,'CARAJAS');
insert into cad_modelos values(297,17,'TOCANTINS');
insert into cad_modelos values(298,17,'XAVANTE');
insert into cad_modelos values(299,17,'VIP');
insert into cad_modelos values(300,76,'TOWNER');
insert into cad_modelos values(301,7,'ACCORD');
insert into cad_modelos values(302,7,'CITY');
insert into cad_modelos values(303,7,'CIVIC');
insert into cad_modelos values(304,7,'CR-V');
insert into cad_modelos values(305,7,'FIT');
insert into cad_modelos values(306,7,'ODYSSEY');
insert into cad_modelos values(307,7,'PASSPORT');
insert into cad_modelos values(308,7,'PRELUDE');
insert into cad_modelos values(309,14,'ACCENT');
insert into cad_modelos values(310,14,'ATOS');
insert into cad_modelos values(311,14,'AZERA');
insert into cad_modelos values(312,14,'COUPE');
insert into cad_modelos values(314,14,'ELANTRA');
insert into cad_modelos values(315,14,'EXCEL');
insert into cad_modelos values(316,14,'GALLOPER');
insert into cad_modelos values(317,14,'GENESIS');
insert into cad_modelos values(318,14,'H1');
insert into cad_modelos values(319,14,'H100');
insert into cad_modelos values(321,14,'I30');
insert into cad_modelos values(323,14,'IX35');
insert into cad_modelos values(324,14,'MATRIX');
insert into cad_modelos values(325,14,'PORTER');
insert into cad_modelos values(326,14,'SANTA FE');
insert into cad_modelos values(327,14,'SCOUPE');
insert into cad_modelos values(328,14,'SONATA');
insert into cad_modelos values(329,14,'TERRACAN');
insert into cad_modelos values(330,14,'TRAJET');
insert into cad_modelos values(331,14,'TUCSON');
insert into cad_modelos values(332,14,'VELOSTER');
insert into cad_modelos values(333,14,'VERACRUZ');
/*insert into cad_modelos values(334,84,'AMIGO');
insert into cad_modelos values(335,84,'HOMBRE');
insert into cad_modelos values(336,84,'RODEO');*/
insert into cad_modelos values(337,15,'J3');
insert into cad_modelos values(338,15,'J5');
insert into cad_modelos values(339,15,'J6');
insert into cad_modelos values(340,86,'DAIMLER');
insert into cad_modelos values(341,86,'S-TYPE');
insert into cad_modelos values(342,86,'X-TYPE');
insert into cad_modelos values(345,86,'MODELOS XJ');
insert into cad_modelos values(352,86,'MODELOS XK');
insert into cad_modelos values(354,87,'CHEROKEE');
insert into cad_modelos values(355,87,'COMMANDER');
insert into cad_modelos values(356,87,'COMPASS');
insert into cad_modelos values(357,87,'GRAND CHEROKEE');
insert into cad_modelos values(358,87,'WRANGLER');
insert into cad_modelos values(359,88,'TOPIC VAN');
insert into cad_modelos values(360,89,'JIPE MONTEZ');
insert into cad_modelos values(361,89,'PICAPE MONTEZ');
insert into cad_modelos values(362,16,'BESTA');
insert into cad_modelos values(363,16,'BONGO');
insert into cad_modelos values(364,16,'CADENZA');
insert into cad_modelos values(365,16,'CARENS');
insert into cad_modelos values(366,16,'CARNIVAL');
insert into cad_modelos values(367,16,'CERATO');
insert into cad_modelos values(368,16,'CERES');
insert into cad_modelos values(369,16,'CLARUS');
insert into cad_modelos values(370,16,'MAGENTIS');
insert into cad_modelos values(371,16,'MOHAVE');
insert into cad_modelos values(372,16,'OPIRUS');
insert into cad_modelos values(373,16,'OPTIMA');
insert into cad_modelos values(374,16,'PICANTO');
insert into cad_modelos values(375,16,'SEPHIA');
insert into cad_modelos values(376,16,'SHUMA');
insert into cad_modelos values(377,16,'SORENTO');
insert into cad_modelos values(378,16,'SOUL');
insert into cad_modelos values(379,16,'SPORTAGE');
insert into cad_modelos values(380,94,'LAIKA');
insert into cad_modelos values(381,94,'NIVA');
insert into cad_modelos values(382,94,'SAMARA');
insert into cad_modelos values(383,12,'GALLARDO');
insert into cad_modelos values(384,12,'MURCIELAGO');
insert into cad_modelos values(385,96,'DEFENDER');
insert into cad_modelos values(386,96,'DISCOVERY');
insert into cad_modelos values(389,96,'FREELANDER');
insert into cad_modelos values(391,96,'NEW RANGE');
insert into cad_modelos values(392,96,'RANGE ROVER');
insert into cad_modelos values(393,97,'ES');
insert into cad_modelos values(396,97,'GS');
insert into cad_modelos values(397,97,'IS-300');
insert into cad_modelos values(398,97,'LS');
insert into cad_modelos values(400,97,'RX');
insert into cad_modelos values(402,97,'SC');
insert into cad_modelos values(403,98,'320');
insert into cad_modelos values(404,98,'620');
insert into cad_modelos values(405,100,'H1');
insert into cad_modelos values(406,101,'ELAN');
insert into cad_modelos values(407,101,'ESPRIT');
insert into cad_modelos values(408,102,'SCORPIO');
insert into cad_modelos values(409,104,'222');
insert into cad_modelos values(410,104,'228');
insert into cad_modelos values(411,104,'3200');
insert into cad_modelos values(412,104,'430');
insert into cad_modelos values(413,104,'COUPE');
insert into cad_modelos values(414,104,'GHIBLI');
insert into cad_modelos values(415,104,'GRANCABRIO');
insert into cad_modelos values(416,104,'GRANSPORT');
insert into cad_modelos values(417,104,'GRANTURISMO');
insert into cad_modelos values(418,104,'QUATTROPORTE');
insert into cad_modelos values(419,104,'SHAMAL');
insert into cad_modelos values(420,104,'SPIDER');
insert into cad_modelos values(422,106,'PICK-UP');
insert into cad_modelos values(423,108,'323');
insert into cad_modelos values(424,108,'626');
insert into cad_modelos values(425,108,'929');
insert into cad_modelos values(426,108,'B-2500');
insert into cad_modelos values(427,108,'B2200');
insert into cad_modelos values(428,108,'MILLENIA');
insert into cad_modelos values(429,108,'MPV');
insert into cad_modelos values(430,108,'MX-3');
insert into cad_modelos values(431,108,'MX-5');
insert into cad_modelos values(432,108,'NAVAJO');
insert into cad_modelos values(433,108,'PROTEGE');
insert into cad_modelos values(434,108,'RX');
insert into cad_modelos values(467,4,'CLASSE A');
insert into cad_modelos values(468,4,'CLASSE B');
insert into cad_modelos values(469,4,'CLASSE R');
insert into cad_modelos values(498,4,'CLASSE GLK');
insert into cad_modelos values(531,4,'SPRINTER');
insert into cad_modelos values(532,110,'MYSTIQUE');
insert into cad_modelos values(533,110,'SABLE');
insert into cad_modelos values(534,112,'550');
insert into cad_modelos values(535,112,'MG6');
insert into cad_modelos values(536,113,'COOPER');
insert into cad_modelos values(537,113,'ONE');
insert into cad_modelos values(538,111,'3000');
insert into cad_modelos values(539,111,'AIRTREK');
insert into cad_modelos values(540,111,'ASX');
insert into cad_modelos values(541,111,'COLT');
insert into cad_modelos values(542,111,'DIAMANT');
insert into cad_modelos values(543,111,'ECLIPSE');
insert into cad_modelos values(544,111,'EXPO');
insert into cad_modelos values(545,111,'GALANT');
insert into cad_modelos values(546,111,'GRANDIS');
insert into cad_modelos values(547,111,'L200');
insert into cad_modelos values(548,111,'L300');
insert into cad_modelos values(549,111,'LANCER');
insert into cad_modelos values(550,111,'MIRAGE');
insert into cad_modelos values(551,111,'MONTERO');
insert into cad_modelos values(552,111,'OUTLANDER');
insert into cad_modelos values(553,111,'PAJERO');
insert into cad_modelos values(554,111,'SPACE WAGON');
insert into cad_modelos values(555,114,'BG-TRUCK');
insert into cad_modelos values(556,43,'350Z');
insert into cad_modelos values(557,43,'ALTIMA');
insert into cad_modelos values(558,43,'AX');
insert into cad_modelos values(559,43,'D-21');
insert into cad_modelos values(560,43,'FRONTIER');
insert into cad_modelos values(562,43,'KING-CAB');
insert into cad_modelos values(563,43,'LIVINA');
insert into cad_modelos values(564,43,'MARCH');
insert into cad_modelos values(565,43,'MAXIMA');
insert into cad_modelos values(567,43,'MURANO');
insert into cad_modelos values(568,43,'NX');
insert into cad_modelos values(569,43,'PATHFINDER');
insert into cad_modelos values(571,43,'PRIMERA');
insert into cad_modelos values(572,43,'QUEST');
insert into cad_modelos values(573,43,'SENTRA');
insert into cad_modelos values(574,43,'STANZA');
insert into cad_modelos values(575,43,'180SX');
insert into cad_modelos values(576,43,'TERRANO');
insert into cad_modelos values(577,43,'TIIDA');
insert into cad_modelos values(578,43,'VERSA');
insert into cad_modelos values(579,43,'X-TRAIL');
insert into cad_modelos values(580,43,'XTERRA');
insert into cad_modelos values(581,43,'ZX');
insert into cad_modelos values(582,22,'106');
insert into cad_modelos values(583,22,'205');
insert into cad_modelos values(584,22,'206');
insert into cad_modelos values(585,22,'207');
insert into cad_modelos values(586,22,'3008');
insert into cad_modelos values(587,22,'306');
insert into cad_modelos values(588,22,'307');
insert into cad_modelos values(589,22,'308');
insert into cad_modelos values(590,22,'405');
insert into cad_modelos values(591,22,'406');
insert into cad_modelos values(592,22,'407');
insert into cad_modelos values(593,22,'408');
insert into cad_modelos values(594,22,'504');
insert into cad_modelos values(595,22,'505');
insert into cad_modelos values(596,22,'508');
insert into cad_modelos values(597,22,'605');
insert into cad_modelos values(598,22,'607');
insert into cad_modelos values(599,22,'806');
insert into cad_modelos values(600,22,'807');
insert into cad_modelos values(601,22,'BOXER');
insert into cad_modelos values(602,22,'HOGGAR');
insert into cad_modelos values(603,22,'PARTNER');
insert into cad_modelos values(604,22,'RCZ');
insert into cad_modelos values(605,123,'GRAN VOYAGER');
insert into cad_modelos values(606,123,'SUNDANCE');
insert into cad_modelos values(607,69,'TRANS-AM');
insert into cad_modelos values(608,69,'TRANS-SPORT');
insert into cad_modelos values(609,70,'911');
insert into cad_modelos values(612,70,'BOXSTER');
insert into cad_modelos values(613,70,'CAYENNE');
insert into cad_modelos values(614,70,'CAYMAN');
insert into cad_modelos values(615,70,'PANAMERA');
insert into cad_modelos values(617,24,'21 SEDAN');
insert into cad_modelos values(618,24,'CLIO');
insert into cad_modelos values(619,24,'DUSTER');
insert into cad_modelos values(620,24,'EXPRESS');
insert into cad_modelos values(621,24,'FLUENCE');
insert into cad_modelos values(622,24,'KANGOO');
insert into cad_modelos values(623,24,'LAGUNA');
insert into cad_modelos values(624,24,'LOGAN');
insert into cad_modelos values(625,24,'MASTER');
insert into cad_modelos values(626,24,'MEGANE');
insert into cad_modelos values(627,24,'SAFRANE');
insert into cad_modelos values(628,24,'SANDERO');
insert into cad_modelos values(629,24,'SCENIC');
insert into cad_modelos values(630,24,'SYMBOL');
insert into cad_modelos values(631,24,'TRAFIC');
insert into cad_modelos values(632,24,'TWINGO');
insert into cad_modelos values(634,137,'9000');
insert into cad_modelos values(635,162,'SL-2');
insert into cad_modelos values(636,130,'CORDOBA');
insert into cad_modelos values(637,130,'IBIZA');
insert into cad_modelos values(638,130,'INCA');
insert into cad_modelos values(641,142,'FORTWO');
insert into cad_modelos values(642,21,'ACTYON SPORTS');
insert into cad_modelos values(643,21,'CHAIRMAN');
insert into cad_modelos values(644,21,'ISTANA');
insert into cad_modelos values(645,21,'KORANDO');
insert into cad_modelos values(646,21,'KYRON');
insert into cad_modelos values(647,21,'MUSSO');
insert into cad_modelos values(648,21,'REXTON');
insert into cad_modelos values(649,8,'FORESTER');
insert into cad_modelos values(650,8,'IMPREZA');
insert into cad_modelos values(651,8,'LEGACY');
insert into cad_modelos values(652,8,'OUTBACK');
insert into cad_modelos values(653,8,'SVX');
insert into cad_modelos values(654,8,'TRIBECA');
insert into cad_modelos values(655,8,'VIVIO');
insert into cad_modelos values(656,59,'BALENO');
insert into cad_modelos values(657,59,'GRAND VITARA');
insert into cad_modelos values(658,59,'IGNIS');
insert into cad_modelos values(660,59,'JIMNY');
insert into cad_modelos values(662,59,'SUPER CARRY');
insert into cad_modelos values(663,59,'SWIFT');
insert into cad_modelos values(664,59,'SX4');
insert into cad_modelos values(665,59,'VITARA');
insert into cad_modelos values(666,59,'WAGON R');
insert into cad_modelos values(667,146,'STARK');
insert into cad_modelos values(668,23,'AVALON');
insert into cad_modelos values(669,23,'BANDEIRANTE');
insert into cad_modelos values(670,23,'CAMRY');
insert into cad_modelos values(671,23,'CELICA');
insert into cad_modelos values(672,23,'COROLLA');
insert into cad_modelos values(673,23,'CORONA');
insert into cad_modelos values(674,23,'HILUX');
insert into cad_modelos values(675,23,'LAND CRUISER');
insert into cad_modelos values(676,23,'MR-2');
insert into cad_modelos values(677,23,'PASEO');
insert into cad_modelos values(678,23,'PREVIA');
insert into cad_modelos values(679,23,'RAV4');
insert into cad_modelos values(680,23,'SUPRA');
insert into cad_modelos values(682,149,'PANTANAL');
insert into cad_modelos values(684,149,'T-4');
insert into cad_modelos values(685,53,'400 SERIES');
insert into cad_modelos values(687,53,'850');
insert into cad_modelos values(688,53,'900 SERIES');
insert into cad_modelos values(700,2,'AMAROK');
insert into cad_modelos values(701,2,'APOLLO');
insert into cad_modelos values(702,2,'BORA');
insert into cad_modelos values(703,2,'CARAVELLE');
insert into cad_modelos values(704,2,'CORRADO');
insert into cad_modelos values(706,2,'EOS');
insert into cad_modelos values(707,2,'EUROVAN');
insert into cad_modelos values(708,2,'FOX');
insert into cad_modelos values(709,2,'FUSCA');
insert into cad_modelos values(710,2,'GOL');
insert into cad_modelos values(711,2,'GOLF');
insert into cad_modelos values(713,2,'JETTA');
insert into cad_modelos values(714,2,'KOMBI');
insert into cad_modelos values(715,2,'LOGUS');
insert into cad_modelos values(717,2,'PARATI');
insert into cad_modelos values(718,2,'PASSAT');
insert into cad_modelos values(719,2,'POINTER');
insert into cad_modelos values(720,2,'POLO');
insert into cad_modelos values(722,2,'SANTANA');
insert into cad_modelos values(723,2,'SAVEIRO');
insert into cad_modelos values(725,2,'SPACEFOX');
insert into cad_modelos values(726,2,'TIGUAN');
insert into cad_modelos values(727,2,'TOUAREG');
insert into cad_modelos values(729,2,'VOYAGE');
insert into cad_modelos values(732,25,'ZDX');
insert into cad_modelos values(737,3,'140');
insert into cad_modelos values(755,2,'BRASILIA');
insert into cad_modelos values(756,13,'BRASILVAN');
insert into cad_modelos values(775,13,'CORCEL');
insert into cad_modelos values(803,3,'PALIO WEEKEND');
insert into cad_modelos values(806,13,'FOCUS SEDAN');
insert into cad_modelos values(807,13,'FIESTA SEDAN');
insert into cad_modelos values(808,13,'FIESTA TRAIL');
insert into cad_modelos values(810,22,'207 SW');
insert into cad_modelos values(811,13,'ESCORT SW');
insert into cad_modelos values(812,22,'307 SEDAN');
insert into cad_modelos values(813,22,'307 SW');
insert into cad_modelos values(815,5,'C4 PALLAS');
insert into cad_modelos values(816,5,'C4 PICASSO');
insert into cad_modelos values(817,5,'C4 VTR');
insert into cad_modelos values(818,24,'CLIO SEDAN');
insert into cad_modelos values(819,23,'COROLLA FIELDER');
insert into cad_modelos values(824,23,'HILUX SW4');
insert into cad_modelos values(825,24,'MEGANE GRAND TOUR');
insert into cad_modelos values(827,24,'SANDERO STEPWAY');
insert into cad_modelos values(829,5,'XSARA PICASSO');
insert into cad_modelos values(1360,131,'COLHEITADEIRA');
insert into cad_modelos values(1361,158,'PICKUP F75');
insert into cad_modelos values(1362,17,'X12');
insert into cad_modelos values(1365,1,'BEL AIR');
insert into cad_modelos values(1366,36,'RX');
insert into cad_modelos values(1369,1,'C-14');
insert into cad_modelos values(1370,155,'SRX4');
insert into cad_modelos values(1372,1,'C-15');
insert into cad_modelos values(1373,1,'BRASIL');
insert into cad_modelos values(1377,18,'POLARA');
insert into cad_modelos values(1380,3,'600');
insert into cad_modelos values(1382,13,'F-01');
insert into cad_modelos values(1383,13,'FALCON');
insert into cad_modelos values(1384,13,'GALAXIE');
insert into cad_modelos values(1386,13,'MAVERICK');
insert into cad_modelos values(1387,13,'MODELO A');
insert into cad_modelos values(1388,13,'NEW FIESTA');
insert into cad_modelos values(1389,82,'LINHA FX');
insert into cad_modelos values(1391,124,'GTS');
insert into cad_modelos values(1392,80,'H3');
insert into cad_modelos values(1394,14,'PRIME');
insert into cad_modelos values(1395,14,'TIBURON');
insert into cad_modelos values(1397,87,'JEEP');
insert into cad_modelos values(1398,87,'CJ5');
insert into cad_modelos values(1399,239,'TC');
insert into cad_modelos values(1404,4,'CLASSE CLC');
insert into cad_modelos values(1405,4,'CLASSE CLS');
insert into cad_modelos values(1408,110,'MONTEREY');
insert into cad_modelos values(1411,114,'TOPSPORT');
insert into cad_modelos values(1412,114,'TARGA');
insert into cad_modelos values(1414,114,'X8');
insert into cad_modelos values(1415,43,'370Z');
insert into cad_modelos values(1418,124,'GTB');
insert into cad_modelos values(1419,124,'GTC');
insert into cad_modelos values(1420,124,'GTE');
insert into cad_modelos values(1421,115,'AUSTIN');
insert into cad_modelos values(1423,24,'7TL');
insert into cad_modelos values(1424,24,'19');
insert into cad_modelos values(1426,175,'CONVERSÍVEL');
insert into cad_modelos values(1427,17,'SUPERMINI');
insert into cad_modelos values(1428,2,'TL');
insert into cad_modelos values(1429,3,'TOPOLINO');
insert into cad_modelos values(1430,23,'SR5');
insert into cad_modelos values(1431,23,'VITZ');
insert into cad_modelos values(1432,2,'VARIANT');
insert into cad_modelos values(1454,57,'CANDANGO');
insert into cad_modelos values(1460,2,'SP2');
insert into cad_modelos values(1466,258,'RECORB');
insert into cad_modelos values(1467,2,'POLAUTO');
insert into cad_modelos values(1508,24,'GORDINI');
insert into cad_modelos values(1509,265,'MINX');
insert into cad_modelos values(1971,23,'ETIOS');
insert into cad_modelos values(1972,1,'ONIX');
insert into cad_modelos values(1973,14,'HB20');
insert into cad_modelos values(1975,36,'330');
insert into cad_modelos values(1976,36,'520');
insert into cad_modelos values(1978,36,'730');
insert into cad_modelos values(1980,36,'M1');
insert into cad_modelos values(1982,36,'SERIE Z');
insert into cad_modelos values(1983,4,'CLASSE SLK');
insert into cad_modelos values(1984,4,'CLASSE C');
insert into cad_modelos values(1985,4,'CLASSE E');
insert into cad_modelos values(1986,4,'CLASSE CL');
insert into cad_modelos values(1987,4,'CLASSE CLK');
insert into cad_modelos values(1988,4,'CLASSE S');
insert into cad_modelos values(1989,4,'CLASSE SL');
insert into cad_modelos values(1990,4,'CLASSE SLS');
insert into cad_modelos values(1991,4,'CLASSE G');
insert into cad_modelos values(1992,4,'CLASSE GL');
insert into cad_modelos values(1993,4,'CLASSE M');
insert into cad_modelos values(2032,288,'1500');
insert into cad_modelos values(2061,14,'EQUUS');
insert into cad_modelos values(2067,12,'350 GT');
insert into cad_modelos values(2068,12,'400 GT');
insert into cad_modelos values(2069,12,'MIURA');
insert into cad_modelos values(2070,12,'ISLERO');
insert into cad_modelos values(2071,12,'ESPADA');
insert into cad_modelos values(2072,12,'COUNTACH');
insert into cad_modelos values(2073,12,'DIABLO');
insert into cad_modelos values(2074,12,'ZAGATO');
insert into cad_modelos values(2075,12,'ALAR');
insert into cad_modelos values(2076,12,'LM002');
insert into cad_modelos values(2077,12,'REVENTON');
insert into cad_modelos values(2078,12,'ANKONIAN');
insert into cad_modelos values(2080,12,'AVENTADOR');
insert into cad_modelos values(2081,12,'SESTO ELEMENTO');
insert into cad_modelos values(2082,15,'J3 TURIN');
insert into cad_modelos values(2083,15,'J2');
insert into cad_modelos values(2084,24,'SANDERO GT');
insert into cad_modelos values(2087,1,'SPIN');
insert into cad_modelos values(2088,1,'TRAILBLAZER');
insert into cad_modelos values(2097,5,'C3 PICASSO');
insert into cad_modelos values(2098,5,'GRAND C4 PICASSO');
insert into cad_modelos values(2099,5,'JUMPER MINIBUS');
insert into cad_modelos values(2100,5,'JUMPER VETRATO');
insert into cad_modelos values(2101,22,'207 SEDAN');
insert into cad_modelos values(2102,22,'207 QUIKSILVER');
insert into cad_modelos values(2103,22,'207 ESCAPADE');
insert into cad_modelos values(2104,22,'308 CC');
insert into cad_modelos values(2105,22,'BOXER PASSAGEIRO');
insert into cad_modelos values(2106,13,'NEW FIESTA SEDAN');
insert into cad_modelos values(2108,13,'TRANSIT PASSAGEIRO');
insert into cad_modelos values(2109,13,'TRANSIT CHASSI');
insert into cad_modelos values(2110,32,'A4 AVANT');
insert into cad_modelos values(2111,32,'S4 AVANT');
insert into cad_modelos values(2112,32,'A5 SPORTBACK');
insert into cad_modelos values(2113,32,'A5 CABRIOLET');
insert into cad_modelos values(2114,32,'S5 COUPE');
insert into cad_modelos values(2115,32,'S5 SPORTBACK');
insert into cad_modelos values(2116,32,'S5 CABRIOLET');
insert into cad_modelos values(2117,32,'A6 AVANT');
insert into cad_modelos values(2118,32,'A6 ALLROAD');
insert into cad_modelos values(2119,32,'S6 AVANT');
insert into cad_modelos values(2120,32,'S7');
insert into cad_modelos values(2121,32,'TT ROADSTER');
insert into cad_modelos values(2122,32,'TT RS');
insert into cad_modelos values(2123,32,'TT RS ROADSTER');
insert into cad_modelos values(2124,32,'TTS ROADSTER');
insert into cad_modelos values(2125,32,'R8 SPYDER');
insert into cad_modelos values(2126,32,'R8 GT');
insert into cad_modelos values(2127,32,'R8 GT SPYDER');
insert into cad_modelos values(2129,10,'F12');
insert into cad_modelos values(2130,10,'458 SPIDER');
insert into cad_modelos values(2131,10,'458 ITALIA');
insert into cad_modelos values(2132,10,'FF');
insert into cad_modelos values(2133,10,'599');
insert into cad_modelos values(2134,10,'SA');
insert into cad_modelos values(2135,10,'CHALLENGE');
insert into cad_modelos values(2136,10,'SUPERAMERICA');
insert into cad_modelos values(2137,10,'F430 SPIDER');
insert into cad_modelos values(2138,10,'430');
insert into cad_modelos values(2139,10,'612 SESSANTA');
insert into cad_modelos values(2140,10,'599 GTB');
insert into cad_modelos values(2141,10,'SCUDERIA SPIDER');
insert into cad_modelos values(2142,10,'512');
insert into cad_modelos values(2143,10,'456 GT');
insert into cad_modelos values(2144,10,'348 GTS');
insert into cad_modelos values(2145,10,'348 SPIDER');
insert into cad_modelos values(2146,10,'F355');
insert into cad_modelos values(2147,10,'F355 SPIDER');
insert into cad_modelos values(2148,10,'F50');
insert into cad_modelos values(2149,10,'355 SPIDER');
insert into cad_modelos values(2150,10,'360 MODENA');
insert into cad_modelos values(2151,111,'PAJERO FULL');
insert into cad_modelos values(2152,111,'PAJERO DAKAR');
insert into cad_modelos values(2153,111,'PAJERO TR4');
insert into cad_modelos values(2154,111,'LANCER SPORTBACK');
insert into cad_modelos values(2155,111,'LANCER EVOLUTION');
insert into cad_modelos values(2156,111,'L200 TRITON SAVANA');
insert into cad_modelos values(2157,111,'L200 TRITON');
insert into cad_modelos values(2159,43,'LIVINA X-GEAR');
insert into cad_modelos values(2160,43,'GRAND LIVINA');
insert into cad_modelos values(2161,21,'NEW ACTYON SPORTS');
insert into cad_modelos values(2162,23,'PRIUS');
insert into cad_modelos values(2163,114,'SPORT');
insert into cad_modelos values(2164,114,'MTS');
insert into cad_modelos values(2165,114,'SPIDER');
insert into cad_modelos values(2166,114,'KABRIO');
insert into cad_modelos values(2167,114,'SAGA');
insert into cad_modelos values(2168,114,'SAGA II');
insert into cad_modelos values(2169,114,'787');
insert into cad_modelos values(2170,114,'X11');
insert into cad_modelos values(2171,295,'GAIOLA');
insert into cad_modelos values(2175,18,'NITRO');
insert into cad_modelos values(2176,18,'CHALLENGER');
insert into cad_modelos values(2177,18,'DART');
insert into cad_modelos values(2178,18,'LE BARON');
insert into cad_modelos values(2179,18,'CORDOBA');
insert into cad_modelos values(2180,18,'CHARGER');
insert into cad_modelos values(2181,19,'WINDSOR');
insert into cad_modelos values(2183,19,'CROSSFIRE');
insert into cad_modelos values(2184,19,'CORDOBA');
insert into cad_modelos values(2185,155,'ESCALADE');
insert into cad_modelos values(2186,41,'RIVIERA');
insert into cad_modelos values(2187,41,'COUPE');
insert into cad_modelos values(2188,41,'CENTURY');
insert into cad_modelos values(2189,41,'APOLLO');
insert into cad_modelos values(2190,41,'CENTURION');
insert into cad_modelos values(2191,41,'EIGHT');
insert into cad_modelos values(2192,41,'ELECTRA');
insert into cad_modelos values(2193,41,'ESTATE WAGON');
insert into cad_modelos values(2194,41,'GRAN SPORT');
insert into cad_modelos values(2195,41,'GSX');
insert into cad_modelos values(2196,41,'INVICTA');
insert into cad_modelos values(2197,41,'LESABRE');
insert into cad_modelos values(2198,41,'LIMITED');
insert into cad_modelos values(2199,41,'PARK AVENUE');
insert into cad_modelos values(2200,41,'RAINIER');
insert into cad_modelos values(2201,41,'REATTA');
insert into cad_modelos values(2202,41,'REGAL');
insert into cad_modelos values(2203,41,'RENDEZVOUS');
insert into cad_modelos values(2204,41,'ROADMASTER');
insert into cad_modelos values(2205,41,'ROYAUM');
insert into cad_modelos values(2206,41,'SKYHAWK');
insert into cad_modelos values(2207,41,'SKYLARK');
insert into cad_modelos values(2208,41,'SOMERSET');
insert into cad_modelos values(2209,41,'SPECIAL');
insert into cad_modelos values(2210,41,'SPORT WAGON');
insert into cad_modelos values(2211,41,'SUPER');
insert into cad_modelos values(2212,41,'TERRAZA');
insert into cad_modelos values(2213,41,'WILDCAT');
insert into cad_modelos values(2214,41,'LACROSSE');
insert into cad_modelos values(2215,41,'ENCLAVE');
insert into cad_modelos values(2217,41,'GL8');
insert into cad_modelos values(2218,41,'HRV');
insert into cad_modelos values(2219,41,'LUCERNE');
insert into cad_modelos values(2230,13,'SIERRA');
insert into cad_modelos values(2231,51,'BROUGHAM');
insert into cad_modelos values(2232,51,'CHAIRMAN');
insert into cad_modelos values(2233,51,'DAMAS');
insert into cad_modelos values(2234,51,'GENTRA');
insert into cad_modelos values(2235,51,'MAEPSY');
insert into cad_modelos values(2236,51,'ISTANA');
insert into cad_modelos values(2237,51,'KALOS');
insert into cad_modelos values(2238,51,'KORANDO');
insert into cad_modelos values(2239,51,'LACETTI');
insert into cad_modelos values(2240,51,'LEMANS');
insert into cad_modelos values(2242,51,'MATIZ');
insert into cad_modelos values(2243,51,'MUSSO');
insert into cad_modelos values(2244,51,'NEXIA');
insert into cad_modelos values(2245,51,'REZZO');
insert into cad_modelos values(2246,51,'ROYALE PRINCE');
insert into cad_modelos values(2247,51,'ROYALE SALON');
insert into cad_modelos values(2248,51,'STATESMAN');
insert into cad_modelos values(2249,51,'TOSCA');
insert into cad_modelos values(2250,51,'WINSTORM');
insert into cad_modelos values(2252,158,'RURAL');
insert into cad_modelos values(2253,18,'D100');
insert into cad_modelos values(2259,4,'170');
insert into cad_modelos values(2261,18,'CUSTOM ROYAL');
insert into cad_modelos values(2262,1,'CLUB COUPE');
insert into cad_modelos values(2263,18,'MAGNUM');
insert into cad_modelos values(2264,1,'GMC 100');
insert into cad_modelos values(2265,69,'SOLSTICE');
insert into cad_modelos values(2266,158,'ITAMARATY');
insert into cad_modelos values(2267,86,'MARK V');
insert into cad_modelos values(2268,124,'GT');
insert into cad_modelos values(2269,145,'CHAMPION');
insert into cad_modelos values(2270,3,'BALILLA');
insert into cad_modelos values(2271,158,'INTERLAGOS');
insert into cad_modelos values(2272,17,'X15');
insert into cad_modelos values(2273,13,'F-85');
insert into cad_modelos values(2274,70,'SPEEDSTER 356');
insert into cad_modelos values(2275,88,'TOPIC FURGAO');
insert into cad_modelos values(2276,88,'TOPIC ESCOLAR');
insert into cad_modelos values(2279,4,'300D');
insert into cad_modelos values(2280,4,'CLASSE TE');
insert into cad_modelos values(2283,23,'T-100');
insert into cad_modelos values(2294,24,'MEGANE SEDAN');
insert into cad_modelos values(2295,32,'A4 CABRIOLET');
insert into cad_modelos values(2298,82,'LINHA G');
insert into cad_modelos values(2299,82,'LINHA G COUPE');
insert into cad_modelos values(2300,82,'LINHA G CONVERSIVEL');
insert into cad_modelos values(2301,82,'LINHA M');
insert into cad_modelos values(2302,82,'LINHA EX');
insert into cad_modelos values(2303,82,'LINHA JX');
insert into cad_modelos values(2304,82,'LINHA QX');
insert into cad_modelos values(2305,86,'MODELOS XF');
insert into cad_modelos values(2306,86,'F-TYPE');
insert into cad_modelos values(2307,86,'MARK VII');
insert into cad_modelos values(2308,86,'MARK VIII');
insert into cad_modelos values(2309,86,'MARK IX');
insert into cad_modelos values(2310,86,'MARK X');
insert into cad_modelos values(2311,86,'E-TYPE');
insert into cad_modelos values(2312,86,'C-TYPE');
insert into cad_modelos values(2313,86,'D-TYPE');
insert into cad_modelos values(2314,86,'MARK I');
insert into cad_modelos values(2315,86,'MARK II');
insert into cad_modelos values(2346,124,'GT4R');
insert into cad_modelos values(2347,124,'SPYDER');
insert into cad_modelos values(2348,124,'GTI');
insert into cad_modelos values(2349,124,'AM1');
insert into cad_modelos values(2350,124,'AM2');
insert into cad_modelos values(2351,124,'AM3');
insert into cad_modelos values(2352,124,'AM4');
insert into cad_modelos values(2353,124,'AMV');
insert into cad_modelos values(2377,7,'ACTY');
insert into cad_modelos values(2378,7,'AIRWAVE');
insert into cad_modelos values(2379,7,'ASCOT');
insert into cad_modelos values(2380,7,'BALLADE');
insert into cad_modelos values(2381,7,'BEAT');
insert into cad_modelos values(2382,7,'CR-X');
insert into cad_modelos values(2383,7,'CONCERTO');
insert into cad_modelos values(2384,7,'CR-Z');
insert into cad_modelos values(2385,7,'DOMANI');
insert into cad_modelos values(2386,7,'EDIX');
insert into cad_modelos values(2387,7,'ELEMENT');
insert into cad_modelos values(2388,7,'EV PLUS');
insert into cad_modelos values(2389,7,'FCX');
insert into cad_modelos values(2390,7,'FR-V');
insert into cad_modelos values(2392,7,'HR-V');
insert into cad_modelos values(2393,7,'HSC');
insert into cad_modelos values(2394,7,'INSIGHT');
insert into cad_modelos values(2396,25,'TL');
insert into cad_modelos values(2397,7,'LIFE DUNK');
insert into cad_modelos values(2398,7,'LOGO');
insert into cad_modelos values(2399,7,'MOBILIO');
insert into cad_modelos values(2400,25,'MDX');
insert into cad_modelos values(2401,7,'ORTHIA');
insert into cad_modelos values(2402,7,'PARTNER VAN');
insert into cad_modelos values(2403,7,'PILOT');
insert into cad_modelos values(2404,7,'RIDGELINE');
insert into cad_modelos values(2405,7,'S2000');
insert into cad_modelos values(2406,7,'S600');
insert into cad_modelos values(2407,7,'S500');
insert into cad_modelos values(2408,7,'S800');
insert into cad_modelos values(2409,7,'STEPWGN');
insert into cad_modelos values(2410,7,'STREAM');
insert into cad_modelos values(2411,7,'THATS');
insert into cad_modelos values(2412,7,'VAMOZ');
insert into cad_modelos values(2413,7,'Z');
insert into cad_modelos values(2414,7,'ZEST');
insert into cad_modelos values(2441,59,'AERIO');
insert into cad_modelos values(2442,59,'ALTO');
insert into cad_modelos values(2443,59,'APV');
insert into cad_modelos values(2444,59,'KEI');
insert into cad_modelos values(2445,59,'LAPIN');
insert into cad_modelos values(2446,59,'MR WAGON');
insert into cad_modelos values(2447,59,'XL-7');
insert into cad_modelos values(2448,59,'VERONA');
insert into cad_modelos values(2477,158,'JEEP CJ');
insert into cad_modelos values(2479,22,'306 CABRIOLET');
insert into cad_modelos values(2484,57,'BELCAR');
insert into cad_modelos values(2485,90,'M715');
insert into cad_modelos values(2492,22,'407 SW');
insert into cad_modelos values(2493,22,'307 CC');
insert into cad_modelos values(2499,1,'STYLELINE');
insert into cad_modelos values(2500,13,'ANGLIA');
insert into cad_modelos values(2508,26,'GT2');
insert into cad_modelos values(2509,1,'YUKON');
insert into cad_modelos values(2510,54,'SPORTSMAN');
insert into cad_modelos values(2514,24,'21 NEVADA');
insert into cad_modelos values(2515,11,'VEYRON');
insert into cad_modelos values(2516,10,'ENZO');
insert into cad_modelos values(2517,22,'306 SW');
insert into cad_modelos values(2528,28,'TI 80');
insert into cad_modelos values(2532,70,'SPYDER 550');
insert into cad_modelos values(2533,10,'380 GTB');
insert into cad_modelos values(2534,149,'T-5');
insert into cad_modelos values(2536,18,'KINGSWAY');
insert into cad_modelos values(2537,1,'SSR');
insert into cad_modelos values(2540,1,'IMPALA');
insert into cad_modelos values(2541,22,'208');
insert into cad_modelos values(2542,1,'GRAND BLAZER');
insert into cad_modelos values(2555,53,'100 SERIES');
insert into cad_modelos values(2558,53,'200 SERIES');
insert into cad_modelos values(2559,53,'300 SERIES');
insert into cad_modelos values(2561,53,'66');
insert into cad_modelos values(2562,53,'700 SERIES');
insert into cad_modelos values(2563,53,'AMAZON');
insert into cad_modelos values(2564,53,'C303');
insert into cad_modelos values(2566,53,'DUETT');
insert into cad_modelos values(2567,53,'L3314');
insert into cad_modelos values(2568,53,'OV 4');
insert into cad_modelos values(2569,53,'P1800');
insert into cad_modelos values(2570,53,'SUGGA');
insert into cad_modelos values(2571,13,'TT');
insert into cad_modelos values(2572,5,'ONCE');
insert into cad_modelos values(2573,13,'DE LUXE');
insert into cad_modelos values(2574,13,'CUSTOM');
insert into cad_modelos values(2575,13,'T-BUCKET');
insert into cad_modelos values(2576,17,'G15');
insert into cad_modelos values(2588,111,'PAJERO FULL 3D');
insert into cad_modelos values(2589,111,'PAJERO SPORT');
insert into cad_modelos values(2590,36,'120 CABRIO');
insert into cad_modelos values(2591,36,'320 TOURING');
insert into cad_modelos values(2592,36,'330 CABRIO');
insert into cad_modelos values(2593,36,'SERIE 5 TOURING');
insert into cad_modelos values(2594,36,'SERIE 6 CABRIO');
insert into cad_modelos values(2595,36,'SERIE M CONVERSIVEL');
insert into cad_modelos values(2596,36,'M5 TOURING');
insert into cad_modelos values(2597,36,'SERIE Z ROADSTER');
insert into cad_modelos values(2599,13,'KA SPORT');
insert into cad_modelos values(2600,2,'CC');
insert into cad_modelos values(2605,16,'CERATO KOUP');
insert into cad_modelos values(2607,1,'ASTRO');
insert into cad_modelos values(2608,23,'COROLLA XRS');
insert into cad_modelos values(2609,23,'ETIOS SEDAN');
insert into cad_modelos values(2611,13,'FREESTYLE');
insert into cad_modelos values(2612,110,'COUGAR');
insert into cad_modelos values(2615,102,'XUV 500');
insert into cad_modelos values(2618,102,'XYLO');
insert into cad_modelos values(2619,102,'BOLERO');
insert into cad_modelos values(2620,102,'THAR');
insert into cad_modelos values(2621,102,'AXE');
insert into cad_modelos values(2622,102,'LEGEND');
insert into cad_modelos values(2623,87,'CJ3');
insert into cad_modelos values(2624,102,'ARMADA');
insert into cad_modelos values(2625,102,'CHASSI');
insert into cad_modelos values(2626,102,'SCORPIO PICK-UP');
insert into cad_modelos values(2627,6,'STAR TRUCK');
insert into cad_modelos values(2628,6,'STAR');
insert into cad_modelos values(2629,6,'STAR VAN CARGO');
insert into cad_modelos values(2630,6,'STAR VAN PASSAGEIROS');
insert into cad_modelos values(2632,141,'ALVORADA');
insert into cad_modelos values(2633,141,'CHAMBORD');
insert into cad_modelos values(2637,141,'PROFISSIONAL');
insert into cad_modelos values(2639,141,'VEDETTE');
insert into cad_modelos values(2640,141,'ARONDE');
insert into cad_modelos values(2641,141,'1200S');
insert into cad_modelos values(2642,141,'1000');
insert into cad_modelos values(2645,14,'HB20X');
insert into cad_modelos values(2646,14,'HB20S');
insert into cad_modelos values(2648,1,'MONZA');
insert into cad_modelos values(2649,1,'CHEVETTE');
insert into cad_modelos values(2650,98,'X60');
insert into cad_modelos values(2651,1,'TRAX');
insert into cad_modelos values(2652,36,'118');
insert into cad_modelos values(2653,36,'120');
insert into cad_modelos values(2654,36,'130');
insert into cad_modelos values(2655,36,'BAVARIA');
insert into cad_modelos values(2656,36,'C-2800');
insert into cad_modelos values(2657,36,'318');
insert into cad_modelos values(2658,36,'320');
insert into cad_modelos values(2659,36,'318 CABRIO');
insert into cad_modelos values(2660,36,'325 CABRIO');
insert into cad_modelos values(2661,36,'530');
insert into cad_modelos values(2662,36,'540');
insert into cad_modelos values(2663,36,'550');
insert into cad_modelos values(2664,36,'740');
insert into cad_modelos values(2665,36,'750');
insert into cad_modelos values(2666,36,'760');
insert into cad_modelos values(2675,341,'MATRIX 4X4');
insert into cad_modelos values(2694,132,'A7');
insert into cad_modelos values(2695,132,'A9');
insert into cad_modelos values(2696,132,'A9 CARGO');
insert into cad_modelos values(2697,132,'T20');
insert into cad_modelos values(2698,132,'T20 BAU');
insert into cad_modelos values(2699,132,'T22');
insert into cad_modelos values(2704,64,'SUPER 90 COUPE');
insert into cad_modelos values(2705,17,'X20');
insert into cad_modelos values(2706,17,'ITAIPU');
insert into cad_modelos values(2707,17,'G800');
insert into cad_modelos values(2708,17,'XEF');
insert into cad_modelos values(2709,17,'MOTOMACHINE');
insert into cad_modelos values(2710,17,'BUGATO');
insert into cad_modelos values(2711,17,'QT');
insert into cad_modelos values(2716,57,'CAICARA');
insert into cad_modelos values(2717,57,'CARCARA');
insert into cad_modelos values(2718,57,'FISSORE');
insert into cad_modelos values(2719,57,'MALZONI');
insert into cad_modelos values(2720,57,'VEMAGUET');
insert into cad_modelos values(2727,5,'C4 LOUNGE');
insert into cad_modelos values(2728,108,'CX-7');
insert into cad_modelos values(2729,147,'TR');
insert into cad_modelos values(2730,147,'LUCENA');
insert into cad_modelos values(2731,147,'SEVETSE');
insert into cad_modelos values(2732,147,'RAGGE');
insert into cad_modelos values(2733,53,'C70');
insert into cad_modelos values(2734,53,'C30');
insert into cad_modelos values(2735,53,'544');
insert into cad_modelos values(2736,53,'S40');
insert into cad_modelos values(2737,53,'S60');
insert into cad_modelos values(2738,53,'S70');
insert into cad_modelos values(2739,53,'S80');
insert into cad_modelos values(2740,53,'V40');
insert into cad_modelos values(2741,53,'V50');
insert into cad_modelos values(2742,53,'V60');
insert into cad_modelos values(2743,53,'V70');
insert into cad_modelos values(2744,53,'S90');
insert into cad_modelos values(2745,53,'XC60');
insert into cad_modelos values(2746,53,'XC70');
insert into cad_modelos values(2747,53,'XC90');
insert into cad_modelos values(2748,53,'P1900');
insert into cad_modelos values(2749,53,'PV36');
insert into cad_modelos values(2750,53,'PV444');
insert into cad_modelos values(2751,53,'PV544');
insert into cad_modelos values(2752,53,'PV51');
insert into cad_modelos values(2753,53,'PV654');
insert into cad_modelos values(2754,53,'C50');
insert into cad_modelos values(2755,4,'190');
insert into cad_modelos values(2756,4,'CLASSE CLA');
insert into cad_modelos values(2757,4,'CLASSE V');
insert into cad_modelos values(2758,4,'VANEO');
insert into cad_modelos values(2759,4,'CITAN');
insert into cad_modelos values(2760,4,'VARIO');
insert into cad_modelos values(2761,4,'CLASSE S CLASSICO');
insert into cad_modelos values(2809,15,'J3S');
insert into cad_modelos values(2810,345,'PICK-UP');
insert into cad_modelos values(2811,345,'VAN');
insert into cad_modelos values(2823,5,'C3 SOLARIS');
insert into cad_modelos values(2824,5,'C3 XTR');
insert into cad_modelos values(2825,5,'C4 SOLARIS');
insert into cad_modelos values(2826,5,'C5 BREAK/TOURER');
insert into cad_modelos values(2827,5,'XSARA BREAK');
insert into cad_modelos values(2828,5,'XSARA VTS');
insert into cad_modelos values(2829,5,'XANTIA BREAK');
insert into cad_modelos values(2830,5,'XM BREAK');
insert into cad_modelos values(2831,5,'C15');
insert into cad_modelos values(2832,5,'NEMO');
insert into cad_modelos values(2833,5,'VISA');
insert into cad_modelos values(2834,5,'C1');
insert into cad_modelos values(2835,5,'C2');
insert into cad_modelos values(2836,5,'C3 PLURIEL');
insert into cad_modelos values(2837,5,'DS4');
insert into cad_modelos values(2838,5,'DS5');
insert into cad_modelos values(2839,5,'JUMPY');
insert into cad_modelos values(2840,5,'C-CROSSER');
insert into cad_modelos values(2841,5,'C35');
insert into cad_modelos values(2842,5,'C25');
insert into cad_modelos values(2843,5,'CX');
insert into cad_modelos values(2844,5,'CX BREAK');
insert into cad_modelos values(2845,5,'AXEL');
insert into cad_modelos values(2846,5,'DYANE');
insert into cad_modelos values(2847,5,'GS/GSA');
insert into cad_modelos values(2848,5,'GS/GSA BREAK');
insert into cad_modelos values(2849,5,'MEHARI');
insert into cad_modelos values(2850,5,'SAXO');
insert into cad_modelos values(2851,5,'SM');
insert into cad_modelos values(2852,5,'ELYSEE');
insert into cad_modelos values(2853,24,'MASTER MINIBUS');
insert into cad_modelos values(2854,47,'CELER');
insert into cad_modelos values(2855,47,'CELER SEDAN');
insert into cad_modelos values(2856,47,'CIELO SEDAN');
insert into cad_modelos values(2857,32,'A1 SPORTBACK');
insert into cad_modelos values(2858,32,'A1 QUATTRO');
insert into cad_modelos values(2859,32,'A3 SPORTBACK');
insert into cad_modelos values(2860,32,'RS4 AVANT');
insert into cad_modelos values(2861,32,'A8L W12');
insert into cad_modelos values(2862,32,'R8 V10');
insert into cad_modelos values(2863,13,'RANGER CD');
insert into cad_modelos values(2864,15,'T140');
insert into cad_modelos values(2865,36,'X1');
insert into cad_modelos values(2866,36,'X3');
insert into cad_modelos values(2867,36,'X5');
insert into cad_modelos values(2868,36,'X6');
insert into cad_modelos values(2869,36,'840');
insert into cad_modelos values(2870,36,'850');
insert into cad_modelos values(2871,36,'645');
insert into cad_modelos values(2872,36,'650');
insert into cad_modelos values(2874,7,'FIT TWIST');
insert into cad_modelos values(2876,346,'MP4');
insert into cad_modelos values(2877,346,'F1');
insert into cad_modelos values(2878,13,'MONDEO SW');
insert into cad_modelos values(2879,13,'ESCORT SEDAN');
insert into cad_modelos values(2880,13,'ESCORT CONVERSIVEL');
insert into cad_modelos values(2881,108,'MX-6');
insert into cad_modelos values(2884,1,'CORISCO');
insert into cad_modelos values(2885,1,'CHEVELLE');
insert into cad_modelos values(2886,13,'EXCURSION');
insert into cad_modelos values(2887,2,'TOURAN');
insert into cad_modelos values(2890,13,'F-10000');
insert into cad_modelos values(2891,22,'HOGGAR ESCAPADE');
insert into cad_modelos values(2901,13,'PHAETON');
insert into cad_modelos values(2913,2,'TRANSPORTER');
insert into cad_modelos values(2914,16,'GRAND BESTA');
insert into cad_modelos values(2915,43,'200SX');
insert into cad_modelos values(2916,43,'240SX');
insert into cad_modelos values(2921,19,'300M');
insert into cad_modelos values(2922,19,'300C TOURING');
insert into cad_modelos values(2928,13,'TORINO');
insert into cad_modelos values(2931,1,'VENTURE');
insert into cad_modelos values(2932,1,'FLEETLINE');
insert into cad_modelos values(2933,1,'FLEETMASTER');
insert into cad_modelos values(2934,1,'DELUXE');
insert into cad_modelos values(2936,13,'ESCORT XR3');
insert into cad_modelos values(2937,1,'MASTER');
insert into cad_modelos values(2938,120,'TORONADO');
insert into cad_modelos values(2939,120,'SIX');
insert into cad_modelos values(2940,120,'EIGHT');
insert into cad_modelos values(2941,120,'DELUXE');
insert into cad_modelos values(2942,120,'SERIES 60');
insert into cad_modelos values(2943,120,'SERIES 70');
insert into cad_modelos values(2944,120,'SERIES 80');
insert into cad_modelos values(2945,120,'SERIES 90');
insert into cad_modelos values(2946,120,'STARFIRE');
insert into cad_modelos values(2947,120,'442');
insert into cad_modelos values(2948,120,'CUTLASS');
insert into cad_modelos values(2949,120,'CUTLASS SUPREME');
insert into cad_modelos values(2950,120,'CUTLASS SALON');
insert into cad_modelos values(2951,120,'CUTLASS CALAIS');
insert into cad_modelos values(2952,120,'CUTLASS CIERA');
insert into cad_modelos values(2953,120,'CUSTOM CRUISER');
insert into cad_modelos values(2954,120,'VISTA CRUISER');
insert into cad_modelos values(2955,120,'F-85');
insert into cad_modelos values(2957,120,'FIRENZA');
insert into cad_modelos values(2958,120,'ACHIEVA');
insert into cad_modelos values(2959,120,'ALERO');
insert into cad_modelos values(2960,120,'AURORA');
insert into cad_modelos values(2961,120,'BRAVADA');
insert into cad_modelos values(2962,120,'INTRIGUE');
insert into cad_modelos values(2963,120,'SILHOUETTE');
insert into cad_modelos values(2972,123,'SUPERBIRD');
insert into cad_modelos values(2973,123,'FURY');
insert into cad_modelos values(2974,123,'SPECIAL');
insert into cad_modelos values(2975,123,'PROWLER');
insert into cad_modelos values(2976,123,'TRAIL DUSTER');
insert into cad_modelos values(2977,123,'VOYAGER');
insert into cad_modelos values(2978,123,'SCAMP');
insert into cad_modelos values(2979,123,'ARROW');
insert into cad_modelos values(2980,123,'PT50');
insert into cad_modelos values(2981,123,'PT57');
insert into cad_modelos values(2982,123,'PT81');
insert into cad_modelos values(2983,123,'PT105');
insert into cad_modelos values(2984,123,'PT125');
insert into cad_modelos values(2985,123,'EXPRESS');
insert into cad_modelos values(2986,123,'VOYAGER EXPRESSO');
insert into cad_modelos values(2987,123,'NEON');
insert into cad_modelos values(2988,123,'LASER');
insert into cad_modelos values(2989,123,'CARAVELLE');
insert into cad_modelos values(2990,123,'STATION WAGON');
insert into cad_modelos values(2991,123,'MODEL Q');
insert into cad_modelos values(2992,123,'MODEL P6');
insert into cad_modelos values(2993,31,'DB9 COUPE');
insert into cad_modelos values(2994,31,'DB9 VOLANTE');
insert into cad_modelos values(2995,31,'VIRAGE COUPE');
insert into cad_modelos values(2996,31,'RAPIDE S');
insert into cad_modelos values(2997,31,'V12 VANTAGE');
insert into cad_modelos values(2998,31,'V8 VANTAGE COUPE');
insert into cad_modelos values(2999,31,'V8 VANTAGE ROADSTER');
insert into cad_modelos values(3000,31,'V8 VANTAGE S COUPE');
insert into cad_modelos values(3001,31,'V8 VANTAGE S ROADSTER');
insert into cad_modelos values(3002,31,'VANQUISH COUPE');
insert into cad_modelos values(3003,31,'VANQUISH VOLANTE');
insert into cad_modelos values(3004,31,'V12 ZAGATO');
insert into cad_modelos values(3005,31,'DB5');
insert into cad_modelos values(3007,31,'DBS');
insert into cad_modelos values(3008,31,'DBS VOLANTE');
insert into cad_modelos values(3009,31,'CYGNET');
insert into cad_modelos values(3010,31,'ONE-77');
insert into cad_modelos values(3011,31,'DBR9');
insert into cad_modelos values(3013,36,'M3');
insert into cad_modelos values(3014,36,'M5');
insert into cad_modelos values(3015,36,'M6');
insert into cad_modelos values(3016,36,'X6 M');
insert into cad_modelos values(3017,113,'CABRIO');
insert into cad_modelos values(3018,113,'COUPE');
insert into cad_modelos values(3019,113,'ROADSTER');
insert into cad_modelos values(3020,113,'COUNTRYMAN');
insert into cad_modelos values(3021,113,'PACEMAN');
insert into cad_modelos values(3022,113,'JOHN COOPER WORKS');
insert into cad_modelos values(3023,122,'ZONDA');
insert into cad_modelos values(3024,8,'NEW XV');
insert into cad_modelos values(3025,8,'IMPREZA WRX HATCH');
insert into cad_modelos values(3026,8,'IMPREZA WRX STI HATCH');
insert into cad_modelos values(3027,8,'IMPREZA WRX STI SEDAN');
insert into cad_modelos values(3028,8,'IMPREZA WRX SEDAN');
insert into cad_modelos values(3030,23,'ETIOS CROSS');
insert into cad_modelos values(3031,12,'HURACAN');
insert into cad_modelos values(3032,2,'UP');
/*insert into cad_modelos values(3080,195,'EXPLORER');*/
insert into cad_modelos values(4964,142,'FORTWO CABRIO');
insert into cad_modelos values(4969,26,'GT');
insert into cad_modelos values(4970,26,'GTL');
insert into cad_modelos values(4971,26,'GTM');
insert into cad_modelos values(4972,26,'C2');
insert into cad_modelos values(4973,26,'CRX');
insert into cad_modelos values(4974,26,'AC 2000');
insert into cad_modelos values(4975,99,'AVIATOR');
insert into cad_modelos values(4976,99,'BLACKWOOD');
insert into cad_modelos values(4977,99,'CAPRI');
insert into cad_modelos values(4978,99,'CONTINENTAL');
insert into cad_modelos values(4979,99,'LS');
insert into cad_modelos values(4980,99,'MARK');
insert into cad_modelos values(4981,99,'MARK LT');
insert into cad_modelos values(4982,99,'MKR');
insert into cad_modelos values(4983,99,'MKS');
insert into cad_modelos values(4984,99,'MKX');
insert into cad_modelos values(4985,99,'MKZ');
insert into cad_modelos values(4986,99,'NAVIGATOR');
insert into cad_modelos values(4987,99,'PREMIERE');
insert into cad_modelos values(4988,99,'TOWN CAR');
insert into cad_modelos values(4989,99,'VERSAILLES');
insert into cad_modelos values(4990,99,'ZEPHYR');
insert into cad_modelos values(4991,1,'CLASSIC');
insert into cad_modelos values(4992,21,'ACTYON');
insert into cad_modelos values(5003,1,'MARAJO');
insert into cad_modelos values(5004,1,'SUPREMA');
insert into cad_modelos values(5005,2,'NEW BEETLE');
insert into cad_modelos values(5006,2,'QUANTUM');
insert into cad_modelos values(5007,2,'CROSSFOX');
insert into cad_modelos values(5008,3,'MILLE');
insert into cad_modelos values(5009,534,'GC2');
insert into cad_modelos values(5010,534,'EC7');
insert into cad_modelos values(5011,98,'530');
insert into cad_modelos values(5012,3,'MOBI');
insert into cad_modelos values(5013,3,'TORO');
insert into cad_modelos values(5014,87,'RENEGADE');
insert into cad_modelos values(5015,24,'DUSTER OROCH');
insert into cad_modelos values(5016,24,'SANDERO RS');
insert into cad_modelos values(5017,14,'HB20R');
insert into cad_modelos values(5018,14,'GRAND SANTA FE');
insert into cad_modelos values(5019,2,'GOLF VARIANT');
insert into cad_modelos values(5020,2,'SPACE CROSS');
insert into cad_modelos values(5021,22,'2008');
insert into cad_modelos values(5022,16,'QUORIS');
insert into cad_modelos values(5023,16,'GRAND CARNIVAL');
insert into cad_modelos values(5024,15,'T8');
insert into cad_modelos values(5025,15,'T6');
insert into cad_modelos values(5026,15,'T5');
insert into cad_modelos values(5027,13,'KA SEDAN');
insert into cad_modelos values(5028,13,'FOCUS FASTBACK');

/***************************************************************************************************/
/*Acesso ao motorista..*/
insert into seg_grupos(id_grupo,nome_grupo,descricao_grupo,ativo_grupo)
	values (2,'Motoristas','Motoristas',true);

insert into seg_grupos(id_grupo,nome_grupo,descricao_grupo,ativo_grupo)
	values (3,'Passageiro','Passageiro',true);

insert into seg_usuarios (id_usuario,nome_usuario,email_usuario,telefone_usuario,login_usuario,senha_usuario,ativo_usuario,fk_grupo_usuario,ativado_email,ativado_sms)
	values (2,'Usuário Motorista','motorista@yougo.com','11962782320','motorista','40bd001563085fc35165329ea1ff5c5ecbdbbeef',TRUE,2,1,1);

insert into cad_detalhes_motorista (fk_usuario,fk_cidade_ativa_motorista,fk_banco_motorista,fk_modelo_carro_motorista,cor_carro_motorista,placa_carro_motorista,motorista_online)
	values (2,1,29,2,'cor','ABC-4321',true);

insert into seg_usuarios (id_usuario,nome_usuario,email_usuario,telefone_usuario,login_usuario,senha_usuario,ativo_usuario,fk_grupo_usuario,ativado_email,ativado_sms)
	values (3,'Usuário Motorista 2','motorista2@yougo.com','11962782321','motorista2','40bd001563085fc35165329ea1ff5c5ecbdbbeef',TRUE,2,1,1);

insert into cad_detalhes_motorista (fk_usuario,fk_cidade_ativa_motorista,fk_banco_motorista,fk_modelo_carro_motorista,cor_carro_motorista,placa_carro_motorista,motorista_online)
	values (3,1,29,2,'cor2','CBA-1234',true);

insert into seg_usuarios (id_usuario,nome_usuario,email_usuario,telefone_usuario,login_usuario,senha_usuario,ativo_usuario,fk_grupo_usuario,ativado_email,ativado_sms)
	values (4,'Usuário Passageiro','passageiro@yougo.com','11962782320','passageiro','40bd001563085fc35165329ea1ff5c5ecbdbbeef',TRUE,3,1,1);

insert into elo_pagamento_cliente value ('1','4','sandbox','4111.1111.1111.1111','2030-12-01','123','1999-04-16',true,'','','',null);

insert into seg_usuarios (id_usuario,nome_usuario,email_usuario,telefone_usuario,login_usuario,senha_usuario,ativo_usuario,fk_grupo_usuario,ativado_email,ativado_sms)
	values 
		(5,'Patrícia Kimelblat'	,'pkimelblat@yougomobile.com.br'		,'11962782329','pkimelblat'			,'40bd001563085fc35165329ea1ff5c5ecbdbbeef',1,1,1,1),
	 	(6,'Cristina Oliveira'	,'cristinaoliveira@yougomobile.com.br'	,'11962782329','cristinaoliveira'	,'40bd001563085fc35165329ea1ff5c5ecbdbbeef',1,1,1,1),
	 	(7,'Caio Acialdi'		,'caioacialdi@yougomobile.com.br'		,'11962782329','caioacialdi'		,'40bd001563085fc35165329ea1ff5c5ecbdbbeef',1,1,1,1);

INSERT INTO seg_models (id_model,link_model, descricao_model)
	VALUES (7,'Model_motorista', 'Model Motoristas');
INSERT INTO seg_controllers (id_controller,link_controller, descricao_controller, fk_model) VALUES (6,'Controller_motorista', 'Motoristas', 7);


INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (NULL, 'Ver corridas', 'Ver corridas', 11, 1);
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (NULL, 'Perfil', 'Perfil do motorista', 12, 2);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (15,'relatorios/view_ver_corridas', 'Ver Corridas', 'Ver Corridas do motorista', 6);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (15,11);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(15,2);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (16,'motorista/view_perfil_motorista', 'Perfil Motorista', 'Perfil Motorista', 6);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (16,12);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(16,2);

/*criando model*/
INSERT INTO seg_models (id_model,link_model, descricao_model) VALUES (8,'Model_adm', 'Model Adm');
/*Criando controller*/
INSERT INTO seg_controllers (id_controller,link_controller, descricao_controller, fk_model) VALUES (7,'Controller_adm', 'Administrativo', 8);

/* menu corridas */
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (NULL, 'Corridas', 'todas as corridas', 13, 1);
/*INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (13, 'Todas', 'Todas as corridas', 14, null);
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (13, 'Cliente', 'Todas as corridas do cliente', 15, null);
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (13, 'Motorista', 'Todas as corridas do motorista', 16, null);*/

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (18,'corridas/view_todas', 'Todas', 'todas as corridas', 7);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (18,13);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(18,1);



/*

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (19,'cliente/view_corridas', 'Cliente', 'corridas do cliente', 7);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (19,15);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(19,1);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (20,'motorista/view_motorista', 'Motorista', 'corridas do motorista', 7);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (20,16);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(20,1);
*/

/*Menu motorista*/
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (NULL, 'Motorista', 'Sobre os motoristas do app', 17, 2);
/*INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (17, 'Aprovações', 'aprovar motoristas', 18, null);*/
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (17, 'Cadastrar motorista', 'cadastrar motorista', 19, null);
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (17, 'Motorista', 'ver todos motorista', 23, null);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (21,'motorista/view_avaliacao', 'Aprovação', 'Aprovar motorista', 7);
-- insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (21,18);
-- insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(21,1);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (22,'motorista/view_cadastrar', 'Cadastrar', 'cadastrar novo motorista', 7);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (22,19);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(22,1);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (26,'motorista/view_motoristas', 'Motoristas', 'ver motoristas', 6);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (26,23);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(26,1);


INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (28,'motorista/editar_motorista', 'Editar Motorista', 'Editar motorista', 6);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(28,1);





INSERT INTO seg_models (id_model,link_model, descricao_model)
	VALUES (9,'Model_termo', 'Model termos de uso');
INSERT INTO seg_controllers (id_controller,link_controller, descricao_controller, fk_model) VALUES (9,'Controller_termo', 'Termos de uso', 9);


/*menu termos*/
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (NULL, 'Termo de uso', 'Alterar termos de uso', 20, 4);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (23,'termo/view_termo', 'Termo de uso', 'alterar termo de uso', 9);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (23,20);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(23,1);



/* cidades-menu */
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (NULL, 'Cidades', 'todas as cidades', 30, 7);
INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (19,'cidades/view_cidades', 'Todas as cidades', 'todas as cidades para habilitação', 6);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (19,30);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(19,1);






/*menu cupons*/
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (NULL, 'Cupons', 'cupons de desconto', 21, 6);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (24,'cupons/view_cupons', 'Cupons', 'cupons de desconto', 7);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (24,21);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(24,1);











INSERT INTO seg_models (id_model,link_model, descricao_model)
	VALUES (10,'Model_tarifas', 'Model de tarifas');
INSERT INTO seg_controllers (id_controller,link_controller, descricao_controller, fk_model) VALUES (10,'Controller_tarifas', 'Tarifas', 10);

/*menu tarifas*/
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (NULL, 'Tarifas', 'tarifas por turno', 22, 5);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (25,'tarifas/view_tarifas', 'Tarifas', 'alterar tarifas por horarios', 10);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (25,22);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(25,1);








/*menu cliente*/
INSERT INTO seg_models (id_model,link_model, descricao_model)
	VALUES (11,'Model_passageiro', 'Model de passageiros');
INSERT INTO seg_controllers (id_controller,link_controller, descricao_controller, fk_model) VALUES (11,'Controller_passageiro', 'Passageiros', 11);



/*menu tarifas*/
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (NULL, 'Passageiro', 'ver Passageiro', 24, 3);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (27,'passageiro/view_passageiro', 'Passageiro', 'Passageiros todos', 11);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (27,24);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(27,1);



/*perfil passageiro*/
INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (31,'passageiro/view_passageiro_perfil', 'Passageiro perfil', 'Passageiros perfil de todos', 11);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(31,1);

/*pagamento*/
INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES (NULL, '7', 'Pagamentos', 'Todos os pagamentos', '31');
INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('pagamento/pagamento_motorista', 'Pagamentos', 'todos os pagamentos', '7');
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 32, 31);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 32,1);





/*Relatorios*/
INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES (NULL, '1', 'Relatórios', 'Relatórios', '32');
INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES ('32', '1', 'Motorista', 'Relatórios do motorista', '33');
INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES ('32', '2', 'Passageiro', 'Relatórios do passageiro', '34');
INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES ('32', '2', 'Calendário', 'Relatórios Calendário', '35');

INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('relatorios/view_motorista_relatorio', 'Motorista', 'relatório motorista', '7');
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 33, 33);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 33,1);

INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('relatorios/view_passageiro_relatorio', 'Passageiro', 'passageiro', '7');
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 34, 34);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 34,1);

INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('relatorios/view_calendario', 'Calendário', 'Calendário', '7');
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 35, 35);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 35,1);


/*Funções*/


/*Geolocalização, calcula distancia entre dois pontos em kilometros.*/
delimiter //
CREATE FUNCTION Geo(lat_ini DOUBLE(18,10), lon_ini DOUBLE(18,10),lat_fim DOUBLE(18,10), lon_fim DOUBLE(18,10))
RETURNS DOUBLE(18,10)
NOT DETERMINISTIC
BEGIN
DECLARE Theta DOUBLE(18,10);
DECLARE Dist DOUBLE(18,10);
DECLARE Miles DOUBLE(18,10);
DECLARE kilometers DOUBLE(18,10);

SET Theta = lon_ini - lon_fim;
SET Dist  = SIN(RADIANS(lat_ini)) * SIN(RADIANS(lat_fim)) +  COS(RADIANS(lat_ini)) * COS(RADIANS(lat_fim)) * COS(RADIANS(Theta));
SET Dist  = ACOS(Dist);
SET Dist  = DEGREES(Dist);
SET Miles = Dist * 60 * 1.1515;
SET kilometers = Miles * 1.609344;

RETURN kilometers;

END;//
delimiter ;


CREATE TABLE IF NOT EXISTS cad_cupons(
	id_cupom int auto_increment,
	codigo_cupom varchar (30) not null,
	nome_cupom varchar (80)  not null,
	valor_desconto_cupom real  not null,
	tipo_desconto_cupom boolean  not null,
	quantidade_cupom int not null,
	data_inicio_cupom date  not null,
	data_termino_cupom date  not null,
	ativo_cupom boolean  not null,

	UNIQUE (codigo_cupom),
	PRIMARY KEY (id_cupom)

);

/*Corrida*/
CREATE TABLE IF NOT EXISTS cad_corridas (

	id_corrida int not null AUTO_INCREMENT,

	fk_passageiro int,

	data_corrida timestamp DEFAULT CURRENT_TIMESTAMP,

	km_corrida int,
	qtd_malas int,
	qtd_passageiros int,
	fk_forma_pagamento int,

	latitude_origem float(10,6),
	longitude_origem float(10,6),
	endereco_origem text,
	fk_cidade_origem int,

	latitude_destino float(10,6),
	longitude_destino float(10,6),
	endereco_destino text,

	latitude_inicio_corrida float(10,6),
	longitude_inicio_corrida float(10,6),

	latitude_fim_corrida float(10,6),
	longitude_fim_corrida float(10,6),

	/*Dados do carro motorista*/
	fk_motorista int,
	fk_modelo_carro_motorista int,
	cor_carro_motorista text,
	placa_carro_motorista character varying(8),
	aviso_proximidade_origem boolean,
	aviso_proximidade_destino boolean,

	/*Taxas*/
	valor_corrida real,
	taxa_yougo real,
	taxa_cancelamento real,
	valor_tarifa real,
	valor_tarifa_minima real,
	fk_cupom int,

	data_inicio_corrida timestamp null,
	data_fim_corrida timestamp null,
	justificativa text,
	fk_cancelado_por int,
	fk_status_corrida int,

	hash_pagamento text,
	token_pagamento text,
	code_pag_seguro text,
	status_pag_seguro int,

	pagamento_motorista boolean,
	pagamento_motorista_data timestamp null,


	PRIMARY KEY (id_corrida),
	FOREIGN KEY (fk_passageiro) 			REFERENCES seg_usuarios 			(id_usuario),
	FOREIGN KEY (fk_motorista) 				REFERENCES seg_usuarios 			(id_usuario),
	FOREIGN KEY (fk_cancelado_por) 			REFERENCES seg_usuarios 			(id_usuario),
	FOREIGN KEY (fk_modelo_carro_motorista) REFERENCES cad_modelos  			(id_modelo),
	FOREIGN KEY (fk_forma_pagamento) 		REFERENCES elo_pagamento_cliente  	(id_pagamento),
	FOREIGN KEY (fk_status_corrida) 		REFERENCES cad_item_grupo  			(id_item_grupo),
	FOREIGN KEY (fk_cidade_origem) 			REFERENCES cad_cidades_atuacao		(id_cidades_atuacao),
	FOREIGN KEY (fk_cupom) 					REFERENCES cad_cupons				(id_cupom)


);

CREATE TABLE IF NOT EXISTS cad_solicitacao_corrida (

	id_solicitacao_corrida int not null AUTO_INCREMENT,
	fk_corrida int,
	fk_motorista int,
	solicitado_em timestamp DEFAULT CURRENT_TIMESTAMP,
	respondido_em timestamp null,
	solicitacao_aceita boolean,

	PRIMARY KEY (id_solicitacao_corrida),
	FOREIGN KEY (fk_motorista) 	REFERENCES seg_usuarios (id_usuario),
	FOREIGN KEY (fk_corrida) 	REFERENCES cad_corridas (id_corrida)

);

CREATE TABLE IF NOT EXISTS cad_avaliacoes_corrida (

	id_avaliacoes_corrida int not null AUTO_INCREMENT,
	fk_corrida int,

	pontualidade_passageiro int,
	simpatia_passageiro int,

	pontualidade_motorista int,
	simpatia_motorista int,
	carro_motorista int,
	servico_motorista int,

	data_avaliacao_motorista timestamp null,
	data_avaliacao_passageiro timestamp null,

	PRIMARY KEY (id_avaliacoes_corrida),
	FOREIGN KEY (fk_corrida) 	REFERENCES cad_corridas (id_corrida)

);

CREATE TABLE IF NOT EXISTS cad_tarifa_corrida (

	id_tarifas_corrida int not null AUTO_INCREMENT,
	valor_tarifa real,
	valor_tarifa_minima real,
	usar_tarifa_minima boolean,
	hora_inicio time,
	hora_fim time,

	PRIMARY KEY (id_tarifas_corrida)
		

);


insert into cad_tarifa_corrida (valor_tarifa,valor_tarifa_minima,usar_tarifa_minima,hora_inicio,hora_fim)
	values  (1.50,10.00,true,'07:00','19:00'),
			(2.50,15.00,true,'19:01','06:59');


CREATE TABLE IF NOT EXISTS cad_configuracoes (

	id_configuracoes int not null AUTO_INCREMENT,
	tempo_espera int,
	km_maximo int,
	taxa_cancelamento real,
	taxa_yougo real,
	termo_de_uso text,
	termo_de_uso_motorista text,
	tempo_cancelamento int,
	tempo_busca int,

	PRIMARY KEY (id_configuracoes)


);

insert into cad_configuracoes (tempo_espera,km_maximo,taxa_cancelamento,taxa_yougo,termo_de_uso )
	values (100,100,10,20,'termo');



CREATE TABLE IF NOT EXISTS cad_tarifa_cidade (

	id_tarifa_cidade int not null AUTO_INCREMENT,
	fk_cidade int,
	nome_tarifa text,
	valor_tarifa real,
	valor_fixo boolean,
	status_tarifa boolean,

	PRIMARY KEY (id_tarifa_cidade),
	FOREIGN KEY (fk_cidade) 	REFERENCES cad_cidades_atuacao (id_cidades_atuacao)

  );

insert into  cad_tarifa_cidade(fk_cidade, nome_tarifa, valor_tarifa, valor_fixo, status_tarifa) VALUES
(1,'nome','null',1,1),
(1,'nome','null',1,1);



CREATE TABLE IF NOT EXISTS hist_tarifa_cidade (

	id_hist_tarifa_cidade int not null AUTO_INCREMENT,
	fk_cidade int,
	nome_tarifa text,
	valor_tarifa real,
	valor_fixo boolean,
	status_tarifa boolean,
	fk_corrida int,

	PRIMARY KEY (id_hist_tarifa_cidade),
	FOREIGN KEY (fk_cidade) 	REFERENCES cad_cidades_atuacao 	(id_cidades_atuacao),
	FOREIGN KEY (fk_corrida) 	REFERENCES cad_corridas		 	(id_corrida)


);

create or replace view view_avaliacoes_passageiro as
	select
	round((sum(pontualidade_passageiro)/count(*)),2) as pontualidade_passageiro,
	round((sum(simpatia_passageiro)/count(*)),2) as simpatia_passageiro,
	fk_passageiro

    	from cad_avaliacoes_corrida
        	inner join cad_corridas on id_corrida = fk_corrida
            inner join seg_usuarios on id_usuario = fk_passageiro
            	where simpatia_passageiro is not null
                and pontualidade_passageiro is not null
                group by fk_passageiro;

create or replace view view_avaliacoes_motorista as
	select
	round((sum(pontualidade_motorista)/count(*)),2) as pontualidade_motorista,
	round((sum(simpatia_motorista)/count(*)),2) as simpatia_motorista,
	round((sum(carro_motorista)/count(*)),2) as carro_motorista,
	round((sum(servico_motorista)/count(*)),2) as servico_motorista,
	fk_motorista

    	from cad_avaliacoes_corrida
        	inner join cad_corridas on id_corrida = fk_corrida
            inner join seg_usuarios on id_usuario = fk_motorista
            	where pontualidade_motorista is not null
                and simpatia_motorista is not null
                and carro_motorista is not null
                and servico_motorista is not null
                group by fk_motorista;

/*Taxas - Preço - Cupom*/
/*Taxa de cancelamento? PG21
Tempo Limite*/

/*Avaliação*/


create or replace view view_custo_final as
SELECT id_corrida, Concat('R$ ', Replace(Round(( 
  CASE fk_status_corrida 
   WHEN 87 THEN taxa_cancelamento 
   WHEN 88 THEN 0 
   ELSE ( CASE  
            WHEN fk_cupom is NULL THEN valor_corrida 
            ELSE 
  ( CASE (SELECT 
   tipo_desconto_cupom 
         FROM   cad_cupons 
         WHERE 
   id_cupom = fk_cupom 
        ) 
     WHEN 0 THEN ( 
     valor_corrida 
     - (SELECT 
     valor_desconto_cupom 
                      FROM 
     cad_cupons 
                      WHERE 
     id_cupom = fk_cupom) 
                 ) 
     ELSE ( valor_corrida 
            - ( ( 
            valor_corrida * 
            (SELECT 
            valor_desconto_cupom 
                             FROM 
            cad_cupons 
                             WHERE 
            id_cupom = fk_cupom) ) 
            / 
                              100 ) 
          ) 
   END ) 
          END ) 
  END ), 2), '.', ',')) AS valor_corrida,

  Round(( 
  CASE fk_status_corrida 
   WHEN 87 THEN taxa_cancelamento 
   WHEN 88 THEN 0 
   ELSE ( CASE  
            WHEN fk_cupom is NULL THEN valor_corrida 
            ELSE 
  ( CASE (SELECT 
   tipo_desconto_cupom 
         FROM   cad_cupons 
         WHERE 
   id_cupom = fk_cupom 
        ) 
     WHEN 0 THEN ( 
     valor_corrida 
     - (SELECT 
     valor_desconto_cupom 
                      FROM 
     cad_cupons 
                      WHERE 
     id_cupom = fk_cupom) 
                 ) 
     ELSE ( valor_corrida 
            - ( ( 
            valor_corrida * 
            (SELECT 
            valor_desconto_cupom 
                             FROM 
            cad_cupons 
                             WHERE 
            id_cupom = fk_cupom) ) 
            / 
                              100 ) 
          ) 
   END ) 
          END ) 
  END ), 2) AS valor_corrida_normal  
FROM   cad_corridas;

-- Custo da corrida, Se existir um cupom deve aplicar o desconto, que pode ser Subtração ou desconto percentual.

create or replace view view_dados_pagseguro as
SELECT
			id_corrida,
			logradouro_usuario,
	        num_residencia_usuario,
	        complemento_usuario,
	        bairro_usuario,
	        cidade_usuario,
	        cep_usuario,
	        nome_usuario,
	        email_usuario,
	        SUBSTRING(celular_usuario,1,2) as ddd,
	        SUBSTRING(celular_usuario,3) as celular,
	        fk_status_corrida,
	        taxa_cancelamento,
	        cpf_usuario,
	        date_format(elo_pagamento_cliente.data_nascimento,'%d/%m/%Y') as data_nascimento,
	        fk_cupom,
	        ROUND(valor_corrida,2) as valor_corrida_original,
	        (select valor_corrida_normal from view_custo_final vcf where vcf.id_corrida = cc.id_corrida) as valor_corrida,
	        nome_cartao,
	        sigla_item_grupo as uf



	FROM seg_usuarios
	inner join cad_corridas as cc 		 on fk_passageiro = id_usuario
	inner join elo_pagamento_cliente on fk_forma_pagamento = id_pagamento
	inner join cad_item_grupo 		 on id_item_grupo = fk_uf_usuario;

create or replace view view_dados_pagseguro_ativacao as
SELECT
			id_pagamento,
			logradouro_usuario,
	        num_residencia_usuario,
	        complemento_usuario,
	        bairro_usuario,
	        cidade_usuario,
	        cep_usuario,
	        nome_usuario,
	        email_usuario,
	        SUBSTRING(celular_usuario,1,2) as ddd,
	        SUBSTRING(celular_usuario,3) as celular,
	        cpf_usuario,
	        date_format(elo_pagamento_cliente.data_nascimento,'%d/%m/%Y') as data_nascimento,
	        nome_cartao,
	        sigla_item_grupo as uf



	FROM seg_usuarios
	inner join elo_pagamento_cliente on fk_usuario = id_usuario
	inner join cad_item_grupo 		 on id_item_grupo = fk_uf_usuario;


CREATE TABLE IF NOT EXISTS seg_log_acesso_mobile (

	id_log_acesso_mobile int NOT NULL AUTO_INCREMENT,
	fk_usuario int,
	data_log_acesso_mobile timestamp DEFAULT CURRENT_TIMESTAMP,
	ip_usuario_acesso_mobile character varying(16),
	acesso_mobile boolean,
	token_acesso character varying(36),
	maquina_usuario_acesso_mobile text,

	PRIMARY KEY (id_log_acesso_mobile),
	UNIQUE(token_acesso),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario)


);


CREATE TABLE logs (
    id INT(11) NOT NULL AUTO_INCREMENT,
    uri VARCHAR(255) NOT NULL,
    method VARCHAR(6) NOT NULL,
    params TEXT DEFAULT NULL,
    api_key VARCHAR(40) NOT NULL,
    ip_address VARCHAR(45) NOT NULL,
    time INT(11) NOT NULL,
    rtime FLOAT DEFAULT NULL,
    authorized VARCHAR(1) NOT NULL,
    response_code smallint(3) DEFAULT '0',
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE notificacoes(
	id_notificacao int auto_increment,
	notificationType varchar (255),
    notificationCode varchar (255),

	primary key(id_notificacao)
);

CREATE TABLE pag_status (
                id_status INT NOT NULL,
                status_pagseguro varchar(100) NOT NULL,
                descricao_status varchar(100) NOT NULL,
                CONSTRAINT pag_status_pk PRIMARY KEY (id_status)
)ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;


insert into pag_status values
(1, 'Aguardando Pagamento', 'O comprador iniciou a transação, mas até o momento o PagSeguro não recebeu nenhuma informação sobre o pagamento'),
(2, 'Em análise', 'O comprador optou por pagar com um cartão de crédito e o PagSeguro está analisando o risco da transação'),
(3, 'Recebido', 'PagSeguro já recebeu uma confirmação da instituição financeira responsável pelo processamento'),
(4, 'Disponível', 'A transação foi paga e chegou ao final de seu prazo de liberação sem ter sido retornada e sem que haja nenhuma disputa aberta'),
(5, 'Em disputa', 'O comprador, dentro do prazo de liberação da transação, abriu uma disputa'),
(6, 'Devolvida', 'O valor da transação foi devolvido para o comprador'),
(7, 'Cancelada', 'A transação foi cancelada sem ter sido finalizada');

create or replace view view_estatisticas_passageiros as
SELECT
    nome_usuario,

    (select id_usuario from seg_usuarios where id_usuario = fk_passageiro) as id_usuario,
    (select email_usuario from seg_usuarios where id_usuario = fk_passageiro) as email_usuario,
    (select celular_usuario from seg_usuarios where id_usuario = fk_passageiro) as celular_usuario,
    (select cpf_usuario from seg_usuarios where id_usuario = fk_passageiro) as cpf_usuario,
    (select ativo_usuario from seg_usuarios where id_usuario = fk_passageiro) as ativo_usuario,

    (select count(*) from cad_corridas where id_usuario = fk_passageiro) total_corridas,

    (select count(*) from cad_corridas where id_usuario = fk_passageiro and fk_status_corrida = 87) total_corridas_canceladas,

    round((select avg(valor_corrida) from cad_corridas where id_usuario = fk_passageiro and fk_status_corrida = 86),2) custo_medio_corridas,

        round(((select ifnull((pontualidade_passageiro+simpatia_passageiro),0) from view_avaliacoes_passageiro where         id_usuario = fk_passageiro)/2),2) media_avaliacoes,

    (SELECT
    nome_cidades_atuacao
        from cad_corridas
            inner join cad_cidades_atuacao on id_cidades_atuacao = fk_cidade_origem
                 where fk_passageiro = id_usuario
		            group by fk_cidade_origem
        		    order by count(fk_cidade_origem) desc
		            limit 1) as cidade_mais_chamada

    from cad_corridas
        inner join seg_usuarios on id_usuario = fk_passageiro
        group by nome_usuario,total_corridas,total_corridas_canceladas,custo_medio_corridas,media_avaliacoes,cidade_mais_chamada,id_usuario,email_usuario,celular_usuario,cpf_usuario,ativo_usuario;


create or replace view view_estatisticas_motorista as
SELECT
    nome_usuario,

    (select id_usuario from seg_usuarios where id_usuario = fk_motorista) as id_usuario,
    (select email_usuario from seg_usuarios where id_usuario = fk_motorista) as email_usuario,
    (select celular_usuario from seg_usuarios where id_usuario = fk_motorista) as celular_usuario,
    (select cpf_usuario from seg_usuarios where id_usuario = fk_motorista) as cpf_usuario,
    (select ativo_usuario from seg_usuarios where id_usuario = fk_motorista) as ativo_usuario,

    (select count(*) from cad_corridas where id_usuario = fk_motorista) total_corridas,

    (select count(*) from cad_corridas where id_usuario = fk_motorista and fk_status_corrida = 88) total_corridas_canceladas,

    round((select avg(valor_corrida) from cad_corridas where id_usuario = fk_motorista and fk_status_corrida = 86),2) custo_medio_corridas,

    (select round((sum(pontualidade_motorista +
		    			simpatia_motorista +
		    			carro_motorista +
		    			servico_motorista)/4),2) from view_avaliacoes_motorista
							where id_usuario = fk_motorista) media_avaliacoes,

    (SELECT
    nome_cidades_atuacao
        from cad_corridas
            inner join cad_cidades_atuacao on id_cidades_atuacao = fk_cidade_origem
                 where fk_motorista = id_usuario
		            group by fk_cidade_origem
        		    order by count(fk_cidade_origem) desc
		            limit 1) as cidade_mais_chamada

    from cad_corridas
        inner join seg_usuarios on id_usuario = fk_motorista
        group by nome_usuario,total_corridas,total_corridas_canceladas,custo_medio_corridas,media_avaliacoes,cidade_mais_chamada,id_usuario,email_usuario,celular_usuario,cpf_usuario,ativo_usuario;


create or replace view view_custo_final_motorista as
select
	id_corrida,
	(valor_corrida - 
     	
     	((valor_corrida * taxa_yougo)/100) -

     	ifnull((select sum(valor_tarifa) from hist_tarifa_cidade where fk_corrida = id_corrida 
     	and valor_fixo = 1 and status_tarifa = 1),0) - 

		ifnull(((valor_corrida * (select sum(valor_tarifa) from hist_tarifa_cidade where fk_corrida = id_corrida 
		and valor_fixo = 0 and status_tarifa = 1))/100),0)
    
    ) valor_corrida

from cad_corridas
where fk_motorista is not null;

INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES (NULL, '5', 'Contato', 'contatos dinamicos', '36');
INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('faq/view_faq', 'Contatos', 'contatos', '7');
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 36, 36);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 36,1);


INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES (NULL, '12', 'Relatório Cidades', 'relatorio das cidades','37');
INSERT INTO seg_aplicacao (link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES ('relatorios/cidades', 'Relatórios cidades', 'relatorios das cidades', '7');
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values ( 37, 37);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values( 37,1);
