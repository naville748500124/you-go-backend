<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_motorista extends MY_Controller {


    function __construct() {
        parent::__construct();
        $this->load->model('model_motorista');
        $this->load->model('model_seguranca');
    }

    function  editar(){
        $dados = $this->input->post();
        print_r($dados);
    }


    function desativar(){
        $flag     = $this->input->post('flag');
        $mensagem = $this->input->post('mensagem');
        $bloquear = $this->input->post('bloquear');
        $email    = $this->input->post('email_user');

        if($bloquear){
            $tipo = "Bloqueado";
        } else {
            $tipo = "Inativado";
        }

        $valor    = $this->model_motorista->desativar($flag,$mensagem,$bloquear);
        $server   = $_SERVER['SERVER_NAME'];

        $this->email->from('yougomobile@yougomobile.com.br', 'You GO | Perfil '.$tipo);
        $this->email->to($email);
        $this->email->subject('You GO | Perfil '.$tipo);
        $this->email->message('
        <body style="padding: 0;margin: 0"><div style="text-align: center;width: 100%;background: #f8d509;height: 200px;margin: 0;padding: 0">
                </div>
                    <div style="width: 80%;display: table;background: #fff;margin: 0 auto;text-align: center;margin-top: -100px;border-radius: 8px;box-shadow: 0 0 2px #ccc;font-family: Arial; ">
                    <center><img src="http://'.$server.base_url().'/style/img/favicon.png" style="width: 200px;"></center>
                    <h4 style="padding: 10px">Você foi '.$tipo.' pela equipe de gerenciamento do YouGo</h4>
                        <center><h4 style="padding: 10px;margin-top: -25px;width: 80%;margin: 0 auto"><span style="font-size: 20px"> Motivo:</span> <br> '.$mensagem.'</h4></center>
                </div>
                <br>
                <br>
                </body>
        ');

        // Enviar...
        if ($this->email->send()) {

          redirect('/main/redirecionar/28/'.$flag);

        } else {
          redirect('/main/redirecionar/28/'.$flag);
        }
    }

    function ativar_painel(){
        $flag = $this->input->get('flag');
        $email = $this->input->get('email_user');
        $valor = $this->model_motorista->ativar_painel($flag);

        $server             = $_SERVER['SERVER_NAME'];
        $this->email->from('yougomobile@yougomobile.com.br', 'You GO | Perfil ativado - Parabéns você foi aprovado');
        $this->email->to($email);
        $this->email->subject('You GO | Perfil ativado - Parabéns você foi aprovado');
        $this->email->message('
        <body style="padding: 0;margin: 0"><div style="text-align: center;width: 100%;background: #f8d509;height: 200px;margin: 0;padding: 0">
                </div>
                    <div style="width: 80%;display: table;background: #fff;margin: 0 auto;text-align: center;margin-top: -100px;border-radius: 8px;box-shadow: 0 0 2px #ccc;font-family: Arial; ">
                    <center><img src="http://'.$server.base_url().'/style/img/favicon.png" style="width: 200px;"></center>
                        <h4 style="padding: 10px">Parabéns você foi ativado, já pode começar a usar seu aplicativo.</h4>
                </div>
                <br>
                <br>
                </body>
        ');

        // Enviar...
        if ($this->email->send()) {

          redirect('/main/redirecionar/28/'.$flag);

        } else {
          redirect('/main/redirecionar/28/'.$flag);
        }


    }
    function sha1()
    {
        $valor =  $this->input->post('senha_usuario');
        $dado =  sha1($valor);
        echo $dado;
    }
    public function filtrar(){

        $flag = $this->input->post();
        redirect('main/redirecionar/26/'.$flag['email_usuario'].'/'.$flag['ativo_usuario'],'refresh');

    }
    public function editar_imagem(){
        $dados = $this->input->post();
        $id_motorista = $this->input->post('id_usuario');
        $tipo = $this->input->post('tipo');

        $endereco = $_SERVER['DOCUMENT_ROOT'].base_url().'/upload/motoristas/motorista_'.$id_motorista;
        $uploaddir = $endereco.'/';

        $uploadfile = $uploaddir.$tipo;
        $destino = $uploadfile.'.png';
        move_uploaded_file($_FILES["arquivos"]["tmp_name"], $destino);
        redirect('/main/redirecionar/16');
    }


    public function bancario(){
        $dados = $this->input->post();
        print_r($dados);
    }


    public function cupons(){
            $nome_cupom             = $this->input->post('nome_cupom');

            $valor_porcentagem_cupom   = $this->input->post('valor_porcentagem_cupom');
            $valor_fixo_cupom   = $this->input->post('valor_fixo_cupom');

            /*$valor_desconto_cupom   = $this->input->post('valor_desconto_cupom');*/
            $tipo_desconto_cupom    = $this->input->post('tipo_desconto_cupom');
            $quantidade_cupom       = $this->input->post('quantidade_cupom');
            $data_inicio_cupom      = $this->input->post('data_inicio_cupom');
            $data_termino_cupom     = $this->input->post('data_fim_cupom');



            $data_inicio_corrigir = explode('/',$data_inicio_cupom);
            $data_inicio_cupom = $data_inicio_corrigir[2].'-'.$data_inicio_corrigir[1].'-'.$data_inicio_corrigir[0];

            $data_termino_corrigir = explode('/',$data_termino_cupom);
            $data_termino_cupom = $data_termino_corrigir[2].'-'.$data_termino_corrigir[1].'-'.$data_termino_corrigir[0];


            if(empty($valor_porcentagem_cupom)){
                $valor = $valor_fixo_cupom;
                $tipo = 0;
            }
            else{
                $valor = $valor_porcentagem_cupom;
                $tipo = 1;
            }


            $valor_corrida = str_replace('.', '', $valor);
            $valor_corrida_real = str_replace(',', '.', $valor_corrida);
           $dados = array(
            'nome_cupom' => $nome_cupom,
            'valor_desconto_cupom' => $valor_corrida_real,
            'tipo_desconto_cupom' => $tipo,
            'quantidade_cupom' => $quantidade_cupom,
            'data_inicio_cupom' => $data_inicio_cupom,
            'data_termino_cupom' =>$data_termino_cupom,
            'ativo_cupom' => 0
            );

            $this->model_motorista->cupons($dados);
        redirect('/main/redirecionar/24');


    }
    function cupons_ativar(){
        $status = $this->input->get('status');
        $id =$this->input->get('id');
        if($status == 0){
            $status_novo =  array('ativo_cupom' => 1);
        }
        else{
            $status_novo =  array('ativo_cupom' => 0);
        }
        $this->model_motorista->atualizar_cupom($id,$status_novo);
        redirect('/main/redirecionar/24');



    }



}
