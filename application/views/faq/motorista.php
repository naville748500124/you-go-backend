<head>

    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/theme-animate.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <script src='http://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.3/TweenMax.min.js'></script>
    <script src='http://s3-us-west-2.amazonaws.com/s.cdpn.io/16327/MorphSVGPlugin.min.js'></script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">




    <style>
        #bloco_preto{
            animation-duration: 2s;
            animation-delay: 0.0s;
            animation-name: fadeInUp;
            width: 100%;
            background: #292929 !important;
            padding: 10px 0 40px 0 !important;
            margin-top: -250px;
            height: 100%;
            display: table;
            box-shadow: 0 0  4px rgb(0,0,0,0.4);
        }

        html{
            overflow-x: hidden;
        }
        #svg_linha{
            width: 100%;
        }



        #botao_voltar{
            width: 60px; height: 60px;background: #171515;border-radius: 50%;text-align: center;color: #fff;margin-top: 20px;position: fixed;
            z-index: 999;
            transition-duration: 0.2s;
            transition-timing-function: ease-in;
            transition-property: all;
            font-size: 30px;
            margin-left: 5%;
        }
        #botao_voltar:hover{
            cursor: pointer;
        }



        @media screen and (max-width: 768px) and (min-width: 500px){
            #svg_linha{
                width: 80%;
                margin: 0 auto;
                margin-left: 10%;
                margin-top: -20px;

            }
        }
        @media screen and (max-width: 500px){
            #svg_linha{
                width: 70%;
                margin: 0 auto;
                margin-left: 15%;
                margin-top: -60px;

            }
        }
        @media screen and (max-width: 450px){
            #svg_linha{
                width: 70%;
                margin: 0 auto;
                margin-left: 15%;
                margin-top: -50px;

            }
        }
        @media screen and (max-width: 380px){
            #svg_linha{
                width: 80%;
                margin: 0 auto;
                margin-left: 15%;
                margin-top: -50px;

            }
        }
        .svg_formar svg path, svg rect {
            fill: transparent !important;
        }
        .logo {
            stroke-dasharray: 560;
            stroke-dashoffset: 560;

            animation:
                logoStroke 2.75s linear forwards,
                logoStrokeFill 0.75s cubic-bezier(0.445, 0.050, 0.550, 0.950) 2s forwards;
        }

        @keyframes logoStroke {
            to {
                stroke-dashoffset: 0;
            }
        }

        @keyframes logoStrokeFill {
            to {
                stroke:  transparent;
                fill:  #fff;
            }
        }

    </style>
    <script>
        $(document).ready(function(){
            $( "#load_geral" ).fadeOut( "slow", function() {
                $('#tudo').show();
            });
        });
    </script>

</head>



<body style="height: 100%;width: 100%;position: absolute;background: url('<?php echo base_url(); ?>style/img/bk_tudo.jpg') fixed center center no-repeat;background-size: cover ">


<!--Load para troca entre views-->
<div  id="load_geral" style="margin-top: 10%;">
    <svg class="mainSVG" viewBox="0 0 800 600" xmlns="http://www.w3.org/2000/svg">
        <defs>
            <path id="puff" d="M4.5,8.3C6,8.4,6.5,7,6.5,7s2,0.7,2.9-0.1C10,6.4,10.3,4.1,9.1,4c2-0.5,1.5-2.4-0.1-2.9c-1.1-0.3-1.8,0-1.8,0
s-1.5-1.6-3.4-1C2.5,0.5,2.1,2.3,2.1,2.3S0,2.3,0,4.4c0,1.1,1,2.1,2.2,2.1C2.2,7.9,3.5,8.2,4.5,8.3z" fill="#fff"/>
            <circle id="dot"  cx="0" cy="0" r="5" fill="#fff"/>
        </defs>

        <circle id="mainCircle" fill="none" stroke="none" stroke-width="2" stroke-miterlimit="10" cx="400" cy="300" r="130"/>
        <circle id="circlePath" fill="none" stroke="none" stroke-width="2" stroke-miterlimit="10" cx="400" cy="300" r="80"/>

        <g id="mainContainer" >
            <g id="car">
                <path id="carRot" fill="#fff"  d="M45.6,16.9l0-11.4c0-3-1.5-5.5-4.5-5.5L3.5,0C0.5,0,0,1.5,0,4.5l0,13.4c0,3,0.5,4.5,3.5,4.5l37.6,0
C44.1,22.4,45.6,19.9,45.6,16.9z M31.9,21.4l-23.3,0l2.2-2.6l14.1,0L31.9,21.4z M34.2,21c-3.8-1-7.3-3.1-7.3-3.1l0-13.4l7.3-3.1
C34.2,1.4,37.1,11.9,34.2,21z M6.9,1.5c0-0.9,2.3,3.1,2.3,3.1l0,13.4c0,0-0.7,1.5-2.3,3.1C5.8,19.3,5.1,5.8,6.9,1.5z M24.9,3.9
l-14.1,0L8.6,1.3l23.3,0L24.9,3.9z"/>
            </g>
        </g>
    </svg>
</div>

<div id="tudo" style="display: none">
    <div id="botao_voltar" onclick="voltar()">
        <p style="margin-top: 7px;" id="sair"> X </p>
    </div>
    <main>
        <!--  Parallax Section  -->
        <div class="parallax-container">
            <div class="parallax">
                <div style="position: absolute;color: #000;z-index: 99;font-size: 30px;margin-top: -25px;left: 50%;margin-left: -45%;width: 320px;fill: #fff;">

                    <div class="center-block svg_formar" style="margin: 0 auto;width: 300px;z-index: 99;margin-top: 15px;">
                        <svg id="svg_linha" class="center-block" height="380" viewBox="0 0 1000 1000" style="padding: 15px" x="0" y="0" enable-background="new 0 0 147 147" xml:space="preserve">
                             <path  class="logo"  fill="#ffffff" stroke="#fff"  stroke-width="3" stroke-miterlimit="10"  d="M439.5 66.6c-54.7.9-103.5 3.3-150 7.4l-23 2-4.3 4.7c-13.5 15-40.6 58-57.4 91-11.7 22.8-11.9 23.3-10.4 26.1.8 1.6 2.3 3.2 3.2 3.6.9.3 21.5.2 45.8-.4 24.2-.5 57.2-1.2 73.1-1.5 16-.2 47.9-.9 71-1.5 54.3-1.3 155.2-1.3 210.5 0 23.9.6 56.3 1.3 72 1.5 15.7.3 48.5 1 73 1.6l44.5 1 2.3-2.7c2.9-3.3 2.8-3.8-4.2-18.4-8-16.6-17.6-34.1-28.4-51.5-14-22.5-36-53.5-38-53.5-.4 0-10.6-.9-22.7-2-30.1-2.6-64.9-4.8-96.5-6-28.1-1-130.3-1.9-160.5-1.4zM121.6 148.6c-13 2-25.5 5.3-30.2 8-5.9 3.3-7.2 8.4-5 18.6 2.4 10.4 5 15.3 10.2 18.9 7.9 5.4 13.9 7.4 28.4 9.3 11.9 1.7 17.8 6.2 24.6 19.1 1 1.7 2.3 2 10.3 2.3l9.1.3-.1-20.8c0-26.7-1.3-49.3-2.8-52.2-2.2-4.1-6.8-5.1-21.8-5-7.6.1-17.8.7-22.7 1.5zM825.2 148.4c-6.1 2.8-6.4 5-7 42.8l-.5 33.8h18.2l3.2-6.1c5.2-9.7 11.7-14.1 22.7-15.5 13.5-1.6 24.9-5.8 30.6-11.1 3.8-3.5 6.3-9.1 7.7-16.7 1.4-8.5.7-14.1-2.2-17.2-2.7-2.8-13.9-6.5-27.4-9-12.8-2.3-41-2.9-45.3-1zM218 212.5c0 .9 20.1 31.6 29.3 44.8 8.4 12 16.7 19.7 21.4 19.7 3.2 0 2.9-1.9-1.6-8.6-12.9-19.3-49.1-60.5-49.1-55.9zM749.3 215c-6.4 5.8-17 17.9-30.5 34.9-12.8 16-17.8 23.4-17.8 26.1 0 2 4.8.9 8.8-2 5.6-4 11-11 25.9-33.5 6.9-10.5 13.4-20.1 14.3-21.5 4.9-7.2 4.6-8.9-.7-4zM862 292.9c-30 11.4-61.4 26.1-79.9 37.6-32.4 20-80.8 64.9-87.8 81.3-1.9 4.5-.9 5.5 4.3 4.7 7.5-1.1 36.3-11.7 56.5-20.8 32.8-14.8 47.7-21.6 57.2-26.1 48.8-23.2 63-30.7 65.6-35 4.5-7.3 7.2-26 5.1-35.9-1.2-6.1-4.2-10.8-6.8-10.6-.9.1-7.3 2.2-14.2 4.8zM107.6 293c-3.3 3.9-3.9 7.8-3.4 20.3.3 9 .8 11 3.6 17l3.3 6.7 31.7 15.6c17.4 8.5 32.5 15.9 33.4 16.4.9.5 6.5 3.1 12.5 5.8 5.9 2.8 14 6.4 17.8 8.2 12.2 5.6 14.7 6.7 28 12.5 22.4 9.9 48.4 19.5 52.6 19.5 5.5 0 2.2-7.1-10-21.3-19.2-22.2-54.7-51.9-79.1-65.9-20.8-12-79.7-37.8-86.1-37.8-.9 0-2.9 1.4-4.3 3zM291 363.9c0 .6.4 1.3 1 1.6 3.7 2.3 24.2 32.3 30.1 44.1 5.2 10.3 6 15.6 3 18.8-1.3 1.3-1.9 2.6-1.4 2.9.4.2 7.3 2.1 15.3 4.1 8 2.1 19.5 5.1 25.5 6.7 34.9 9.2 83.7 18.2 113.5 20.9 31.6 2.9 84.4-5.2 159.4-24.6 30.7-7.9 28.7-7.2 25.9-9.1-6.7-4.5 1.1-22.5 21.9-50.3 5.1-6.9 9.9-13.4 10.7-14.4 1.8-2.5 2.3-2.6-19.9 5.9-29.1 11.2-61.7 21.3-88.5 27.5-4.9 1.1-9.9 2.3-11 2.6-1.1.2-6.9 1.3-13 2.4-6 1.1-13 2.4-15.5 2.9-9.6 2-37.7 4.2-54.2 4.1-16.5 0-41.2-1.9-52.2-4.1-2.8-.6-6-1.2-7.1-1.4-31-5.2-82.9-19.9-128.6-36.6-14.6-5.3-14.9-5.4-14.9-4zM246.2 574.7l.3 5.8h513l.3-5.8.3-5.7H245.9l.3 5.7zM37.1 648.9c-1 .6 1.8 4.4 11.1 14.7 16.1 18 59.3 66.8 67.7 76.4 3.6 4.1 6.8 7.7 7.1 8 .3.3 2.2 2.4 4.3 4.8l3.7 4.2v59h53v-29.4c0-28.7 0-29.4 2.2-32.2 1.2-1.6 2.5-3.1 2.8-3.4.3-.3 2.7-3.2 5.5-6.5 2.7-3.3 5.9-7.1 7.2-8.5 18.3-20.7 72.9-86.6 72.2-87.1-.6-.3-15.3-.7-32.8-.8l-31.8-.2-15.4 19.8c-8.5 10.9-20.1 26-25.9 33.5-5.8 7.6-10.8 13.7-11.1 13.8-.5 0-24.3-28.9-37.4-45.4-1.6-2.1-3.7-4.7-4.7-5.8-.9-1-3.1-3.8-5-6-1.8-2.3-4.3-5.3-5.5-6.7l-2.1-2.6-31.9-.2c-17.5-.2-32.4.1-33.2.6zM656 648.7c-14.4 1-28.3 2.7-34.9 4.4-19.5 5-26.7 16.1-29.8 45.9-1 9.9-1 58.3 0 68.5 2.4 23 9.1 36.3 21.2 41.6 6.4 2.8 20.3 5.6 30.3 6 15.1.7 98.4.4 105.7-.3 27.2-2.8 39.2-11.5 43.4-31.8 1.1-5.5 3.1-34.1 3.1-45.8V731H685v20H772.3l-.7 10.8c-2 30.9-4.9 32.6-57.6 33.8-29.9.7-73.4-.9-82.4-3-12-2.8-15.2-7.9-17.3-27.1-.9-8.4-1-62.3-.1-68 3-19.7 6.5-23.5 23.8-26.6 10.4-1.9 96.6-2 109.5-.1 16.9 2.5 21 6.5 22.2 22.1l.6 8.2 11.5-.3 11.6-.3-.2-6.5c-.6-15.3-4-23.8-13-32.1-9.7-9-17.9-11.2-49.2-12.8-14.9-.8-65.7-1.1-75-.4z"/>
                            <path class="logo" fill="#ffffff" stroke="#fff" stroke-width="3" stroke-miterlimit="10"   d="M277.1 695.1c-5.1.4-10.9 1.1-13 1.5-19.2 4-28.6 10.9-34.2 25.1-2.1 5.3-2.3 7.8-2.7 28.7-.6 33.8 1.6 45 10.5 54 11.6 11.8 24.4 14.3 71.8 14.4 47.7.2 63.4-2.7 73.7-13.5 8.8-9.2 11.6-22.3 11.3-51.3-.4-25.4-2-33.6-8.2-42.2-6.7-9.3-20.8-15-41.3-16.7-13.1-1.2-53.8-1.2-67.9 0zm59.5 35.3c7.8 2.4 11 6.3 12.5 15 1.5 9.1.6 23.3-2 29-3.6 8.3-12.3 10.8-37.1 10.8-24.5-.1-34.6-3.5-37.1-12.5-1.5-5.4-1.5-25 0-30.4 1.6-5.9 5.3-9.9 10.9-11.7 7.1-2.3 9.2-2.4 29.2-2.1 13.7.2 19.8.7 23.6 1.9zM410.4 698.5c-1.1 2.7.3 76.6 1.5 83.4a43.8 43.8 0 0 0 11.7 22.4c10.4 10.5 25.1 14.6 52.1 14.5 28.3-.1 39.7-3.8 48.6-15.6l4.2-5.6.3 9.2.3 9.2H571l-.2-59.3-.3-59.2-21.6-.3-21.6-.2-.6 33.2c-.4 18.3-1.1 35.1-1.6 37.2-1.3 5.4-5.9 11.5-10.1 13.7-5.4 2.8-13.3 4-26.2 4-14.9.1-21.9-1.3-26.4-5.3-6.9-6.1-6.8-5.4-7.4-45.8l-.5-36.5-21.8-.3c-19-.2-21.8 0-22.3 1.3zM860.2 699c-20 1.7-29 4.9-36.6 13-6.2 6.7-9.2 13.1-11.5 25.1-3.7 19.3-1.7 44.6 4.5 57.1 6.4 12.7 17.9 19.5 36.9 21.7 11.5 1.4 57.5 1.4 68.5.1 29.4-3.6 41.5-20.6 41.5-58-.1-22.7-4.3-36.8-13.7-46.3-6.1-6.1-13.4-9.5-24.6-11.3-10.6-1.7-50.8-2.6-65-1.4zm33.1 16.1c34.9.3 44.8 5.2 48.7 24.4 1.4 6.5 1.7 26.4.6 33.5-1.2 7.7-3.7 14-6.9 17.7-7 8-17.3 10.2-47.4 10.1-45.5-.1-55.2-5.4-57.3-31.7-1.3-16.2.1-31.9 3.6-38.5 3.6-7 7.7-10.3 15.4-12.6 5.6-1.7 23.4-4.1 25.1-3.4.3.2 8.5.4 18.2.5zM223 871.2c-.1 1 0 48.1 0 51.1 0 .5 1.5.7 3.3.5l3.2-.3.3-22.8c.2-21.4.3-22.7 2.1-22.7 1 0 2.1.6 2.4 1.4.3.8 3 5.2 6 9.8 3 4.5 9.3 14.3 14 21.5 7.8 12 8.9 13.3 11.5 13.3 2.7 0 3.7-1.2 10.8-12.3 4.3-6.7 10.7-16.7 14.2-22.2 6.5-10.2 8.8-12.8 10.3-11.9.5.3.9 10.9.9 23.5v23l3.3-.3 3.2-.3v-52l-7.2-.3c-7.2-.3-7.3-.3-9.2 2.8-10.9 18-25.8 40.9-26.4 40.7-.4-.1-7-9.9-14.6-21.7l-13.8-21.5-7.2-.3c-5.3-.2-7.1 0-7.1 1zM359 871c-4.2 1-7.5 4.7-8.9 9.8-.6 2-1.1 9.4-1.1 16.4 0 14.6 1.3 19.5 6.4 23.1 3 2.1 4.1 2.2 24.9 2.4 34.1.4 35.2-.4 35.2-26.2 0-13-.3-15.5-2-19-3.3-6.5-5.1-7-29.5-7.2-11.8-.1-23.1.2-25 .7zm45.2 6.8c1.4.6 2.9 1.6 3.3 2.3.4.6.7 7.6.7 15.6-.1 17.1-.8 18.8-7.4 19.8-2.4.4-12.3.5-22.1.3-17-.3-18-.4-19.7-2.5-3.7-4.6-3.7-29 0-33.6 1.3-1.6 3.4-2.3 7.6-2.8 8.2-1 34.3-.3 37.6.9zM456 896.5v26.6l25.1-.1c33.5-.2 35.9-1.2 35.9-15 0-6.9-1.3-10.4-4.6-11.9l-2.4-1 2.5-2.7c2.1-2.3 2.5-3.7 2.5-8.8 0-6.9-1.4-9.7-6-12.1-2.3-1.2-7.7-1.5-28-1.5h-25v26.5zm50.1-18.8c.6.6 1.4 3.1 1.6 5.6.4 3.6.1 4.9-1.6 6.6-2 2-3.1 2.1-22.6 2.1H463v-16.2l21 .4c14.1.2 21.3.7 22.1 1.5zm-.6 22.3c3.2 1.2 4.8 5.3 4 10.2-1 5.9-2.3 6.2-25.5 6.3h-20.5l-.3-8.8-.3-8.7h20c11 0 21.1.4 22.6 1zM558 896.5v26.6l3.3-.3 3.2-.3v-52l-3.2-.3-3.3-.3v26.6zM606 896.4V923h50v-3c0-1.7-.5-3-1.2-3s-10.2-.2-21.1-.3l-19.9-.2-.1-23-.2-23-3.7-.3-3.8-.3v26.5zM693 896.5V923h52v-6l-22.2-.2-22.3-.3-.3-8.7-.3-8.8 21.3-.2 21.3-.3.3-3.3.3-3.2H700v-16h45v-6h-52v26.5z"/>
                         </svg>
                    </div>
                </div>
                <img src="<?php echo base_url(); ?>style/img/bk_tudo.jpg"></div>
        </div>
        <div  id="bloco_preto">
            <div style="width: 100%;margin: 0 auto;">
                <p  style="width: 80%;margin: 0 auto;text-align: center;font-family: Arial;color: #fff;font-size: 25px;margin-top: 35px">Central de ajuda do Motorista </p>
                <div id="lugar"></div>
            </div>
        </div>
</div>
</body>



<script>
    function getId(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }

    $(document).ready(function () {

        var videoId = getId('<?= $links["link_youtube_motorista"]; ?>');

        var iframeMarkup = ' <iframe style="width: 95%;margin-left: 2.5%;height: 300px;margin-top: 40px;border-radius: 8px" src="https://www.youtube.com/embed/'
            + videoId + '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
        $('#lugar').html(iframeMarkup);

    });

</script>

<script>
    /*$(document).ready(function () {*/
    function voltar(){
        $('#botao_voltar').css('margin-top','50%');
        $('#botao_voltar').css('margin-left','40%');
        $('#sair').css('display','none');
        setTimeout(function () {
            $('#botao_voltar').css('width','150%');
            $('#botao_voltar').css('height','150%');
            $('#botao_voltar').css('margin-top','-45%');
            $('#botao_voltar').css('margin-left','-25%');
        },400);
        setTimeout(function () {
            retornar();
        },850);
    }


    function retornar(){
        $("#status").html("...");

        console.log('Pagamento enviado.')

        <?php
        $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
        $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
        $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
        $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
        $webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

        if($iPod || $iPhone || $iPad){

            echo "window.location = 'megamil.net.webview-avancado://voltar';";


        } else if($Android){

            echo 'Android.exibirAvisoAndroid();
                $("#status").html($("#status").html()+" Chamado via Android");';

        }  else {

        }
        ?>
    }


    /*});*/
</script>
<script>
    TweenMax.set('#circlePath', {
        attr: {
            r: document.querySelector('#mainCircle').getAttribute('r')
        }
    })
    MorphSVGPlugin.convertToPath('#circlePath');

    var xmlns = "http://www.w3.org/2000/svg",
        xlinkns = "http://www.w3.org/1999/xlink",
        select = function(s) {
            return document.querySelector(s);
        },
        selectAll = function(s) {
            return document.querySelectorAll(s);
        },
        mainCircle = select('#mainCircle'),
        mainContainer = select('#mainContainer'),
        car = select('#car'),
        mainSVG = select('.mainSVG'),
        mainCircleRadius = Number(mainCircle.getAttribute('r')),
        //radius = mainCircleRadius,
        numDots = mainCircleRadius / 2,
        step = 360 / numDots,
        dotMin = 0,
        circlePath = select('#circlePath')

    //
    //mainSVG.appendChild(circlePath);
    TweenMax.set('svg', {
        visibility: 'visible'
    })
    TweenMax.set([car], {
        transformOrigin: '50% 50%'
    })
    TweenMax.set('#carRot', {
        transformOrigin: '0% 0%',
        rotation:30
    })

    var circleBezier = MorphSVGPlugin.pathDataToBezier(circlePath.getAttribute('d'), {
        offsetX: -20,
        offsetY: -5
    })



    //console.log(circlePath)
    var mainTl = new TimelineMax();

    function makeDots() {
        var d, angle, tl;
        for (var i = 0; i < numDots; i++) {

            d = select('#puff').cloneNode(true);
            mainContainer.appendChild(d);
            angle = step * i;
            TweenMax.set(d, {
                //attr: {
                x: (Math.cos(angle * Math.PI / 180) * mainCircleRadius) + 400,
                y: (Math.sin(angle * Math.PI / 180) * mainCircleRadius) + 300,
                rotation: Math.random() * 360,
                transformOrigin: '50% 50%'
                //}
            })

            tl = new TimelineMax({
                repeat: -1
            });
            tl
                .from(d, 0.2, {
                    scale: 0,
                    ease: Power4.easeIn
                })
                .to(d, 1.8, {
                    scale: Math.random() + 2,
                    alpha: 0,
                    ease: Power4.easeOut
                })

            mainTl.add(tl, i / (numDots / tl.duration()))
        }
        var carTl = new TimelineMax({
            repeat: -1
        });
        carTl.to(car, tl.duration(), {
            bezier: {
                type: "cubic",
                values: circleBezier,
                autoRotate: true
            },
            ease: Linear.easeNone
        })
        mainTl.add(carTl, 0.05)
    }

    makeDots();
    mainTl.time(120);
    TweenMax.to(mainContainer, 20, {
        rotation: -360,
        svgOrigin: '400 300',
        repeat: -1,
        ease: Linear.easeNone
    });
    mainTl.timeScale(1.1)
</script>