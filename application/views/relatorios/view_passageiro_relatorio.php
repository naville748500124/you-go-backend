
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/cupons.css" >
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/theme-animate.css" >
    <link type="text/css" href="<?php echo base_url(); ?>style/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
    <meta name="theme-color" content="#292929">
</head>




<div id="planilha" class="tm30 wow fadeInDown" data-wow-duration="1s"  style="width: 400px;height: 250px;position: absolute;margin-left: 50%;left: -200px;margin-top: 10%;z-index: 999;display: none">
    <div id="box_ex">
        <a class="button" onclick="fechar()" id="close-btn" href="#"></a>
        <!-- <a class="button" onclick="fechar()" id="minimize-btn" href="#"></a> -->
        <h1 class="text animated fadeInDown" style="text-align: center;color: #fff;margin-top: 80px;">Planilha</h1>
        <div class="loading">
            <div class="dot" id="dot1"></div>
            <div class="dot" id="dot2"></div>
            <div class="dot" id="dot3"></div>
            <div class="dot" id="dot4"></div>
            <div class="dot" id="dot5"></div>
        </div>
        <h4 class="text" id="starting-txt" style="color: #fff;"> Exportando...</h4>
    </div>
</div>
<script>
    function  planilha() {
        $('#planilha').css('display','block');
        setTimeout(function(){ 
            window.open('http://<?php echo $_SERVER['SERVER_NAME'].base_url(); ?>Controller_adm/excel_passageiro_relatorio','_blank');
            }, 5000);
        setTimeout(function(){ 
                fechar();
            }, 6000);

    }
    function  fechar() {
        $('#planilha').css('display','none');
    }
</script>


<div>
    <div class="col-lg-11">
        <h1 class="fade_1"><i class="fa fa-2x fa-user lado_topo" data-wow-duration="2s" data-wow-delay="0.0s" > </i>  Passageiros - Relatório</h1>
    </div>  


    <div class="col-lg-1">
        <h1 class="fade_1"><i onclick="planilha()" class="fas fa-file-excel lado_topo" style="margin-top: 50px;text-align: right" data-wow-duration="2s" data-wow-delay="0.0s" > </i></h1>
    </div>

    <div class="lado_direito" style="width: 100%;height: 2px;background: #f8d509;margin-top: 30px;overflow-x: hidden"></div>
</div>

<div class="lado_direito" style="width: 100%;height: 2px;background: transparent;margin-top: 40px;margin-bottom: 10px"></div>

<table class="table table-bordered table-hover" align="center">
    <thead style="background: #f1f1f1">
    <tr>
        <th>ID</th>
        <th>Nome</th>
        <th>E-mail</th>
        <th>Telefone</th>
        <th>CPF</th>
        <th>Status</th>

        <th>Total de corridas</th>
        <!--<th>Corridas realizadas</th>-->
        <th>Corridas canceladas</th>
        <th>Custo médio de corridas</th>
        <th>Média de avaliação</th>
        <th>Cidade mais chamada</th>

    </tr>
    </thead>
    <tbody>
    <?php foreach($dados_iniciais as $pas) { ?>

        <tr>
            <th style="text-align: center;padding-top: 20px;"><?php echo $pas['id_usuario']; ?></th>
            <th style="text-align: center;padding-top: 20px;"><?php echo $pas['nome_usuario']; ?></th>
            <th style="text-align: center;padding-top: 20px;"><?php echo $pas['email_usuario']; ?></th>
            <td class="mascara_cel" style="text-align: center;padding-top: 20px;white-space: nowrap"><?php echo $pas['celular_usuario']; ?></td>
            <td class="mascara_cpf" style="text-align: center;padding-top: 20px;white-space: nowrap"><?php echo $pas['cpf_usuario']; ?></td>
            <th style="text-align: center">
                <p style="margin-top: 13px;text-align: center">
                    <?php if($pas['ativo_usuario'] == 1){ ?>
                        <span style="color: #7da957">Ativado</span>
                    <?php }else{ ?>
                        <span style="color: #db2d49">Inativo</span>
                    <?php } ?>
                </p>
            </th>
            <th style="text-align: center;padding-top: 20px;"><?php echo $pas['total_corridas']; ?></th>
            <!--<th style="text-align: center"><?php /*echo $mot['total_corridas']-$mot['total_corridas_canceladas']; */?></th>-->
            <th style="text-align: center;padding-top: 20px;color: #ff0000"><?php echo $pas['total_corridas_canceladas']; ?></th>
            <th style="text-align: center;padding-top: 20px;">R$ <?php echo $pas['custo_medio_corridas']; ?></th>
            <th style="text-align: center;padding-top: 20px;"><?php echo $pas['media_avaliacoes']; ?></th>
            <th style="text-align: center;padding-top: 20px;"><?php echo $pas['cidade_mais_chamada']; ?></th>
        </tr>



    <?php } ?>


</tbody>
</table>





<style>
    #box_ex {
        width: 440px;
        height: 248px;
        margin: auto;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        position: absolute;
        background-color: #292929;
    }
    #logo {
        margin: 13px 0 0 13px;
        float: left;
    }
    #logo img {
        width: 18px;
        margin: auto;
        float: left;
        -webkit-filter: grayscale(100%) brightness(5);
        filter: grayscale(100%) brightness(5);
    }
    #logo h6 {
        color: white;
        font-size: 16px;
        font-weight: 200;
        font-family: Segoe UI;
        letter-spacing: 0px;
        margin: 0px auto auto 5px;
        float: left;
    }
    #box h1 {
        color: white !important;
        font-size: 65px;
        letter-spacing: -2px;
        margin: 67px 0 0 138px;
    }
    #box .text {
        color: #9fd5b7;
        font-weight: 400;
        font-family: Segoe UI;
    }
    #box h4 {
        font-size: 12px;
        font-weight: 400;
        opacity: 50%;
    }
    #starting-txt {
        margin: 60px 12px 0;
        float: left;
    }
    #author-txt {
        margin: 60px 17px 0;
        float: right;
    }
    #author-txt a {
        color: inherit;
        text-decoration: none;
    }
    .text img {
        width: 15px;
    }
    .button {
        width: 10px;
        height: 10px;
        right: 23px;
        top: 12px;
        opacity: 1;
        position: absolute;
    }
    .button:hover {
        opacity: 0.3;
    }
    #close-btn:before,
    #close-btn:after {
        width: 2px;
        height: 12px;
        left: 15px;
        content: ' ';
        background-color: white;
        position: absolute;
    }
    #close-btn:before {
        transform: rotate(45deg);
    }
    #close-btn:after {
        transform: rotate(-45deg);
    }
    #minimize-btn {
        width: 10px;
        height: 2px;
        right: 42px;
        top: 20px;
        content: ' ';
        background-color: white;
        position: absolute;
        float: right;
    }
    .loading {
        height: 20px;
        /* border: 1px solid #ddd; */
    }
    .dot {
        width: 4px;
        height: 4px;
        top: 160px;
        left: -10%;
        margin: auto;
        border-radius: 5px;
        background: white;
        position: absolute;
    }
    #dot1 {
        animation: dotslide 2.8s infinite cubic-bezier(0.2, .8, .8, 0.2);
    }
    #dot2 {
        animation: dotslide 2.8s .2s infinite cubic-bezier(0.2, .8, .8, 0.2);
    }
    #dot3 {
        animation: dotslide 2.8s .4s infinite cubic-bezier(0.2, .8, .8, 0.2);
    }
    #dot4 {
        animation: dotslide 2.8s .6s infinite cubic-bezier(0.2, .8, .8, 0.2);
    }
    #dot5 {
        animation: dotslide 2.8s .8s infinite cubic-bezier(0.2, .8, .8, 0.2);
    }
    @keyframes dotslide {
        0% {
            left: -20%;
        }
        100% {
            left: 110%;
        }
    }
</style>

