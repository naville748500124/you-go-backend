<head>
    <meta name="viewport" content="width=device-width, user-scalable=no">

    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/motoristas_todos.css" >
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/theme-animate.css" >
    <link type="text/css" href="<?php echo base_url(); ?>style/css/bootstrap.min.css" rel="stylesheet" />
    <script src="<?php echo base_url(); ?>style/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>style/js/script_efeito.js"></script>
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">

</head>

<?php
$dominio= $_SERVER['HTTP_HOST'];
$url = "http://" . $dominio;
header('location: '.$url.'/.'.base_url().'./main/redirecionar/16');
?>

<style>
    .quadrado_cima{
        width: 100%;
        margin-top: 30px;
        height: 140px;
        display: table;
    }
    .quadrado{
        background: #fff;border-radius: 5px;box-shadow: 0 0 8px #ccc;height:  280px;text-align: center;

        transition-duration: 0.4s;
        transition-timing-function: ease-in;
        transition-property: all;
    }
    .circulo{
        text-align: center;margin: 0 auto;border-radius: 50%;background: #f8d509;width: 120px;height: 120px;border: 10px solid #fff;box-shadow: 3px 0 8px #ccc;
        color: #292929;
        transition-duration: 0.4s;
        transition-timing-function: ease-in;
        transition-property: all;
    }
    .quadrado:hover .circulo{
        margin: 0 auto;margin-top: -15px;background: #666;border: 10px solid #fff;
        box-shadow: 3px 0 8px #ccc;
        color: #fff;
        cursor: pointer;
        transition-duration: 0.4s;
        transition-timing-function: ease-in;
        transition-property: all;
    }
    .quadrado:hover{
        cursor: pointer;
        box-shadow: 0 0 35px #ccc;
        transition-duration: 0.2s;
        transition-timing-function: ease-in;
        transition-property: all;


    .circulo:hover{
        margin: 0 auto;margin-top: -15px;background: #666;border: 10px solid #fff;box-shadow: 3px 0 8px #ccc;
        color: #fff;
        cursor: pointer;
        transition-duration: 0.4s;
        transition-timing-function: ease-in;
        transition-property: all;
    }
</style>



<div id="tudo" style="margin-top: 180px">


    <div class="hidden-lg hidden-md hidden-sm col-xs-12">&nbsp;</div>

    <a href="">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="quadrado col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="quadrado_cima">
                <h1 class="circulo" >
                    <i class="fas fa-user fa-2x" style="margin-top: 15px;"></i>
                </h1>
            </div>
            <div class="quadrado_baixo">
                <h3 style="font-size: 20px;margin-top: -5px;font-weight: bolder">Perfil</h3>
                <h4  style="font-size: 18px;color: #8c8c8c;margin-top: 15px">Veja suas informações pessoais e de <br> seu carro   já cadastrados no APP.</h4>
            </div>
        </div>
    </div>
    </a>

    <div class="hidden-lg hidden-md hidden-sm col-xs-12">&nbsp;</div>

    <a href="">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="quadrado col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="quadrado_cima">
                    <h1 class="circulo" >
                        <i class="fas fa-flag fa-2x " style="margin-top: 15px;"></i>
                    </h1>
                </div>
                <div class="quadrado_baixo">
                    <h3 style="font-size: 20px;margin-top: -5px;font-weight: bolder">CORRIDAS</h3>
                    <h4  style="font-size: 18px;color: #8c8c8c;margin-top: 15px">Veja sua lista de corridas <br> relizadas no APP.</h4>
                </div>
            </div>
        </div>
    </a>

    <a href="">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="quadrado col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="quadrado_cima">
                    <h1 class="circulo" >
                        <i class="fas fa-phone fa-2x " style="margin-top: 15px;"></i>
                    </h1>
                </div>
                <div class="quadrado_baixo">
                    <h3 style="font-size: 20px;margin-top: -5px;font-weight: bolder">CONTATO</h3>
                    <h4  style="font-size: 18px;color: #8c8c8c;margin-top: 15px">Entre em contato com <br> a YouGO.</h4>
                </div>
            </div>
        </div>
    </a>





</div>