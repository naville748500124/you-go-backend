<head>
    <meta name="viewport" content="width=device-width, user-scalable=no">

    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/theme-animate.css" >
    <link type="text/css" href="<?php echo base_url(); ?>style/css/bootstrap.min.css" rel="stylesheet" />
    <script src="<?php echo base_url(); ?>style/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>style/js/script_efeito.js"></script>
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">

</head>


<style>


    ::-webkit-scrollbar-track {
        background-color: transparent;
    }
    ::-webkit-scrollbar {
        width: 5px;
        background: transparent;
    }
    ::-webkit-scrollbar-thumb  {
        border-radius: 15px;
        background: #f8d509;
    }

    #foto{
        width: 180px;display: table; height: 180px; margin: 0 auto;border-radius: 50%;box-shadow: 0 0 2px #fff;
        margin-top: 20px;
    }
    #usuario{
        margin: 0 auto;margin-top: 40px;
        display: table;
    }
    .nome{
        text-align: center;margin-top: 20px;font-size: 14px;font-weight: bolder;
    }
    .dados{
        text-align: center;margin-top: -10px;font-size: 14px;font-weight: bolder;
    }
    #editar{
        width: 80%;margin-top: -5px; background-color: #292929;border-color: #000;
    }

    @media screen and (max-width: 1199px){

        #foto{
            width: 130px; height: 130px; margin: 0 auto;border-radius: 50%;box-shadow: 0 0 2px #000;
        }

        #usuario{
            margin-top: 50px; height: 340px;
        }
    }
    .inp_classe{
        width: 100%;
        margin: 0 auto;
        margin-top: 15px;
        padding-left: 10px;
        background: transparent;
        color: #f8d509;
        border: none;
        border-bottom: 1px solid #fff;
    }
    .inp_classe:focus{
        border: none;
        border-bottom: 1px solid #ccc;
        box-shadow: none;
    }
    textarea:focus, input:focus, select:focus {
        border-bottom: 1px solid #f8d509;
        outline: 1px;
        color: #fff;
    }
    .botao_alt {
        opacity: .80;
        background: #292929;
        border: 2px solid #292929;
        margin-top: 10px;
        padding: 11px 45px;
        color: #f8d509;
        position: relative;
        overflow: hidden;
        cursor: pointer;
    }

    .botao_alt:hover {
        opacity: 1;
        transition-duration: 0.3s;
        transition-timing-function: ease-in;
        transition-property: all;
    }
    .botao_alt span {
        position: relative;
        z-index: 100;
        font-size: 16px;
    }
   /* .botao_alt:before, .botao_alt:after {
        content: '';
        position: absolute;
        display: block;
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
    }
    .botao_alt:before {
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0 , 0);
        background-color: #000;
        color: #000;
        border: 1px solid #000;
        -webkit-transition: -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19), -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
    }
    .botao_alt:after {
        background-color: #000;
        color: #000;
        border: 1px solid #000;
        -webkit-transform: translate3d(100%, 0, 0);
        transform: translate3d(100%, 0, 0);
        -webkit-transition: -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62), -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
    }
    .botao_alt:hover:before {
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
    }
    .botao_alt:hover:after {
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);

    }
*/



    .botao_atv {
        opacity: .80;
        background: #f8d509;
        border: 2px solid #f8d509;
        margin-top: 10px;
        padding: 8px 45px;
        color: #000;
        position: relative;
        overflow: hidden;
        cursor: pointer;
    }

    .botao_atv:hover {
        opacity: 1;
        transition-duration: 0.3s;
        transition-timing-function: ease-in;
        transition-property: all;
    }
    .botao_atv span {
        position: relative;
        z-index: 100;
        font-size: 16px;
    }
    .botao_atv:before, .botao_atv:after {
        content: '';
        position: absolute;
        display: block;
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
    }
    .botao_atv:before {
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0 , 0);
        background-color: #EAC009;
        color: #EAC009;
        border: 1px solid #EAC009;
        -webkit-transition: -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19), -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
    }
    .botao_atv:after {
        background-color: #EAC009;
        color: #EAC009;
        border: 1px solid #EAC009;
        -webkit-transform: translate3d(100%, 0, 0);
        transform: translate3d(100%, 0, 0);
        -webkit-transition: -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62), -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
    }
    .botao_atv:hover:before {
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
    }
    .botao_atv:hover:after {
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);

    }



    .botao_datv {
        opacity: .80;
        background: #ccc;
        border: 2px solid #ccc;
        margin-top: 10px;
        padding: 8px 45px;
        color: #000;
        position: relative;
        overflow: hidden;
        cursor: pointer;
    }

    .botao_datv:hover {
        color: #fff;
        opacity: 1;
        transition-duration: 0.3s;
        transition-timing-function: ease-in;
        transition-property: all;
    }
    .botao_datv span {
        position: relative;
        z-index: 100;
        font-size: 16px;
    }
    .botao_datv:before, .botao_datv:after {
        content: '';
        position: absolute;
        display: block;
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
    }
    .botao_datv:before {
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0 , 0);
        background-color: #666;
        color: #000;
        border: 1px solid #666;
        -webkit-transition: -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19), -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
    }
    .botao_datv:after {
        background-color: #666;
        color: #000;
        border: 1px solid #666;
        -webkit-transform: translate3d(100%, 0, 0);
        transform: translate3d(100%, 0, 0);
        -webkit-transition: -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62), -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
    }
    .botao_datv:hover:before {
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
    }
    .botao_datv:hover:after {
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);

    }

    .select2-container--default .select2-selection--single{
        background: transparent !important;
        background-color: transparent !important;
        border: none;
        border-bottom: 1px solid #fff;
        border-radius: 0;
        margin-top: 10px;
    }
    input[type='search'] {
        padding: 2px 10px;
        border-radius: 0px;
        background: 0;
        border: none;
        color: #fff !important;
        border-bottom: 1px solid #292929;
        visibility: visible;
        animation-duration: 0.5s;
        animation-delay: 0s;
        animation-name: fadeInUp;
        margin-bottom: 10px;
    }
    .select2-dropdown {
        background-color: #292929;
        color: #fff;
        border: 1px solid #000;
        border-radius: 4px;
        box-sizing: border-box;
        display: block;
        position: absolute;
        left: -100000px;
        width: 100%;
        z-index: 1051;
    }
    .select2-container--default .select2-results__option--highlighted[aria-selected] {
        background-color: #f8d509;
        color: #fff;
    }
    .select2-container--default .select2-results__option[aria-selected=true] {
        background-color: #171515;
        color: #f8d509;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        color: #f8d509 !important;
    }




    ::-webkit-input-placeholder {
        color: #ccc;
    }

    :-moz-placeholder { /* Firefox 18- */
        color: #ccc;
    }

    ::-moz-placeholder {  /* Firefox 19+ */
        color: #ccc;
    }

    :-ms-input-placeholder {
        color: #ccc;
    }



    .voltar{
        text-align: center;border-radius: 50%;background: #f8d509;width: 80px;height: 80px;border: 10px solid #fff;box-shadow: 3px 0 8px #ccc;
        color: #fff;
        transition-duration: 0.4s;
        transition-timing-function: ease-in;
        transition-property: all;
    }
    .voltar_pessoal{
        text-align: center;
        border-radius: 50%;
        width: 80px;
        height: 80px;
        border: 10px solid #fff;
        color: #fff;
        box-shadow: none;
        margin-top: -50px;
        position: absolute;
        margin-left: 73%;
        background: #666;
        z-index: 99;
        transition-duration: 0.4s;
        transition-timing-function: ease-in;
        transition-property: all;
    }
    .voltar:hover{
        background: #666;border: 10px solid #fff;
        box-shadow: 3px 0 8px #ccc;
        color: #fff;
        cursor: pointer;
        transition-duration: 0.3s;
        transition-timing-function: ease-in;
        transition-property: all;
    }
    .docs{
        width: 100%;
    }
    .docs:hover{
        opacity: .60;
        cursor: pointer;
    }
    #carro{
        display: none;
    }

    ::-webkit-scrollbar-track {
        background-color: transparent;
    }
    ::-webkit-scrollbar {
        width: 0px;
        background: transparent;
    }
    ::-webkit-scrollbar-thumb  {
        border-radius: 15px;
        background: #f8d509;
    }
</style>






<script>
    function perfil() {
        sair_modal();
        $('#preto').css('display', 'block');
        $('#preto').animate({'opacity':'.60'}, 1200, 'linear');
        $('#perfil').css('display','block');
    }
    function veiculo_1() {
        sair_modal();
        $('#preto').css('display', 'block');
        $('#preto').animate({'opacity':'.60'}, 1200, 'linear');
        $('#veiculo_1').css('display','block');
    }
    function veiculo_2() {
        sair_modal();
        $('#preto').css('display', 'block');
        $('#preto').animate({'opacity':'.60'}, 1200, 'linear');
        $('#veiculo_2').css('display','block');
    }
    function veiculo_3() {
        sair_modal();
        $('#preto').css('display', 'block');
        $('#preto').animate({'opacity':'.60'}, 1200, 'linear');
        $('#veiculo_3').css('display','block');
    }
    function cnh() {
        sair_modal();
        $('#preto').css('display', 'block');
        $('#preto').animate({'opacity':'.60'}, 1200, 'linear');
        $('#cnh').css('display','block');
    }
    function seguro() {
        sair_modal();
        $('#preto').css('display', 'block');
        $('#preto').animate({'opacity':'.60'}, 1200, 'linear');
        $('#seguro').css('display','block');
    }
    function comprovante() {
        sair_modal();
        $('#preto').css('display', 'block');
        $('#preto').animate({'opacity':'.60'}, 1200, 'linear');
        $('#comprovante').css('display','block');
    }
    function documento() {
        sair_modal();
        $('#preto').css('display', 'block');
        $('#preto').animate({'opacity':'.60'}, 1200, 'linear');
        $('#documento').css('display','block');
    }
    function rg() {
        sair_modal();
        $('#preto').css('display', 'block');
        $('#preto').animate({'opacity':'.60'}, 1200, 'linear');
        $('#rg').css('display','block');
    }
    function antecedentes() {
        sair_modal();
        $('#preto').css('display', 'block');
        $('#preto').animate({'opacity':'.60'}, 1200, 'linear');
        $('#antecedentes').css('display','block');
    }
    function sair_modal() {
        $('#perfil').css('display','none');
        $('#calendario').css('display','none');
        $('#veiculo_1').css('display','none');
        $('#veiculo_2').css('display','none');
        $('#veiculo_3').css('display','none');
        $('#cnh').css('display','none');
        $('#seguro').css('display','none');
        $('#comprovante').css('display','none');
        $('#documento').css('display','none');
        $('#rg').css('display','none');
        $('#antecedentes').css('display','none');

        $('#preto').css('display', 'none');
        $('#preto').animate({'opacity':'0'}, 10, 'linear');
    }
</script>

<style>
    #preto{
        margin-top: 50px;
    }
    .fotos_mostrar{
        width: 1000px;height: 450px;position: fixed;margin-left: 50%;left: -500px;margin-top: 180px;z-index: 999;
    }
    @media screen and (max-width: 1024px){
        .fotos_mostrar{
            width: 95%;
            margin-left: 0;
            left: 2.5%;
        }
    }
    .fechar_x{
        font-size: 20px;color: #fff;
        margin-top: -53px;
        padding: 33px;
        margin-right: -60px;
        font-weight: bolder;
        float: right;
    }
    #perfil{
        display: none;
    }
    #veiculo_1{
        display: none;
    }
    #veiculo_2{
        display: none;
    }
    #veiculo_3{
        display: none;
    }
    #cnh{
        display: none;
    }
    #seguro{
        display: none;
    }
    #comprovante{
        display: none;
    }
    #documento{
        display: none;
    }
    #rg{
        display: none;
    }
    #antecedentes{
        display: none;
    }
    .arrumar_botao{
        margin-right: -15px;
        margin-top: -35px;
        float: right;
    }
</style>
        <div class="fotos_mostrar" id="perfil"    style="background: url('<?php echo base_url(); ?>upload/motoristas/motorista_<?php echo $dados_iniciais['dados']['id_usuario']; ?>/motorista.png') center center no-repeat;background-size: 80%; ">
            <span class="fechar_x voltar" style="box-shadow: none" onclick="sair_modal()"><span class="arrumar_botao"><i class="fas fa-2x fa-times"  style="margin-top: 15px"></i></span></span>
        </div>
        <div class="fotos_mostrar" id="veiculo_1" style="background: url('<?php echo base_url(); ?>upload/motoristas/motorista_<?php echo $dados_iniciais['dados']['id_usuario']; ?>/veiculo_1.png') center center no-repeat;background-size: 80%; ">
            <span class="fechar_x voltar" style="box-shadow: none" onclick="sair_modal()"><span class="arrumar_botao"><i class="fas fa-2x fa-times"  style="margin-top: 15px"></i></span></span>
        </div>
        <div class="fotos_mostrar" id="veiculo_2" style="background: url('<?php echo base_url(); ?>upload/motoristas/motorista_<?php echo $dados_iniciais['dados']['id_usuario']; ?>/veiculo_2.png') center center no-repeat;background-size: 80%; ">
            <span class="fechar_x voltar" style="box-shadow: none" onclick="sair_modal()"><span class="arrumar_botao"><i class="fas fa-2x fa-times"  style="margin-top: 15px"></i></span></span>
        </div>
        <div class="fotos_mostrar" id="veiculo_3" style="background: url('<?php echo base_url(); ?>upload/motoristas/motorista_<?php echo $dados_iniciais['dados']['id_usuario']; ?>/veiculo_3.png') center center no-repeat;background-size: 80%; ">
            <span class="fechar_x voltar" style="box-shadow: none" onclick="sair_modal()"><span class="arrumar_botao"><i class="fas fa-2x fa-times"  style="margin-top: 15px"></i></span></span>
        </div>
        <div class="fotos_mostrar" id="cnh"       style="background: url('<?php echo base_url(); ?>upload/motoristas/motorista_<?php echo $dados_iniciais['dados']['id_usuario']; ?>/cnh.png') center center no-repeat;background-size: 80%; ">
            <span class="fechar_x voltar" style="box-shadow: none" onclick="sair_modal()"><span class="arrumar_botao"><i class="fas fa-2x fa-times"  style="margin-top: 15px"></i></span></span>
        </div>
        <div class="fotos_mostrar" id="seguro"    style="background: url('<?php echo base_url(); ?>upload/motoristas/motorista_<?php echo $dados_iniciais['dados']['id_usuario']; ?>/seguro.png') center center no-repeat;background-size: 80%; ">
            <span class="fechar_x voltar" style="box-shadow: none" onclick="sair_modal()"><span class="arrumar_botao"><i class="fas fa-2x fa-times"  style="margin-top: 15px"></i></span></span>
        </div>
        <div class="fotos_mostrar" id="comprovante" style="background: url('<?php echo base_url(); ?>upload/motoristas/motorista_<?php echo $dados_iniciais['dados']['id_usuario']; ?>/comprovante.png') center center no-repeat;background-size: 80%; ">
            <span class="fechar_x voltar" style="box-shadow: none" onclick="sair_modal()"><span class="arrumar_botao"><i class="fas fa-2x fa-times"  style="margin-top: 15px"></i></span></span>
        </div>
        <div class="fotos_mostrar" id="documento" style="background: url('<?php echo base_url(); ?>upload/motoristas/motorista_<?php echo $dados_iniciais['dados']['id_usuario']; ?>/documento.png') center center no-repeat;background-size: 80%; ">
            <span class="fechar_x voltar" style="box-shadow: none" onclick="sair_modal()"><span class="arrumar_botao"><i class="fas fa-2x fa-times"  style="margin-top: 15px"></i></span></span>
        </div>
        <div class="fotos_mostrar" id="rg" style="background: url('<?php echo base_url(); ?>upload/motoristas/motorista_<?php echo $dados_iniciais['dados']['id_usuario']; ?>/rg.png') center center no-repeat;background-size: 80%; ">
            <span class="fechar_x voltar" style="box-shadow: none" onclick="sair_modal()"><span class="arrumar_botao"><i class="fas fa-2x fa-times"  style="margin-top: 15px"></i></span></span>
        </div>
        <div class="fotos_mostrar" id="antecedentes" style="background: url('<?php echo base_url(); ?>upload/motoristas/motorista_<?php echo $dados_iniciais['dados']['id_usuario']; ?>/antecedentes.png') center center no-repeat;background-size: 80%; ">
            <span class="fechar_x voltar" style="box-shadow: none" onclick="sair_modal()"><span class="arrumar_botao"><i class="fas fa-2x fa-times"  style="margin-top: 15px"></i></span></span>
        </div>



        <!--calendario-->
        <div id="calendario" class="tm30 wow fadeInDown" data-wow-duration="1s"  style="width: 600px;height: 250px;background: #fff;position: absolute;margin-left: 50%;left: -300px;margin-top: 15%;box-shadow: 0 0 2px #000;z-index: 999;display: none">
            <h3 style="text-align: center;margin-top: 20px;margin-bottom: 20px;"> Resumo mensal</h3>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 10px;">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="text-align: center;padding-bottom: 0">Hoje</div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="text-align: center;padding-bottom: 0">Semana</div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="text-align: center;padding-bottom: 0">Mês</div>
            </div>
            <div class="tm30 wow fadeInLeft" data-wow-duration="1s" style="width: 80%;height: 2px;background: #666;margin: 0 auto;overflow-x: hidden"></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 10px;">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="text-align: center"><?php echo $dados_iniciais['relatorio']['dia']; ?></div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="text-align: center"><?php echo $dados_iniciais['relatorio']['semana']; ?></div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="text-align: center"><?php echo $dados_iniciais['relatorio']['mes']; ?></div>
            </div>

        </div>
        <script>
            function  calendario() {
                sair_modal();
                $('#preto').css('display', 'block');
                $('#preto').animate({'opacity':'.60'}, 1200, 'linear');
                $('#calendario').css('display','block');
            }
        </script>

<div id="tudo" style="margin-top: 120px">




                          <div id="mensagem" style="padding: 15px;border-radius: 5px;background: #ccc;position: fixed;width: 400px;left: 50%;margin-left: -200px;margin-top: 200px;z-index: 999;display: none;">
                            <form style="width: 80%; margin: 0 auto;display:table;" id="desativando_form" action="<?php echo base_url();?>controller_motorista/desativar" method="post">
                                <p style="text-align: center">Motivo</p>
                                <input type="hidden" name="bloquear" id="bloquear">
                                <textarea style="color: #000;width: 100%;height:60px" placeholder="motivo" name="mensagem"></textarea>
                                <input type="hidden" name="flag" value="<?php echo $dados_iniciais['dados']['id_usuario']; ?>">
                                <input type="hidden" name="email_user" value="<?php echo $dados_iniciais['dados']['email_usuario']; ?>">
                                <button id="botao_desativa" type="button" style="width: 47%;margin-right: 5%;padding: 7px 15px;margin-top: 10px;background: #292929;color: #fff; border: none"></button>
                                <span onclick="fechar_desativar()" style="width: 40%;cursor: pointer;padding: 8px 45px;margin-top: 10px;background: #292929;color: #fff; border: none">Cancelar</span>
                            </form>
                          </div>


                          <script type="text/javascript">
                            function desativar(){
                                $("#bloquear").val("0");
                                $("#botao_desativa").text("Inativar");
                                $('#mensagem').css('display','block');
                            }

                            function bloquear(){
                                $("#bloquear").val("1");
                                $("#botao_desativa").text("Bloquear");
                                $('#mensagem').css('display','block');
                            }
                            function fechar_desativar(){
                              $('#mensagem').css('display','none');
                            }

                            $(document).ready(function(){
                              $('#botao_desativa').click(function(){
                                $('#desativando_form').submit();
                                console.log('oii');
                              });

                              var mask = "LLL-NNNN",
                                pattern = {
                                    'translation': {
                                        'L': {
                                            pattern: /[A-Za-z]/
                                        },
                                        'N': {
                                            pattern: /[0-9]/
                                        }
                                    }
                                };

                            $('#placa_veiculo').mask(mask, pattern);

                            });
                          </script>



<div class="col-lg-7 col-md-7 col-sm-7" style="border-right: 8px solid #ccc;height: 750px;background: linear-gradient(#fff,#fff);overflow-y: scroll">
    <div style="margin-top: 50px;">
        <a href="<?php echo base_url();?>main/redirecionar/26" style="display:table;">
            <div class="voltar">
                <i class="fas fa-2x fa-arrow-left" style="margin-top: 15px"></i>
            </div>
        </a>


        <div class="voltar" style="float: left;margin-top: -80px;margin-left: 90px" onclick="calendario()">
            <i class="fas fa-calendar-alt fa-2x" style="margin-top: 15px"></i>
        </div>


        <div style="display: table;padding: 10px;width: 100%">
            <div class=""  style="float: right;margin-top: -60px;margin-right: 30px">
                <p style="margin-top: -10px;text-align: right">Banco: <?php echo $dados_iniciais['bancos']['banco']; ?></p>
                <p style="margin-top: -10px;text-align: right">Agencia: <?php echo $dados_iniciais['dados']['agencia_motorista']; ?></p>
                <p style="margin-top: -10px;text-align: right">Conta: <?php echo $dados_iniciais['dados']['conta_motorista']; ?> - <?php echo $dados_iniciais['dados']['digito_motorista']; ?></p>

                <?php if($dados_iniciais['dados']['tipo_conta'] == 1) { ?>
                    <p style="margin-top: -10px;text-align: right">Conta Corrente</p>
                <?php } else { ?>
                    <p style="margin-top: -10px;text-align: right">Poupança</p>
                <?php } ?>

                <?php if($dados_iniciais['dados']['motivo_inativacao'] != ""): ?>
                    <?=$dados_iniciais['dados']['motivo_inativacao'];?>
                <?php endif; ?>

            </div>
        </div>

        <div  id="foto" onclick="perfil()" style=";background: url('<?php echo base_url(); ?>upload/motoristas/motorista_<?php echo $dados_iniciais['dados']['id_usuario']; ?>/motorista.png') center center no-repeat;background-size: cover;"  class="tm30 wow fadeInDown" data-wow-duration="2s"></div>
        <h3 style="text-align: center"><?php echo $dados_iniciais['dados']['nome_usuario']; ?></h3>
        <div class="col-lg-12 col-md-12 col-sm-12">&nbsp;</div>

        <div class="col-lg-3 col-md-3 col-sm-3" align="center">
            Veículo
           <img src="<?php echo base_url();?>upload/motoristas/motorista_<?php echo $dados_iniciais['dados']['id_usuario']; ?>/veiculo_1.png" class="docs" onclick="veiculo_1()">
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3" align="center">
            Veículo
             <img src="<?php echo base_url();?>upload/motoristas/motorista_<?php echo $dados_iniciais['dados']['id_usuario']; ?>/veiculo_2.png" class="docs" onclick="veiculo_2()">
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3" align="center">
            Veículo
           <img src="<?php echo base_url();?>upload/motoristas/motorista_<?php echo $dados_iniciais['dados']['id_usuario']; ?>/veiculo_3.png" class="docs" onclick="veiculo_3()">
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3" align="center">
            CNH
            <img src="<?php echo base_url();?>upload/motoristas/motorista_<?php echo $dados_iniciais['dados']['id_usuario']; ?>/cnh.png" class="docs"  onclick="cnh()">
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12" >&nbsp;</div>

        <div class="col-lg-3 col-md-3 col-sm-3" align="center">
            Seguro
           <img src="<?php echo base_url();?>upload/motoristas/motorista_<?php echo $dados_iniciais['dados']['id_usuario']; ?>/seguro.png" class="docs" onclick="seguro()">
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3" align="center">
            Comprovante
           <img src="<?php echo base_url();?>upload/motoristas/motorista_<?php echo $dados_iniciais['dados']['id_usuario']; ?>/comprovante.png" class="docs" onclick="comprovante()">
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3" align="center">
            Documento
             <img src="<?php echo base_url();?>upload/motoristas/motorista_<?php echo $dados_iniciais['dados']['id_usuario']; ?>/documento.png" class="docs" onclick="documento()">
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3" align="center">
            RG
            <img src="<?php  echo base_url();?>upload/motoristas/motorista_<?php  echo $dados_iniciais['dados']['id_usuario']; ?>/rg.png" class="docs"  onclick="rg()">
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12" >&nbsp;</div>

        <div class="col-lg-3 col-md-3 col-sm-3" align="center">
            Antecedentes
            <img src="<?php  echo base_url();?>upload/motoristas/motorista_<?php  echo $dados_iniciais['dados']['id_usuario']; ?>/antecedentes.png" class="docs" onclick="antecedentes()">
        </div>

    </div>
</div>











<div class="col-lg-5 col-lg-5 col-sm-5" style="height: 750px;background: #666;padding: 0; margin: 0;color: #fff">



    <script>

        $('img').error(function(){
            $(this).prop('src','<?php echo base_url() ?>style/img/favicon.png');
        });

        function trocar_dados() {
            $('.fa-car').css('display','none');
            $('#troca_dado').css('margin-left','40%');
            $('#troca_dado').css('margin-top','40%');
            $('#troca_dado').css('border','none');
            $('#troca_dado').css('background','#999');
            $('#pessoal').css('display','none');
            $('#botoes').css('display','none');

            setTimeout(function () {
                $('#troca_dado').css('margin-top','0');
                $('#troca_dado').css('z-index','0');
                $('#troca_dado').css('margin-left','0');
                $('#troca_dado').css('width','100%');
                $('#troca_dado').css('height','100%');
                $('#troca_dado').css('opacity','1');
                document.getElementById("troca_dado").setAttribute("onclick", "none");

            }, 300);
            setTimeout(function () {
                $('#carro').css('display','block');
                $('#troca_dado').css('border-radius','0');
            }, 420);

        }

    </script>
    <script>
        function trocar_voltar(){

            $('#troca_dado').css('background','#666');
            $('#troca_dado').css('margin-left','40%');
            $('#troca_dado').css('margin-top','40%');
            $('#troca_dado').css('width','120px');
            $('#troca_dado').css('height','120px');
            $('#carro').css('display','none');


            setTimeout(function () {
            $('#carro').css('display','none');
            $('#botoes').css('display','block');

                $("#local").html('');
                var clone = '<div class="voltar"   class="tm30 wow fadeIn" data-wow-duration="3s" id="troca_dado" style="box-shadow: none;margin-top: 10px;position: absolute;margin-left: 80%;background: #666;z-index: 9" onclick="trocar_dados()">\n' +
                    '<i class="fas fa-2x fa-car" style="margin-top: 15px;"></i>';
                $('#local').append(clone);
                $('#pessoal').css('display','block');
            }, 600);


        }

        /*
    </div>*/
     </script>


    <script>




        /*recebe all do formulario */
        function editar() {

            var nome = document.getElementById('nome').value;
            var sexo = document.getElementById('fk_genero').value;
            var rg = document.getElementById('rg_usuario').value;
            var cpf = document.getElementById('cpf_usuario').value;
            var senha = document.getElementById('senha_usuario').value;
            var email = document.getElementById('email_usuario').value;
            var telefone = document.getElementById('telefone_usuario').value;
            var celular = document.getElementById('celular_usuario').value;
            var cep = document.getElementById('cep_usuario').value;
            var logradouro = document.getElementById('logradouro_usuario').value;
            var complemento = document.getElementById('complemento_usuario').value;
            var bairro = document.getElementById('bairro_usuario').value;
            var cidade = document.getElementById('cidade_usuario').value;
            var numero = document.getElementById('numero_usuario').value;
            var uf = document.getElementById('fk_uf_usuario').value;
            var cidade_trabalho = document.getElementById('cidade_trabalho').value;
            var marca_veiculo = document.getElementById('marca_veiculo').value;
            var modelo_veiculo = document.getElementById('modelo_veiculo').value;
            var placa_veiculo = document.getElementById('placa_veiculo').value;
            var cor_veiculo = document.getElementById('cor_veiculo').value;


      /*      console.log(nome);
            console.log(sexo);
            console.log(rg);
            console.log(cpf);
            console.log(email);
            console.log(senha);
            console.log(telefone);
            console.log(celular);
            console.log(cep);
            console.log(logradouro);
            console.log(complemento);
            console.log(bairro);
            console.log(cidade);
            console.log(numero);
            console.log(uf);
            console.log(cidade_trabalho);
            console.log(marca_veiculo);
            console.log(modelo_veiculo);
            console.log(placa_veiculo);
            console.log(cor_veiculo);*/


            var settings_sha1 = {
                "async": true,
                "crossDomain": true,
                "url": "<?php  echo base_url();?>controller_motorista/sha1",
                "method": "POST",
                "data": {
                    "senha_usuario": senha
                }
            }
            $.ajax(settings_sha1).done(function (response) {
                  if(senha == ""){
                      senha = '<?php $dados_iniciais['dados']['senha_usuario']; ?>';
                } else {
                    senha = response;
                }

            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "<?php echo base_url(); ?>controller_webservice/editar_cadastro_motorista",
                "method": "POST",
                "headers": {
                    "content-type": "application/x-www-form-urlencoded",
                    "authorization": "Basic <?php echo base64_encode($dados_iniciais['dados']['email_usuario'].':'.$dados_iniciais['dados']['senha_usuario']);?>",
                    "cache-control": "no-cache"
                },
                "data": {
                    "tipo": "1",
                    "dashboard": "1",
                    "id_usuario": <?php echo $dados_iniciais['dados']['id_usuario']; ?>,
                    "email_usuario_original": '<?php echo $dados_iniciais['dados']['email_usuario']; ?>',
                    "cpf_usuario_original": '<?php echo $dados_iniciais['dados']['cpf_usuario']; ?>',
                    "nome_usuario": nome,
                    "email_usuario": email,
                    "telefone_usuario": telefone,
                    "senha_usuario": senha,
                    "rg_usuario": rg,
                    "cpf_usuario": cpf,
                    "celular_usuario": celular,
                    "logradouro_usuario": logradouro,
                    "cidade_usuario": cidade,
                    "bairro_usuario": bairro,
                    "cep_usuario": cep,
                    "fk_uf_usuario": uf,
                    "complemento_usuario": complemento,
                    "num_residencia_usuario": numero,
                    "fk_genero": sexo
                }
            }

            $.ajax(settings).done(function (response) {
                console.log('RESPOSTA: '+response);
                if(response.status == 1)
                {
                    $.toast().reset('all');
                        $.toast({
                            heading: ' ',
                            text: 'Edição do motorista realizada com sucesso',
                            hideAfter : 3000,
                            position: 'top-right',
                            icon: 'success'
                        });
                } else if (response.status == 0) {
                    $.toast().reset('all');
                    $.toast({
                        heading: ' ',
                        text: response.resultado,
                        hideAfter : 3000,
                        position: 'top-right',
                        icon: 'error'
                    });
                } else {
                  $.toast().reset('all');
                  $.toast({
                      heading: ' ',
                      text: 'Falha na edição',
                      hideAfter : 3000,
                      position: 'top-right',
                      icon: 'error'
                  });
                }
            }).error(function(request, status, error){

              $.toast().reset('all');
              $.toast({
                  heading: ' ',
                  text: request.responseJSON.resultado,
                  hideAfter : 3000,
                  position: 'top-right',
                  icon: 'error'
              });

            });

        });
        }













        function editar_t2() {
            var cidade_trabalho = document.getElementById('cidade_trabalho').value;
            var modelo_veiculo = document.getElementById('modelo_veiculo').value;
            var placa_veiculo = document.getElementById('placa_veiculo').value;
            var cor_veiculo = document.getElementById('cor_veiculo').value;

            if(placa_veiculo == "<?php echo $dados_iniciais['dados']['placa_carro_motorista']; ?>"){
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "<?php echo base_url(); ?>controller_webservice/editar_cadastro_motorista",
                    "method": "POST",
                    "headers": {
                        "content-type": "application/x-www-form-urlencoded",
                        "authorization": "Basic <?php echo base64_encode($dados_iniciais['dados']['email_usuario'].':'.$dados_iniciais['dados']['senha_usuario']);?>",
                        "cache-control": "no-cache"
                    },
                    "data": {
                        "tipo": "2",
                        "dashboard": "1",
                        "id_usuario": <?php echo $dados_iniciais['dados']['id_usuario']; ?>,
                        "email_usuario_original": '<?php echo $dados_iniciais['dados']['email_usuario']; ?>',
                        "cpf_usuario_original": '<?php echo $dados_iniciais['dados']['cpf_usuario']; ?>',
                        "fk_cidade_ativa_motorista": cidade_trabalho,
                        "fk_modelo_carro_motorista": modelo_veiculo,
                        "cor_carro_motorista": cor_veiculo
                    }
                }
            }
            else{

                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "<?php echo base_url(); ?>controller_webservice/editar_cadastro_motorista",
                    "method": "POST",
                    "headers": {
                        "content-type": "application/x-www-form-urlencoded",
                        "authorization": "Basic <?php echo base64_encode($dados_iniciais['dados']['email_usuario'].':'.$dados_iniciais['dados']['senha_usuario']);?>",
                        "cache-control": "no-cache"
                    },
                    "data": {
                        "tipo": "2",
                        "dashboard": "1",
                        "id_usuario": <?php echo $dados_iniciais['dados']['id_usuario']; ?>,
                        "email_usuario_original": '<?php echo $dados_iniciais['dados']['email_usuario']; ?>',
                        "cpf_usuario_original": '<?php echo $dados_iniciais['dados']['cpf_usuario']; ?>',
                        "fk_cidade_ativa_motorista": cidade_trabalho,
                        "fk_modelo_carro_motorista": modelo_veiculo,
                        "cor_carro_motorista": cor_veiculo,
                        "placa_carro_motorista": placa_veiculo
                    }
                }

            }


            $.ajax(settings).done(function (response) {
                console.log(response);
                if(response.status == 1)
                {
                    $.toast().reset('all');
                    $.toast({
                        heading: ' ',
                        text: 'Edição do motorista realizada com sucesso',
                        hideAfter : 3000,
                        position: 'top-right',
                        icon: 'success'
                    });
                }
                else{
                    $.toast().reset('all');
                    $.toast({
                        heading: ' ',
                        text: ' Falha na edição do motorista',
                        hideAfter : 3000,
                        position: 'top-right',
                        icon: 'error'
                    });
                }
            });

        }

    </script>



    <div id="local">

    <div class="voltar" id="troca_dado" style="box-shadow: none;margin-top: 10px;position: absolute;margin-left: 80%;background: #666;z-index: 9" onclick="trocar_dados()">
        <i class="fas fa-2x fa-car" style="margin-top: 15px;"></i>
    </div>

    </div>


    <div  class="tm30 wow fadeIn" data-wow-duration="3s" style="max-height: 100%; overflow-y: scroll;">
        <div id="usuario"  >

            <form style="width: 90%;margin: 0 auto;margin-top: 40px" action="<?php echo base_url(); ?>controller_motorista/editar" method="post">

            <div id="pessoal" class="tm30 wow fadeIn" data-wow-duration="3s">
                Nome
                <input type="text" value="<?php echo $dados_iniciais['dados']['nome_usuario']; ?>" class="inp_classe" id="nome" >
                Gênero
                <?php if($dados_iniciais['dados']['fk_genero'] == 82){
                    echo'
                     <select  style="border: none;border-bottom: 1px solid #fff;margin-top: 10px;color: #fff" class="inp_form_cadastro" id="fk_genero" name="fk_genero">
                        <option value="82" style="background: #292929;color: #fff"> Masculino</option>
                        <option value="81" style="background: #292929;color: #fff"> Feminino</option>
                    </select>';
                }
                else{
                    echo'
                     <select  style="border: none;border-bottom: 1px solid #fff;margin-top: 10px;color: #fff" class="inp_form_cadastro" id="fk_genero" name="fk_genero">
                           <option value="81" style="background: #292929;color: #fff"> Feminino</option>
                        <option value="82" style="background: #292929;color: #fff"> Masculino</option>
                    </select>';
                }
                ?>

                RG
                <input type="text" value="<?php echo $dados_iniciais['dados']['rg_usuario']; ?>" class="inp_classe" placeholder="RG" id="rg_usuario" maxlength="12">
                CPF
                <input type="text" value="<?php echo $dados_iniciais['dados']['cpf_usuario']; ?>" class="inp_classe" placeholder="CPF" id="cpf_usuario" maxlength="14">
                E-mail
                <input type="text" value="<?php echo $dados_iniciais['dados']['email_usuario']; ?>" class="inp_classe" placeholder="E-mail" id="email_usuario">
                Senha
                <input type="password" class="inp_classe" placeholder="Senha" id="senha_usuario">
                Telefone
                <input type="text" value="<?php echo $dados_iniciais['dados']['telefone_usuario']; ?>" class="inp_classe" placeholder="Telefone" id="telefone_usuario" maxlength="14">
                Celular
                <input type="text" value="<?php echo $dados_iniciais['dados']['celular_usuario']; ?>" class="inp_classe" placeholder="Celular" id="celular_usuario" maxlength="15">
                CEP
                <input type="text" value="<?php echo $dados_iniciais['dados']['cep_usuario']; ?>" class="inp_classe"  placeholder="CEP" id="cep_usuario" onblur="pesquisacep(this.value);" maxlength="9">
                <?php print_r($dados_iniciais['ufs'][0]['nome_item_grupo']); ?>
                <div style="width: 47%;margin-right: 1%;float: left">
                    UF
                    <select name="" id="fk_uf_usuario" >


                        <?php
                                for($cont = 1;$cont<=27;$cont++){
                                    if($cont == $dados_iniciais['dados']['fk_uf_usuario']){
                                        echo '<option value="'.$cont.'" selected>'.$dados_iniciais['ufs'][$cont-1]['sigla_item_grupo'].'</option>';
                                    }else{
                                        echo '<option value="'.$cont.'">'.$dados_iniciais['ufs'][$cont-1]['sigla_item_grupo'].'</option>';
                                    }
                                }
                        ?>

                    </select>
                </div>
                Cidade
                <input type="text" value="<?php echo $dados_iniciais['dados']['cidade_usuario']; ?>" class="inp_classe" placeholder="Cidade"  style="width: 49%" id="cidade_usuario">
                Logradouro
                <input type="text" value="<?php echo $dados_iniciais['dados']['logradouro_usuario']; ?>" class="inp_classe"  placeholder="Logradouro" id="logradouro_usuario">
                Bairro/Número
                <br>
                <input type="text" value="<?php echo $dados_iniciais['dados']['bairro_usuario']; ?>" class="inp_classe" placeholder="Bairro" style="width: 79%" id="bairro_usuario">
                
                <input type="text" value="<?php echo $dados_iniciais['dados']['num_residencia_usuario']; ?>" class="inp_classe" placeholder="Número"  style="width: 19%" id="numero_usuario">
                <br>Complemento
                <input type="text" value="<?php echo $dados_iniciais['dados']['complemento_usuario']; ?>" class="inp_classe" placeholder="Complemento" id="complemento_usuario">

                <p>&nbsp;</p>
                <?php if($dados_iniciais['dados']['ativado_sms'] == 0){
                    echo '<p><span style="color: #f8d509"> Não ativo por SMS </span></p>';
                }
                else{
                    echo '<p>Ativo por SMS</p>';
                }
                ?>
                <!--Email (baixo) e sms (cima-->
                <?php if($dados_iniciais['dados']['ativado_email'] == 0){
                    echo '<p> <span style="color: #f8d509"> Não ativo por E-mail</span></p>';
                }
                else{
                    echo '<p>Ativo por E-mail</p>';
                }
                ?>
            </div>





                <!--Aqui vem sobre o carro-->
                <div id="carro" style="z-index: 9999;width: 100%;margin-top: 30px" class="tm30 wow fadeIn" data-wow-duration="1s">
                    <div class="voltar_pessoal" id="troca_dado_pessoal"  style="cursor: pointer" onclick="trocar_voltar()">
                        <i class="fas fa-2x fa-user" style="margin-top: 15px;"></i>
                    </div>
                    <p>&nbsp;</p>
                    <!--select de cidade-->
                    Cidade
                    <select name="cidade_trabalho" id="cidade_trabalho">
                        <?php foreach ($dados_iniciais['cidades'] as $cidades) {
                            if($cidades['id_cidades_atuacao'] == $dados_iniciais['dados']['fk_cidade_ativa_motorista']){ ?>
                                <option value="<?php echo $cidades['id_cidades_atuacao']; ?>" selected><?php echo $cidades['nome_cidades_atuacao']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $cidades['id_cidades_atuacao']; ?>"><?php echo $cidades['nome_cidades_atuacao']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>









                    <?php 

                    $achar_marca = 0;
                    $guardar = 0;
                    foreach ($dados_iniciais['modelo'] as $modelos) {
                        if($modelos['id_modelo'] == $dados_iniciais['dados']['fk_modelo_carro_motorista']){
                            ?>
                            <?php $achar_marca = $modelos['fk_montadora']; ?>
                            <?php $guardar = $modelos['id_modelo']; ?>
                        <?php }
                    }
                    ?>




                    <script>
                        $(document).ready(function () {

                            $('#marca_veiculo').change(function () {
                                console.log('entrou');
                                var url_modelo = "<?php echo base_url(); ?>controller_webservice/busca_modelo?id_montadora=";
                                var montadora = document.getElementById('marca_veiculo').value;
                                var settings = {
                                    "async": true,
                                    "crossDomain": true,
                                    "url": url_modelo+montadora,
                                    "method": "GET",
                                    "headers": {
                                        "cache-control": "no-cache",
                                        "postman-token": "b4f801dd-1000-c5aa-1315-5c6b90644816"
                                    }
                                }
                                $.ajax(settings).done(function (response) {
                                    $("#modelo_veiculo").html('');
                                    for(const i in response.modelos){
                                        var clone = '<option class="opcoes_modelo" style="background: #292929;color: #fff"> '+ response.modelos[i].modelo +'</option>';
                                        $('#modelo_veiculo').append(clone);
                                    }

                                });
                            });

                        });
                    </script>

                    <!--select da montadora-->
                    Marca
                    <select name="marca_veiculo" id="marca_veiculo">
                        <?php foreach ($dados_iniciais['montadora'] as $montadoras) { ?>
                            <?php if($montadoras['id_montadora'] == $achar_marca){ ?>
                                <option value="<?php echo $montadoras['id_montadora']; ?>" selected><?php echo $montadoras['montadora']; ?></option>
                            <?php } else { ?>
                                <option value="<?php echo  $montadoras['id_montadora']; ?>"><?php echo $montadoras['montadora']; ?> </option>
                            <?php } ?>
                        <?php } ?>
                    </select>




                 <!--select da modelos-->
                 Modelo
                    <select name="modelo" id="modelo_veiculo">
                        <?php foreach ($dados_iniciais['modelo'] as $modelos) {
                            if($modelos['id_modelo'] == $dados_iniciais['dados']['fk_modelo_carro_motorista']){
                                ?>
                                <?php $achar_marca = $modelos['fk_montadora']; ?>
                                <?php $guardar = $modelos['id_modelo']; ?>
                                <!--agora mostra todas que não são a certa -->
                                    <?php foreach ($dados_iniciais['modelo'] as $modelos_e) {?>
                                      <?php   if($modelos_e['fk_montadora'] == $achar_marca){ ?>
                                        <?php if($modelos_e['id_modelo'] == $guardar){ ?>
                                        <option value="<?php echo $modelos['id_modelo']; ?>" selected><?php echo $modelos['modelo']; ?> </option>
                                           <?php }else{ ?>
                                        <option value="<?php echo $modelos_e['id_modelo']; ?>" ><?php echo $modelos_e['modelo']; ?> </option>
                                        <?php } ?>
                                       <?php } ?>
                                     <?php } ?>

                            <?php }
                            }
                        ?>
                    </select>





                    Cor do carro
                    <input type="text" value="<?php echo $dados_iniciais['dados']['cor_carro_motorista']; ?>" class="inp_classe" style="z-index: 999;position: relative" placeholder="Cor do carro" id="cor_veiculo">
                    Placa do carro
                    <input type="text" value="<?php echo $dados_iniciais['dados']['placa_carro_motorista']; ?>" class="inp_classe" style="z-index: 999;position: relative; text-transform:uppercase;" placeholder="Placa do carro" id="placa_veiculo">
                    <p>&nbsp;</p>
                    <span onclick="editar_t2()" class="botao_alt"  style="margin-top: 80px;border-radius: 5px"><span>Alterar</span></span>

                </div>





                <!--Parte comujm-->
                <span id="botoes">
                <center>
                    <br>
                    <!--<a href="<?php echo base_url()?>main/redirecionar/5/<?php echo $dados_iniciais['dados']['id_usuario']; ?> ">-->
                    <span onclick="editar()" class="botao_alt"  style="margin-top: 30px;border-radius: 5px"><span>Alterar</span></span>

                    <?php if($dados_iniciais['dados']['ativo_usuario'] == 0) {  
                            if($dados_iniciais['dados']['bloqueado'] == 1) { ?>
                                Bloqueado!
                            <?php } else { ?>
                        <a href="<?php echo base_url();?>controller_motorista/ativar_painel?flag=<?php echo $dados_iniciais['dados']['id_usuario']; ?>&email_user=<?php echo $dados_iniciais['dados']['email_usuario']; ?>" class="botao_alt"  style="margin-top: 30px;border-radius: 5px;background: #f8d509;color: #000;text-decoration: none"><span>Ativar</span></a>
                        <?php } ?>
                    <?php } else {?>
                        <a  onclick="desativar()" class="botao_alt"  style="margin-top: 30px;border-radius: 5px;background: #ccc;color: #000;text-decoration: none"><span>Inativar</span></a>
                        <a  onclick="bloquear()" class="botao_alt"  style="margin-top: 30px;border-radius: 5px;background: red;color: #000;text-decoration: none"><span>Bloquear</span></a>
                    <?php } ?>
                </center>
                 </span>



                <!--fecha tudo-->
            </form>
        </div>
    </div>
    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tm30 wow fadeInUp"></div>

</div>




</div>



<script type="text/javascript">

    function mascara(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascara()",1)
    }
    function execmascara(){
        v_obj.value=v_fun(v_obj.value)
    }
    function mtel(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        v=v.replace(/^(\d{5})(\d)/g,"$1-$2"); //Coloca parênteses em volta dos dois primeiros dígitos
        return v;
    }
    function mtel2(cpf){
        cpf=cpf.replace(/\D/g,"");             //Remove tudo o que não é dígito
        cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2");
        cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2");
        cpf=cpf.replace(/(\d{3})(\d{1,2})$/,"$1-$2");
        return cpf;
    }
    function mtel3(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
        v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
        return v;
    }
    function mtel4(rg){
        rg=rg.replace(/\D/g,"");
        rg=rg.replace(/(\d{2})(\d)/,"$1.$2");
        rg=rg.replace(/(\d{3})(\d)/,"$1.$2");
        rg=rg.replace(/(\d{3})(\d{1,2})$/,"$1-$2");

        return rg;
    }
    function id( el ){
        return document.getElementById( el );
    }
    window.onload = function(){
        id('telefone_usuario').onkeyup = function(){
            mascara( this, mtel3 );
        }
        id('rg_usuario').onkeyup = function(){
            mascara( this, mtel4 );
        }
        id('cpf_usuario').onkeyup = function(){
            mascara( this, mtel2 );
        }
        id('celular_usuario').onkeyup = function(){
            mascara( this, mtel3 );
        }
        id('cep_usuario').onkeyup = function(){
            mascara( this, mtel );
        }
        id('numero_usuario').onkeyup = function(){
            mascara( this, mtel );
        }
    }
</script>



<script type="text/javascript">

    function meu_callback(conteudo) {
        if (!("erro" in conteudo)) {
            //Atualiza os campos com os valores.
            document.getElementById('logradouro_usuario').value=(conteudo.endereco.logradouro);
            document.getElementById('bairro_usuario').value=(conteudo.endereco.bairro);
            document.getElementById('cidade_usuario').value=(conteudo.endereco.localidade);
            $('#fk_uf_usuario').val(conteudo.endereco.uf_id).trigger('change');

        } else {
            //CEP não Encontrado.
            limpa_formulário_cep();
            alert("CEP não encontrado.");
        }
    }

    function limpa_formulário_cep() {
        //Limpa valores do formulário de cep.
        document.getElementById('logradouro_usuario').value=("");
        document.getElementById('bairro_usuario').value=("");
        document.getElementById('cidade_usuario').value=("");
        document.getElementById('fk_uf_usuario').value=("");

    }

    function pesquisacep(valor) {

        //Nova variável "cep" somente com dígitos.
        var cep = valor.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                document.getElementById('logradouro_usuario').value="...";
                document.getElementById('bairro_usuario').value="...";
                document.getElementById('fk_uf_usuario').value="UF";
                document.getElementById('cidade_usuario').value="...";

                //Cria um elemento javascript.
                var script = document.createElement('script');

                //Sincroniza com o callback.
                script.src = '<?php echo base_url() ?>Controller_webservice/buscar_cep?cep='+ cep +'&callback=meu_callback';

                //Insere script no documento e carrega o conteúdo.
                document.body.appendChild(script);

            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    };





</script>
