<head>
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/cupons.css" >
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/theme-animate.css" >
    <link type="text/css" href="<?php echo base_url(); ?>style/css/bootstrap.min.css" rel="stylesheet" />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <meta name="theme-color" content="#292929">
</head>



<div>
    <h1 class="fade_1"><i class="fa fa-2x fa-user lado_topo" data-wow-duration="2s" data-wow-delay="0.0s" > </i>  Passageiros</h1>

    <div class="lado_direito" style="width: 100%;height: 2px;background: #f8d509;margin-top: 30px;overflow-x: hidden"></div>
</div>

<div class="lado_direito" style="width: 100%;height: 2px;background: transparent;margin-top: 40px;margin-bottom: 10px"></div>

<table class="table table-bordered table-hover" align="center">
    <thead style="background: #f1f1f1">
    <tr>
        <th>Nome</th>
        <th>E-mail</th>
        <th>CPF</th>
        <th>RG</th>
        <th>Endereço</th>
        <th>CEP</th>
        <th>Telefone</th>
        <th style="text-align: center">Motivo (Inativo/Bloqueado)</th>
        <th class="no-filter" style="text-align: center">Status</th>
        <!--<th class="no-filter" style="text-align: center">Corridas</th>-->
        <!--<th class="no-filter" style="text-align: center">Ver mais</th>-->
    </tr>
    </thead>
    <tbody>
    <?php foreach ($dados_iniciais as $passageiro) { ?>

    <tr>
        <td><p style="margin-top: 15px;margin-left: 5px;text-align: center"><?php echo $passageiro['nome_usuario']; ?></p></td>
        <td><p style="margin-top: 15px;margin-left: 5px;text-align: center"><?php echo $passageiro['email_usuario']; ?></p></td>
        <td  class="mascara_cpf" style="text-align: center;padding-top: 25px"><p style="margin-top: 15px;margin-left: 5px;text-align: center"><?php echo $passageiro['cpf_usuario']; ?></p></td>

        <td  class="mascara_rg" style="text-align: center;padding-top: 25px" ><p style="margin-top: 15px;margin-left: 5px;text-align: center"><?php echo $passageiro['rg_usuario']; ?></p></td>

        <td ><p style="margin-top: 15px;margin-left: 5px;text-align: center"><?php echo $passageiro['logradouro_usuario']; ?></p></td>

        <td class="mascara_cep" style="text-align: center;padding-top: 25px"><p style="margin-top: 15px;margin-left: 5px;text-align: center"><?php echo $passageiro['cep_usuario']; ?></p></td>

        <td class="mascara_cel" style="text-align: center;padding-top: 25px"><p ><?php
                echo $passageiro['celular_usuario'] ;

                /*echo $passageiro['telefone_usuario'];*/
                ?></p></td>
        <td style="text-align: center;padding-top: 25px"><p style="margin-top: 15px;margin-left: 5px;text-align: center"><?php echo $passageiro['motivo_inativacao']; ?></p></td>
        <td style="padding-left:  30px;padding-right: 30px;text-align: center;padding-top: 25px">
        <?php if($passageiro['ativo_usuario'] == '0'){?>
        <!--    <p style="margin-top: 15px;text-align: center">
                <span style="color: #db2d49">Inativo</span>
            </p> -->
            <?php if($passageiro['bloqueado'] == 1) { ?>
                <center>Bloqueado!</center>
            <?php } else { ?>
                <center><a href="<?php echo base_url(); ?>controller_adm/ativar_passageiro?id=<?php echo $passageiro['id_usuario']; ?>" class="btn btn-danger" style="background-color: #f8d509;border-color: #e3c009 !important; width: 150px" > &nbsp;Ativar</button> </a></center>
            <?php } ?>

        <?php } else { ?>

            <div id="motivo_<?php echo $passageiro['id_usuario']; ?>" class="motivo" style="display: none">
                <form method="post" action="<?php echo base_url(); ?>controller_adm/desativar_passageiro">
                    <input type="hidden" name="id" value="<?php echo $passageiro['id_usuario']; ?>">
                    <select class="form-control" name="bloquear" width="100%">
                        <option value="1" selected>Inativar</option>
                        <option value="2">Bloquear</option>
                    </select>
                    <input type="text" name="motivo" placeholder="Motivo" class="form_modal fade_1" id="motivo">
                    <center>
                    <button class="btn btn-danger" style="background-color: #292929;width: 110px;border-color: #000;margin-top: 10px" > &nbsp;Confirmar</button>
                    </center>
                </form>
                
            </div>
            <!-- abaixo botao de desativacao -->
            <center>
                <a  class="btn enviar_desativar btn-danger" style="background-color: #292929;width: 150px;border-color: #000;" id="botao_<?php echo $passageiro['id_usuario']; ?>" onclick="desativando(<?php echo $passageiro['id_usuario']; ?>)" > &nbsp;Inativar/Bloquear </a>
            </center>



            <script type="text/javascript">
                function desativando(id){
                    $(".motivo").css('display','none');
                    $(".enviar_desativar").css('display','block');
                    $('#motivo_'+id).css('display','block');
                    $('#botao_'+id).css('display','none');
                }
            </script>   

            <!-- <p style="margin-top: 15px;text-align: center">
                <span style="color: #7da957">Ativado</span>
            </p> -->
        <?php } ?>
        </td>
        <!--<td><center><a href="<?php /*echo base_url(); */?>controller_passageiro/ver_corridas?id=<?php /*echo $passageiro['id_usuario']; */?>" class="btn btn-danger" style="background-color: #292929;border-color: #000;" > &nbsp;Ver corridas</button> </a></center></td>-->
      <!--  <td><center><a href="<?php /*echo base_url()*/?>main/redirecionar/31/<?php /*echo $passageiro['id_usuario']; */?> "><button  type="submit" class="botao_alt"  style="margin-top: -0px;border-radius: 5px"><span style="white-space: nowrap">Ver mais</span></button></a> </center></td>-->
    </tr>

    <?php } ?>


    </tbody>
</table>


<style>

    .botao_alt {
        opacity: .80;
        background: #292929;
        border: 2px solid #292929;
        margin-top: 10px;
        padding: 8px 45px;
        color: #f8d509;
        position: relative;
        overflow: hidden;
        cursor: pointer;
    }

    .botao_alt:hover {
        opacity: 1;
        transition-duration: 0.3s;
        transition-timing-function: ease-in;
        transition-property: all;
    }
    .botao_alt span {
        position: relative;
        z-index: 100;
        font-size: 16px;
    }
    .botao_alt:before, .botao_alt:after {
        content: '';
        position: absolute;
        display: block;
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
    }
    .botao_alt:before {
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0 , 0);
        background-color: #000;
        color: #000;
        border: 1px solid #000;
        -webkit-transition: -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19), -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
    }
    .botao_alt:after {
        background-color: #000;
        color: #000;
        border: 1px solid #000;
        -webkit-transform: translate3d(100%, 0, 0);
        transform: translate3d(100%, 0, 0);
        -webkit-transition: -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62), -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
    }
    .botao_alt:hover:before {
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
    }
    .botao_alt:hover:after {
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
    }
</style>