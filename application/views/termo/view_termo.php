<head>
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/termos.css" >
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/theme-animate.css" >
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <script src="<?php echo base_url(); ?>style/ckeditor/ckeditor.js"></script>
    <link type="text/css" href="<?php echo base_url(); ?>style/css/bootstrap.min.css" rel="stylesheet" />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">



   <script type="text/javascript">
        window.onload = function()  {
            CKEDITOR.replace( 'termos' );
            CKEDITOR.replace( 'termos_mot' );
            var texto = document.getElementById('termos').value;
            var texto_mot = document.getElementById('termos_mot').value;
            var clone = ' '+ texto +' ';
            var clone_mot = ' '+ texto +' ';
            $('#texto_termo').append(clone);
            $('#texto_termo_mot').append(clone_mot);
        };

    </script>

    <script>
        timer = setInterval(updateDiv,10);
        function updateDiv(){
            var editorText = CKEDITOR.instances.termos.getData();
            $('#texto_termo').html(editorText);
        }

        timer = setInterval(updateDiv_sec,10);
        function updateDiv_sec(){
            var editorText = CKEDITOR.instances.termos_mot.getData();
            $('#texto_termo_mot').html(editorText);
        }
    </script>
    <script>
        function ver_motorista() {
            $('#passageiro').css('display','none');
            $('#motorista').css('display','block');
        }
        function ver_passageiro() {
            $('#motorista').css('display','none');
            $('#passageiro').css('display','block');
        }
    </script>
</head>

<div>

    <?php
    $termo_uso = $dados_iniciais[0];
    $termo = $termo_uso['termo_de_uso'];
    $termo_de_uso_motorista = $termo_uso['termo_de_uso_motorista'];

    ?>


<div id="passageiro">
    <h1 class="fade_1"><i class="fa fa-2x fa-newspaper-o" data-wow-duration="2s" data-wow-delay="0.0s" > </i>  &nbsp;&nbsp;Mudar Termo de uso <span style="font-weight: bolder"> Passageiro</span></h1>
    <div class="lado_direito" style="width: 66.666%;height: 2px;background: #f8d509;margin-top: 30px;overflow-x: hidden"></div>
    <div class="col-lg-8 col-md-12 col-sm-12"   style="padding: 0">
    <form action="<?php echo base_url(); ?>Controller_termo/termo" method="POST">

        <textarea  class="state-change-target textarea" id="termos" name="termos"  onkeyup="escrevendo()"  >
            <?php echo $termo; ?>
        </textarea>
        <button  type="submit" class="botao lado_baixo botao_envia_pagamento" ><span>Salvar</span></button>
        <button  type="button" class="botao lado_baixo botao_envia_pagamento" onclick="ver_motorista()"><span>Ver termo do motorista</span></button>
    </form>
    </div>
    <div class="col-lg-4 hidden-md hidden-sm hidden-xs" style="padding: 0">
        <div class="col-lg-12  col-md-12 col-sm-12" style="background: url('<?php echo base_url();?>style/img/cel.png')center center no-repeat;background-size: 300px;height: 650px;width: 95%;margin-left: 5%;padding: 0;border-radius: 8px;margin-top: -30px">
            <div class="header" style="width: 254px;background: #f8d509;margin: 0 auto;margin-top: 100px;margin-left: 52px">
                <p style="text-align: right;color: #fff;font-weight: bolder;padding: 15px 10px">You GO</p>
            </div>
            <div style="background: #292929;width: 254.3px;margin-left: 52.3px;margin-top: -10px;height: 409px;color: #fff;overflow-y: scroll">
                <body>
                <style>
                    p{
                        text-align: center;
                        width: 100%;
                    }

                    ::-webkit-scrollbar-track {
                        background-color: transparent;
                    }
                    ::-webkit-scrollbar {
                        width: 0px;
                        background: transparent;
                    }
                    ::-webkit-scrollbar-thumb  {
                        border-radius: 15px;
                        background: #f8d509;
                    }
                </style>

                <span style="text-align: center;color: #fff;margin-top: 10px;padding: 0 15px;" id="cel">
                    <h3 style="font-size: 14px;color: #fff;width: 230px;margin: 0 auto;word-wrap: break-word" id="texto_termo"></h3>
                </span>

                </body>
            </div>
        </div>
    </div>
</div>
















<div id="motorista" style="display: none">
    <h1 class="fade_1"><i class="fa fa-2x fa-newspaper-o" data-wow-duration="2s" data-wow-delay="0.0s" > </i>  &nbsp;&nbsp;Mudar Termo de uso <span style="font-weight: bolder"> Motorista</span></h1>
    <div class="lado_direito" style="width: 66.666%;height: 2px;background: #f8d509;margin-top: 30px;overflow-x: hidden"></div>
    <div class="col-lg-8 col-md-12 col-sm-12"   style="padding: 0">
        <form action="<?php echo base_url(); ?>Controller_termo/termo_motorista" method="POST">

        <textarea  class="state-change-target textarea" id="termos_mot" name="termos_mot"  onkeyup="escrevendo()"  >
            <?php echo $termo_de_uso_motorista; ?>
        </textarea>
            <button  type="submit" class="botao lado_baixo botao_envia_pagamento" ><span>Salvar</span></button>
            <button  type="button" class="botao lado_baixo botao_envia_pagamento" onclick="ver_passageiro()" ><span>Ver termo do passageiro</span></button>
        </form>
    </div>
    <div class="col-lg-4 hidden-md hidden-sm hidden-xs" style="padding: 0">
        <div class="col-lg-12  col-md-12 col-sm-12" style="background: url('<?php echo base_url();?>style/img/cel.png')center center no-repeat;background-size: 300px;height: 650px;width: 95%;margin-left: 5%;padding: 0;border-radius: 8px;margin-top: -30px">
            <div class="header" style="width: 254px;background: #f8d509;margin: 0 auto;margin-top: 100px;margin-left: 52px">
                <p style="text-align: right;color: #fff;font-weight: bolder;padding: 15px 10px">You GO</p>
            </div>
            <div style="background: #292929;width: 254.3px;margin-left: 52.3px;margin-top: -10px;height: 409px;color: #fff;overflow-y: scroll">
                <body>
                <style>
                    p{
                        text-align: center;
                        width: 100%;
                    }

                    ::-webkit-scrollbar-track {
                        background-color: transparent;
                    }
                    ::-webkit-scrollbar {
                        width: 0px;
                        background: transparent;
                    }
                    ::-webkit-scrollbar-thumb  {
                        border-radius: 15px;
                        background: #f8d509;
                    }
                </style>

                <span style="text-align: center;color: #fff;margin-top: 10px;padding: 0 15px;" id="cel">
                    <h3 style="font-size: 14px;color: #fff;width: 230px;margin: 0 auto;word-wrap: break-word" id="texto_termo_mot"></h3>
                </span>

                </body>
            </div>
        </div>
    </div>
</div>




</div>