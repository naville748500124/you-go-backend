<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_notificacoes extends MY_Model {

		//Notificações do usuário
		public function view_notificacoes(){

			$id = $this->session->userdata('usuario');

			if(!is_null($id) && $id != '' && $id > 0){

				return $this->db->query("select 
										id_notificacao,
										titulo_notificacao,
										date_format(data_envio_notificacao,'%d/%m/%Y as  %H:%i:%s') as data_envio_notificacao,
										nome_usuario,
										data_leitura
										from cad_notificacao
										inner join cad_hist_notificacao on id_notificacao = fk_notificacao
										inner join seg_usuarios on fk_usuario = id_usuario
										where fk_usuario_destino = {$id} 
											and (data_limite_notificacao = 0 
													or data_limite_notificacao >= current_date);")->result();

			} else {
				die();
			}


		}

		public function view_notificar($where = null){

			if (isset($where[0])) {
				$this->db->select("*,date_format(data_limite_notificacao,'%d/%m/%Y') as data_limite_notificacao");
				$notificacoes = $this->db->get_where('cad_notificacao', array('id_notificacao' => $where[0]))->row();

				if (isset($notificacoes)) {
					foreach ($notificacoes as $key => $value) {
						$this->session->set_flashdata("{$key}",$value);
					}
				}
			}

			return array ('usuarios' => $this->db->get_where('seg_usuarios',array('ativo_usuario' => 1))->result(),
						  'grupos' => $this->db->get('seg_grupos')->result());

		}

		public function view_editar_notificacao($where = null){
			$this->db->select("*,date_format(data_limite_notificacao,'%d/%m/%Y') as data_limite_notificacao");
			$notificacoes = $this->db->get_where('cad_notificacao', array('id_notificacao' => $where[0]))->row();

			if (isset($notificacoes)) {
				foreach ($notificacoes as $key => $value) {
					$this->session->set_flashdata("{$key}_edicao",$value);
				}
			}

			return array ('usuarios' => $this->db->get('seg_usuarios')->result(),
						  'grupos' => $this->db->get('seg_grupos')->result(),

						  'notificados' => $this->db->query("SELECT nome_usuario, 
						    date_format(data_envio_notificacao,'%d/%m/%Y as  %H:%i:%s') as data_envio_notificacao, 
						    date_format(data_leitura,'%d/%m/%Y as  %H:%i:%s') as data_leitura,
						    notificacao_enviada
						    
						    from cad_notificacao
						    inner join cad_hist_notificacao on id_notificacao = fk_notificacao
						    inner join seg_usuarios on id_usuario = fk_usuario_destino
						    where id_notificacao = {$where[0]};")->result());

		}

		//Notificações enviadas pelo usuário
		public function view_hist_notificacoes(){

			return $this->db->query("select 
										id_notificacao,

										titulo_notificacao,

										date_format(data_notificacao,'%d/%m/%Y as  %H:%i:%s') as data_notificacao,

										(select count(*) from cad_hist_notificacao where fk_notificacao = id_notificacao) notificados,

										(select count(*) from cad_hist_notificacao where fk_notificacao = id_notificacao and data_leitura > 0) lidos

										from cad_notificacao
										inner join seg_usuarios on fk_usuario = id_usuario
										where fk_usuario = {$this->session->userdata('usuario')};")->result();

		}

		public function update($valores = null){

			$tabela = "cad_notificacao";
			$id = 'id_notificacao';

			$comparar = $this->db->get_where($tabela,array($id => $valores[$id]))->row_array();

			foreach ($valores as $key => $valor) {
				if ($valor != $comparar[$key]) {
					$log = array (
									'fk_usuario'=> $this->session->userdata('usuario'),
									'fk_aplicacao'=> $this->session->userdata('id_aplicacao_atual'),
									'original_edicao'=> $comparar[$key],
									'novo_edicao'=> "{$valor}",
									'campo_edicao'=> "{$key}",
									'tabela_edicao'=> $tabela,
									'id_edicao'=> $valores[$id],
								);

					$this->db->insert('seg_log_edicao',$log);
				}
			}

			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,$valores);

			$e = $this->db->error();
			if ($e['code'] != 0) {
				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'Model_notificacoes / update';
				return false;		
			} else {
				return true;
			}

		}

		public function create($valores = null){

			$this->db->insert('cad_notificacao',$valores);

			$e = $this->db->error();
			if ($e['code'] != 0) {
				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'Model_notificacoes / create';
				return 100;		
			} else {
				return $this->db->insert_id();
			}

		}

		public function loadNotificacao($usuario = null, $id = null){

			$data_leitura = $this->db->query("select data_leitura from cad_hist_notificacao
								where fk_usuario_destino = {$usuario} 
									and fk_notificacao = {$id}")->row()->data_leitura;

			if ($data_leitura == 0) {
				$this->db->query("update cad_hist_notificacao set data_leitura = current_timestamp, 															  notificacao_enviada = true
								where fk_usuario_destino = {$usuario} 
									and fk_notificacao = {$id}");
			}

			return $this->db->query("select *, 
							date_format(data_limite_notificacao,'%d/%m/%Y') as data_limite_notificacao,
							date_format(data_envio_notificacao,'%d/%m/%Y as  %H:%i:%s') as data_envio_notificacao,
							date_format(data_leitura,'%d/%m/%Y as %H:%i:%s') as data_leitura
							 from cad_notificacao 
							 inner join cad_hist_notificacao on id_notificacao = fk_notificacao 
							 where id_notificacao = {$id} and fk_usuario_destino = {$usuario}")->row();

		}

		//Cadastra quem deve ser notificado.
		public function notificar($id_notificacao = null, $usuarios = null, $grupos = null, $fk_usuario_nao_destino = null){

			if (count($usuarios) == 0 && count($grupos) == 0) {
				return true;
			} else {
				
				$usuario_where = "";

				if (count($usuarios) > 0) {
					foreach ($usuarios as $value) {
						if ($usuario_where == "") {
							$usuario_where .= $value;
						} else {
							$usuario_where .= ','.$value;
						}
					}
				}
				
				$grupo_where = "";

				if (count($grupos) > 0) {
					foreach ($grupos as $value) {
						if ($grupo_where == "") {
							$grupo_where .= $value;
						} else {
							$grupo_where .= ','.$value;
						}
					}
				}

				/*Define usuário que não devem receber a notificação*/
				$usuario_nao_where = "";

				if (count($fk_usuario_nao_destino) > 0) {
					foreach ($fk_usuario_nao_destino as $value) {
						if ($usuario_nao_where == "") {
							$usuario_nao_where .= $value;
						} else {
							$usuario_nao_where .= ','.$value;
						}
					}
				}

				if ($usuario_nao_where != "") {
					$usuario_nao_where = " and id_usuario not in (".$usuario_nao_where.")";
				}
				/*Define usuário que não devem receber a notificação*/

				if ($usuario_where == "") { //Caso seja direcionado a grupos apenas
					
					$ids = $this->db->query('select id_usuario
											from seg_usuarios
											inner join seg_grupos on fk_grupo_usuario = id_grupo
											where id_grupo in ('.$grupo_where.') '.$usuario_nao_where.'')->result();

				} else if ($grupo_where == "") { //Caso seja direcionado a usuários apenas

					$ids = $this->db->query('select id_usuario
											from seg_usuarios
											where id_usuario in ('.$usuario_where.')')->result();

				} else {

					$ids = $this->db->query('select id_usuario
											from seg_usuarios
											inner join seg_grupos on fk_grupo_usuario = id_grupo
											where id_grupo in ('.$grupo_where.') '.$usuario_nao_where.'

											union 

											select id_usuario
											from seg_usuarios
											where fk_grupo_usuario not in ('.$grupo_where.') and id_usuario in ('.$usuario_where.')')->result();

				}

				foreach ($ids as $id) {
					
					$dados = array('fk_usuario_destino' => $id->id_usuario,'fk_notificacao' => $id_notificacao);
					$this->db->insert('cad_hist_notificacao',$dados);

				}

			}

		}

		public function badges(){

			return $this->db->query('select count(*) as qtd from cad_hist_notificacao
										inner join cad_notificacao on id_notificacao = fk_notificacao
										where fk_usuario_destino = '.$this->session->userdata('usuario').' 
										and data_leitura = 0 and (data_limite_notificacao >= current_date or data_limite_notificacao = 0)')->row()->qtd;

		}

		public function ajaxNotificacoes(){

			$usuario = $this->session->userdata('usuario');

			$notificacoes = $this->db->query("select id_notificacao,titulo_notificacao, nome_usuario
										from cad_notificacao
										inner join cad_hist_notificacao on id_notificacao = fk_notificacao
										inner join seg_usuarios on id_usuario = fk_usuario
										where fk_usuario_destino = {$usuario}
										and data_leitura = 0 and notificacao_enviada <> true")->result();

			//Altera para indicar que a notificação não deve aparecer novamente.
			foreach ($notificacoes as $notificacao) {
				$this->db->query("update cad_hist_notificacao set notificacao_enviada = true
									where fk_usuario_destino = {$usuario} 
										and fk_notificacao = {$notificacao->id_notificacao}");
			}

			return $notificacoes;

		}

		###################################################################################################################
		//Notificações PUSH

		public function carregarFirebaseTokens() {
			return $this->db->query('SELECT distinct(token) FROM cad_tokens;');
		}

		public function novo_Token($token = null){
			return $this->db->insert('cad_tokens',$token);
		}

		public function deletarToken($token = null) {

			$this->db->where('token',$token);
			return $this->db->delete("cad_tokens");	
		
		}

		public function atualizarToken($novo = null, $antigo = null) {	

			return $this->db->query("update cad_tokens set token = '".$novo."' where token = '".$antigo."';");

		}


	}