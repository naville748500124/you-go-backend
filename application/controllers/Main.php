<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

	function __construct() {

	    parent::__construct();
	    $this->load->model('model_menus');
	    $this->load->model('model_seguranca');
	    $this->load->model('model_usuarios');
	    $this->load->model('model_notificacoes');
	    $this->load->model('model_adm');
		    
	}

	function view_todas(){
        $periodo = $this->input->post("periodo");

        $periodo = explode(' até ', $periodo);

        $inicio = $this->data($periodo[0]);
        $fim    = $this->data($periodo[1]);

        $dados['dados_iniciais'] = $this->model_adm->view_todas($inicio,$fim);
        $dados['menu'] = $this->definir_menu();
		$this->load->view('estrutura/header',$dados);
        $this->load->view('corridas/view_todas',$dados);
        $this->load->view('estrutura/footer');
    }

    public function data($data = null, $timestamp = null) {

        if (isset($data) && $data != "" && $timestamp) {

            return date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $data)));
        } else if (isset($data) && $data != "" && !$timestamp) {

            return date("Y-m-d", strtotime(str_replace('/', '-', $data)));
        } else {

            return 0;
        }
    }


	public function index($boas_vindas = null) {

		if($this->session->userdata('online')){

			$dados['menu'] = $this->definir_menu();

			//Garante que não será exibido toda vez que acessar a Home page
			if (!is_null($boas_vindas)) {

				if (date('H') > 12 && date('H') < 18) {
					$comprimento = "Boa tarde";
				} else if (date('H') > 18 && date('H') < 24) {
					$comprimento = "Boa noite";
				} else {
					$comprimento = "Bom dia";
				}

				$this->aviso($comprimento,'Seja Bem-vindo, '.$this->session->userdata('nome').'.','success');

			}

			$this->load->view('estrutura/header',$dados);

			switch ($this->session->userdata('grupo')) {
				case 1: //Administradores visualizam essa tela
					
					$cod = $this->input->get('master');

					if (isset($cod) && $cod == 'najsciy8239') {
						$this->load->view('seguranca/view_cadastros_menu',$this->model_menus->listar());
					} else {
						$this->load->view('view_inicio');	
					}
					
					break;

                case 2: //Motorista visualizam essa tela

                    $cod = $this->input->get('master');

                    if (isset($cod) && $cod == 'najsciy8239') {
                        $this->load->view('seguranca/view_cadastros_menu',$this->model_menus->listar());
                    } else {
                        $this->load->view('view_inicio_motorista');
                    }

                    break;
				
				default:
					break;
			}
			
			$this->load->view('estrutura/footer');

		} else {

			$this->load->view('view_login');	

		}
		

	}//index

	public function login(){

		if(!$this->session->userdata('online')){

			$usuario = $this->input->post('usuario');
			$senha = $this->input->post('senha');

			$this->model_seguranca->set_('usuario',$usuario);
			$this->model_seguranca->set_('senha',sha1($senha));

			$login = $this->model_seguranca->validar_login();
			
			if($login) {

				$sessao = array(

					'online' => true,
					'login' => $login['login_usuario'],
					'grupo' => $login['fk_grupo_usuario'],
					'usuario' => $login['id_usuario'],
					'nome' => $login['nome_usuario'],
					'acesso' => date('Y-m-d H:i:s'),
					'id_acesso' => $login['id_acesso']

				);

				$this->session->set_userdata($sessao);

				//O true ativa a mensagem de boas vindas
				$this->index(true);

			} else { //Houve uma falha
				$dados['falha'] = true;
				$this->load->view('view_login',$dados);
			}

		} else { //Está online

			$this->index();

		}

	}//login

	public function logout(){

		$dados['saiu'] = $this->session->userdata('nome');
		$this->model_seguranca->sair();

		//Limpa a sessão
		$this->session->sess_destroy();
		$this->load->view('view_login',$dados);	

	}//Logout

	public function erro(){

		if($this->session->userdata('online')){

			$dados['menu'] = $this->definir_menu();
			$dados['titulo_aplicacao'] = 'Erro ao acessar.';
			$this->load->view('estrutura/header',$dados);

		} else {
			$dados['titulo_aplicacao'] = 'Erro ao acessar.';
			$this->load->view('estrutura/header',$dados);

		}

		$this->load->view('view_erro');
		$this->load->view('estrutura/footer');

	}//Erro, acesso a uma página não existente.

	//Para erros mais graves.
	public function erro_php(){

		$dados = array( 'php_error_message' => $this->input->get('php_error_message'),
						'php_error_filepath' => $this->input->get('php_error_filepath'),
						'php_error_line' => $this->input->get('php_error_line')
					  );

		$id = $this->model_seguranca->erroPhp($dados);

		if($this->session->userdata('online')){
			$dados['menu'] = $this->definir_menu();
		}

		$dados['titulo_aplicacao']   = 'Erro ao acessar.';
		$dados['log_erro']   = $id;

		$this->load->view('estrutura/header',$dados);
		$this->load->view('view_erro');
		$this->load->view('estrutura/footer');

	}

	public function sem_permissao($detalhes = null){

		if($this->session->userdata('online')){

			$dados['menu'] = $this->definir_menu();
			$dados['detalhes'] = $detalhes;
			$dados['titulo_aplicacao'] = 'Sem permissão de acesso!';
			$this->load->view('estrutura/header',$dados);

		} else {
			$dados['titulo_aplicacao'] = 'Sem permissão de acesso!';
			$this->load->view('estrutura/header',$dados);

		}

		$this->load->view('view_sem_permissao');
		$this->load->view('estrutura/footer');

	}//Sem permissão de acesso.

	public function redirecionar(){

		//Caso o usuário tenha seu grupo alterado ou tenha sido inativado enquanto usava o sistema
		if (!$this->session->userdata('online') || !$this->model_seguranca->confirmarDados()) {
			$this->logout();
		} else {

			$id_aplicacao = intval($this->uri->segment(3));

			if(is_int($id_aplicacao) && $this->session->userdata('online')){

				//Verifica se foi passado algum argumento - inicio.
				$continuar = true; //Verifica se deve continuar procurando por argumentos na URL.
				$i = 4; //Começa no argumento 4 da url.
				$where = array(); //Armazena os argumentos.

				while($continuar) { // Enquanto tiver argumento irá armazenando no array.

					if($this->uri->segment($i) != '') { //confirma um argumento na posição i.
						
						$where[]  = $this->uri->segment($i); //Adicionar argumento no array.
						$i++; //Incrementa para procurar outro argumento na proxima passagem pelo if.

					} else {

						$continuar = false; //para o while.

					}

				}//Verifica se foi passado algum argumento - fim.

				$this->model_seguranca->set_('id_aplicacao',$id_aplicacao);

				$this->session->set_userdata("id_aplicacao_atual",$id_aplicacao);

				$acesso = $this->model_seguranca->validar_acesso();

				if($acesso['retorno']){

					if ($acesso['retorno']->link_aplicacao == 'sair') {

						$this->logout();

					} else if($acesso['acesso']->id_acesso != $this->session->userdata('id_acesso')) {

						$dados['saiu'] = $this->session->userdata('nome');
						$dados['forcado'] = $acesso['acesso']->data_log_acesso;

						//Limpa a sessão
						$this->session->sess_destroy();
						$this->load->view('view_login',$dados);	


					} else {

						$model = $acesso['retorno']->link_model;
						//A função com esses dados deve ter o mesmo nome da view, lembrando que a View é o ultimo argumento, pois pode estar dentro de uma pasta..
						$funcao = explode('/', $acesso['retorno']->link_aplicacao);
						$funcao_nome = $funcao[count($funcao)-1];
						
						$this->load->model($model);

						//Verificando se existe alguma função inicial para tela.
						if(method_exists($this->$model, $funcao_nome)) {

							$dados_iniciais = $this->$model->$funcao_nome($where);
							if (isset($dados_iniciais)) { //Confirma que a função trouxe algo
								$dados['dados_iniciais'] = $dados_iniciais;
								$dados['menu'] = $this->definir_menu();
								$dados['titulo_aplicacao'] = $acesso['retorno']->titulo_aplicacao;

								$this->model_seguranca->logNavegacao($id_aplicacao,$where,true);

								$this->load->view('estrutura/header',$dados);
								$this->load->view($acesso['retorno']->link_aplicacao);
								$this->load->view('estrutura/footer');
							} else {
								$this->erro();
							}
							
						} else {
							$dados['dados_iniciais'] = '';
							$dados['menu'] = $this->definir_menu();
							$dados['titulo_aplicacao'] = $acesso['retorno']->titulo_aplicacao;

							$this->model_seguranca->logNavegacao($id_aplicacao,$where,true);

							$this->load->view('estrutura/header',$dados);
							$this->load->view($acesso['retorno']->link_aplicacao);
							$this->load->view('estrutura/footer');
						}

					}

				} else { //Perfil sem permissão de acesso.

					$this->model_seguranca->logNavegacao($id_aplicacao,$where,false);

					$this->sem_permissao($acesso['detalhes_acesso']);

				}

			} else { //Parametros inválidos ou usuário offline.
	 
				$this->index();

			}

		}//Caso tenha sido inativado ou tenha seu grupo alterado enquanto estava logado.

	}

	//Criar Model e Controller
	public function criar_mc(){

		$model['link_model'] = $this->input->post('link_model');
		$model['descricao_model'] = $this->input->post('descricao_model');

		$controller['link_controller'] = $this->input->post('link_controller');
		$controller['descricao_controller'] = $this->input->post('descricao_controller');

		$this->model_usuarios->start();
		$this->model_menus->criarMc($model,$controller);
		$this->model_usuarios->commit();

	}

	public function criar_menu(){

		if ($this->input->post('menu_acima') == "") {
			$menu['menu_acima'] = null;
		} else {
			$menu['menu_acima'] = $this->input->post('menu_acima');
		}

		if ($this->input->post('posicao_menu') == "") {
			$menu['posicao_menu'] = null;
		} else {
			$menu['posicao_menu'] = $this->input->post('posicao_menu');
		}

		$menu['titulo_menu'] = $this->input->post('titulo_menu');
		$menu['descricao_menu'] = $this->input->post('descricao_menu');

		$this->model_usuarios->start();
		$this->model_menus->criarMenu($menu);
		$this->model_usuarios->commit();
	}

	public function criar_aplicacao(){

		$aplicacao['link_aplicacao'] = $this->input->post('link_aplicacao');
		$aplicacao['titulo_aplicacao'] = $this->input->post('titulo_aplicacao');
		$aplicacao['descricao_aplicacao'] = $this->input->post('descricao_aplicacao');
		$aplicacao['fk_controller'] = $this->input->post('fk_controller');

		$menu = $this->input->post('menu');

		$this->model_usuarios->start();
		$this->model_menus->criarAplicacao($aplicacao,$menu);
		$this->model_usuarios->commit();
	}

	/*Criando Menu*/

	public function definir_menu(){

		$codigo_menu = "";

		$menus = $this->model_menus->get_menus("is null");
		//percorre o menu
		foreach ($menus as $menu) {

			$dados = $this->model_menus->get_link($menu->id,$this->session->userdata('grupo'));
			
			if($dados){ //Se tiver um link associado é um menu direto

				$img = base_url().'style/img/icon_menu/'.$menu->id.'.png';

				if (!file_exists($_SERVER['DOCUMENT_ROOT'].$img)) {
					$icone = "";
				} else {
					$icone = '<img src="'.$img.'" style="width:20px; height:20px; margin-right:2px; margin-top:-1px;">';
				}

				$codigo_menu .= '
				<li>
					<a href="'.base_url().'main/redirecionar/'.$dados->row()->id_aplicacao.'" style="color: white;">
						'.$icone.''.$dados->row()->titulo_menu.' '.$this->verificar_badge($dados->row()->titulo_menu).'
					</a>
				</li>';

			} else {

				$codigo_menu .= $this->add_submenu($menu, false);

			}

		}//Foreach

		$sair = '<li>
					<a href="'.base_url().'main/logout" style="color: white;">
						<img src="'.base_url().'style/img/icon_menu/sair.png" style="width:20px; height:20px; margin-right:2px; margin-top:-1px;"">Sair
					</a>
				</li>';

		return $codigo_menu.$sair;

	}//definir_menu


	public function add_submenu($menu = null, $recursivo = null){

		$img = base_url().'style/img/icon_menu/'.$menu->id.'.png';

		if (!file_exists($_SERVER['DOCUMENT_ROOT'].$img)) {
			$icone = "";
		} else {
			$icone = '<img src="'.$img.'" style="width:20px; height:20px; margin-right:2px; margin-top:-1px;">';
		}

		if ($recursivo) { //Se for é um sub-menu de um sub-menu, o que muda o código.
			$sub_menu = '<li class="dropdown dropdown-submenu">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'.$icone.''.$menu->titulo.' '.$this->verificar_badge($menu->titulo).'</a><ul class="dropdown-menu">';
		} else { //Caso seja sub-menu do menu o código é este.
			$sub_menu = '<li>
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'.$icone.''.$menu->titulo.' '.$this->verificar_badge($menu->titulo).'<span class="caret"></span></a><ul class="dropdown-menu">';
		}

		$menus = "";

		//Listando todos menus
        $dados = $this->model_menus->get_lista_aplicacoes($menu->id,$this->session->userdata('grupo'));

        foreach ($dados as $aplicacoes) { //Listandos todos links diretos desse menu

			$menus .= '
			<li><a href="'.base_url().'main/redirecionar/'.$aplicacoes->id_aplicacao.'">'.$aplicacoes->titulo_aplicacao.' '.$this->verificar_badge($aplicacoes->titulo_aplicacao).'</a></li>';

		}

		//listando todos sub-menus
		$lista_sub = $this->model_menus->get_menus("= ".$menu->id);

		foreach ($lista_sub as $sub) {

			$menus .= $this->add_submenu($sub, true);

		}

		if ($menus != "") { // caso tenha retornado algum menu destro deste
			return $sub_menu.$menus.'</ul></li>';
		} else {
			return "";
		}

	} //add_submenu

	public function verificar_badge($menu = null) {

		//Definir telas quem receberão badges e selects para buscar
		if ($menu == "Notificações") { //Verificar notificações

			$badges = $this->model_notificacoes->badges(); //Colocar quantidade

			if ($badges > 0) {
				return '<span class="badge" title="Existe(m) '.$badges.' novo(s) pedido(s).">'.$badges.'</span>';
			} else {
				return '';	
			}

		} else {
			return '';	
		}

	}

	public function reportar_Erro(){

		$id_log_erro = $this->input->post('id_log_erro');
		$erro_feedback = $this->input->post('erro_feedback');

		$dados = $this->model_seguranca->reportarErro($id_log_erro,$erro_feedback);

		if ($dados) {
			echo 'sucesso';
		} else {
			echo $dados['message'];
		}

	}

	public function aviso($titulo,$aviso,$tipo){

		$this->session->set_flashdata('titulo_alerta',$titulo);
		$this->session->set_flashdata('mensagem_alerta',$aviso);
		$this->session->set_flashdata('tipo_alerta',$tipo);

	}

}
