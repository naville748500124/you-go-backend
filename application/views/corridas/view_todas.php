<head>
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/cupons.css" >
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/theme-animate.css" >
    <meta name="theme-color" content="#292929">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
</head>

<script type="text/javascript">
    $(document).ready(function(){
        $("#periodo").change(function(){
            $("form").submit();
        });
    });
</script>

<div id="planilha" class="tm30 wow fadeInDown" data-wow-duration="1s"  style="width: 400px;height: 250px;position: absolute;margin-left: 50%;left: -200px;margin-top: 10%;z-index: 999;display: none">
    <div id="box_ex">
        <a class="button" onclick="fechar()" id="close-btn" href="#"></a>
        <!-- <a class="button" onclick="fechar()" id="minimize-btn" href="#"></a> -->
        <h1 class="text animated fadeInDown" style="text-align: center;color: #fff;margin-top: 80px;">Planilha</h1>
        <div class="loading">
            <div class="dot" id="dot1"></div>
            <div class="dot" id="dot2"></div>
            <div class="dot" id="dot3"></div>
            <div class="dot" id="dot4"></div>
            <div class="dot" id="dot5"></div>
        </div>
        <h4 class="text" id="starting-txt" style="color: #fff;"> Exportando...</h4>
    </div>
</div>
<script>
    function  planilha() {
        $('#planilha').css('display','block');
        setTimeout(function(){ 
            window.open('http://<?php echo $_SERVER['SERVER_NAME'].base_url(); ?>Controller_adm/excel_corridas','_blank');
            }, 5000);
        setTimeout(function(){ 
                fechar();
            }, 6000);

    }
    function  fechar() {
        $('#planilha').css('display','none');
    }
</script>

<div class="col-lg-11">
        <h1 class="fade_1"><i class="fas fa-flag  fa-2x lado_topo" data-wow-duration="2s" data-wow-delay="0.0s" > </i> Corridas</h1>
    <div class="lado_direito" style="width: 100%;height: 2px;background: #f8d509;margin-top: 30px;overflow-x: hidden"></div>
</div>
<div class="col-lg-1">
    <h1 class="fade_1"><i onclick="planilha()" class="fas fa-file-excel lado_topo" style="margin-top: 50px;text-align: right" data-wow-duration="2s" data-wow-delay="0.0s" > </i></h1>
    Excel
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>



<div style="width: 100%" align="right"> 
    <form method="post" action="<?php echo base_url(); ?>main/view_todas">
        <label for="periodo">Período</label>
        <input class="form-control range_data" type="text" name="periodo" id="periodo" style="width: 200px">
    </form>
</div>


<div style="max-width: 100%; overflow: scroll;">
    <table class="table table-bordered table-hover" align="center">
        <thead style="background: #f1f1f1">
        <tr>

            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">ID</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Motorista</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Passageiro</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">R$</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">% YouGo</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">R$ Motorista</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Km</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Partida</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Destino</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Data</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Status</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Justificativa</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Status PagSeguro</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Ver</td>
        </tr>
        <tr>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th class="status_corrida"> </th>
            <th > </th>
            <th > </th>
            <td> </td>
        </tr>
        </thead>
        <tbody>
    <?php
    foreach ($dados_iniciais['dados'] as $dados){ ?>

        <tr>
            <td ><p style="margin-top: 15px;text-align: center"><?php echo $dados['id_corrida']; ?></p></td>
            <td ><p style="margin-top: 15px;text-align: center"><?php echo $dados['motorista']; ?></p></td>
            <td ><p style="margin-top: 15px;text-align: center"><?php echo $dados['passageiro']; ?></p></td>
            <td ><p class="mascara_monetaria" style="margin-top: 15px;text-align: center"><?php if($dados['fk_status_corrida'] == 87) {echo 'R$ 0,00';}else {echo $dados['valor_corrida'];} ?></p></td>
            <td ><p style="margin-top: 15px;text-align: center"><?php echo $dados['taxa_yougo']; ?>%</p></td>
            <td ><p class="mascara_monetaria" style="margin-top: 15px;text-align: center"><?php echo $dados['valor_motorista']; ?></p></td>
            <td ><p style="margin-top: 15px;text-align: center"><?php echo $dados['km_corrida']; ?> Km &nbsp;</p></td>
            <td ><p style="margin-top: 15px;text-align: center"><?php echo $dados['endereco_origem']; ?> </p></td>
            <td ><p style="margin-top: 15px;text-align: center"><?php echo $dados['endereco_destino']; ?></p></td>
            <td ><p style="margin-top: 15px;text-align: center"><?php echo $dados['data_corrida']; ?></p></td>
            <?php if ($dados['justificativa'] == "Tempo limite de procurar motorista.") { ?>
                <td ><p style="margin-top: 15px;text-align: center">Sem Motorista</p></td>
                <td ><p style="margin-top: 15px;text-align: center">Sem Motorista</p></td>
            <?php } else { ?>
                <td ><p style="margin-top: 15px;text-align: center"><?php echo $dados['status']; ?></p></td>
                <td ><p style="margin-top: 15px;text-align: center"><?php echo $dados['justificativa']; ?></p></td>
            <?php } ?>
            <td ><p style="margin-top: 15px;text-align: center"><?php

                    if(empty($dados['status_pagseguro'])){
                        echo 'Aguardando';
                    } else {
                        echo $dados['status_pagseguro'];
                    } ?>
                </p></td>
            <td><center><a href="<?php echo base_url()?>controller_adm/corrida_ver?id=<?php echo $dados['id_corrida']; ?>" target="_blank"><button  type="submit" class="botao_alt"  style="margin-top: -0px;border-radius: 5px"><span style="white-space: nowrap">Ver mais</span></button></center></td></a>
        </tr>


    <?php } ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#filtro_status').change(function(){
            console.log($(this).val());
            $('#filtro_status_corrida').val($(this).val());
            $('#filtro_status_corrida').keyup();

        });
    });
</script>


<style>

    .botao_alt {
        opacity: .80;
        background: #292929;
        border: 2px solid #292929;
        margin-top: 10px;
        padding: 8px 45px;
        color: #f8d509;
        position: relative;
        overflow: hidden;
        cursor: pointer;
    }

    .botao_alt:hover {
        opacity: 1;
        transition-duration: 0.3s;
        transition-timing-function: ease-in;
        transition-property: all;
    }
    .botao_alt span {
        position: relative;
        z-index: 100;
        font-size: 16px;
    }
    .botao_alt:before, .botao_alt:after {
        content: '';
        position: absolute;
        display: block;
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
    }
    .botao_alt:before {
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0 , 0);
        background-color: #000;
        color: #000;
        border: 1px solid #000;
        -webkit-transition: -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19), -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
    }
    .botao_alt:after {
        background-color: #000;
        color: #000;
        border: 1px solid #000;
        -webkit-transform: translate3d(100%, 0, 0);
        transform: translate3d(100%, 0, 0);
        -webkit-transition: -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
        transition: transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62), -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
    }
    .botao_alt:hover:before {
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
    }
    .botao_alt:hover:after {
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
    }

    #box_ex {
        width: 440px;
        height: 248px;
        margin: auto;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        position: absolute;
        background-color: #292929;
    }
    #logo {
        margin: 13px 0 0 13px;
        float: left;
    }
    #logo img {
        width: 18px;
        margin: auto;
        float: left;
        -webkit-filter: grayscale(100%) brightness(5);
        filter: grayscale(100%) brightness(5);
    }
    #logo h6 {
        color: white;
        font-size: 16px;
        font-weight: 200;
        font-family: Segoe UI;
        letter-spacing: 0px;
        margin: 0px auto auto 5px;
        float: left;
    }
    #box h1 {
        color: white !important;
        font-size: 65px;
        letter-spacing: -2px;
        margin: 67px 0 0 138px;
    }
    #box .text {
        color: #9fd5b7;
        font-weight: 400;
        font-family: Segoe UI;
    }
    #box h4 {
        font-size: 12px;
        font-weight: 400;
        opacity: 50%;
    }
    #starting-txt {
        margin: 60px 12px 0;
        float: left;
    }
    #author-txt {
        margin: 60px 17px 0;
        float: right;
    }
    #author-txt a {
        color: inherit;
        text-decoration: none;
    }
    .text img {
        width: 15px;
    }
    .button {
        width: 10px;
        height: 10px;
        right: 23px;
        top: 12px;
        opacity: 1;
        position: absolute;
    }
    .button:hover {
        opacity: 0.3;
    }
    #close-btn:before,
    #close-btn:after {
        width: 2px;
        height: 12px;
        left: 15px;
        content: ' ';
        background-color: white;
        position: absolute;
    }
    #close-btn:before {
        transform: rotate(45deg);
    }
    #close-btn:after {
        transform: rotate(-45deg);
    }
    #minimize-btn {
        width: 10px;
        height: 2px;
        right: 42px;
        top: 20px;
        content: ' ';
        background-color: white;
        position: absolute;
        float: right;
    }
    .loading {
        height: 20px;
        /* border: 1px solid #ddd; */
    }
    .dot {
        width: 4px;
        height: 4px;
        top: 160px;
        left: -10%;
        margin: auto;
        border-radius: 5px;
        background: white;
        position: absolute;
    }
    #dot1 {
        animation: dotslide 2.8s infinite cubic-bezier(0.2, .8, .8, 0.2);
    }
    #dot2 {
        animation: dotslide 2.8s .2s infinite cubic-bezier(0.2, .8, .8, 0.2);
    }
    #dot3 {
        animation: dotslide 2.8s .4s infinite cubic-bezier(0.2, .8, .8, 0.2);
    }
    #dot4 {
        animation: dotslide 2.8s .6s infinite cubic-bezier(0.2, .8, .8, 0.2);
    }
    #dot5 {
        animation: dotslide 2.8s .8s infinite cubic-bezier(0.2, .8, .8, 0.2);
    }
    @keyframes dotslide {
        0% {
            left: -20%;
        }
        100% {
            left: 110%;
        }
    }
</style>
