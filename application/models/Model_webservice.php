<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_webservice extends MY_Model {

		############################### Querys ###############################
		public function validar_login($valores){

			$this->db->select('id_usuario, email_usuario,senha_usuario,nome_usuario,cpf_usuario,ativado_sms, ativado_email, ativo_usuario, celular_usuario,fk_grupo_usuario, (select motorista_online from cad_detalhes_motorista where id_usuario = fk_usuario) as motorista_online,
				ifnull((CASE
						    WHEN fk_grupo_usuario = 2 THEN (select id_corrida from cad_corridas where fk_motorista = id_usuario and (fk_status_corrida < 86 or (hash_pagamento is null and fk_status_corrida < 88)) order by id_corrida desc limit 1)
						    WHEN fk_grupo_usuario = 3 THEN (select id_corrida from cad_corridas where fk_passageiro = id_usuario and (fk_status_corrida < 86 or (hash_pagamento is null and fk_status_corrida < 88)) order by id_corrida desc limit 1)
						END),0) as ultima_corrida,
						ifnull((SELECT fk_banco_motorista from cad_detalhes_motorista where fk_usuario = id_usuario),0) as dados_bancarios

				');

			$this->db->from('seg_usuarios');
			$this->db->join('seg_grupos','fk_grupo_usuario = id_grupo');
			$this->db->where('email_usuario',$valores['email_usuario']);
			$this->db->where('senha_usuario',$valores['senha_usuario']);
			$this->db->where('fk_grupo_usuario',$valores['fk_grupo_usuario']);
			$login = $this->db->get();

			if (isset($login) && !is_null($login) && $login->num_rows() == 1) {

				$login = $login->row_array();

				//Histórico de acesso mobile
				$token_acesso = md5(uniqid(rand(), true));

				$this->db->set('fk_usuario' 					, $login['id_usuario']);
				$this->db->set('ip_usuario_acesso_mobile' 		, $_SERVER['REMOTE_ADDR']);
				$this->db->set('maquina_usuario_acesso_mobile' 	, $_SERVER['HTTP_USER_AGENT']);
				$this->db->set('acesso_mobile' 					, true);
				$this->db->set('token_acesso' 					, $token_acesso);
				$this->db->insert('seg_log_acesso_mobile');

				$login['token_acesso'] 	= $token_acesso;

				return $login;
			} else {
				return false;
			}

		}

		public function validar_token($token) {

			//Pelo Token descubro o id do usuário no where, verifico se o token atual é o último gerado
			return $this->db->query("SELECT
						id_usuario,
					    fk_grupo_usuario,
					    email_usuario,
					    cpf_usuario,
					    motorista_online,
					    (ativo_usuario and ativado_sms and ativado_email) ativo,
					    (token_acesso = '{$token}') token_ativo
					    from
					    	seg_log_acesso_mobile slam
					        	inner join seg_usuarios on id_usuario = slam.fk_usuario
					        	left join cad_detalhes_motorista cdm on cdm.fk_usuario = id_usuario
					        	where slam.fk_usuario = (select fk_usuario from seg_log_acesso_mobile where token_acesso = '{$token}')
					            order by id_log_acesso_mobile desc
					            limit 1");
		}

		public function dados_bancarios($id_usuario) {
			return $this->db->query('select fk_banco_motorista,agencia_motorista,conta_motorista,digito_motorista,tipo_conta
										from cad_detalhes_motorista
											where fk_usuario = '.$id_usuario)->row_array();
		}

		public function dados_empresa(){
			return $this->db->query("select tel_empresa, email_empresa, tel_empresa_passageiro, email_empresa_passageiro, cep_empresa, uf_empresa, cidade_empresa, bairro_empresa, rua_empresa, numero_empresa,link_youtube from cad_dados_empresa limit 1")->row();
		}

		public function validar_login_facebook($valores){

			$this->db->select('id_usuario, email_usuario,senha_usuario,nome_usuario,cpf_usuario,ativado_sms, ativado_email, ativo_usuario, celular_usuario,fk_grupo_usuario, (select motorista_online from cad_detalhes_motorista where id_usuario = fk_usuario) as motorista_online,
				ifnull((CASE
						    WHEN fk_grupo_usuario = 2 THEN (select id_corrida from cad_corridas where fk_motorista = id_usuario and (fk_status_corrida < 86 or (hash_pagamento is null and fk_status_corrida < 88)) order by id_corrida desc limit 1)
						    WHEN fk_grupo_usuario = 3 THEN (select id_corrida from cad_corridas where fk_passageiro = id_usuario and (fk_status_corrida < 86 or (hash_pagamento is null and fk_status_corrida < 88)) order by id_corrida desc limit 1)
						END),0) as ultima_corrida,
						ifnull((SELECT fk_banco_motorista from cad_detalhes_motorista where fk_usuario = id_usuario),0) as dados_bancarios

				');

			$this->db->from('seg_usuarios');
			$this->db->join('seg_grupos','fk_grupo_usuario = id_grupo');
			$this->db->where('facebook_usuario',$valores['facebook_usuario']);
			$this->db->where('fk_grupo_usuario',$valores['fk_grupo_usuario']);
			$login = $this->db->get();

			if (isset($login) && !is_null($login) && $login->num_rows() == 1) {
				$login = $login->row_array();

				//Histórico de acesso mobile
				$token_acesso = md5(uniqid(rand(), true));

				$this->db->set('fk_usuario' 					, $login['id_usuario']);
				$this->db->set('ip_usuario_acesso_mobile' 		, $_SERVER['REMOTE_ADDR']);
				$this->db->set('maquina_usuario_acesso_mobile' 	, $_SERVER['HTTP_USER_AGENT']);
				$this->db->set('acesso_mobile' 					, true);
				$this->db->set('token_acesso' 					, $token_acesso);
				$this->db->insert('seg_log_acesso_mobile');

				$login['token_acesso'] 	= $token_acesso;

				return $login;
			} else {
				return false;
			}

		}

		public function filtroUf($uf){

			return $this->db->query("select
										id_item_grupo
											from cad_item_grupo
											where lower(sigla_item_grupo) = lower('{$uf}')")
							->row()
							->id_item_grupo;

		}

		public function ativarSms($id_usuario){

			return $this->db->query("update seg_usuarios
										set ativado_sms = 1
											where   id_usuario = '{$id_usuario}'");

		}

		public function ativarEmail($email) {
		    $this->db->where('email_usuario', $email);
		    $this->db->where('ativado_email', 0);
		    return $this->db->update('seg_usuarios', array('ativado_email'=>'1'));
		}

		public function validar_email_unico($email,$grupo){
			return $this->db->get_where("seg_usuarios",array('fk_grupo_usuario' => $grupo, 'email_usuario' => $email));
		}

		public function validar_cpf_unico($cpf,$grupo){
			return $this->db->get_where("seg_usuarios",array('fk_grupo_usuario' => $grupo, 'cpf_usuario' => $cpf));
		}
		######################################################
		//Editar Perfil
		######################################################
		public function editarUsuario($valores,$id_usuario){

			//Alterar
			$tabela = "seg_usuarios";
			$id = 'id_usuario';

			$this->gerarHistorico($id,$tabela,$valores,$id_usuario);
			$this->db->where(array($id => $id_usuario));
			$this->db->update($tabela,$valores);


			return $this->verificarErros($this->db->error(),'Model_webservice / editarUsuario');

		}
		######################################################
		//WS4 Lista Modelos
		######################################################
		public function buscarModelo($id_montadora,$nome = null){

			if(isset($nome)){

				return $this->db->query("select id_modelo,
											concat((select montadora
														from cad_montadoras
															where id_montadora = fk_montadora),' - ',modelo) modelo
									from cad_modelos
										where lower(concat((select montadora
														from cad_montadoras
															where id_montadora = fk_montadora),' - ',modelo)) like lower('%{$nome}%')
											order by modelo asc")->result();

			} else {

				return $this->db->select('id_modelo,modelo')
							->order_by("modelo", "asc")
							->get_where('cad_modelos',array('fk_montadora' => $id_montadora))
							->result();

			}

		}
		######################################################
		//WS5 Pré Cadastro Motorista
		######################################################
		public function listarPreCadastroMotorista(){
			//Lista de Gêneros, Cidades ativas, Marcas de carro.

			$dados['generos'] = $this->db->query('SELECT id_item_grupo as id, nome_item_grupo as nome
											from cad_item_grupo where fk_grupo = 3')
										 ->result();

			$dados['cidades'] = $this->db->query('SELECT id_cidades_atuacao as id, nome_cidades_atuacao as nome
											from cad_cidades_atuacao where ativa_cidades_atuacao = true order by nome_cidades_atuacao asc')
										 ->result();

			$dados['marcas'] = $this->db->query('SELECT id_montadora as id,montadora as nome
													from cad_montadoras
														where (select count(*) from cad_modelos where fk_montadora = id_montadora) > 0
															order by montadora asc')
										 ->result();

			return $dados;


		}
		######################################################
		//WS6 Cadastro Motorista
		######################################################
		public function cadastroMotorista($valores = null){

			$dados_usuario = array (

				'nome_usuario' => $valores['nome_usuario'],
				'email_usuario' => $valores['email_usuario'],
				'telefone_usuario' => $valores['telefone_usuario'],
				'senha_usuario' => $valores['senha_usuario'],
				'facebook_usuario' => $valores['facebook_usuario'],
				'rg_usuario' => $valores['rg_usuario'],
				'cpf_usuario' => $valores['cpf_usuario'],
				'celular_usuario' => $valores['celular_usuario'],
				'logradouro_usuario' => $valores['logradouro_usuario'],
				'cidade_usuario' => $valores['cidade_usuario'],
				'fk_uf_usuario' => $valores['fk_uf_usuario'],
				'bairro_usuario' => $valores['bairro_usuario'],
				'cep_usuario' => $valores['cep_usuario'],
				'complemento_usuario' => $valores['complemento_usuario'],
				'num_residencia_usuario' => $valores['num_residencia_usuario'],
				'fk_genero' => $valores['fk_genero'],

				'ativo_usuario' => 0, //Inicia inativo
				'fk_grupo_usuario' => 2, //Motorista
				'login_usuario' => $valores['email_usuario']

			);

			$this->db->insert('seg_usuarios',$dados_usuario);
			$id = $this->db->insert_id();


			if($id > 0){

				$dados_motorista = array (

					'fk_usuario' => $id,
					'fk_cidade_ativa_motorista' => $valores['fk_cidade_ativa_motorista'],
					'fk_modelo_carro_motorista' => $valores['fk_modelo_carro_motorista'],
					'cor_carro_motorista' => $valores['cor_carro_motorista'],
					'placa_carro_motorista' => $valores['placa_carro_motorista']

				);

				$this->db->insert('cad_detalhes_motorista',$dados_motorista);

				// /*Replicando para conta de passageiro*/
				// $dados_usuario['fk_grupo_usuario'] = 3;
				// $this->db->insert('seg_usuarios',$dados_usuario);
				// $id_passageiro = $this->db->insert_id();

			}


			if($this->verificarErros($this->db->error(),'Model_webservice / cadastroMotorista')) {
				//return array('id' => $id, 'id_passageiro' => $id_passageiro);
				return array('id' => $id);
			} else {
				return false;
			}


		}
		######################################################
		//WS7 Pré Edição Motorista
		######################################################
		public function preEditarMotorista($id_usuario){

			$dados['generos'] 	= $this->db->query('SELECT id_item_grupo as id, nome_item_grupo as nome
											from cad_item_grupo where fk_grupo = 3')
										 ->result();

			$dados['cidades'] 	= $this->db->query('SELECT id_cidades_atuacao as id, nome_cidades_atuacao as nome
											from cad_cidades_atuacao where ativa_cidades_atuacao = true order by nome_cidades_atuacao asc')
										 ->result();

			$dados['marcas'] 	= $this->db->query('SELECT id_montadora as id,montadora as nome
													from cad_montadoras
														where (select count(*) from cad_modelos where fk_montadora = id_montadora) > 0
															order by montadora asc')
										 ->result();

			$dados['motorista'] = $this->db->query("SELECT
										nome_usuario,
									    email_usuario,
									    telefone_usuario,
									    facebook_usuario,
									    rg_usuario,
									    cpf_usuario,
									    celular_usuario,
									    logradouro_usuario,
									    cidade_usuario,
									    fk_uf_usuario,
									    bairro_usuario,
									    cep_usuario,
									    complemento_usuario,
									    num_residencia_usuario,
									    fk_genero
									    	from seg_usuarios
									    	inner join cad_detalhes_motorista on id_usuario = fk_usuario
									    	where id_usuario = {$id_usuario}")->row_array();

			$dados['veiculo'] = $this->db->query("SELECT
									    fk_cidade_ativa_motorista,
									    (select fk_montadora from cad_modelos where id_modelo = fk_modelo_carro_motorista) as fk_montadora_carro_motorista,
									    fk_modelo_carro_motorista,
									    cor_carro_motorista,
									    placa_carro_motorista
									    	from seg_usuarios
									    	inner join cad_detalhes_motorista on id_usuario = fk_usuario
									    	where id_usuario = {$id_usuario}")->row_array();

			return $dados;


		}
		######################################################
		//WS8 Editar Cadastro Motorista
		######################################################
		public function editarMotorista($dados,$tipo_edicao = null){

			if (!is_null($tipo_edicao) && $tipo_edicao == 1) { //Edição do usuário

				$this->db->where(array('id_usuario' => $dados['id_usuario']));
				$this->db->update('seg_usuarios',$dados);


			} else if (!is_null($tipo_edicao) && $tipo_edicao == 2) { //Edição da tabela do motorista.

				$dados['ativo_usuario'] = false;
				$dados['motivo_inativacao'] = 'Inativado para realálise dos dados do veículo';

				$this->db->where(array('fk_usuario' => $dados['fk_usuario']));
				$this->db->update('cad_detalhes_motorista',$dados);

			} else { //Editou uma imagem
				//Quando é editado inativa o perfil
				$dados['ativo_usuario'] = false;
				$dados['motivo_inativacao'] = 'Inativado para reanálise das fotos';
				$this->ficarOnline(0,$dados['id_usuario']);
				$this->db->where(array('id_usuario' => $dados['id_usuario']));
				$this->db->update('seg_usuarios',$dados);
			}

			return $this->verificarErros($this->db->error(),'Model_webservice / editarMotorista');

		}
		######################################################
		//WS9 Ficar online
		######################################################
		public function ficarOnline($status,$id_usuario){

			return $this->db->query("UPDATE cad_detalhes_motorista
										set motorista_online = {$status}
											where fk_usuario = {$id_usuario}");


		}

		######################################################
		//WS10 Lista bancos
		######################################################
		public function buscarBancos() {
			return $this->db->select("id_item_grupo as id_banco, nome_item_grupo as banco")
							->get_where("cad_item_grupo",array('fk_grupo' => 2))->result();
		}
		######################################################
		//WS10.1 Cadastrar / Editar Dados bancários
		######################################################
		public function atualizarDadosBancarios($valores){

			//Alterar
			$tabela = "cad_detalhes_motorista";
			$id = 'fk_usuario';

			$this->gerarHistorico($id,$tabela,$valores,$valores['fk_usuario']);
			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,$valores);


			return $this->verificarErros($this->db->error(),'Model_webservice / atualizarDadosBancarios');

		}
		######################################################
		//WS11 Cadastro Passageiro
		######################################################
		public function cadastroPassageiro($valores = null){

			$this->db->insert('seg_usuarios',$valores);
			$id = $this->db->insert_id();

            if($this->verificarErros($this->db->error(),'Model_webservice / cadastroPassageiro')) {
                return $id;
            } else {
                return false;
            }


		}
		######################################################
		//WS12 Pré Edição Cliente
		######################################################
		public function preEditarCliente($id_usuario){

			return $this->db->query("SELECT
										nome_usuario,
										fk_genero,
										rg_usuario,
										cpf_usuario,
										email_usuario,
										senha_usuario,
										telefone_usuario,
										celular_usuario,
										facebook_usuario,
										logradouro_usuario,
										cidade_usuario,
										fk_uf_usuario,
										bairro_usuario,
										cep_usuario,
										num_residencia_usuario,
										complemento_usuario
									    	from seg_usuarios
									    	where id_usuario = {$id_usuario}")->row_array();


		}
		######################################################
		//WS13 Editar Passageiro
		######################################################
		public function editarPassageiro($valores){

			//Alterar
			$tabela = "seg_usuarios";
			$id = 'id_usuario';

			$this->gerarHistorico($id,$tabela,$valores,$valores['id_usuario']);
			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,$valores);

			return $this->verificarErros($this->db->error(),'Model_webservice / editarPassageiro');

		}
		######################################################
		//WS14 Cadastro forma de Pagamento
		######################################################
		public function novaFormaPagamento($valores = null){

			$this->db->insert('elo_pagamento_cliente',$valores);
			$id = $this->db->insert_id();

            if($this->verificarErros($this->db->error(),'Model_webservice / novaFormaPagamento')) {
                return $id;
            } else {
                return false;
            }

		}

		public function validarNumCartao($cartao,$id_usuario,$id_pagamento){
			return $this->db->query("select *
										from elo_pagamento_cliente
											where numero_cartao = '{$cartao}'
											  and fk_usuario = {$id_usuario} 
											  and id_pagamento <> {$id_pagamento} 
											  and cartao_ativo = true");
		}

		######################################################
		//WS15 Listar Formas de pagamento
		######################################################
		public function listarFormasPagamentos($id_usuario){

			return $this->db->query("SELECT
										id_pagamento,
										nome_cartao,
										concat('****.****.****.',right(numero_cartao,4)) as numero_cartao,
										date_format(data_vencimento_cartao,'%m/%Y') as data_vencimento_cartao,
										status_pagseguro,
										status_pag_seguro,
										(status_pag_seguro = 3 or status_pag_seguro = 4) as aprovado
									    	from elo_pagamento_cliente
									    		inner join pag_status on status_pag_seguro = id_status
									    		where fk_usuario = {$id_usuario}
									    		and cartao_ativo = 1")->result_array();


		}
		######################################################
		//WS15.1 Remover Formas de pagamento
		######################################################
		public function removerFormaPagamento($id_pagamento){

			return $this->db->query("UPDATE elo_pagamento_cliente 
										SET nome_cartao = null, 
										numero_cartao = null,
										data_vencimento_cartao = null,
										cvv_cartao = null,
										data_nascimento = null,
										hash_pagamento = null,
										token_pagamento = null,
										code_pag_seguro = null,
										status_pag_seguro = 0,
										cartao_ativo = 0 
									    	where id_pagamento = {$id_pagamento}");


		}
		public function reativarFormaPagamento($id_pagamento){

			return $this->db->query("UPDATE elo_pagamento_cliente 
										SET cartao_ativo = 1
									    	where id_pagamento = {$id_pagamento}");


		}
		######################################################
		//WS16 Editar forma de Pagamento
		######################################################
		public function editarFormaPagamento($valores){

			//Alterar
			$tabela = "elo_pagamento_cliente";
			$id = 'id_pagamento';

			$this->gerarHistorico($id,$tabela,$valores,$valores['fk_usuario']);
			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,$valores);

			return $this->verificarErros($this->db->error(),'Model_webservice / editarFormaPagamento');

		}

		######################################################
	    //WS17 Custo corrida
	    ######################################################
		public function dados_cupom($cupom){

			return $this->db->query("select id_cupom, nome_cupom, valor_desconto_cupom, tipo_desconto_cupom
										from cad_cupons
											where ativo_cupom = true
												and quantidade_cupom > 0
												and current_date >= data_inicio_cupom
												and current_date <= data_termino_cupom
												and nome_cupom = '{$cupom}'");


		}

		public function validar_cupom($id_cupom){

			$dados = $this->db->query("select id_cupom,(quantidade_cupom - 1) quantidade_cupom_atualizada
										from cad_cupons
											where ativo_cupom = true
												and quantidade_cupom > 0
												and current_date >= data_inicio_cupom
												and current_date <= data_termino_cupom
												and id_cupom = {$id_cupom}");

			if(isset($dados) && $dados->num_rows() > 0) {
				$dados = $dados->row();

				$this->db->where('id_cupom',$dados->id_cupom);
				$this->db->update('cad_cupons',array('quantidade_cupom' => $dados->quantidade_cupom_atualizada));
				return $dados->id_cupom;
			} else {
				return null;
			}

		}

		######################################################
		//WS17.1 Solicitar Corrida
		######################################################
		public function solicitarCorrida($dados){

			$this->db->insert('cad_corridas',$dados);
			$id = $this->db->insert_id();

			if($this->verificarErros($this->db->error(),'Model_webservice / cadastroMotorista')) {
				return $id;
			} else {
				return false;
			}


		}

		public function validarCorrida($km,$cidade) {

			$custos['geral'] = $this->db->query("
				SELECT (CASE
						    WHEN (valor_tarifa * {$km}) > valor_tarifa_minima THEN valor_tarifa * {$km}
						    WHEN usar_tarifa_minima THEN valor_tarifa_minima
						    else (valor_tarifa * {$km})
						END) as valor_corrida,
					(CASE
						    WHEN usar_tarifa_minima THEN valor_tarifa_minima
						    else 0
						END) as valor_tarifa_minima,
					(km_maximo > {$km}) as km_valido,
					valor_tarifa,
					km_maximo,
					taxa_cancelamento,
					taxa_yougo,

					(SELECT id_cidades_atuacao
						from cad_cidades_atuacao
							where lower(nome_cidades_atuacao) = lower('{$cidade}')
								and ativa_cidades_atuacao = true
									limit 1) fk_cidade
      						from cad_tarifa_corrida, cad_configuracoes
      							where current_time BETWEEN hora_inicio AND hora_fim
        								OR (NOT current_time BETWEEN hora_fim
        									AND hora_inicio
        										AND hora_inicio > hora_fim)")->row();

			if(isset($custos['geral']->fk_cidade)) {
					$custos['taxas'] = $this->db->query("select * from cad_tarifa_cidade where status_tarifa = 1 and  fk_cidade = ".$custos['geral']->fk_cidade)->result_array();
			} else {
				 $custos = false;
			}



			return $custos;

		}

		public function historicoTaxasCorrida($dados,$id){

			foreach ($dados as $taxa) {
				$taxa['fk_corrida'] = $id;
				unset($taxa['id_tarifa_cidade']); //Como esta copiando o original o ID estava junto.
				$this->db->insert('hist_tarifa_cidade',$taxa);

			}

		}

		public function comprovanteCorrida($id_corrida){

			$dados['geral'] = $this->db->select("*, 
				(select valor_corrida from view_custo_final vcf where vcf.id_corrida = cad_corridas.id_corrida) custo_final, (select (case tipo_desconto_cupom
														when 0 then concat('R$ ',REPLACE(round(valor_desconto_cupom,2),'.',','))
            											when 1 then concat(REPLACE(round(valor_desconto_cupom,2),'.',','),' %') end) from cad_cupons
            											where id_cupom = fk_cupom) as cupom,
            											
            											concat('R$ ',format(valor_corrida,2,'de_DE')) as valor_corrida_original")
									   ->get_where('cad_corridas',		array('id_corrida' => $id_corrida))->row();
			$dados['taxas'] = $this->db->get_where('hist_tarifa_cidade',array('fk_corrida' => $id_corrida))->result();

			return $dados;

		}

		######################################################
		//WS18 Deslicamento Motorista
		######################################################
		public function deslocamentoMotorista($valores){

			//Alterar
			$tabela = "cad_detalhes_motorista";
			$id 	= 'fk_usuario';

			$this->gerarHistorico($id,$tabela,$valores,$valores['fk_usuario']);
			$this->db->set('data_atualizacao_deslocamento', 'NOW()', FALSE);
			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,$valores);

			return $this->verificarErros($this->db->error(),'Model_webservice / deslocamentoMotorista');

		}
		######################################################
		//WS19 Cancelar Corrida
		######################################################
		public function cancelarCorrida($id_corrida,$justificativa,$id_usuario,$status,$gratis = 0) {


			$corrida = $this->db->select('id_corrida,valor_corrida,fk_motorista')
									 ->get_where('cad_corridas',array('id_corrida'=>$id_corrida))->row();

			if(isset($corrida->fk_motorista) && $corrida->fk_motorista != ""){
				$this->ficarOnline(1,$corrida->fk_motorista);
			}
		

			if(CANCELADO_MOTORISTA == $status) {
				$this->ficarOnline(1,$id_usuario);
			} else {
				$corrida = $this->db->get_where('cad_corridas',array('id_corrida'=>$id_corrida))->row();

				if(isset($corrida->fk_motorista) && $corrida->fk_motorista != ""){
					$this->ficarOnline(1,$corrida->fk_motorista);
				}
			}

			if($gratis != 1){
				$preco = $corrida->valor_corrida;
			} else {
				$preco = 0;
			}

			$this->db->query("update cad_corridas
												set data_fim_corrida = current_timestamp(),
												justificativa = '{$justificativa}',
												fk_cancelado_por = {$id_usuario},
												fk_status_corrida = {$status},
												valor_corrida = {$preco}
													where id_corrida = {$id_corrida}");
												

			return $this->db->query("update cad_solicitacao_corrida
												set solicitacao_aceita = false
												where fk_corrida = {$id_corrida}
													and solicitacao_aceita is null");


		}
		######################################################
		//WS20 Acompanhar Motorista
		######################################################
		public function acompanharCorrida($fk_grupo_usuario,$id_usuario,$id_corrida) {

			if($fk_grupo_usuario == MOTORISTA) {

				return $this->db->query("SELECT
										latitude_origem,
										longitude_origem,
										endereco_origem,
										latitude_destino,
										longitude_destino,
										endereco_destino,
										qtd_malas,
										qtd_passageiros,
										id_corrida,
										fk_passageiro as id_passageiro,
										nome_usuario,
										celular_usuario,
										(select valor_corrida_normal from view_custo_final vcf where 
										vcf.id_corrida = cc.id_corrida) as valor_corrida,
										fk_status_corrida cod,
										nome_item_grupo as status

                                        from cad_corridas cc
                                        inner join cad_solicitacao_corrida on id_corrida = fk_corrida
                                        inner join seg_usuarios on id_usuario = fk_passageiro
                                        inner join cad_item_grupo on id_item_grupo = fk_status_corrida
                                        	where id_corrida = {$id_corrida}
                                        	and cc.fk_motorista = {$id_usuario}");


			} else {

					return $this->db->query("SELECT
											fk_status_corrida as cod,
											nome_item_grupo as status,
										    valor_corrida as custo_corrida,
										    geo(latitude_atual,longitude_atual,latitude_origem,longitude_origem)
										    	as distancia,

										    latitude_atual as latitude_motorista,
										    longitude_atual as longitude_motorista,

										    latitude_origem,
											longitude_origem,
											endereco_origem,

											endereco_destino,
										    latitude_destino,
											longitude_destino,

										    nome_usuario,
										    celular_usuario,
										    id_usuario,
										    fk_motorista,
										    nome_usuario,
										    celular_usuario,
										    (select valor_corrida_normal from view_custo_final vcf where 
											vcf.id_corrida = cc.id_corrida) as valor_corrida,
										    cc.cor_carro_motorista as cor_veiculo,
										    cc.placa_carro_motorista as placa_veiculo,
										    concat((select concat(montadora,' ',modelo) from cad_modelos inner join cad_montadoras on id_montadora = fk_montadora where id_modelo = cc.fk_modelo_carro_motorista)) as carro,

										    (select round((sum(pontualidade_motorista +
										    			simpatia_motorista +
										    			carro_motorista +
										    			servico_motorista)/4),2) from view_avaliacoes_motorista vam
															where vam.fk_motorista = cc.fk_motorista) as nota_media,

											((SELECT tempo_busca from cad_configuracoes) - 
											TIMESTAMPDIFF(SECOND,cc.data_corrida,CURRENT_TIME)) as tempo_busca,

											((SELECT tempo_cancelamento from cad_configuracoes) - 
											TIMESTAMPDIFF(SECOND,cc.data_inicio_corrida,CURRENT_TIME)) as tempo_cancelamento

										    	from cad_corridas cc
										    		inner join cad_item_grupo on id_item_grupo = fk_status_corrida
										    		left join seg_usuarios on id_usuario = fk_motorista
										        	left join cad_detalhes_motorista cdm on cdm.fk_usuario = cc.fk_motorista
														where id_corrida = {$id_corrida}");
			}

		}
		######################################################
		//WS21 Avalias Corrida
		######################################################
		public function avaliarCorrida($valores,$grupo) {

			$tabela = "cad_avaliacoes_corrida";
			$id 	= 'fk_corrida';

			$existe = $this->db->get_where($tabela,array($id => $valores[$id]));

			if($existe->num_rows() > 0){ //Update

				if($grupo == MOTORISTA) {
					$this->db->set('data_avaliacao_motorista', 'NOW()', FALSE);
				} else {
					$this->db->set('data_avaliacao_passageiro', 'NOW()', FALSE);
				}

				$this->db->where(array($id => $valores[$id]));
				$this->db->update($tabela,$valores);

			} else {

				if($grupo == MOTORISTA) {
					$this->db->set('data_avaliacao_motorista', 'NOW()', FALSE);
				} else {
					$this->db->set('data_avaliacao_passageiro', 'NOW()', FALSE);
				}
				$this->db->insert($tabela,$valores);

			}

			return $this->verificarErros($this->db->error(),'Model_webservice / avaliarCorrida');

		}
		######################################################
		//WS22 Motorista Aceita / Rejeita Corrida
		######################################################
		public function responderCorrida($valores){

			if($valores['fk_status_corrida'] == BUSCANDO_MOTORISTA) { // Recusou

				return $this->cancelarSolicitacoes($valores['id_solicitacao_corrida']);

			} else if($valores['fk_status_corrida'] == AGUARDANDO_MOTORISTA) { //Só aceita se ainda houver tempo

				$this->db->query("UPDATE cad_solicitacao_corrida
								SET solicitacao_aceita = true,
									respondido_em = current_timestamp
										WHERE id_solicitacao_corrida = {$valores['id_solicitacao_corrida']}
											and TIMESTAMPDIFF(SECOND,solicitado_em,CURRENT_TIME) <= (SELECT tempo_espera from cad_configuracoes)
											and TIMESTAMPDIFF(SECOND,solicitado_em,CURRENT_TIME) <= (SELECT tempo_busca from cad_configuracoes)
											and solicitacao_aceita is null");

				if($this->db->affected_rows() == 1){ //Conseguiu atualizar a solicitação

					$dados = $this->db->query("SELECT
												fk_usuario as fk_motorista,
												fk_modelo_carro_motorista,
												cor_carro_motorista,
												placa_carro_motorista
													from cad_detalhes_motorista
														where fk_usuario =
															(select cad_solicitacao_corrida.fk_motorista
																from cad_solicitacao_corrida
																	where id_solicitacao_corrida = {$valores['id_solicitacao_corrida']})")->row_array();

					$this->ficarOnline(0,$dados['fk_motorista']); //Deixa offline

					$now = date('Y-m-d H:i:s');
					$dados['data_inicio_corrida'] = $now;
					$dados['fk_status_corrida'] = $valores['fk_status_corrida'];

					$this->db->where(array('id_corrida' => $valores['id_corrida']));
					return $this->db->update('cad_corridas',$dados);


				} else { //Não foi respondida a tempo.


					$dados = $this->db->query("SELECT solicitacao_aceita,
														TIMESTAMPDIFF(SECOND,solicitado_em,CURRENT_TIME) <= (SELECT tempo_espera from cad_configuracoes) tempo_responder
														FROM  cad_solicitacao_corrida
															WHERE id_solicitacao_corrida
																	= {$valores['id_solicitacao_corrida']}")->row();

					if(is_null($dados->solicitacao_aceita)) {

						if(!$dados->tempo_responder){
							return -1; //Acabou o tempo
						}

					} else if($dados->solicitacao_aceita) {
						return -2; // Já aceitou a corrida
					} else {
						return -3; // Já recusou a corrida
					}


				}
			}

			return $this->verificarErros($this->db->error(),'Model_webservice / responderCorrida');

		}
		######################################################
		//WS23 Iniciar Corrida
		######################################################
		public function iniciarCorrida($valores,$id_usuario){

			//Alterar
			$tabela = "cad_corridas";
			$id 	= 'id_corrida';

			$this->gerarHistorico($id,$tabela,$valores,$id_usuario);
			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,$valores);

			return $this->verificarErros($this->db->error(),'Model_webservice / iniciarCorrida');

		}
		######################################################
		//WS24 Finalizar Corrida
		######################################################
		public function finalizarCorrida($valores,$id_usuario){

			//Alterar
			$tabela = "cad_corridas";
			$id 	= 'id_corrida';

			$this->gerarHistorico($id,$tabela,$valores,$id_usuario);
			$this->db->query("update {$tabela} set
									latitude_fim_corrida 	= {$valores['latitude_fim_corrida']},
									longitude_fim_corrida 	= {$valores['longitude_fim_corrida']},
									fk_status_corrida 		= {$valores['fk_status_corrida']},
									data_fim_corrida		= current_timestamp
										where id_corrida = {$valores['id_corrida']}");

			$this->ficarOnline(1,$id_usuario); //Deixa online

			return $this->verificarErros($this->db->error(),'Model_webservice / finalizarCorrida');

		}

		public function statusCorrida($id_corrida){
			return $this->db->query("SELECT id_item_grupo cod ,nome_item_grupo status from cad_item_grupo
										where id_item_grupo = (SELECT fk_status_corrida
																from cad_corridas
																	where id_corrida = {$id_corrida})")->row();
		}
		######################################################
	    //WS25 Viagens
	    ######################################################
	    public function listarCorridas($id_usuario, $id_grupo){

	    	if($id_grupo == MOTORISTA) {
	    		$filtro 	= "fk_motorista"; //Quero o nome do passageiro
	    		$usuario 	= "fk_passageiro"; //Quero o nome do motorista

	    		$select = "concat('R$ ',format(valor_corrida,2,'de_DE')) as valor_corrida,";

	    	} else if ($id_grupo == PASSAGEIRO) {
	    		$filtro 	= "fk_passageiro"; //Quero o nome do motorista
	    		$usuario 	= "fk_motorista"; //Quero o nome do motorista

	    		$select = "(select valor_corrida from view_custo_final vcf where vcf.id_corrida = cc.id_corrida) as valor_corrida,";

	    	}

	    	return $this->db->query("
	    		SELECT
					id_corrida,
					id_usuario,
					pagamento_motorista,
				    date_format(data_corrida,'%d/%m/%Y às %H:%i:%s') as data_corrida_inicio,
				    date_format(data_fim_corrida,'%d/%m/%Y às %H:%i:%s') as data_corrida_fim,
				    ifnull((select (data_avaliacao_passageiro is not null) from cad_avaliacoes_corrida
				    	where fk_corrida = id_corrida),0) passageiro_avaliou,
				    ifnull((select (data_avaliacao_motorista is not null) from cad_avaliacoes_corrida
				    	where fk_corrida = id_corrida),0) motorista_avaliou,
				    nome_usuario,
				    endereco_origem,
					endereco_destino,
				    {$select}
					km_corrida,
				    concat((select concat(montadora,' - ',modelo) from cad_modelos inner join cad_montadoras on id_montadora = fk_montadora where id_modelo = fk_modelo_carro_motorista),' ',cor_carro_motorista ) as carro,
				    placa_carro_motorista,
					justificativa as motivo_cancelamento,
				    (select nome_item_grupo from cad_item_grupo where id_item_grupo = fk_status_corrida) as status_corrida

				    from cad_corridas cc
				    	inner join seg_usuarios on id_usuario = {$usuario}
				    		where {$filtro} = {$id_usuario}
				    		and !(fk_status_corrida = 87 and valor_corrida = 0)
				    		order by data_corrida desc")->result();



	    }
	    ######################################################
		//26 Média Avaliações
		######################################################
	    public function mediaAvaliacoes($id_usuario,$fk_grupo_usuario) {

	    	if($fk_grupo_usuario == MOTORISTA) {
	    		return $this->db->get_where('view_avaliacoes_motorista',
	    									array('fk_motorista' => $id_usuario))->row();
	    	} else if ($fk_grupo_usuario == PASSAGEIRO) {
	    		return $this->db->get_where('view_avaliacoes_passageiro',
	    									array('fk_passageiro' => $id_usuario))->row();
	    	}


	    }

	    public function histPagamentos($id_usuario,$data_corrida = null, $data_fim_corrida = null,$limit = 0, $offset = 0,$pago = null) {

	    	$where = "";

	    	if(!is_null($data_corrida)) {
	    		$where .= " and date('{$data_corrida}') <= date(data_corrida)";
	    	}

	    	if(!is_null($data_fim_corrida)) {
	    		$where .= " and date('{$data_fim_corrida}') >= date(data_fim_corrida)";
	    	}

	    	if(!is_null($pago)) {
					if($pago == 0){
							$where .= " and (pagamento_motorista = 0 || pagamento_motorista is null)";
					} else {
							$where .= " and pagamento_motorista = 1";
					}
	    	}

	    	if(!is_null($limit)) {
	    		$where .= "ORDER BY id_corrida DESC limit {$limit} ";
	    	}

	    	if(!is_null($offset)) {
	    		$where .= " offset {$offset} ";
	    	}

	    	return $this->db->query("SELECT	id_corrida,
											nome_usuario,
									        concat('R$',replace(round((select valor_corrida from view_custo_final_motorista vcfm where vcfm.id_corrida = cad_corridas.id_corrida),2),'.',',')) as valor_corrida,
											date_format(data_corrida,'%d/%m/%Y às %H:%i:%s') as data_corrida_inicio,
				    						date_format(data_fim_corrida,'%d/%m/%Y às %H:%i:%s') as data_corrida_fim,
									        ifnull(pagamento_motorista,0) as pagamento_motorista
									        from cad_corridas
											inner join cad_item_grupo on id_item_grupo = fk_status_corrida
											inner join seg_usuarios on id_usuario = fk_motorista
											    	where fk_motorista = {$id_usuario}
									                	and fk_status_corrida = 86
									                	{$where} ")->result();
	    }
		######################################################
		//Mostrar motorista próximo
		######################################################
		public function existeSolicitacao($id_corrida){

			/* Buscando por solicitações em aberto que tenham passado do tempo limite de espera, ou que tenha sido aceita
			*/

			return $this->db->query("
				SELECT (TIMESTAMPDIFF(SECOND,solicitado_em,CURRENT_TIME) <= tempo_espera) as em_tempo,
					id_solicitacao_corrida,
					solicitacao_aceita,
					latitude_atual,
					longitude_atual,
					nome_usuario,
					celular_usuario,
					id_usuario,
					concat((select concat(montadora,' - ',modelo) from cad_modelos inner join cad_montadoras on id_montadora = fk_montadora where id_modelo = fk_modelo_carro_motorista),' Placa: (',placa_carro_motorista,') ',cor_carro_motorista ) as carro
			    	from cad_configuracoes,cad_solicitacao_corrida
			    		inner join cad_detalhes_motorista on fk_motorista = fk_usuario
			    			inner join seg_usuarios on id_usuario = fk_usuario
				        		where fk_corrida = {$id_corrida}
			        				order by solicitado_em desc
				        				limit 1")->row();

		}
		public function criarSolicitacoes($fk_corrida){

			/* Buscando o motorista que atenda:
				1) Mostrou estar ativo nos últimos 60 segundos,
				2) Não rejeitou anteriormente uma solicitação desta mesma corrida,
				3) Não existe outra corrida na qual já foi selecionado e ainda não respondeu
				4) Não esteja atualmente em outra corrida / offline
				5) É o proxímo da origem da corrida
				6) Atua na mesma cidade
			*/
			$motorista = $this->db->query("
				SELECT
					id_usuario,
                	round(geo(latitude_atual,longitude_atual,latitude_origem,longitude_origem),3) distancia
			    		from cad_corridas, seg_usuarios
			    			inner join cad_detalhes_motorista on id_usuario = cad_detalhes_motorista.fk_usuario
			    				where fk_grupo_usuario = ".MOTORISTA."
			        				and id_corrida = {$fk_corrida}
			        				and motorista_online = true
			        				and fk_cidade_origem = fk_cidade_ativa_motorista
			        				and TIMESTAMPDIFF(SECOND,data_atualizacao_deslocamento,CURRENT_TIME) < ".TEMPO_CONSIDERADO_ATIVO."
			        				and id_usuario not in (select fk_motorista from cad_solicitacao_corrida 									where fk_corrida = {$fk_corrida})
			        				and id_usuario not in (select fk_motorista from cad_solicitacao_corrida, 								cad_configuracoes
			        											where solicitacao_aceita is null
			        												and TIMESTAMPDIFF(SECOND,solicitado_em,CURRENT_TIME) <= tempo_espera)
                        				order by distancia asc
                            				limit 1")->row();

			if(!is_null($motorista)){

				$dados = array ('fk_corrida' => $fk_corrida, 'fk_motorista' => $motorista->id_usuario);
				$this->db->insert('cad_solicitacao_corrida',$dados);



			}

		}
		public function cancelarSolicitacoes($id_solicitacao_corrida){

			return $this->db->query("UPDATE cad_solicitacao_corrida
										set solicitacao_aceita = false,
											respondido_em = current_timestamp
												where id_solicitacao_corrida = {$id_solicitacao_corrida}
													and solicitacao_aceita is null");


		}

		public function  corridaEmEspera($id_usuario){

			return $this->db->query("
				SELECT
					latitude_origem,
				    longitude_origem,
				    endereco_origem,
				    latitude_destino,
				    longitude_destino,
				    endereco_destino,
				    qtd_malas,
					qtd_passageiros,
					valor_corrida,

					(tempo_espera - TIMESTAMPDIFF(SECOND,solicitado_em,CURRENT_TIME)) tempo_restante,

				    id_corrida,
				    (select id_solicitacao_corrida
                    	from cad_solicitacao_corrida, cad_configuracoes
                    		where fk_motorista = {$id_usuario}
                        		and TIMESTAMPDIFF(SECOND,solicitado_em,CURRENT_TIME) <= tempo_espera
                            	and solicitacao_aceita is null) id_solicitacao,
				    fk_passageiro as id_passageiro,
				    nome_usuario,
				    celular_usuario

				    	from cad_configuracoes, cad_corridas
				    	left join cad_solicitacao_corrida csc1 on id_corrida = fk_corrida
				    	inner join seg_usuarios on id_usuario = fk_passageiro
				        	where id_solicitacao_corrida = (
				        					select id_solicitacao_corrida
				                                	from cad_solicitacao_corrida csc2, cad_configuracoes
				                                		where fk_motorista = {$id_usuario}
					                                		and TIMESTAMPDIFF(SECOND,solicitado_em,CURRENT_TIME) <= tempo_espera
				                                        	and solicitacao_aceita is null
				                                        	and csc1.fk_corrida = csc2.fk_corrida)
                                	and fk_status_corrida = 83")->row();

		}

		public function distanciaMinima($id_corrida,$latitude_atual,$longitude_atual){

			$push_proximidade = $this->db->query("
				SELECT

				((geo({$latitude_atual},{$longitude_atual},latitude_origem,longitude_origem) < ".DISTANCIA_AVISAR.") and fk_status_corrida = 84 and (aviso_proximidade_origem is null or aviso_proximidade_origem = false)) as avisar_origem,

					((geo({$latitude_atual},{$longitude_atual},latitude_destino,longitude_destino) < ".DISTANCIA_AVISAR.") and fk_status_corrida = 85 and (aviso_proximidade_destino is null or aviso_proximidade_destino = false)) as avisar_destino

					FROM cad_corridas
						where id_corrida = {$id_corrida}");


			if(!is_null($push_proximidade->row())){

				if($push_proximidade->row()->avisar_origem) {

					$this->db->query("update cad_corridas set aviso_proximidade_origem = true
										where id_corrida = {$id_corrida}");

				} else if ($push_proximidade->row()->avisar_destino) {

					$this->db->query("update cad_corridas set aviso_proximidade_destino = true
										where id_corrida = {$id_corrida}");

				}

				return $push_proximidade->row();

			}

		return null;


		}

		public function descobrirMotorista($id_corrida){

			$id = $this->db->select('fk_motorista as id')
							->get_where('cad_corridas',array('id_corrida' => $id_corrida))
							->row();

			if(is_null($id)){
				return null;
			} else {
				return $id->id;
			}

		}

		public function detalhesMotorista($id_corrida){

				return $this->db->query("select nome_usuario as motorista, placa_carro_motorista as placa, modelo
											from cad_corridas
    											inner join cad_modelos on id_modelo = fk_modelo_carro_motorista
        										inner join seg_usuarios on id_usuario = fk_motorista
        											where id_corrida = {$id_corrida}")
							->row();

		}

		public function descobrirPassageiro($id_corrida){

			$id = $this->db->select('fk_passageiro as id')
							->get_where('cad_corridas',array('id_corrida' => $id_corrida))
							->row();

			if(is_null($id)){
				return null;
			} else {
				return $id->id;
			}

		}

		######################################
		//Notificações PUSH
		######################################
		public function carregarFirebaseTokens($id_usuario = null) {

			$dados = $this->db->query("select distinct(token) as token,fk_usuario
												from cad_tokens
													where fk_usuario = {$id_usuario}");

			return $dados;

		}

		public function atualizarInserirToken($id_usuario,$aparelho,$token){

			$id_token = $this->db->query("select id_token
											from cad_tokens
												where fk_usuario = {$id_usuario}
													and aparelho = {$aparelho}")->row();

			if(isset($id_token)){

				return $this->db->query("update cad_tokens set token = '{$token}' where id_token = {$id_token->id_token};");

			} else {
				return $this->db->query("insert into cad_tokens (fk_usuario,aparelho,token)
											values ({$id_usuario},{$aparelho},'{$token}');");
			}

		}

		public function deletarToken($token = null) {

			$this->db->where('token',$token);
			return $this->db->delete("cad_tokens");

		}

		public function atualizarToken($novo = null, $antigo = null) {

			return $this->db->query("update cad_tokens set token = '".$novo."' where token = '".$antigo."';");

		}

	}

?>
