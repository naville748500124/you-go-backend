<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_passageiro extends MY_Model {

		private $regras = array (

			'nome_usuario' => 'required',
			'fk_genero' => 'required',
			'rg_usuario' => 'required',
			'cpf_usuario' => 'required',
			'email_usuario' => 'required',
			'senha_usuario' => 'required',
			'telefone_usuario' => 'max_length[11]',
			'celular_usuario' => 'required',
			'facebook_usuario' => 'max_length[40]',
			'logradouro_usuario' => 'required',
			'cidade_usuario' => 'required',
			'fk_uf_usuario' => 'required',
			'bairro_usuario' => 'required',
			'cep_usuario' => 'required',
			'num_residencia_usuario' => 'required',
			'complemento_usuario' => 'max_length[200]',
			'ativo_usuario' => 'required', 
			'fk_grupo_usuario'	=> 'required',
			'login_usuario'	=> 'required'
			

		);

		private $regras_bancos = array (

			'fk_usuario' 		 	 => 'required',
			'nome_cartao' 		 	 => 'required',
			'numero_cartao'  	 	 => 'required',
			'data_vencimento_cartao' => 'required',
			'cvv_cartao'   			 => 'required'

		);

		private $nomes = array (

			'nome_usuario' => 'Nome',
			'email_usuario' => 'E-mail',
			'telefone_usuario' => 'Telefone',
			'senha_usuario' => 'Senha',
			'facebook_usuario' => 'Facebook ID',
			'rg_usuario' => 'RG',
			'cpf_usuario' => 'CPF',
			'celular_usuario' => 'Celular',
			'logradouro_usuario' => 'Logradouro',
			'cidade_usuario' => 'Cidade',
			'fk_uf_usuario' => 'UF',
			'bairro_usuario' => 'Bairro',
			'cep_usuario' => 'CEP',
			'num_residencia_usuario' => 'Número',
			'complemento_usuario' => 'Complemento',
			'fk_genero' => 'Gênero',
			'ativo_usuario' => 'Status', 
			'fk_grupo_usuario'	=> 'Grupo',
			'login_usuario'	=> 'Login'

		);

		private $nomes_bancos = array (

			'fk_usuario' 		 => 'Fk Usuário',
 			'fk_banco_motorista' => 'Banco',
			'agencia_motorista'  => 'Agência',
			'conta_motorista'    => 'Conta',
			'digito_motorista'   => 'Dígito'

		);


		public function regras(){
			return $this->regras;
		}

		public function regras_bancos(){
			return $this->regras_bancos;
		}

		public function nomes(){
			return $this->nomes;
		}

		public function nomes_bancos(){
			return $this->nomes_bancos;
		}
        public function view_passageiro(){
             return $this->db->get_where('seg_usuarios',array('fk_grupo_usuario' => 3))->result_array();
             /*$this->db->last_query();
             die();*/
        }


	}