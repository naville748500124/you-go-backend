<script type="text/javascript">
	history.replaceState({pagina: "lista_notificacoes"}, "Lista de notificações", "<?php echo base_url() ?>main/redirecionar/10");
</script>

<style type="text/css">
	.btn {
		color: white;
		text-shadow: 1px 1px 1px black;
	}
</style>

<div class="row">
	<div class="col-md-4">
		<h1> <i class="glyphicon glyphicon-file"></i> Nova Notificação</h1>
	</div>
	<div class="col-md-8" align="right">
		<button type="button" class="btn btn-info" id="voltar"> <i class="glyphicon glyphicon-backward"></i> Voltar</button>
		<button type="button" class="btn btn-info" id="recarregar" url="<?php echo $_SERVER ['REQUEST_URI'] ?>"> <i class="glyphicon glyphicon-refresh"></i> Recarregar</button>
		<button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_historico" id="historico_modal" cod="<?php echo $this->session->flashdata('id_notificacao_edicao'); ?>"> <i class="glyphicon glyphicon-time"></i> Histórico Edições</button>
		<a class="btn btn-success" href="<?php echo base_url(); ?>main/redirecionar/11">
			<i class="glyphicon glyphicon-plus-sign"></i> Nova Notificação
		</a>
	</div>
</div>
<hr>

<?php echo form_open('controller_notificacoes/editar_notificacao'); ?>

<input type="hidden" name="id_notificacao" value="<?php echo $this->session->flashdata('id_notificacao_edicao'); ?>">

<div class="row">

	<div class="col-md-10">
		<div class="form-group has-feedback">
			<label class="control-label" for="titulo_notificacao">Título Notificação</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="titulo_notificacao" name="titulo_notificacao" placeholder="Título Notificação" aviso="Título Notificação" value="<?php echo $this->session->flashdata('titulo_notificacao_edicao'); ?>" maxlength="20">
		</div>
	</div>

	<div class="col-md-2" title="Caso a Notificação só possa ser lida por um certo tempo.">
		<div class="form-group has-feedback">
			<label class="control-label" for="data_limite_notificacao">Data limite (Opcional)</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control mascara_data" id="data_limite_notificacao" name="data_limite_notificacao" placeholder="Descrição do Grupo" aviso="Descrição do Grupo" value="<?php echo $this->session->flashdata('data_limite_notificacao_edicao'); ?>">
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="form-group has-feedback">
			<label class="control-label" for="notificacao">Notificação</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<textarea class="form-control" id="notificacao" name="notificacao" placeholder="Notificação" aviso="Notificação"><?php echo $this->session->flashdata('notificacao_edicao'); ?></textarea>
		</div>
	</div>
</div>

<hr>

<div class="row finalizar_formulario">
	<div class="col-md-2" title="Criar uma nova notificação se baseando nesta">
		<a class="btn btn-success" href="<?php echo base_url(); ?>main/redirecionar/11/<?php echo $this->session->flashdata('id_notificacao_edicao'); ?>">
			<i class="glyphicon glyphicon-retweet"></i> Clonar
		</a>
	</div>
	<div class="col-md-7"></div>
	<div class="col-md-3" align="right">
		<button type="button" class="btn btn-danger" title="Limpar os dados dos campos" id="apagar"> <i class="glyphicon glyphicon-trash"></i> Limpar</button>
		<button type="submit" class="btn btn-success" id="validar_Enviar" title="Criar"> <i class="glyphicon glyphicon-send"></i> Editar </button>
	</div>
</div>

<hr>

<h3>Notificações enviadas:</h3>

<table class="table table-bordered table-hover" align="center">
	<thead align="center">
		<th>Usuário</th>
		<th>Enviado em:</th>
		<th>Lido Em:</th>
		<th>Recebido</th>
	</thead>
	<tbody align="center">	
	<?php 

		foreach ($dados_iniciais['notificados'] as $notificacao) {
			echo '<tr>';

			echo '<td>'.$notificacao->nome_usuario.'</td>';
			echo '<td>'.$notificacao->data_envio_notificacao.'</td>';
			if ($notificacao->data_leitura == 0) {
				echo '<td>Não lido</td>';
			} else {
				echo '<td>'.$notificacao->data_leitura.'</td>';
			}

			if ($notificacao->notificacao_enviada) {
				echo '<td>Recebido</td>';
			} else {
				echo '<td>Não Recebido</td>';
			}
			echo '</tr>';
		}

	?>
	</tbody>
</table>

<?php echo form_close(); ?>

<script type="text/javascript">
	$(document).ready(function(){

		tinymce.init({
			selector: 'textarea',
			language: 'pt_BR',
			height: 250,
			theme: 'modern',
			upload_action: '<?php echo base_url(); ?>Controller_notificacoes/upload_imagem',
			upload_file_name: 'imagem',
			plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars  fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality upload',
			'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc '

			],
			toolbar1: 'undo redo |  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link image upload',
			// toolbar2: 'print preview media | forecolor backcolor emoticons | codesample | styleselect ',
			image_advtab: true,
			templates: [
			{ title: 'Test template 1', content: 'Test 1' },
			{ title: 'Test template 2', content: 'Test 2' }
			],
			content_css: [
			'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
			'//www.tinymce.com/css/codepen.min.css'
			]
		});

		$('#id_grupo').select2({
			tags: true,
			tokenSeparators: [';', ' ']
		});

		$('#fk_usuario_destino').select2({
			tags: true,
			tokenSeparators: [';', ' ']
		});

	});
</script>