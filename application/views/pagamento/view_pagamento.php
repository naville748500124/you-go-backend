<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="refresh" content="600">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src='http://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.3/TweenMax.min.js'></script>
        <script src='http://s3-us-west-2.amazonaws.com/s.cdpn.io/16327/MorphSVGPlugin.min.js'></script>
        
        <?php if(ENVIRONMENT == 'development') { ?>

                <script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
            
        <?php } else { ?>

                <script type="text/javascript" src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>

        <?php } ?>

    </head>

    <body style="background: #292929">

    <div style="width: 300px;margin: 0 auto;margin-top: 80px">
        <svg class="mainSVG"  viewBox="0 0 800 600" xmlns="http://www.w3.org/2000/svg">
            <defs>
                <path fill="#f8d509" id="puff"  d="M4.5,8.3C6,8.4,6.5,7,6.5,7s2,0.7,2.9-0.1C10,6.4,10.3,4.1,9.1,4c2-0.5,1.5-2.4-0.1-2.9c-1.1-0.3-1.8,0-1.8,0s-1.5-1.6-3.4-1C2.5,0.5,2.1,2.3,2.1,2.3S0,2.3,0,4.4c0,1.1,1,2.1,2.2,2.1C2.2,7.9,3.5,8.2,4.5,8.3z" />
                <circle id="dot"  cx="0" cy="0" r="5" fill="#000"/>
            </defs>
            <circle id="mainCircle" fill="none" stroke="none" stroke-width="2" stroke-miterlimit="10" cx="400" cy="300" r="130"/>
            <circle id="circlePath" fill="none" stroke="none" stroke-width="2" stroke-miterlimit="10" cx="400" cy="300" r="80"/>
            <g id="mainContainer" >
                <g id="car">
                    <path id="carRot" fill="#fff" d="M45.6,16.9l0-11.4c0-3-1.5-5.5-4.5-5.5L3.5,0C0.5,0,0,1.5,0,4.5l0,13.4c0,3,0.5,4.5,3.5,4.5l37.6,0C44.1,22.4,45.6,19.9,45.6,16.9z M31.9,21.4l-23.3,0l2.2-2.6l14.1,0L31.9,21.4z M34.2,21c-3.8-1-7.3-3.1-7.3-3.1l0-13.4l7.3-3.1C34.2,1.4,37.1,11.9,34.2,21z M6.9,1.5c0-0.9,2.3,3.1,2.3,3.1l0,13.4c0,0-0.7,1.5-2.3,3.1C5.8,19.3,5.1,5.8,6.9,1.5z M24.9,3.9l-14.1,0L8.6,1.3l23.3,0L24.9,3.9z"/>
                </g>
            </g>
        </svg>
    </div>
    <p style="font-family: Arial;text-align: center;color: #fff;font-weight: normal;margin-top: -30px"></p>
    <h4 style="font-family: Arial;text-align: center;color: #fff;font-weight: normal;margin-top: -10px"> Efetuando o pagamento<span id="status">.</span></h4>

    <footer style="bottom: 0px; position: absolute; color: white; padding: 20px;">
    </footer>

    </body>

<script type="text/javascript">

    gerarToken();

    //gerar token
    function gerarToken() {

        console.log('gerarToken');
        var brand = null;

        var sessao_id = '<?php echo $id;?>';
        var numero    = '<?php echo preg_replace('/[^0-9]/', '', $dados['numero_cartao']); ?>';
        var codigo    = '<?php echo $dados['cvv_cartao']; ?>';
        var mes       = '<?php echo $dados['mes']; ?>';
        var ano       = '<?php echo $dados['ano']; ?>';
        var data_nascimento       = '<?php echo $dados['data_nascimento']; ?>';
        var bin       = numero.substr(0,6);

        console.log(numero);

        try{

            PagSeguroDirectPayment.setSessionId(sessao_id);

            PagSeguroDirectPayment.getBrand({
                    cardBin: bin,
                    success: function(response) {
                        brand = response.brand;

                        console.log("Bandeira: " + brand.name)
                        var param = {
                                        cardNumber: numero,
                                        cvv: codigo,
                                        expirationMonth: mes,
                                        expirationYear: ano,
                                        success: function(response) {
                                            console.log("Token Gerado: " + response.card.token)
                                            var hash = PagSeguroDirectPayment.getSenderHash();
                                            console.log("hash "+hash);

                                            // $("#status").html("Hash Gerado <small>("+hash+")</small> <br> Token Gerado <small>("+response.card.token+")</small> <br> Enviando dados para o PagSeguro...");
                                            $("#status").html("..");

                                            var pagamento = {
                                                "async": true,
                                                "crossDomain": true,
                                                "url": "<?php  echo base_url();?>controller_pagamento/gerarXml",
                                                "method": "POST",
                                                "data": {
                                                    "hash": hash,
                                                    "fk_corrida": <?php echo $_GET['fk_corrida']; ?>,
                                                    "data_nascimento": data_nascimento,
                                                    "token": response.card.token
                                                }
                                            }

                                            $.ajax(pagamento).done(function (response) {
                                                var retorno = response;

                                                console.log(response);
                                                
                                                var settings = {
                                                  "async": true,
                                                  "crossDomain": true,
                                                  "url": "https://ws.sandbox.pagseguro.uol.com.br/v2/transactions?"+retorno+"&email=patricia.kimelblat@gmail.com&token=429960B234D9430BA0A5BDEE0A291E7E",
                                                  "method": "GET",
                                                  "headers": {
                                                    "Cache-Control": "no-cache"
                                                  }
                                                }

                                                $.ajax(settings).done(function (response) {
                                                    console.log(response);
                                                    console.log("tudo OK");
                                                    retornar("...");
                                                }).success(function(){
                                                    console.log(response);
                                                    console.log("tudo OK");
                                                    retornar("...");                                                    
                                                }).fail(function(jqXHR, textStatus, errorThrown){
                                                    console.log("Fail");
                                                    console.log(jqXHR);
                                                    console.log(textStatus);
                                                    console.log(errorThrown);
                                                    retornar("...*");
                                                });

                                            });



                                        },
                                        error: function(response) {
                                            console.log("Erro ao gerar token")
                                        },
                                        complete: function(response) {}
                                    }
                        param.brand = brand.name;
                        PagSeguroDirectPayment.createCardToken(param);

                    },
                    error: function(response) {
                        console.log("Erro ao pegar bandeira");
                    },
                    complete: function(response) {}
            });
        } catch(err) {
            console.log(err.message);
        }
    } //Fim função gerar Token

    function retornar(text){
         $("#status").html(text);

              console.log('Pagamento enviado.')

        <?php 
            $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
            $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
            $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
            $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
            $webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

            if($iPod || $iPhone || $iPad){

                echo "window.location = 'megamil.net.webview-avancado://voltar';";
                

            } else if($Android){

                echo 'Android.exibirAvisoAndroid();
                $("#status").html($("#status").html()+" Chamado via Android");';

            }  else {

            }
          ?>
    }

    //Função de animação do load
    TweenMax.set('#circlePath', {
        attr: {
            r: document.querySelector('#mainCircle').getAttribute('r')
        }
    })
    MorphSVGPlugin.convertToPath('#circlePath');

    var xmlns = "http://www.w3.org/2000/svg",
        xlinkns = "http://www.w3.org/1999/xlink",
        select = function(s) {
            return document.querySelector(s);
        },
        selectAll = function(s) {
            return document.querySelectorAll(s);
        },
        mainCircle = select('#mainCircle'),
        mainContainer = select('#mainContainer'),
        car = select('#car'),
        mainSVG = select('.mainSVG'),
        mainCircleRadius = Number(mainCircle.getAttribute('r')),
        //radius = mainCircleRadius,
        numDots = mainCircleRadius / 2,
        step = 360 / numDots,
        dotMin = 0,
        circlePath = select('#circlePath')

    //
    //mainSVG.appendChild(circlePath);
    TweenMax.set('svg', {
        visibility: 'visible'
    })
    TweenMax.set([car], {
        transformOrigin: '50% 50%'
    })
    TweenMax.set('#carRot', {
        transformOrigin: '0% 0%',
        rotation:30
    })

    var circleBezier = MorphSVGPlugin.pathDataToBezier(circlePath.getAttribute('d'), {
        offsetX: -20,
        offsetY: -5
    })



    //console.log(circlePath)
    var mainTl = new TimelineMax();

    function makeDots() {
        var d, angle, tl;
        for (var i = 0; i < numDots; i++) {

            d = select('#puff').cloneNode(true);
            mainContainer.appendChild(d);
            angle = step * i;
            TweenMax.set(d, {
                //attr: {
                x: (Math.cos(angle * Math.PI / 180) * mainCircleRadius) + 400,
                y: (Math.sin(angle * Math.PI / 180) * mainCircleRadius) + 300,
                rotation: Math.random() * 360,
                transformOrigin: '50% 50%'
                //}
            })

            tl = new TimelineMax({
                repeat: -1
            });
            tl
                .from(d, 0.2, {
                    scale: 0,
                    ease: Power4.easeIn
                })
                .to(d, 1.8, {
                    scale: Math.random() + 2,
                    alpha: 0,
                    ease: Power4.easeOut
                })

            mainTl.add(tl, i / (numDots / tl.duration()))
        }
        var carTl = new TimelineMax({
            repeat: -1
        });
        carTl.to(car, tl.duration(), {
            bezier: {
                type: "cubic",
                values: circleBezier,
                autoRotate: true
            },
            ease: Linear.easeNone
        })
        mainTl.add(carTl, 0.05)
    }

    makeDots();
    mainTl.time(120);
    TweenMax.to(mainContainer, 20, {
        rotation: -360,
        svgOrigin: '400 300',
        repeat: -1,
        ease: Linear.easeNone
    });
    mainTl.timeScale(1.1)
</script>
</html>
