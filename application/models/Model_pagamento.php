<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_pagamento extends MY_Model {

	public function pagamento($id_corrida){
            
            return 	$this->db->query("select fk_usuario,numero_cartao, 
            									MONTH(data_vencimento_cartao) as mes, 
            									YEAR(data_vencimento_cartao) as ano, cvv_cartao, data_nascimento from elo_pagamento_cliente 
                                                    where id_pagamento = (select fk_forma_pagamento from cad_corridas where id_corrida = {$id_corrida})")->row_array();


    }

    public function pagamento_Ativacao($id_pagamento){
            
            return  $this->db->query("select fk_usuario,numero_cartao, 
                                                MONTH(data_vencimento_cartao) as mes, 
                                                YEAR(data_vencimento_cartao) as ano, cvv_cartao, data_nascimento from elo_pagamento_cliente 
                                                    where id_pagamento = {$id_pagamento}")->row_array();


    }

    public function corrida($id_corrida){
        return $this->db->get_where('view_dados_pagseguro',
                array('id_corrida' => $id_corrida)
            )->row_array();


    }

    public function cobrarAtivacaoConta($id_pagamento){
        return $this->db->get_where('view_dados_pagseguro_ativacao',
            array('id_pagamento' => $id_pagamento)
        )->row_array();

    }

    public function atualizarPagamento($pagamento,$fk_corrida){
        $this->db->where('id_corrida',$fk_corrida);
       return $this->db->update('cad_corridas',$pagamento);
    }

    public function atualizarPagamentoAtivacao($pagamento,$id_pagamento){
        $this->db->where('id_pagamento',$id_pagamento);
       return $this->db->update('elo_pagamento_cliente',$pagamento);
    }

    public function atualizaNotificacao($dados){
        return $this->db->insert('notificacoes',$dados);
        /*notificationCode;*/
    }
    public function consultaNotificacao($dados,$id_corrida){
        $this->db->where('id_corrida',$id_corrida);
        return $this->db->update('cad_corridas',$dados);
    }

    public function consultaNotificacaoAtivacao($pagamento,$id_pagamento){
        $this->db->where('id_pagamento',$id_pagamento);
       return $this->db->update('elo_pagamento_cliente',$pagamento);
    }

}

