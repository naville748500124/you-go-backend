<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_usuarios extends MY_Controller {

	function __construct() {

	    parent::__construct();
	    $this->load->model('model_usuarios');
		    
	}

	public function criar_usuario(){

		$this->form_validation->set_rules('nome_usuario','Nome do Usuário','required');
		$this->form_validation->set_rules('email_usuario','E-mail do Usuário','required|is_unique[seg_usuarios.email_usuario]');
		$this->form_validation->set_rules('telefone_usuario','Telefone do Usuário','required');
		$this->form_validation->set_rules('login_usuario','Login do Usuário','required|is_unique[seg_usuarios.login_usuario]');
		$this->form_validation->set_rules('senha_usuario','Senha do Usuário','required');
		$this->form_validation->set_rules('ativo_usuario','Status do Usuário','required');
		$this->form_validation->set_rules('fk_grupo_usuario','Grupo do Usuário','required');

		$dados = array (
					'usuario_criou_usuario' => $this->session->userdata('usuario'),
					'nome_usuario' => $this->input->post('nome_usuario'),
					'email_usuario' => $this->input->post('email_usuario'),
					'telefone_usuario' => $this->input->post('telefone_usuario'),
					'login_usuario' => $this->input->post('login_usuario'),
					'senha_usuario' => sha1($this->input->post('senha_usuario')),
					'ativo_usuario' => $this->input->post('ativo_usuario'),
					'fk_grupo_usuario' => $this->input->post('fk_grupo_usuario')
				);

		if ($this->form_validation->run()) {

			$this->model_usuarios->start();
			
			$id = $this->model_usuarios->create($dados);

			$commit = $this->model_usuarios->commit();

			if ($commit['status']) {
				$this->aviso('Registro Criado','Usuário "'.$this->input->post('nome_usuario').'" criado com sucesso!.','success',false);

				redirect('main/redirecionar/5/'.$id);
			} else {
				$this->aviso('Falha ao criar','Erro(s) ao inserir dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);
				$this->session->set_flashdata($dados);

				redirect('main/redirecionar/7');
			}


		} else {

			$this->aviso('Falha ao criar','Erro(s) no formulário: '.validation_errors(),'error',true);
			$this->session->set_flashdata($dados);

			redirect('main/redirecionar/7');

		}

	}

	public function editar_usuario(){
	
		$this->form_validation->set_rules('nome_usuario','Nome do usuario','required');

		if($this->input->post('email_usuario') != $this->input->post('email_inicial')){
			$this->form_validation->set_rules('email_usuario','E-mail do usuario','required|is_unique[seg_usuarios.email_usuario]');
		}

		$this->form_validation->set_rules('telefone_usuario','Telefone do usuario','required');

		if($this->input->post('login_usuario') != $this->input->post('login_inicial')){
			$this->form_validation->set_rules('login_usuario','Login do usuario','required|is_unique[seg_usuarios.login_usuario]');	
		}
		
		$this->form_validation->set_rules('ativo_usuario','Status do usuario','required');
		$this->form_validation->set_rules('fk_grupo_usuario','Grupo do usuario','required');

		$dados = array (
					'id_usuario' => $this->input->post('id_usuario'),
					'nome_usuario' => $this->input->post('nome_usuario'),
					'email_usuario' => $this->input->post('email_usuario'),
					'telefone_usuario' => $this->input->post('telefone_usuario'),
					'login_usuario' => $this->input->post('login_usuario'),
					'ativo_usuario' => $this->input->post('ativo_usuario'),
					'fk_grupo_usuario' => $this->input->post('fk_grupo_usuario')
				);

		//Houve alteração de senha?
		if($this->input->post('senha_usuario') != ""){
			$dados['senha_usuario'] = sha1($this->input->post('senha_usuario'));
		} 

		if ($this->form_validation->run()) {
			
			$this->model_usuarios->start();
			$this->model_usuarios->update($dados);

			$commit = $this->model_usuarios->commit();

			if ($commit['status']) {
				$this->aviso('Registro Editado','Usuário "'.$this->input->post('nome_usuario').'" editado com sucesso!.','success',false);
			} else {
				$this->aviso('Falha ao editar','Erro(s) ao atualizar dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);
			}


			redirect('main/redirecionar/5/'.$this->input->post('id_usuario'));

		} else {

			$this->aviso('Falha ao editar','Erro(s) no formulário: '.validation_errors(),'error',true);

			redirect('main/redirecionar/5/'.$this->input->post('id_usuario'));

		}

	}

	public function editar_perfil(){

		$this->form_validation->set_rules('nome_usuario','Nome do usuario','required');

		if($this->input->post('email_usuario') != $this->input->post('email_inicial')){
			$this->form_validation->set_rules('email_usuario','E-mail do usuario','required|is_unique[seg_usuarios.email_usuario]');
		}

		$this->form_validation->set_rules('telefone_usuario','Telefone do usuario','required');

		if($this->input->post('login_usuario') != $this->input->post('login_inicial')){
			$this->form_validation->set_rules('login_usuario','Login do usuario','required|is_unique[seg_usuarios.login_usuario]');	
		}

		$dados = array (
					'id_usuario' => $this->input->post('id_usuario'),
					'nome_usuario' => $this->input->post('nome_usuario'),
					'email_usuario' => $this->input->post('email_usuario'),
					'telefone_usuario' => $this->input->post('telefone_usuario'),
					'login_usuario' => $this->input->post('login_usuario')
				);

		//Houve alteração de senha?
		if($this->input->post('senha_usuario') != ""){
			$dados['senha_usuario'] = sha1($this->input->post('senha_usuario'));
		} 

		if ($this->form_validation->run()) {
			
			$this->model_usuarios->start();
			$this->model_usuarios->update($dados);

			$commit = $this->model_usuarios->commit();

			if ($commit['status']) {
				$this->session->set_userdata("login",$this->input->post('login_usuario'));
				$this->session->set_userdata("nome",$this->input->post('nome_usuario'));
				$this->aviso('Registro Editado','Usuário "'.$this->input->post('nome_usuario').'" editado com sucesso!.','success',false);
			} else {
				$this->aviso('Falha ao editar','Erro(s) ao atualizar dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);
			}

			redirect('main/redirecionar/1');

		} else {

			$this->aviso('Falha ao editar','Erro(s) no formulário: '.validation_errors(),'error',true);
			redirect('main/redirecionar/1');

		}

	}

	public function esqueci_Senha() {

		$caracteres = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,1,2,3,4,5,6,7,8,9,0';
		$caractereVetor = explode(',',$caracteres);
		$senha = '';

		while(strlen($senha) < 10) { //Cria uma nova senha com 10 caracteres.
			
			$indice = mt_rand(0, count($caractereVetor) - 1);
			$senha .= $caractereVetor[$indice];

		}
		$this->model_usuarios->start();
		$email = $this->model_usuarios->senha_Email(sha1($senha),$this->input->post('login'));
		//Usuário inativo ou desativado
		if($email == ""){

			//echo 'Usuário está desabilitado ou não existe, entre em contato com o administrador!';
			$json = array(
				'resultado' => "Dados incorretos ou perfil inativo",
				'status' => 0
			);

		} else { //Senha enviada para o E-mail.

			// Detalhes do Email. 
			$this->email->from('yougomobile@yougomobile.com.br', 'Yougo | Troca de senha'); 
			$this->email->to($email); 
			$this->email->subject('YouGo | Troca de senha'); 
					$this->email->message('<h1>
											<a href="http://'.$_SERVER['HTTP_HOST'].base_url().'">
												Yougo
											</a>
										   </h1> 
									Recebemos sua solicitação de nova senha. <br> 
									Sua nova senha agora é: <strong>'.$senha.'</strong><br>
									<img src="http://'.$_SERVER['HTTP_HOST'].base_url().'/style/img/rodape.png" alt="Rodapé">'); 

			// Enviar... 
			if ($this->email->send()) { 
				$this->model_usuarios->commit();
				$json = array(
					'resultado' => "Nova Senha enviada para o E-mail: (".$email.")",
					'status' => 1
				);
			} else {
				$this->model_usuarios->rollback();
				$json = array(
					'resultado' => "Erro ao enviar senha. <br>".$this->email->print_debugger(),
					'status' => 0
				);
			}

		}

		header('Content-Type: application/json');
		echo json_encode($json);


	}

	public function filtro_ajax(){

		$this->load->model("model_relatorios");

		$parametros = array(); //Recebe os valores com nomes certos do filtro

		$parametros['tabela']           = $this->input->get("tabela");
		$parametros['filtro_campo']     = $this->input->get("filtro_campo");
		$parametros['filtro_ordenacao'] = $this->input->get("filtro_ordenacao");
		$parametros['filtro_ordem']     = $this->input->get("filtro_ordem");
		$parametros['filtro_limite']    = $this->input->get("filtro_limite");

		$this->model_relatorios->start();

		//Carregando os campos dinamicos que irei receber via get, que são todos do filtro.
		$campos = $this->model_relatorios->listarCampos($parametros['tabela'],$parametros['filtro_campo']);

		$filtro = array();
		foreach ($campos as $key => $campo) {
			if ($campo['descricao_campo'] != "") {
				$campo_valor = $this->input->get($campo['nome_campo']);
				if (isset($campo_valor) && $campo_valor != "") {
					$campo['valor'] = $campo_valor;
					$filtro[$campo['nome_campo']] = $campo;
				}
			} 
		}

		$resultados = $this->model_relatorios->filtroAjax($parametros,$filtro);

		if ($this->model_relatorios->commit()) {
			echo '<table class="table table-bordered table-hover" align="center">
				<thead align="center">';
				
					foreach ($campos as $chave => $campo) {
						if ($campo['selecionado']) {
							if ($campo['descricao_campo'] == "ID") 
								echo '<th style="width: 60px;" align="center" class="no-filter">Editar</th>';
							echo  "<th>{$campo['descricao_campo']}: </th>";
						}
					}

				echo '</thead>
				<tbody align="center">';	
					
					foreach ($resultados as $resultado) {

						echo "<tr>";
						foreach ($parametros['filtro_campo'] as $select_) {
							if ($select_ == "id")
								echo '<td><a href="'.base_url().'main/redirecionar/5/'.$resultado[$select_].'"><button class="btn btn-info"> <i class="glyphicon glyphicon-edit"> </i> Editar</button></a></td>';

							if ($select_ == "telefone_usuario") {
								echo "<td class=\"mascara_cel\">".$resultado[$select_]."</td>";
							} else {
								echo "<td>".$resultado[$select_]."</td>";
							}
						}
						echo "</tr>";

					}
					
				echo '</tbody>
			</table>';

		}

	}

	public function aviso($titulo,$aviso,$tipo,$fixo){

		//Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
			$aviso_ = str_replace('
', '', $aviso);

		$aviso = str_replace('\'', '"', $aviso_);

		$this->session->set_flashdata('titulo_alerta',$titulo);
		$this->session->set_flashdata('mensagem_alerta',$aviso);
		$this->session->set_flashdata('tipo_alerta',$tipo);
		$this->session->set_flashdata('mensagem_fixa',$fixo);

	}

}