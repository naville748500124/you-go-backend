
<head>
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/login_yougo.css" >
     <title>YouGo</title>
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/theme-animate.css" >
    <link rel="icon" href="<?php echo base_url(); ?>style/img/favicon.png" >
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>style/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>style/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>style/js/script_efeito.js"></script>
    <meta name="theme-color" content="#292929">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url(); ?>style/css/jquery.toast.css" rel="stylesheet" />
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.toast.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.mask.js"></script>
</head>
<body>
<script>
    $(document).ready(function(){

            var url_modelo = "<?php echo base_url() ?>controller_webservice/busca_modelo?id_montadora=";
            var montadora = "25";
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": url_modelo+montadora,
                "method": "GET",
                "headers": {
                    "cache-control": "no-cache",
                    "postman-token": "b4f801dd-1000-c5aa-1315-5c6b90644816"
                }
              }
            $.ajax(settings).done(function (response) {
                $("#modelo_veiculo").html('');
                for(const i in response.modelos){
                    var clone = '<option class="opcoes_modelo" style="background: #292929;color: #fff" value="'+ response.modelos[i].id_modelo +'"> '+ response.modelos[i].modelo +'</option>';
                    $('#modelo_veiculo').append(clone);
                }

            });



        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "<?php echo base_url() ?>controller_webservice/pre_cadastro_motorista",
            "method": "GET",
            "headers": {
                "cache-control": "no-cache",
                "postman-token": "ea37fa41-27ad-d7a4-0305-705935aafce9"
            }
        }

        $.ajax(settings).done(function (response) {
            $("#marca_veiculo").html('');
            for(const x in response.marcas){
                var clone = '<option class="opcoes_modelo" style="background: #292929;color: #fff" value="'+ response.marcas[x].id +'"> '+ response.marcas[x].nome +'</option>';
                $('#marca_veiculo').append(clone);
            };


            $("#cidade_trabalho").html('');
            for(const c in response.cidades){
                var clone = '<option class="opcoes_modelo" style="background: #292929;color: #fff" value="'+ response.cidades[c].id +'"> '+ response.cidades[c].nome +'</option>';
                $('#cidade_trabalho').append(clone);
            };
        });




        $('#marca_veiculo').change(function () {
            var url_modelo = "<?php echo base_url() ?>controller_webservice/busca_modelo?id_montadora=";
            var montadora = document.getElementById('marca_veiculo').value;
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": url_modelo+montadora,
                "method": "GET",
                "headers": {
                    "cache-control": "no-cache",
                    "postman-token": "b4f801dd-1000-c5aa-1315-5c6b90644816"
                }
            }
            $.ajax(settings).done(function (response) {
                $("#modelo_veiculo").html('');
                for(const i in response.modelos){
                    var clone = '<option class="opcoes_modelo" style="background: #292929;color: #fff" value="'+ response.modelos[i].id_modelo +'"> '+ response.modelos[i].modelo +'</option>';
                    $('#modelo_veiculo').append(clone);
                }

            });
        });
    });


</script>
<script>

        var nome = null;            
        var sexo = null;            
        var rg = null;              
        var cpf = null;             
        var email = null;           
        var confirmaremail = null;  
        var senha = null;           
        var confirmarsenha = null;  
        var telefone = null;        
        var celular = null;         
        var cep = null;             
        var logradouro = null;      
        var complemento = null;      
        var bairro = null;          
        var cidade = null;          
        var numero = null;          
        var uf = null;              
        var cidade_trabalho = null; 
        var marca_veiculo = null;   
        var modelo_veiculo = null;   
        var placa_veiculo = null;   
        var cor_veiculo = null;   
        //Imagens
        var pessoal_valor = null;
        var cnh_valor = null;
        var seguro_valor = null;
        var carro1_valor = null;
        var carro2_valor = null;
        var carro3_valor = null;
        var documento_valor = null;
        var comprovante_valor = null;  
        var antecedentes_valor = null;
        var rg_valor = null;



        /*recebe all do formulario */
        function consultar(){
            // Guarda o valor do campo NOME na variável nome.
            nome                = document.getElementById('nome').value;
            sexo                = document.getElementById('sexo').value;
            rg                  = document.getElementById('rg').value;
            cpf                 = document.getElementById('cpf').value;
            email               = document.getElementById('email').value;
            confirmaremail      = document.getElementById('confirmaremail').value;
            senha               = document.getElementById('senha').value;
            confirmarsenha      = document.getElementById('confirmarsenha').value;
            telefone            = document.getElementById('telefone').value;
            celular             = document.getElementById('celular').value;
            cep                 = document.getElementById('cep').value;
            logradouro          = document.getElementById('rua').value;
            complemento          = document.getElementById('complemento').value;
            bairro              = document.getElementById('bairro').value;
            cidade              = document.getElementById('cidade').value;
            numero              = document.getElementById('numero').value;
            uf                  = document.getElementById('uf').value;
            cidade_trabalho     = document.getElementById('cidade_trabalho').value;
            marca_veiculo       = document.getElementById('marca_veiculo').value;
            modelo_veiculo       = document.getElementById('modelo_veiculo').value;
            placa_veiculo       = document.getElementById('placa_veiculo').value;
            cor_veiculo         = document.getElementById('cor_veiculo').value;



            if(nome == ''){verfificar_campos();return false;}
            if(sexo == ''){verfificar_campos();return false;}
            if(rg == ''){verfificar_campos();return false;}
            if(cpf == ''){verfificar_campos();return false;}
            if(email == ''){verfificar_campos();return false;}
            if(confirmaremail == ''){verfificar_campos();return false;}
            if(senha == ''){verfificar_campos();return false;}
            if(confirmarsenha == ''){verfificar_campos();return false;}
            if(telefone == ''){verfificar_campos();return false;}
            if(celular == ''){verfificar_campos();return false;}
            if(cep == ''){verfificar_campos();return false;}
            if(logradouro == ''){verfificar_campos();return false;}
            if(bairro == ''){verfificar_campos();return false;}
            if(cidade == ''){verfificar_campos();return false;}
            if(numero == ''){verfificar_campos();return false;}
            if(uf == ''){verfificar_campos();return false;}
            if(cidade_trabalho == ''){verfificar_campos();return false;}
            if(marca_veiculo == ''){verfificar_campos();return false;}
            if(modelo_veiculo == ''){verfificar_campos();return false;}
            if(placa_veiculo == ''){verfificar_campos();return false;}
            if(cor_veiculo == ''){verfificar_campos();return false;}

            function verfificar_campos(){
                $.toast().reset('all');
                $.toast({
                    heading: ' ',
                    text: 'Preencha todos os dados',
                    hideAfter : 3000,
                    position: 'top-right',
                    icon: 'warning'
                });
            }

            /*
            ############################################
            Edu caso de algum problema remova essa parte
            ############################################
            */
            if(senha == confirmarsenha){
            }else{
                $.toast().reset('all');
                $.toast({
                    heading: ' ',
                    text: 'Senhas diferentes',
                    hideAfter : 3000,
                    position: 'top-right',
                    icon: 'success'
                });
                return false;
            }

/*
############################################
            até aqui
############################################
*/

            var carregou = 0; 


/*pessaol*/
            var filesSelected = document.getElementById("pessoal").files;
            if(filesSelected.length == 0){verificarBase64();return false;}
            var fileToLoad = filesSelected[0];
            var fileReader = new FileReader();
            fileReader.onload = function() {
                pessoal_valor = fileReader['result'];
                carregou++;
                if(carregou == 10){
                    enviar();
                }
            };
            fileReader.readAsDataURL(fileToLoad);

/*CNH*/
            var filesSelected_cnh = document.getElementById("cnh").files;
            if(filesSelected.length == 0){verificarBase64();return false;}
            var fileToLoad_cnh = filesSelected_cnh[0];
            var fileReader_cnh = new FileReader();
            fileReader_cnh.onload = function() {
                cnh_valor = fileReader_cnh['result'];
                carregou++;
                if(carregou == 10){
                    enviar();
                }


            };
            fileReader_cnh.readAsDataURL(fileToLoad_cnh);


/*seguro*/
            var filesSelected_seguro = document.getElementById("seguro").files;
            if(filesSelected.length == 0){verificarBase64();return false;}
            var fileToLoad_seguro = filesSelected_seguro[0];
            var fileReader_seguro = new FileReader();
            fileReader_seguro.onload = function() {
                seguro_valor = fileReader_seguro['result'];
                carregou++;
                if(carregou == 10){
                    enviar();
                }
            };
            fileReader_seguro.readAsDataURL(fileToLoad_seguro);

/*carro*/
            var filesSelected_carro = document.getElementById("carro").files;
            if(filesSelected.length == 0){verificarBase64();return false;}
            var fileToLoad_carro = filesSelected_carro[0];
            var fileReader_carro = new FileReader();
            fileReader_carro.onload = function() {
                carro1_valor = fileReader_carro['result'];
                carregou++;
                if(carregou == 10){
                    enviar();
                }
            };
            fileReader_carro.readAsDataURL(fileToLoad_carro);

/*carro _img2*/
            var filesSelected_carro_img2 = document.getElementById("carro_img2").files;
            if(filesSelected.length == 0){verificarBase64();return false;}
            var fileToLoad_carro_img2 = filesSelected_carro_img2[0];
            var fileReader_carro_img2 = new FileReader();
            fileReader_carro_img2.onload = function() {
                carro2_valor = fileReader_carro_img2['result'];
                carregou++;
                if(carregou == 10){
                    enviar();
                }
            };
            fileReader_carro_img2.readAsDataURL(fileToLoad_carro_img2);


/*carro _img3*/
            var filesSelected_carro_img3 = document.getElementById("carro_img3").files;
            if(filesSelected.length == 0){verificarBase64();return false;}
            var fileToLoad_carro_img3 = filesSelected_carro_img3[0];
            var fileReader_carro_img3 = new FileReader();
            fileReader_carro_img3.onload = function() {
                carro3_valor = fileReader_carro_img3['result'];
                carregou++;
                if(carregou == 10){
                    enviar();
                }
            };
            fileReader_carro_img3.readAsDataURL(fileToLoad_carro_img3);

/*documento*/
            var filesSelected_documento = document.getElementById("documento").files;
            if(filesSelected.length == 0){verificarBase64();return false;}
            var fileToLoad_documento = filesSelected_documento[0];
            var fileReader_documento = new FileReader();
            fileReader_documento.onload = function() {
                documento_valor = fileReader_documento['result'];
                carregou++;
                if(carregou == 10){
                    enviar();
                }
            };
            fileReader_documento.readAsDataURL(fileToLoad_documento);


/*comprovante*/
            var filesSelected_comprovante = document.getElementById("comprovante").files;
            if(filesSelected.length == 0){verificarBase64();return false;}
            var fileToLoad_comprovante = filesSelected_comprovante[0];
            var fileReader_comprovante = new FileReader();
            fileReader_comprovante.onload = function() {
                comprovante_valor = fileReader_comprovante['result'];
                carregou++;
                if(carregou == 10){
                    enviar();
                }
            };
            fileReader_comprovante.readAsDataURL(fileToLoad_comprovante);




        /*rg*/
        var filesSelected_rg = document.getElementById("rg_inp").files;
        if(filesSelected.length == 0){verificarBase64();return false;}
        var fileToLoad_rg = filesSelected_rg[0];
        var fileReader_rg = new FileReader();
        fileReader_rg.onload = function() {
            rg_valor = fileReader_rg['result'];
            carregou++;
            if(carregou == 10){
                enviar();
            }
        };
        fileReader_rg.readAsDataURL(fileToLoad_rg);



        /*antecedentes*/
        var filesSelected_antecedentes = document.getElementById("antecedentes").files;
        if(filesSelected.length == 0){verificarBase64();return false;}
        var fileToLoad_antecedentes = filesSelected_antecedentes[0];
        var fileReader_antecedentes = new FileReader();
        fileReader_antecedentes.onload = function() {
            antecedentes_valor = fileReader_antecedentes['result'];
            carregou++;
            if(carregou == 10){
                enviar();
            }
        };
        fileReader_antecedentes.readAsDataURL(fileToLoad_antecedentes);

        }


        function verificarBase64(){
            if(pessoal_valor != '' &&
                cnh_valor != '' &&
                seguro_valor != '' &&
                carro1_valor != '' &&
                carro2_valor != '' &&
                carro3_valor != '' &&
                documento_valor != '' &&
                rg_valor != '' &&
                antecedentes_valor != '' &&
                comprovante_valor != '') {
                enviar();
            }
            else{
                $.toast().reset('all');
                $.toast({
                    heading: ' ',
                    text: 'Falha no cadastro de imagens, você deve inserir todas as imagens solicitadas',
                    hideAfter : 3000,
                    position: 'top-right',
                    icon: 'warning'
                });
            }
        }

        function enviar(){

            var settings_sha1 = {
                "async": true,
                "crossDomain": true,
                "url": "<?php  echo base_url();?>/controller_motorista/sha1",
                "method": "POST",
                "data": {
                    "senha_usuario": senha
                }
            }
            $.ajax(settings_sha1).done(function (response) {
                senha = response;


                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "<?php echo base_url() ?>controller_webservice/cadastro_motorista",
                    "method": "POST",
                    "headers": {
                        "content-type": "application/x-www-form-urlencoded",
                        "cache-control": "no-cache"
                    },
                    "data": {
                        "nome_usuario": nome,
                        "email_usuario":   email,
                        "telefone_usuario": telefone,
                        "senha_usuario": senha,
                        "facebook_usuario": "null",
                        "rg_usuario": rg,
                        "cpf_usuario": cpf,
                        "celular_usuario": celular,
                        "logradouro_usuario": logradouro,
                        "cidade_usuario": cidade,
                        "fk_uf_usuario": "1",
                        "bairro_usuario": bairro,
                        "cep_usuario": cep,
                        "complemento_usuario": complemento,
                        "num_residencia_usuario": numero,
                        "fk_genero": sexo,
                        "fk_cidade_ativa_motorista": cidade_trabalho,
                        "fk_modelo_carro_motorista": modelo_veiculo,
                        "cor_carro_motorista": cor_veiculo,
                        "placa_carro_motorista": placa_veiculo,
                        "motorista": pessoal_valor,
                        "cnh": cnh_valor,
                        "rg": rg_valor,
                        "antecedentes": antecedentes_valor,
                        "seguro":  seguro_valor,
                        "documento": documento_valor,
                        "veiculo_1": carro1_valor,
                        "veiculo_2": carro2_valor,
                        "veiculo_3": carro3_valor,
                        "comprovante": comprovante_valor
                    }
                }




                console.log(nome);
                console.log(email);
                console.log(telefone);
                console.log(senha);
                console.log(rg);
                console.log(cpf);
                console.log(celular);
                console.log(logradouro);
                console.log(cidade);
                console.log(bairro);
                console.log(cep);
                console.log(complemento);
                console.log(numero);
                console.log(sexo);
                console.log(cidade_trabalho);
                console.log(modelo_veiculo);
                console.log(cor_veiculo);
                console.log(placa_veiculo);
                console.log(pessoal_valor);
                console.log(cnh_valor);
                console.log(rg_valor);
                console.log(antecedentes_valor);
                console.log(seguro_valor);
                console.log(documento_valor);
                console.log(carro1_valor);
                console.log(carro2_valor);
                console.log(carro3_valor);
                console.log(comprovante_valor);


                $.ajax(settings).done(function (response) {
                    $.toast().reset('all');
                            $.toast({
                                heading: ' ',
                                text: 'Cadastro feito com sucesso',
                                hideAfter : 3000,
                                position: 'top-right',
                                icon: 'success'
                            });
                });
                $('.app__logout').click();
            });

        }

</script>











<div class="cont">
    <div class="demo ">

        <!--Tela de login-->
        <div class="login">
            <div class="center-block" style="margin: 0 auto;">
                <p>&nbsp;</p>
                <svg id="svg_linha" class="center-block" height="380" viewBox="0 0 1000 1000" style="padding: 15px" x="0" y="0" enable-background="new 0 0 147 147" xml:space="preserve">
                    <path  class="logo"  fill="#ffffff" stroke="#000"  stroke-width="3" stroke-miterlimit="10"  d="M439.5 66.6c-54.7.9-103.5 3.3-150 7.4l-23 2-4.3 4.7c-13.5 15-40.6 58-57.4 91-11.7 22.8-11.9 23.3-10.4 26.1.8 1.6 2.3 3.2 3.2 3.6.9.3 21.5.2 45.8-.4 24.2-.5 57.2-1.2 73.1-1.5 16-.2 47.9-.9 71-1.5 54.3-1.3 155.2-1.3 210.5 0 23.9.6 56.3 1.3 72 1.5 15.7.3 48.5 1 73 1.6l44.5 1 2.3-2.7c2.9-3.3 2.8-3.8-4.2-18.4-8-16.6-17.6-34.1-28.4-51.5-14-22.5-36-53.5-38-53.5-.4 0-10.6-.9-22.7-2-30.1-2.6-64.9-4.8-96.5-6-28.1-1-130.3-1.9-160.5-1.4zM121.6 148.6c-13 2-25.5 5.3-30.2 8-5.9 3.3-7.2 8.4-5 18.6 2.4 10.4 5 15.3 10.2 18.9 7.9 5.4 13.9 7.4 28.4 9.3 11.9 1.7 17.8 6.2 24.6 19.1 1 1.7 2.3 2 10.3 2.3l9.1.3-.1-20.8c0-26.7-1.3-49.3-2.8-52.2-2.2-4.1-6.8-5.1-21.8-5-7.6.1-17.8.7-22.7 1.5zM825.2 148.4c-6.1 2.8-6.4 5-7 42.8l-.5 33.8h18.2l3.2-6.1c5.2-9.7 11.7-14.1 22.7-15.5 13.5-1.6 24.9-5.8 30.6-11.1 3.8-3.5 6.3-9.1 7.7-16.7 1.4-8.5.7-14.1-2.2-17.2-2.7-2.8-13.9-6.5-27.4-9-12.8-2.3-41-2.9-45.3-1zM218 212.5c0 .9 20.1 31.6 29.3 44.8 8.4 12 16.7 19.7 21.4 19.7 3.2 0 2.9-1.9-1.6-8.6-12.9-19.3-49.1-60.5-49.1-55.9zM749.3 215c-6.4 5.8-17 17.9-30.5 34.9-12.8 16-17.8 23.4-17.8 26.1 0 2 4.8.9 8.8-2 5.6-4 11-11 25.9-33.5 6.9-10.5 13.4-20.1 14.3-21.5 4.9-7.2 4.6-8.9-.7-4zM862 292.9c-30 11.4-61.4 26.1-79.9 37.6-32.4 20-80.8 64.9-87.8 81.3-1.9 4.5-.9 5.5 4.3 4.7 7.5-1.1 36.3-11.7 56.5-20.8 32.8-14.8 47.7-21.6 57.2-26.1 48.8-23.2 63-30.7 65.6-35 4.5-7.3 7.2-26 5.1-35.9-1.2-6.1-4.2-10.8-6.8-10.6-.9.1-7.3 2.2-14.2 4.8zM107.6 293c-3.3 3.9-3.9 7.8-3.4 20.3.3 9 .8 11 3.6 17l3.3 6.7 31.7 15.6c17.4 8.5 32.5 15.9 33.4 16.4.9.5 6.5 3.1 12.5 5.8 5.9 2.8 14 6.4 17.8 8.2 12.2 5.6 14.7 6.7 28 12.5 22.4 9.9 48.4 19.5 52.6 19.5 5.5 0 2.2-7.1-10-21.3-19.2-22.2-54.7-51.9-79.1-65.9-20.8-12-79.7-37.8-86.1-37.8-.9 0-2.9 1.4-4.3 3zM291 363.9c0 .6.4 1.3 1 1.6 3.7 2.3 24.2 32.3 30.1 44.1 5.2 10.3 6 15.6 3 18.8-1.3 1.3-1.9 2.6-1.4 2.9.4.2 7.3 2.1 15.3 4.1 8 2.1 19.5 5.1 25.5 6.7 34.9 9.2 83.7 18.2 113.5 20.9 31.6 2.9 84.4-5.2 159.4-24.6 30.7-7.9 28.7-7.2 25.9-9.1-6.7-4.5 1.1-22.5 21.9-50.3 5.1-6.9 9.9-13.4 10.7-14.4 1.8-2.5 2.3-2.6-19.9 5.9-29.1 11.2-61.7 21.3-88.5 27.5-4.9 1.1-9.9 2.3-11 2.6-1.1.2-6.9 1.3-13 2.4-6 1.1-13 2.4-15.5 2.9-9.6 2-37.7 4.2-54.2 4.1-16.5 0-41.2-1.9-52.2-4.1-2.8-.6-6-1.2-7.1-1.4-31-5.2-82.9-19.9-128.6-36.6-14.6-5.3-14.9-5.4-14.9-4zM246.2 574.7l.3 5.8h513l.3-5.8.3-5.7H245.9l.3 5.7zM37.1 648.9c-1 .6 1.8 4.4 11.1 14.7 16.1 18 59.3 66.8 67.7 76.4 3.6 4.1 6.8 7.7 7.1 8 .3.3 2.2 2.4 4.3 4.8l3.7 4.2v59h53v-29.4c0-28.7 0-29.4 2.2-32.2 1.2-1.6 2.5-3.1 2.8-3.4.3-.3 2.7-3.2 5.5-6.5 2.7-3.3 5.9-7.1 7.2-8.5 18.3-20.7 72.9-86.6 72.2-87.1-.6-.3-15.3-.7-32.8-.8l-31.8-.2-15.4 19.8c-8.5 10.9-20.1 26-25.9 33.5-5.8 7.6-10.8 13.7-11.1 13.8-.5 0-24.3-28.9-37.4-45.4-1.6-2.1-3.7-4.7-4.7-5.8-.9-1-3.1-3.8-5-6-1.8-2.3-4.3-5.3-5.5-6.7l-2.1-2.6-31.9-.2c-17.5-.2-32.4.1-33.2.6zM656 648.7c-14.4 1-28.3 2.7-34.9 4.4-19.5 5-26.7 16.1-29.8 45.9-1 9.9-1 58.3 0 68.5 2.4 23 9.1 36.3 21.2 41.6 6.4 2.8 20.3 5.6 30.3 6 15.1.7 98.4.4 105.7-.3 27.2-2.8 39.2-11.5 43.4-31.8 1.1-5.5 3.1-34.1 3.1-45.8V731H685v20H772.3l-.7 10.8c-2 30.9-4.9 32.6-57.6 33.8-29.9.7-73.4-.9-82.4-3-12-2.8-15.2-7.9-17.3-27.1-.9-8.4-1-62.3-.1-68 3-19.7 6.5-23.5 23.8-26.6 10.4-1.9 96.6-2 109.5-.1 16.9 2.5 21 6.5 22.2 22.1l.6 8.2 11.5-.3 11.6-.3-.2-6.5c-.6-15.3-4-23.8-13-32.1-9.7-9-17.9-11.2-49.2-12.8-14.9-.8-65.7-1.1-75-.4z"/>
                    <path class="logo" fill="#ffffff" stroke="#000" stroke-width="3" stroke-miterlimit="10"   d="M277.1 695.1c-5.1.4-10.9 1.1-13 1.5-19.2 4-28.6 10.9-34.2 25.1-2.1 5.3-2.3 7.8-2.7 28.7-.6 33.8 1.6 45 10.5 54 11.6 11.8 24.4 14.3 71.8 14.4 47.7.2 63.4-2.7 73.7-13.5 8.8-9.2 11.6-22.3 11.3-51.3-.4-25.4-2-33.6-8.2-42.2-6.7-9.3-20.8-15-41.3-16.7-13.1-1.2-53.8-1.2-67.9 0zm59.5 35.3c7.8 2.4 11 6.3 12.5 15 1.5 9.1.6 23.3-2 29-3.6 8.3-12.3 10.8-37.1 10.8-24.5-.1-34.6-3.5-37.1-12.5-1.5-5.4-1.5-25 0-30.4 1.6-5.9 5.3-9.9 10.9-11.7 7.1-2.3 9.2-2.4 29.2-2.1 13.7.2 19.8.7 23.6 1.9zM410.4 698.5c-1.1 2.7.3 76.6 1.5 83.4a43.8 43.8 0 0 0 11.7 22.4c10.4 10.5 25.1 14.6 52.1 14.5 28.3-.1 39.7-3.8 48.6-15.6l4.2-5.6.3 9.2.3 9.2H571l-.2-59.3-.3-59.2-21.6-.3-21.6-.2-.6 33.2c-.4 18.3-1.1 35.1-1.6 37.2-1.3 5.4-5.9 11.5-10.1 13.7-5.4 2.8-13.3 4-26.2 4-14.9.1-21.9-1.3-26.4-5.3-6.9-6.1-6.8-5.4-7.4-45.8l-.5-36.5-21.8-.3c-19-.2-21.8 0-22.3 1.3zM860.2 699c-20 1.7-29 4.9-36.6 13-6.2 6.7-9.2 13.1-11.5 25.1-3.7 19.3-1.7 44.6 4.5 57.1 6.4 12.7 17.9 19.5 36.9 21.7 11.5 1.4 57.5 1.4 68.5.1 29.4-3.6 41.5-20.6 41.5-58-.1-22.7-4.3-36.8-13.7-46.3-6.1-6.1-13.4-9.5-24.6-11.3-10.6-1.7-50.8-2.6-65-1.4zm33.1 16.1c34.9.3 44.8 5.2 48.7 24.4 1.4 6.5 1.7 26.4.6 33.5-1.2 7.7-3.7 14-6.9 17.7-7 8-17.3 10.2-47.4 10.1-45.5-.1-55.2-5.4-57.3-31.7-1.3-16.2.1-31.9 3.6-38.5 3.6-7 7.7-10.3 15.4-12.6 5.6-1.7 23.4-4.1 25.1-3.4.3.2 8.5.4 18.2.5zM223 871.2c-.1 1 0 48.1 0 51.1 0 .5 1.5.7 3.3.5l3.2-.3.3-22.8c.2-21.4.3-22.7 2.1-22.7 1 0 2.1.6 2.4 1.4.3.8 3 5.2 6 9.8 3 4.5 9.3 14.3 14 21.5 7.8 12 8.9 13.3 11.5 13.3 2.7 0 3.7-1.2 10.8-12.3 4.3-6.7 10.7-16.7 14.2-22.2 6.5-10.2 8.8-12.8 10.3-11.9.5.3.9 10.9.9 23.5v23l3.3-.3 3.2-.3v-52l-7.2-.3c-7.2-.3-7.3-.3-9.2 2.8-10.9 18-25.8 40.9-26.4 40.7-.4-.1-7-9.9-14.6-21.7l-13.8-21.5-7.2-.3c-5.3-.2-7.1 0-7.1 1zM359 871c-4.2 1-7.5 4.7-8.9 9.8-.6 2-1.1 9.4-1.1 16.4 0 14.6 1.3 19.5 6.4 23.1 3 2.1 4.1 2.2 24.9 2.4 34.1.4 35.2-.4 35.2-26.2 0-13-.3-15.5-2-19-3.3-6.5-5.1-7-29.5-7.2-11.8-.1-23.1.2-25 .7zm45.2 6.8c1.4.6 2.9 1.6 3.3 2.3.4.6.7 7.6.7 15.6-.1 17.1-.8 18.8-7.4 19.8-2.4.4-12.3.5-22.1.3-17-.3-18-.4-19.7-2.5-3.7-4.6-3.7-29 0-33.6 1.3-1.6 3.4-2.3 7.6-2.8 8.2-1 34.3-.3 37.6.9zM456 896.5v26.6l25.1-.1c33.5-.2 35.9-1.2 35.9-15 0-6.9-1.3-10.4-4.6-11.9l-2.4-1 2.5-2.7c2.1-2.3 2.5-3.7 2.5-8.8 0-6.9-1.4-9.7-6-12.1-2.3-1.2-7.7-1.5-28-1.5h-25v26.5zm50.1-18.8c.6.6 1.4 3.1 1.6 5.6.4 3.6.1 4.9-1.6 6.6-2 2-3.1 2.1-22.6 2.1H463v-16.2l21 .4c14.1.2 21.3.7 22.1 1.5zm-.6 22.3c3.2 1.2 4.8 5.3 4 10.2-1 5.9-2.3 6.2-25.5 6.3h-20.5l-.3-8.8-.3-8.7h20c11 0 21.1.4 22.6 1zM558 896.5v26.6l3.3-.3 3.2-.3v-52l-3.2-.3-3.3-.3v26.6zM606 896.4V923h50v-3c0-1.7-.5-3-1.2-3s-10.2-.2-21.1-.3l-19.9-.2-.1-23-.2-23-3.7-.3-3.8-.3v26.5zM693 896.5V923h52v-6l-22.2-.2-22.3-.3-.3-8.7-.3-8.8 21.3-.2 21.3-.3.3-3.3.3-3.2H700v-16h45v-6h-52v26.5z"/>
                </svg>

            </div>
            <div class="login__form" style="margin-top: -50px">

                <form method="post" action="<?php echo base_url(); ?>main/login">
                <div class="login__row">
                    <svg class="login__icon name svg-icon svg-classe icones_login"  viewBox="0 0 20 20">
                        <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
                    </svg>
                    <input type="text" class="login__input name form-control" placeholder="Usuário" aria-describedby="basic-addon1" id="usuario" name="usuario" required/>
                </div>
                <div class="login__row">
                    <svg class="login__icon pass svg-icon svg-classe icones_login" viewBox="0 0 20 20">
                        <path d="M0,20 20,20 20,8 0,8z M10,13 10,16z M4,8 a6,8 0 0,1 12,0" />
                    </svg>
                    <input type="password" class="login__input pass form-control"   placeholder="Senha" aria-describedby="basic-addon1" name="senha" required />
                </div>

                <button type="submit" class="login__submit acessar">Entrar</button>
                </form>
                <p class="login__signup login__submit_cadastro" data-wow-duration="2s" data-wow-delay="0.3s">Ainda não tem uma conta?&nbsp;<a>Cadastrar</a></p>
                <span style="width:48%; font-size: 12px; text-align:center;  display: inline-block;margin-top: 30px" class="login__signup"><a class="small-text" id="esqueciSenha" href="#">Esqueceu sua senha?</a></span>
            </div>

        </div>



        <!-- Aqui dentro fica o formulario de cadastro-->
        <div class="app" onload="initialize()">

            <div class="app__top" style="background: ">
                <div class="app__logout">
                    <svg version="1" xmlns="http://www.w3.org/2000/svg" class="app__logout-icon svg-icon"  width="40" height="40" viewBox="0 0 40 40">
                        <path  d="M9.6 11.3A15.2 15.2 0 0 0 7 15.5c0 2 5.3 7.5 7.2 7.5 1.6 0 1.9-.5 1.6-2.4-.5-2.3-.2-2.5 3.6-2.8 3.3-.2 4.1-.7 4.1-2.3 0-1.6-.8-2.1-4.1-2.3-3.8-.3-4.1-.5-3.6-2.8.7-3.7-3.1-3.2-6.2.9z"/>

                    </svg>
                </div>


                <br>
                <br>
                <h3 style="color: #fff">Formulario de cadastro</h3>
                <form align="left">
                    <br>
                    <span style="color: #fff">Nome</span>
                    <input placeholder="Nome" type="text"  class="inp_form_cadastro" id="nome" name="nome">
                    <br>
                    <br>
                    <span style="color: #fff">Gênero</span>
                    <select  style="border: none;border-bottom: 1px solid #fff;margin-top: 10px" class="inp_form_cadastro" id="sexo">
                        <option value="82" style="background: #292929;color: #fff"> Masculino</option>
                        <option value="81" style="background: #292929;color: #fff"> Feminino</option>
                    </select>

                    <br><br>
                    <span style="color: #fff">RG</span>
                    <input placeholder="RG" type="tel" class="inp_form_cadastro" id="rg" maxlength="12">
                    
                    <br><br>
                    <span style="color: #fff">CPF</span>
                    <input placeholder="CPF" type="tel" class="inp_form_cadastro" id="cpf" maxlength="14">
                    
                    <br><br>
                    <span style="color: #fff">E-mail</span>
                    <input placeholder="E-mail" type="email" class="inp_form_cadastro" id="email">
                    
                    <br><br>
                    <span style="color: #fff">Confirmar E-mail</span>
                    <input placeholder="Confirmar E-mail" type="email" class="inp_form_cadastro" id="confirmaremail">
                    
                    <br><br>
                    <span style="color: #fff">Senha</span>
                    <input placeholder="Senha" type="password" class="inp_form_cadastro" id="senha">
                    
                    <br><br>
                    <span style="color: #fff">Confirmar Senha</span>
                    <input placeholder="Confirmar senha" class="inp_form_cadastro" type="password" id="confirmarsenha">
                    
                    <br><br>
                    <span style="color: #fff">Telefone / Celular</span> <br>
                    <input placeholder="Telefone" type="tel" class="inp_form_cadastro" style="width: 49%" id="telefone" maxlength="14">
                    <input placeholder="Celular" type="tel" class="inp_form_cadastro" style="width: 49%" id="celular" maxlength="15">
                    
                    <br><br>
                    <span style="color: #fff">CEP</span>
                    <input placeholder="CEP" type="tel" class="inp_form_cadastro" id="cep"  onblur="pesquisacep(this.value);" maxlength="9" >
                    
                    <br><br>
                    <span style="color: #fff">Logradouro / Número</span> <br>
                    <input placeholder="Logradouro" type="text" class="inp_form_cadastro" id="rua" style="width: 79%">
                    <input placeholder="N°" type="tel" class="inp_form_cadastro" id="numero" style="width: 19%">
                    
                    <br><br>
                    <span style="color: #fff">Bairro</span>
                    <input placeholder="Bairro" type="text" class="inp_form_cadastro" id="bairro">
                    
                    <br><br>
                    <span style="color: #fff">Cidade / UF</span> <br>
                    <input placeholder="Cidade" type="text" class="inp_form_cadastro" style="width: 79%" id="cidade">
                    <input placeholder="UF" type="text" class="inp_form_cadastro" style="width: 19%" id="uf" maxlength="2">
                    
                    <br><br>
                    <span style="color: #fff">Complemento</span>
                    <input placeholder="Complemento" type="text" class="inp_form_cadastro" id="complemento">
                    <br>
                    <br>
                    <br>

                    <h3 style="color: #fff">Informações sobre o carro</h3>
                    <span style="color: #fff" align="left">Cidade</span>
                    <select  style="border: none;border-bottom: 1px solid #fff;margin-top: 10px"  class="inp_form_cadastro" id="cidade_trabalho">
                    </select>
                    <br><br>
                    <span style="color: #fff" align="left">Marca</span>
                    <select  style="border: none;border-bottom: 1px solid #fff;margin-top: 10px" class="inp_form_cadastro" id="marca_veiculo">
                    </select>
                    <br><br>
                    <span style="color: #fff" align="left">Modelo</span>
                    <select  style="border: none;border-bottom: 1px solid #fff;margin-top: 10px" class="inp_form_cadastro" id="modelo_veiculo" >
                    </select>
                    <br><br>
                    <span style="color: #fff" align="left">Placa</span>
                    <input style="text-transform:uppercase" placeholder="Placa do veículo" type="text" class="inp_form_cadastro" id="placa_veiculo" maxlength="8">
                    <br><br>
                    <span style="color: #fff" align="left">COR</span>
                    <input placeholder="Cor do veículo"   type="text" class="inp_form_cadastro" id="cor_veiculo">
                    <br>
                    <br>
                    <br>
                    <h3 style="color: #fff">Anexos de fotos</h3>
                    <br>


                    <div align="center">

                    <!--Upload pessoal-->
                    <div class="btn_cadastro" onclick="pessoal()" id="pessoal_abrir"><span>Pessoal</span></div>
                    <div class="btn_cadastro" onclick="pessoal_fechar()" style="display: none;background: #292929" id="pessoal_sair"><span>Pessoal</span></div>
                    <label for="pessoal" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="pessoal_icon"><i class="fa fa-2x fa-image"> </i> &nbsp;<img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="pessoal_aparecer_img" style="display: none" /> </label>
                    <input type="file"  style="display: none" class="cadastro_imagens" id="pessoal" accept="image/*" onchange="pessoal_apareer_img(event)">
                    <script>
                        function pessoal_apareer_img(event){
                            var aparecer = document.getElementById('pessoal_aparecer_img');
                            aparecer.src = URL.createObjectURL(event.target.files[0]);
                            $('#pessoal_aparecer_img').css('display','inline-block');
                        }

                        function  pessoal() {
                            $('#pessoal_icon').css('display','block');
                            $('#pessoal_sair').css('display','block');
                            $('#pessoal_abrir').css('display','none');
                        }
                        function  pessoal_fechar() {
                            $('#pessoal_icon').css('display','none');
                            $('#pessoal_sair').css('display','none');
                            $('#pessoal_abrir').css('display','block');
                        }
                    </script>



                    <!--Upload Rg-->
                    <div class="btn_cadastro" onclick="rg_abrir()" id="rg_abrir"><span>RG</span></div>
                    <div class="btn_cadastro" onclick="rg_fechar()" style="display: none;background: #292929" id="rg_sair"><span>RG</span></div>
                    <label for="rg_inp" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="rg_icon"><i class="fa fa-2x fa-image"></i> &nbsp;<img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="rg_aparecer_img" style="display: none" />  </label>
                    <input type="file"  style="display: none" class="cadastro_imagens tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="rg_inp"  accept="image/*" onchange="rg_apareer_img(event)">
                    <script>
                        function rg_apareer_img(event){
                            var aparecer = document.getElementById('rg_aparecer_img');
                            aparecer.src = URL.createObjectURL(event.target.files[0]);
                            $('#rg_aparecer_img').css('display','inline-block');
                        }
                        function  rg_abrir() {
                            $('#rg_icon').css('display','block');
                            $('#rg_sair').css('display','block');
                            $('#rg_abrir').css('display','none');
                        }
                        function  rg_fechar() {
                            $('#rg_icon').css('display','none');
                            $('#rg_sair').css('display','none');
                            $('#rg_abrir').css('display','block');
                        }
                    </script>


                    <!--Upload Antecedentes-->
                    <div class="btn_cadastro" onclick="antecedentes()" id="antecedentes_abrir"><span>Antecedentes</span></div>
                    <div class="btn_cadastro" onclick="antecedentes_fechar()" style="display: none;background: #292929" id="antecedentes_sair"><span>Antecedentes</span></div>
                    <label for="antecedentes" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="antecedentes_icon"><i class="fa fa-2x fa-image"></i> &nbsp;<img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="antecedentes_aparecer_img" style="display: none" />  </label>
                    <input type="file"  style="display: none" class="cadastro_imagens tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="antecedentes"  accept="image/*" onchange="antecedentes_apareer_img(event)">
                    <script>
                        function antecedentes_apareer_img(event){
                            var aparecer = document.getElementById('antecedentes_aparecer_img');
                            aparecer.src = URL.createObjectURL(event.target.files[0]);
                            $('#antecedentes_aparecer_img').css('display','inline-block');
                        }
                        function  antecedentes() {
                            $('#antecedentes_icon').css('display','block');
                            $('#antecedentes_sair').css('display','block');
                            $('#antecedentes_abrir').css('display','none');
                        }
                        function  antecedentes_fechar() {
                            $('#antecedentes_icon').css('display','none');
                            $('#antecedentes_sair').css('display','none');
                            $('#antecedentes_abrir').css('display','block');
                        }
                    </script>



                    <!--Upload CNH-->
                    <div class="btn_cadastro" onclick="cnh()" id="cnh_abrir"><span>CNH</span></div>
                    <div class="btn_cadastro" onclick="cnh_fechar()" style="display: none;background: #292929" id="cnh_sair"><span>CNH</span></div>
                    <label for="cnh" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="cnh_icon"><i class="fa fa-2x fa-image"></i> &nbsp;<img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="cnh_aparecer_img" style="display: none" /> </label>
                    <input type="file"  style="display: none" class="cadastro_imagens tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="cnh" accept="image/*" onchange="cnh_apareer_img(event)">
                    <script>
                        function cnh_apareer_img(event){
                            var aparecer = document.getElementById('cnh_aparecer_img');
                            aparecer.src = URL.createObjectURL(event.target.files[0]);
                            $('#cnh_aparecer_img').css('display','inline-block');
                        }
                        function  cnh() {
                            $('#cnh_icon').css('display','block');
                            $('#cnh_sair').css('display','block');
                            $('#cnh_abrir').css('display','none');
                        }
                        function  cnh_fechar() {
                            $('#cnh_icon').css('display','none');
                            $('#cnh_sair').css('display','none');
                            $('#cnh_abrir').css('display','block');
                        }
                    </script>

                    <!--Upload seguro-->
                    <div class="btn_cadastro" onclick="seguro()" id="seguro_abrir"><span>Seguro</span></div>
                    <div class="btn_cadastro" onclick="seguro_fechar()" style="display: none;background: #292929" id="seguro_sair"><span>Seguro</span></div>
                    <label for="seguro" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="seguro_icon"><i class="fa fa-2x fa-image"></i> &nbsp;<img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="seguro_aparecer_img" style="display: none" />  </label>
                    <input type="file"  style="display: none" class="cadastro_imagens tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="seguro" accept="image/*" onchange="seguro_apareer_img(event)">
                    <script>
                        function seguro_apareer_img(event){
                            var aparecer = document.getElementById('seguro_aparecer_img');
                            aparecer.src = URL.createObjectURL(event.target.files[0]);
                            $('#seguro_aparecer_img').css('display','inline-block');
                        }
                        function  seguro() {
                            $('#seguro_icon').css('display','block');
                            $('#seguro_sair').css('display','block');
                            $('#seguro_abrir').css('display','none');
                        }
                        function  seguro_fechar() {
                            $('#seguro_icon').css('display','none');
                            $('#seguro_sair').css('display','none');
                            $('#seguro_abrir').css('display','block');
                        }
                    </script>


                    <!--Upload carro-->
                    <div class="btn_cadastro" onclick="carro()" id="carro_abrir"><span>Carro</span></div>
                    <div class="btn_cadastro" onclick="carro_fechar()" style="display: none;background: #292929" id="carro_sair"><span>Carro</span></div>
                    <!--img 1-->
                    <label for="carro" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="carro_icon"><i class="fa fa-2x fa-image"></i> &nbsp;<img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="carro_aparecer_img" style="display: none" />  </label>
                    <input type="file"  style="display: none" class="cadastro_imagens tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="carro" accept="image/*" onchange="carro_apareer_img(event)">

                    <!--img 2-->
                    <label for="carro_img2" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" id="carro_icon_img2"><i class="fa fa-2x fa-image"></i> &nbsp;<img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="carro_aparecer_img_img2" style="display: none" />  </label>
                    <input type="file"  style="display: none" class="cadastro_imagens tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="carro_img2" accept="image/*" onchange="carro_apareer_img_img2(event)">
                    <!--img 3-->
                    <label for="carro_img3" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s" id="carro_icon_img3"><i class="fa fa-2x fa-image"></i> &nbsp;<img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="carro_aparecer_img_img3" style="display: none" />  </label>
                    <input type="file"  style="display: none" class="cadastro_imagens tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="carro_img3" accept="image/*" onchange="carro_apareer_img_img3(event)">

                    <script>
                        function carro_apareer_img(event){
                            var aparecer = document.getElementById('carro_aparecer_img');
                            aparecer.src = URL.createObjectURL(event.target.files[0]);
                            $('#carro_aparecer_img').css('display','inline-block');
                        }
                        function carro_apareer_img_img2(event){
                            var aparecer = document.getElementById('carro_aparecer_img_img2');
                            aparecer.src = URL.createObjectURL(event.target.files[0]);
                            $('#carro_aparecer_img_img2').css('display','inline-block');
                        }
                        function carro_apareer_img_img3(event){
                            var aparecer = document.getElementById('carro_aparecer_img_img3');
                            aparecer.src = URL.createObjectURL(event.target.files[0]);
                            $('#carro_aparecer_img_img3').css('display','inline-block');
                        }
                        function  carro() {
                            $('#carro_icon').css('display','block');
                            $('#carro_icon_img2').css('display','block');
                            $('#carro_icon_img3').css('display','block');
                            $('#carro_sair').css('display','block');
                            $('#carro_abrir').css('display','none');
                        }
                        function  carro_fechar() {
                            $('#carro_icon').css('display','none');
                            $('#carro_icon_img2').css('display','none');
                            $('#carro_icon_img3').css('display','none');
                            $('#carro_sair').css('display','none');
                            $('#carro_abrir').css('display','block');
                        }
                    </script>


                    <!--Upload documento-->
                    <div class="btn_cadastro" onclick="documento()" id="documento_abrir"><span>Documentos do carro</span></div>
                    <div class="btn_cadastro" onclick="documento_fechar()" style="display: none;background: #292929" id="documento_sair"><span>Documentos do carro</span></div>
                    <label for="documento" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="documento_icon"><i class="fa fa-2x fa-image"></i>  <img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="documento_aparecer_img" style="display: none" />  </label>
                    <input type="file"  style="display: none" class="cadastro_imagens tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="documento" accept="image/*" onchange="documento_apareer_img(event)" >
                    <script>
                        function documento_apareer_img(event){
                            var aparecer = document.getElementById('documento_aparecer_img');
                            aparecer.src = URL.createObjectURL(event.target.files[0]);
                            $('#documento_aparecer_img').css('display','inline-block');
                        }
                        function  documento() {
                            $('#documento_icon').css('display','block');
                            $('#documento_sair').css('display','block');
                            $('#documento_abrir').css('display','none');
                        }
                        function  documento_fechar() {
                            $('#documento_icon').css('display','none');
                            $('#documento_sair').css('display','none');
                            $('#documento_abrir').css('display','block');
                        }
                    </script>

                    <!--Upload comprovante-->
                    <div class="btn_cadastro" onclick="comprovante()" id="comprovante_abrir"><span>Comprovante de residência</span></div>
                    <div class="btn_cadastro" onclick="comprovante_fechar()" style="display: none;background: #292929" id="comprovante_sair"><span>Comprovante de residência</span></div>
                    <label for="comprovante" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="comprovante_icon"><i class="fa fa-2x fa-image"></i> &nbsp;<img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="comprovante_aparecer_img" style="display: none" />  </label>
                    <input type="file"  style="display: none" class="cadastro_imagens tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="comprovante"  accept="image/*" onchange="comprovante_apareer_img(event)">
                    <script>
                        function comprovante_apareer_img(event){
                            var aparecer = document.getElementById('comprovante_aparecer_img');
                            aparecer.src = URL.createObjectURL(event.target.files[0]);
                            $('#comprovante_aparecer_img').css('display','inline-block');
                        }
                        function  comprovante() {
                            $('#comprovante_icon').css('display','block');
                            $('#comprovante_sair').css('display','block');
                            $('#comprovante_abrir').css('display','none');
                        }
                        function  comprovante_fechar() {
                            $('#comprovante_icon').css('display','none');
                            $('#comprovante_sair').css('display','none');
                            $('#comprovante_abrir').css('display','block');
                        }
                    </script>






                    </div>
                </form>
                <p style="margin-top: 20px"></p>

            <button style="background: transparent;margin-top: 30px;" onclick="consultar()">

                    <div class="svg-wrapper" style="cursor: pointer">
                        <svg height="60" width="280" xmlns="http://www.w3.org/2000/svg">
                            <rect class="shape" height="60" width="312" />
                        </svg>
                        <div class="text" >Cadastrar</div>
                    </div>

            </button>
            <p>&nbsp;</p>

        </div>
    </div>
</div>


</body>


<script type="text/javascript">
    $(document).ready(function(){

        var mask = "LLL-NNNN",
            pattern = {
                'translation': {
                    'L': {
                        pattern: /[A-Za-z]/
                    },
                    'N': {
                        pattern: /[0-9]/
                    }
                }
            };

        $('#placa_veiculo').mask(mask, pattern);


        var animating = false,
            submitPhase1 = 200,
            submitPhase2 = 400,
            logoutPhase1 = 800,
            $login = $(".login"),
            $app = $(".app");

        function ripple(elem, e) {
            $(".ripple").remove();
            var elTop = elem.offset().top,
                elLeft = elem.offset().left,
                x = e.pageX - elLeft,
                y = e.pageY - elTop;
            var $ripple = $("<div class='ripple'></div>");
            $ripple.css({top: y, left: x});
            elem.append($ripple);
        };

        $(document).on("click", ".login__submit_cadastro", function(e) {
            if (animating) return;
            animating = true;
            var that = this;
            ripple($(that), e);
            $(that).addClass("processing");
            setTimeout(function() {
                $(that).addClass("success");
                setTimeout(function() {
                    $app.show();
                    $app.css("top");
                    $app.addClass("active");
                }, submitPhase2 - 70);
                setTimeout(function() {
                    $login.hide();
                    $login.addClass("inactive");
                    animating = false;
                    $(that).removeClass("success processing");
                }, submitPhase2);
            }, submitPhase1);
        });

        $(document).on("click", ".app__logout", function(e) {
            if (animating) return;
            $(".ripple").remove();
            animating = true;
            var that = this;
            $(that).addClass("clicked");
            setTimeout(function() {
                $app.removeClass("active");
                $login.show();
                $login.css("top");
                $login.removeClass("inactive");
            }, logoutPhase1 - 120);
            setTimeout(function() {
                $app.hide();
                animating = false;
                $(that).removeClass("clicked");
            }, logoutPhase1);
        });






        /*PRa cima jean, baixo eduardo*/


        var erro_email = false;

        $(".validar_email").focusout(function(){

            console.log('Validando E-mail...');

            if($(this).val() != "") {

                var filtro = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

                if(!filtro.test($(this).val())) {

                    toast('E-mail', 'E-mail inválido!','error',true);
                    erro_email = true;
                    $(this).focus();
                    return false;

                } else {

                    erro_email = false;
                    $.toast().reset('all');

                }

            } else {

                erro_email = false;
                $.toast().reset('all');

            }

        });

        if (!window.Notification) {
            console.log('Habilite as Notificações');
        } else {
            Notification.requestPermission().then(function(p){
                if (p === 'denied') {
                    console.log('Notificações Não Aceitas.');
                } else if (p === 'granted') {
                    console.log('Notificações Aceitas.');
                }
            });
        }

        $('#esqueciSenha').click(function(){

            if ($('#usuario').val() != "") {

                $('#login').hide();
                $('#img_load').show();

                $.ajax({
                    url: "<?php echo base_url(); ?>controller_usuarios/esqueci_Senha",
                    type: "post",
                    data: {login: $("#usuario").val()},
                    datatype: 'json',
                    success: function(data){

                        if (data['status'] == 1) {
                            toast('Senha alterada com sucesso!',data['resultado'],'success',false);
                        } else {
                            toast('Senha não alterada ou erro ao enviar E-mail!',data['resultado'],'error',true);
                        }


                        $('#login').show();
                        $('#img_load').hide();


                    },
                    error:function(){
                        toast('Falha','Falha ao alterar senha.','error',true);

                        $('#login').show();
                        $('#img_load').hide();

                    }
                });

            } else {
                toast('Digite o usuário.','Usuário em branco.','error',true);
            }

        });

        <?php
        //Caso a tentativa de login falhe
        if(isset($falha)){

            echo "$.toast({
                heading: 'Falha no login',
                text: [
                    'Usuário e/ou senha inválidos', 
                    'Ou Login e/ou Grupo inativo',
                    'Entre em contato com um Administrador.'
                  ],
                hideAfter: false,
                position: 'top-right',
                icon: 'error'
            });";
        }


        if(isset($ativacao_email)){

            echo 'console.log(\'fudeu\');';

            echo "$.toast({
                heading: 'E-mail ativado',
                text: [
                    'E-mail ativado com sucesso!'
                  ],
                hideAfter: false,
                position: 'top-right',
                icon: 'success'
            });";
        } 

        if(isset($ativacao_email_falhou)){

            echo "$.toast({
                heading: 'E-mail ativado',
                text: [
                    'E-mail ativado com sucesso!'
                  ],
                hideAfter: false,
                position: 'top-right',
                icon: 'error'
            });";
        }

        //Saiu do sistema.
        if(isset($saiu)){
            //Aviso quando outro login foi realizado
            $forcado_ = "";
            if (isset($forcado) && $forcado != "") {
                $forcado_ = "Novo acesso registrado em: ".$forcado;
            }

            echo "toast('Até logo ".$saiu."!','Acesso encerrado. ".$forcado_."','success',false);";

        }

        ?>

        function toast(titulo,mensagem,tipo,fixo){

            $.toast().reset('all');

            if (fixo) {

                $.toast({
                    heading: titulo,
                    text: mensagem,
                    hideAfter: false,
                    position: 'top-right',
                    icon: tipo
                });

            } else {

                $.toast({
                    heading: titulo,
                    text: mensagem,
                    showHideTransition: 'fade',
                    position: 'top-right',
                    hideAfter : 5000,
                    icon: tipo
                });

            }
        }

    });
</script>




<script type="text/javascript">


    function limpa_formulário_cep() {
        //Limpa valores do formulário de cep.
        document.getElementById('rua').value=("");
        document.getElementById('bairro').value=("");
        document.getElementById('cidade').value=("");
        document.getElementById('uf').value=("");
        document.getElementById('ibge').value=("");
    }

    function meu_callback(conteudo) {
        if (!("erro" in conteudo)) {
            //Atualiza os campos com os valores.
            document.getElementById('rua').value=(conteudo.logradouro);
            document.getElementById('bairro').value=(conteudo.bairro);
            document.getElementById('cidade').value=(conteudo.localidade);
            document.getElementById('uf').value=(conteudo.uf);
        } //end if.
        else {
            //CEP não Encontrado.
            limpa_formulário_cep();
            alert("CEP não encontrado.");
        }
    }

    function pesquisacep(valor) {

        //Nova variável "cep" somente com dígitos.
        var cep = valor.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                document.getElementById('rua').value="...";
                document.getElementById('bairro').value="...";
                document.getElementById('uf').value="UF";
                document.getElementById('cidade').value="...";

                //Cria um elemento javascript.
                var script = document.createElement('script');

                //Sincroniza com o callback.
                script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

                //Insere script no documento e carrega o conteúdo.
                document.body.appendChild(script);

            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    };





</script>








<script type="text/javascript">

    function mascara(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascara()",1)
    }
    function execmascara(){
        v_obj.value=v_fun(v_obj.value)
    }
    function mtel(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
        v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
        return v;
    }
    function mtel2(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        v=v.replace(/^(\d{5})(\d)/g,"$1-$2"); //Coloca parênteses em volta dos dois primeiros dígitos
        return v;
    }
    function mtel3(cpf){
        cpf=cpf.replace(/\D/g,"");
        cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2");
        cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2");
        cpf=cpf.replace(/(\d{3})(\d{1,2})$/,"$1-$2");

        return cpf;
    }
    function mtel4(rg){
          rg=rg.replace(/\D/g,"");
          rg=rg.replace(/(\d{2})(\d)/,"$1.$2");
          rg=rg.replace(/(\d{3})(\d)/,"$1.$2");
          rg=rg.replace(/(\d{3})(\d{1,2})$/,"$1-$2");

        return rg;
    }/*
    function mtel5(v){
        v=v.replace(/^(\d{4})(\d)/,"$1-$2");
        return v;
    }*/
    function id( el ){
        return document.getElementById( el );
    }
    window.onload = function(){
        id('telefone').onkeyup = function(){
            mascara( this, mtel );
        }
        id('celular').onkeyup = function(){
            mascara( this, mtel );
        }
        id('cep').onkeyup = function(){
            mascara( this, mtel2 );
        }
        id('cpf').onkeyup = function(){
            mascara( this, mtel3 );
        }
        id('rg').onkeyup = function(){
            mascara( this, mtel4 );
        }
        /*id('placa_veiculo').onkeyup = function(){
            mascara( this, mtel5 );
        }*/
    }
</script>



