<head>
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/cupons.css" >
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/theme-animate.css" >
    <meta name="theme-color" content="#292929">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">

</head>

<div>
    <h1 class="fade_1"><i class="fas fa-flag  fa-2x lado_topo" data-wow-duration="2s" data-wow-delay="0.0s" > </i> Corridas</h1>

    <a href="<?= base_url(); ?>main/redirecionar/15/30"><button style="float: right;background: #292929;color: #fff;border: none;padding: 8px 15px;margin-right: 0px;">30 dias</button></a>
    <a href="<?= base_url(); ?>main/redirecionar/15/15"><button style="float: right;background: #292929;color: #fff;border: none;padding: 8px 15px;margin-right: 2px;">15 dias</button></a>
    <a href="<?= base_url(); ?>main/redirecionar/15/5"><button style="float: right;background: #292929;color: #fff;border: none;padding: 8px 15px;margin-right: 2px;">5 dias</button></a>
    <div class="lado_direito" style="width: 100%;height: 2px;background: #f8d509;margin-top: 30px;overflow-x: hidden"></div>
</div>
<?php
$cont = 1;
foreach ($dados_iniciais['dados'] as $dados){ ?>

    <?php if($dados['valor_corrida'] == 0.00){}else { ?>

        <a href="<?php echo base_url() ?>controller_adm/corrida_ver?id=<?php echo $dados['id_corrida']; ?>"
           target="_blank" style="text-decoration: none;color: inherit">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="padding: 10px 10px 25px 10px;">
                <?php if ($dados['fk_status_corrida'] == 87 or $dados['fk_status_corrida'] == 88){ ?>
                <div style="box-shadow:4px 4px #cccccc54;padding: 0;background: #292929;border: 1px solid #ccc;color: #ffff;display: table;height: 540px"
                     id="corrida" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php }else{ ?>
                    <div style="box-shadow:4px 4px #cccccc54;padding: 0;background: #fff;border: 1px solid #ccc;display: table;height:  540px"
                         id="corrida" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php } ?>
                        <div class="mapa"
                             style="width: 100%;height: 200px;background: url('<?php echo base_url(); ?>upload/passageiros/passageiro_<?php echo $dados['fk_passageiro']; ?>/passageiro.png') center center no-repeat;background-size: cover;">
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                        </div>
                        <div style="padding: 10px;">
                            <?php if ($dados['fk_status_corrida'] == 87 or $dados['fk_status_corrida'] == 88) { ?>
                                <p style="text-align: center;margin-top: 5px"><span
                                            style="font-size: 15px;font-weight: bolder">Corrida Cancelada</span></p>
                            <?php } ?>
                            <p style="text-align: center;margin-top: -5px"><span style="font-weight: bolder">RS</span>
                                <span style="font-size:30px "><?php echo str_replace('.', ',', $dados['valor_corrida']); ?></span>
                            </p>
                            <p style="text-align: center;margin-top: -10px"><span
                                        style="font-size: 13px;font-weight: bolder">ID da corrida:</span> <?php echo $dados['id_corrida']; ?>
                                &nbsp;&nbsp;&nbsp;</p>
                            <p style="text-align: center;margin-top: -10px"><span
                                        style="font-size: 13px;font-weight: bolder">Km:</span> <?php echo $dados['km_corrida']; ?>
                                &nbsp;&nbsp;&nbsp;</p>
                            <p style="text-align: center;margin-top: -10px"><span
                                        style="font-size: 13px;font-weight: bolder">Taxa YouGO: </span><?php echo $dados['taxa_yougo']; ?>
                                % &nbsp;&nbsp;</p>
                            <p style="text-align: center;margin-top: 10px"><span
                                        style="font-size: 13px;font-weight: bolder">Partida:</span> <?php echo $dados['endereco_origem']; ?>
                                &nbsp;&nbsp;</p>
                            <p style="text-align: center;margin-top: 10px"><span
                                        style="font-size: 13px;font-weight: bolder;margin-top: 10px;">Destino:</span> <?php echo $dados['endereco_destino']; ?>
                                &nbsp;&nbsp;</p>
                            <p style="text-align: center;margin-top: 10px"><span
                                        style="font-size: 13px;font-weight: bolder">Placa do carro:</span> <?php echo $dados['placa_carro_motorista']; ?>
                            </p>
                            <?php if ($dados['pagamento_motorista'] == 1) { ?>
                                <p style="text-align: center;margin-top: 10px;color: #79da57"><span
                                            style="font-size: 13px;font-weight: bolder">Corrida paga</span></p>
                            <?php } else { ?>
                                <p style="text-align: center;margin-top: 10px;color: #ff0000"><span
                                            style="font-size: 13px;font-weight: bolder">Corrida ainda não paga</span>
                                </p>
                            <?php } ?>
                        </div>

                        <?php
                        if (!empty($dados['data_corrida'])) {
                            $data = $dados['data_corrida'];
                            $data_dias = explode('-', $data);
                            $data_hora = explode(' ', $data_dias[2]);
                            ?>
                            <p style="text-align: center;margin-top: -10px;font-size: 12px;text-align: right">
                                Hora: <?php echo $data_hora[1]; ?> &nbsp;&nbsp;&nbsp;</p>
                            <p style="text-align: center;margin-top: -10px;font-size: 12px;text-align: right">
                                Data: <?php echo $data_hora[0] . '/' . $data_dias[1] . '/' . $data_dias[0]; ?> &nbsp;&nbsp;&nbsp;</p>
                        <?php } ?>


                    </div>
                </div>
        </a>
        <?php
        if($cont == 4){
            echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -25px">&nbsp;</div>';
            $cont = 0;
        }
        $cont++;
    }
} ?>






<style>


    ::-webkit-scrollbar-track {
        background-color: transparent;
    }
    ::-webkit-scrollbar {
        width: 8px;
        background: transparent;
    }
    ::-webkit-scrollbar-thumb  {
        border-radius: 15px;
        background: #292929;
    }

    #corrida:hover .cliente{
        box-shadow: 0 0 10px #666;
    }
    
    .cliente{
        width: 200px;
        border-radius: 50%;
        height: 200px;
        box-shadow: 0 0 7px #666;
        margin: 0 auto;

    }
</style>