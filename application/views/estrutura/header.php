<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
  <meta http-equiv="refresh" content="600">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php

    if (isset($titulo_aplicacao)){

        echo '<title>You GO | '.$titulo_aplicacao.'</title>';

     } else {

        echo '<title>You GO</title>';

     }

  ?>
  <link rel="shortcut icon" href="<?php echo base_url() ?>style/img/favicon.png">
	<link type="text/css" href="<?php echo base_url(); ?>style/css/bootstrap.min.css" rel="stylesheet" />
  <link type="text/css" href="<?php echo base_url(); ?>style/css/select2.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>style/css/jquery.toast.css" rel="stylesheet" />
  <link type="text/css" href="<?php echo base_url(); ?>style/css/estilo.css" rel="stylesheet" />
  <link type="text/css" href="<?php echo base_url(); ?>style/css/plugin_tinymce.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/jquery-ui/jquery-ui.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/css/sweetalert.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/css/daterangepicker.css">


    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/jquery-ui/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>style/js/bootstrap.min.js"></script>
  <!-- Aceita Mascaras nas listas -->
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.mask.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/script.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.toast.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/sweetalert.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/select2.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/moment.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/daterangepicker.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.priceformat.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/tinymce/tinymce.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/plugin_tinymce.js"></script>


  <!-- DataTable -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/DataTables/datatables.min.css"/>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/DataTables/DataTables/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/datatable.js"></script>

  <script type="text/javascript" DEFER="DEFER">
    /*Só é executado após o carregamento total da página*/
    $(document).ready(function(){

      $( "#load_geral" ).fadeOut( "slow", function() {
        $('.principal').show();
      });

      /*Usar notificações no navegador para informações remotas e a qualquer momento, com o navegador aberto*/
      setInterval(function () {
        console.log("Buscando notificações...");

        $.ajax({url: '<?php echo base_url(); ?>Controller_notificacoes/ajax_Notificacoes',
                type: 'json',
                method: 'POST'
        }).done(function(dados){

          $.each(dados,function(key,value){

            var usuario = dados[key].nome_usuario;
            var titulo = dados[key].titulo_notificacao;

            if (Notification.permission=='granted') {

                notificar = new Notification('Nova Notificação de: '+usuario,{
                  'body' : titulo,
                  'icon' : '<?php echo base_url() ?>style/img/favicon.png',
                  'tag' : '1'
                });

                setTimeout(notificar.close.bind(notificar), 5000);
                console.log(Notification.permission);

              } else {

                $.toast({
                      icon: 'info',
                      position: 'top-right',
                      hideAfter: false,
                      heading: 'Nova Notificação de: '+usuario,
                      text: titulo
                  });

                console.log('Por favor, Habilite as Notificações '+Notification.permission);
                console.log(Notification.permission);
              }

          });

        })

      }, 60000);


      <?php if ($this->session->flashdata('tipo_alerta') != "") {
        echo "$.toast({
                icon: '".$this->session->flashdata('tipo_alerta')."',
                heading: '".$this->session->flashdata('titulo_alerta')."',
                text: '".$this->session->flashdata('mensagem_alerta')."',";
                if ($this->session->flashdata('mensagem_fixa')) {
                  echo "hideAfter: false,";
                } else {
                  echo "showHideTransition: 'fade',";
                }
                echo "position: 'top-right',
            });";

        $this->session->set_flashdata('tipo_alerta','');
        $this->session->set_flashdata('titulo_alerta','');
        $this->session->set_flashdata('mensagem_alerta','');
        $this->session->set_flashdata('mensagem_fixa','');

      }

    ?>

    $('#erro_feedback').click(function(){

      var cod = $(this).attr('cod');

      swal({
          title: "Nos Ajude",
          text: "Por favor, tente descrever o que estava fazendo para ajudar a corrigir este erro.",
          type: "input",
          showCancelButton: true,
          closeOnConfirm: false,
          animation: "slide-from-top",
          inputPlaceholder: "Descreva o que estava fazendo."
        },
        function(inputValue){

          if (inputValue != "") {

            $.ajax({
              type: "post",
              url: "<?php echo base_url(); ?>Main/reportar_Erro",
              data:{id_log_erro: cod,
                    erro_feedback: inputValue},
              error: function(returnval) {
                 mensagem('Error',returnval,'error');
              },
              success: function (returnval) {
                if(returnval == 'sucesso') {

                  swal("Obrigado!", "Obrigado pelo Feedback!");


                } else {

                  swal("Falha =/", "Falha ao reportar erro! "+returnval);

                }

              }
          })

          }

        });
      });


    });
  </script>

  <!-- Usado quando o usuário desabilitou o JavaScript -->
  <noscript>
    <meta http-equiv="refresh" content=1;url="http://enable-javascript.com/pt/">
    <div style="background-color: red; height: 100%; width: 100%; position: absolute; z-index: 200000;" align="center">
      <h1 style="color: white;">HABILITE O JAVASCRIPT PARA USAR O SISTEMA!.</h1>
      <iframe src="http://enable-javascript.com/pt/" width="100%" height="100%"></iframe>

    </div>
  </noscript>

</head>
<body>


<!--deixar a tela escura-->
<style>#preto{display: none;background: #000;opacity: 0;margin-top: -20px;}</style>
<div id="preto" onclick="sair_modal()"  style="position: fixed;z-index: 99;height: 100%;width: 100%">&nbsp;</div>

<?php if (isset($menu)) { ?>

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>style/img/favicon.png" width="45px" height="50px;" style="margin-top: -8px"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <?php echo $menu; ?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<?php } ?>

<!--Load para troca entre views-->
<div  id="load_geral" style="margin-top: 10%;">

  <div class="col-md-4"></div>
  <div class="col-md-4">


      <svg class="mainSVG" viewBox="0 0 800 600" xmlns="http://www.w3.org/2000/svg">
          <defs>
              <path id="puff" d="M4.5,8.3C6,8.4,6.5,7,6.5,7s2,0.7,2.9-0.1C10,6.4,10.3,4.1,9.1,4c2-0.5,1.5-2.4-0.1-2.9c-1.1-0.3-1.8,0-1.8,0
	s-1.5-1.6-3.4-1C2.5,0.5,2.1,2.3,2.1,2.3S0,2.3,0,4.4c0,1.1,1,2.1,2.2,2.1C2.2,7.9,3.5,8.2,4.5,8.3z" fill="#000"/>
              <circle id="dot"  cx="0" cy="0" r="5" fill="#000"/>
          </defs>

          <circle id="mainCircle" fill="none" stroke="none" stroke-width="2" stroke-miterlimit="10" cx="400" cy="300" r="130"/>
          <circle id="circlePath" fill="none" stroke="none" stroke-width="2" stroke-miterlimit="10" cx="400" cy="300" r="80"/>

          <g id="mainContainer" >
              <g id="car">
                  <path id="carRot" fill="#000"  d="M45.6,16.9l0-11.4c0-3-1.5-5.5-4.5-5.5L3.5,0C0.5,0,0,1.5,0,4.5l0,13.4c0,3,0.5,4.5,3.5,4.5l37.6,0
	C44.1,22.4,45.6,19.9,45.6,16.9z M31.9,21.4l-23.3,0l2.2-2.6l14.1,0L31.9,21.4z M34.2,21c-3.8-1-7.3-3.1-7.3-3.1l0-13.4l7.3-3.1
	C34.2,1.4,37.1,11.9,34.2,21z M6.9,1.5c0-0.9,2.3,3.1,2.3,3.1l0,13.4c0,0-0.7,1.5-2.3,3.1C5.8,19.3,5.1,5.8,6.9,1.5z M24.9,3.9
	l-14.1,0L8.6,1.3l23.3,0L24.9,3.9z"/>
              </g>
          </g>
      </svg>
      <script src='http://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.3/TweenMax.min.js'></script>
      <script src='http://s3-us-west-2.amazonaws.com/s.cdpn.io/16327/MorphSVGPlugin.min.js'></script>


      <script>
          TweenMax.set('#circlePath', {
              attr: {
                  r: document.querySelector('#mainCircle').getAttribute('r')
              }
          })
          MorphSVGPlugin.convertToPath('#circlePath');

          var xmlns = "http://www.w3.org/2000/svg",
              xlinkns = "http://www.w3.org/1999/xlink",
              select = function(s) {
                  return document.querySelector(s);
              },
              selectAll = function(s) {
                  return document.querySelectorAll(s);
              },
              mainCircle = select('#mainCircle'),
              mainContainer = select('#mainContainer'),
              car = select('#car'),
              mainSVG = select('.mainSVG'),
              mainCircleRadius = Number(mainCircle.getAttribute('r')),
              //radius = mainCircleRadius,
              numDots = mainCircleRadius / 2,
              step = 360 / numDots,
              dotMin = 0,
              circlePath = select('#circlePath')

          //
          //mainSVG.appendChild(circlePath);
          TweenMax.set('svg', {
              visibility: 'visible'
          })
          TweenMax.set([car], {
              transformOrigin: '50% 50%'
          })
          TweenMax.set('#carRot', {
              transformOrigin: '0% 0%',
              rotation:30
          })

          var circleBezier = MorphSVGPlugin.pathDataToBezier(circlePath.getAttribute('d'), {
              offsetX: -20,
              offsetY: -5
          })



          //console.log(circlePath)
          var mainTl = new TimelineMax();

          function makeDots() {
              var d, angle, tl;
              for (var i = 0; i < numDots; i++) {

                  d = select('#puff').cloneNode(true);
                  mainContainer.appendChild(d);
                  angle = step * i;
                  TweenMax.set(d, {
                      //attr: {
                      x: (Math.cos(angle * Math.PI / 180) * mainCircleRadius) + 400,
                      y: (Math.sin(angle * Math.PI / 180) * mainCircleRadius) + 300,
                      rotation: Math.random() * 360,
                      transformOrigin: '50% 50%'
                      //}
                  })

                  tl = new TimelineMax({
                      repeat: -1
                  });
                  tl
                      .from(d, 0.2, {
                          scale: 0,
                          ease: Power4.easeIn
                      })
                      .to(d, 1.8, {
                          scale: Math.random() + 2,
                          alpha: 0,
                          ease: Power4.easeOut
                      })

                  mainTl.add(tl, i / (numDots / tl.duration()))
              }
              var carTl = new TimelineMax({
                  repeat: -1
              });
              carTl.to(car, tl.duration(), {
                  bezier: {
                      type: "cubic",
                      values: circleBezier,
                      autoRotate: true
                  },
                  ease: Linear.easeNone
              })
              mainTl.add(carTl, 0.05)
          }

          makeDots();
          mainTl.time(120);
          TweenMax.to(mainContainer, 20, {
              rotation: -360,
              svgOrigin: '400 300',
              repeat: -1,
              ease: Linear.easeNone
          });
          mainTl.timeScale(1.1)
      </script>
  </div>

</div>

<!--Div Principal-->
<div class="container principal" hidden="hidden">

<div class="modal fade" id="modal_historico" tabindex="-1" role="dialog" aria-labelledby="modal_historicoLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal_historicoLabel">Histórico de edições</h4>
      </div>
      <div class="modal-body">

        <div class="progress_modal_historico">
      <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
        <span class="sr-only">50%</span>
      </div>
    </div>

        <div id="load_modal_historico"></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  $(document).ready(function(){
    $('.progress_modal_historico').hide();

    $('#historico_modal').click(function () {

        $('.progress_modal_historico').show();
        var id = $(this).attr('cod');

        $('#load_modal_historico').load('<?php echo base_url() ?>Controller_relatorios/load_historico_edicoes',{id: id},function(){
          $('.progress_modal_historico').hide();

          dataTableLoad();

        });

    });

  });
</script>
<style type="text/css">
  @media screen and (max-width: 1300px){
    .nav>li>a {
    padding: 16px 10px !important;
    }
  }
</style>
