<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH.'/third_party/pag/PagSeguroAssinaturas.php';
use CWG\PagSeguro\PagSeguroAssinaturas;

class Controller_pagamento extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('model_pagamento');

		$email = "patricia.kimelblat@gmail.com";		 		//E-mail do dono dos planos
		$token = "429960B234D9430BA0A5BDEE0A291E7E"; 	//Token gerado no painel do PagSeguro
		$senha = "YouGo2018";
		

		/*
			$email = "jean.lucas395@gmail.com";		 		//E-mail do dono dos planos
			$token = "E15B7E599D88494997F0A09CCB5CF4EF"; 	//Token gerado no painel do PagSeguro
			$email = "dev@navillebrasil.com";		 		//E-mail do dono dos planos
			$token = "F7AA2499B3604854BAE8927D525D433E"; 	//Token gerado no painel do PagSeguro
		*/

		$this->autenticacao = "?email={$email}&token={$token}";

		if(ENVIRONMENT == 'development'){
			$this->url = "https://ws.sandbox.pagseguro.uol.com.br";
			$sandbox = true;
		} else {
			$this->url = "https://ws.pagseguro.uol.com.br";
			$sandbox = false;
		}

		$this->pagseguro = new PagSeguroAssinaturas($email, $token, $sandbox);
	}


	public function assinatura() {

		/**
		 * @todo Usar BaseAuth
		 * */

		$session_id 	= $this->pagseguro->iniciaSessao();
		$id_corrida		= $this->input->get('fk_corrida');
		$dados_cartao	= $this->model_pagamento->pagamento($id_corrida);
	
		$valores = array('id' => $session_id, 'fk_corrida' => $id_corrida, 'dados' => $dados_cartao);

		$this->load->view('pagamento/view_pagamento', $valores);
	}

	public function ativar_cartao() {

		/**
		 * @todo Usar BaseAuth
		 * */

		$session_id 	= $this->pagseguro->iniciaSessao();
		$fk_cartao		= $this->input->get('fk_cartao');
		$dados_cartao	= $this->model_pagamento->pagamento_Ativacao($fk_cartao);
	
		$valores = array('id' => $session_id, 'fk_cartao' => $fk_cartao, 'dados' => $dados_cartao);

		$this->load->view('pagamento/view_ativacao', $valores);
	}


	public function gerarXml() {

		$fk_cartao = $this->input->post('fk_cartao');

		if(isset($fk_cartao)) { //Cobrança de corrida

			$dados	= $this->model_pagamento->cobrarAtivacaoConta($fk_cartao);
			$dados['valor_corrida'] = '2.00'; 

		} else {
			
			$fk_corrida = $this->input->post('fk_corrida');
			$dados 		= $this->model_pagamento->corrida($fk_corrida);

			//Corrida cancelada pelo passageiro.
	        if($dados['fk_status_corrida'] == CANCELADO_PASSAGEIRO){
	        	$dados['valor_corrida'] = $dados['taxa_cancelamento'];
	        }

		}
        
        $token 		= $this->input->post('token');
	    $hash 		= $this->input->post('hash');


        if($dados['valor_corrida'] >= 1) { //Pagseguro não gera cobrança menor a 1 real.

			######################################################	
			//Cartão de Crédito
			######################################################	
			$holder = array (
	            'name' 		=> 	$dados['nome_cartao'],
				'birthDate' => 	$dados['data_nascimento'],
				'documents' => 	array("document" 	=> 
									array(	
										"type" 		=> "CPF", 
										"value" 	=> $dados['cpf_usuario']
									)
								),
				'phone' => array("areaCode" => $dados['ddd'],"number" => $dados['celular'])
			);

			$billingAddress = array(

				'street' 		=> $dados['logradouro_usuario'],
				'number' 		=> $dados['num_residencia_usuario'],
				'complement' 	=> $dados['complemento_usuario'],
				'district' 		=> $dados['bairro_usuario'],
				'city' 			=> $dados['cidade_usuario'],
				'state' 		=> $dados['uf'],
				'country' 		=> "BRA",
				'postalCode' 	=> $dados['cep_usuario']

			);

			$installment = array( //Opção de parcelamento.

				'quantity' 						=> 1,
				'value' 						=> $dados['valor_corrida']//,
				//'noInterestInstallmentQuantity' => 1 //Quantidade de parcelas sem juros

			);

			######################################################	
			//Cartão de Crédito
			######################################################	
			$creditCard = array(

				'token' 			=> 	$token,
				'holder' 			=>  $holder,
				'billingAddress' 	=>	$billingAddress,
				'installment' 		=> 	$installment

			);

			######################################################	
			//Dados comprador
			######################################################	
			$sender = array(
				'name' 		=>  $dados['nome_usuario'],
				'email' 	=>  $dados['email_usuario'],
				'hash' 		=>  $hash,
				'phone' 	=>  array("areaCode" => $dados['ddd'], "number" => $dados['celular']),
				'documents' => 	array("document" => 
									array(	
										"type" => "CPF", 
										"value" => $dados['cpf_usuario']
									)
								),
			);

			######################################################	
			//Itens vendidos
			######################################################	
			if(isset($fk_cartao)) {
				$description = "Ativar conta Yougo ".date('d/m/Y H:i:s');
				$id_item = $fk_cartao;
			} else {
				$description = "Corrida Yougo ".date('d/m/Y H:i:s');
				$id_item = $fk_corrida;
			}

			$items = array(
				'item' => array(
					'id' 			=> $id_item,
					'description' 	=> $description,
					'quantity' 		=> 1,
					'amount' 		=> $dados['valor_corrida']
				)
			);

			######################################################	
			//Entrega
			######################################################	
			$shipping = array (
				'type' => 3,
				'cost' => 0.00,
				'address' => array (
					'street' 		=> $dados['logradouro_usuario'],
					'number' 		=> $dados['num_residencia_usuario'],
					'complement' 	=> $dados['complemento_usuario'],
					'district' 		=> $dados['bairro_usuario'],
					'city' 			=> $dados['cidade_usuario'],
					'state' 		=> $dados['uf'],
					'country' 		=> "BRA",
					'postalCode' 	=> $dados['cep_usuario']



				)
			);

			######################################################	
			//Gerar XML
			######################################################	
			$server = $_SERVER['SERVER_NAME'];

			if(isset($fk_cartao)) {
				$reference = "PAGAMENTO_{$fk_cartao}";
				$notificationURL = $server.base_url()."controller_pagamento/notificacao_ativacao";
			} else {
				$reference = "CORRIDA_{$fk_corrida}";
				$notificationURL = $server.base_url()."controller_pagamento/notificacao";				
			}

			
			$payment = array (
				'mode' 				=> "default", 		//Modo do pagamento
				'method' 			=> "creditCard", 	//Meio de pagamento, aceita creditCard, boleto ou eft.
				'currency' 			=> "BRL",
				'notificationURL' 	=> $notificationURL,
				'extraAmount' 		=> "0.00",			//Valores extras a serem cobrados
				'reference' 		=> $reference,		//Refênciar o pagamento do nosso lado para o PagSeguro

				'shipping' 			=> $shipping,
				'creditCard' 		=> $creditCard,
				'items'				=> $items,
				'sender'			=> $sender
			);
			
			$xml_user_info = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><payment></payment>");
			$this->array_to_xml($payment,$xml_user_info);
			$xml_pagamento = $xml_user_info->asXML();	

			echo $xml_pagamento;

			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => $this->url."/v2/transactions".$this->autenticacao,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => $xml_pagamento,
			  CURLOPT_HTTPHEADER => array(
			    "Cache-Control: no-cache",
			    "Content-Type: application/xml"
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			  echo "cURL Error #:" . $err;
			} else {

			   $objeto = simplexml_load_string($response);
			    // echo '<pre> <code>';
			    // 	var_dump($objeto);
			    // echo '</code> </pre>';

			   $pagamento = array(
			   	'token_pagamento' => $token,
			   	'hash_pagamento' => $hash,
			   	'code_pag_seguro' => $objeto->code,
			   	'status_pag_seguro' => $objeto->status
			   	);
			   	
			   	if(isset($fk_cartao)) {
			   		$pag_atualizado = $this->model_pagamento->atualizarPagamentoAtivacao($pagamento,$fk_cartao);
			   	} else {
			   		$pag_atualizado = $this->model_pagamento->atualizarPagamento($pagamento,$fk_corrida);
			   	}
			   //echo $pag_atualizado;
			   echo "initialDate=".date('Y-m-d').'T'.date('H:i');

			}

		} else { // Fim IF que valida se o valor da corrida é maior que 1
			echo "Corrida com valor menor que 0";
			//var_dump($dados);
		}

	}

	function array_to_xml($array, &$xml_user_info) {
	    foreach($array as $key => $value) {
	        if(is_array($value)) {
	            if(!is_numeric($key)) {
	                $subnode = $xml_user_info->addChild("$key");
	                $this->array_to_xml($value, $subnode);
	            } else {
	                $subnode = $xml_user_info->addChild("item$key");
	                $this->array_to_xml($value, $subnode);
	            }
	        } else {
	            $xml_user_info->addChild("$key",htmlspecialchars("$value"));
	        }
	    }
	}













	public function gerando_token(){

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.sandbox.paypal.com/v1/oauth2/token",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "grant_type=client_credentials",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Basic QVVnTzA5YnYzTGNtWmZUbldEbkhZTnlvaC1uX2UwcE9tN184a01xVEcxWVc1VDF5empxNXptUkFKdmRoWUVSUEpRVlVTbkcwTnBNNm5sODc6RUFISkNjRXB4aVdmb3h2UlNfdTVfMEMtYmEwN21KVXo2bmZrNnQ3LTgtVXV0RzlXTGhPOXJrTG9SYkpJSDRJcDFPUjg3aVVid0NBVFN3cnY=",
		    "Cache-Control: no-cache",
		    "Content-Type: application/x-www-form-urlencoded",
		    "Postman-Token: f5468dd7-47f7-4927-9d6e-925cfa452ded"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $response;
		}

	}

	public function listar_saldo(){

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.sandbox.paypal.com/v2/wallet/balance-accounts",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Cache-Control: no-cache",
		    "Postman-Token: ed7e2fb7-7df9-4877-b298-7b2eea12a16d"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $response;
		}
		
	}

	public function criar_cobranca(){

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.sandbox.paypal.com/v1/payments/payment",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "{\n  \"intent\": \"sale\",\n  \"payer\": {\n  \"payment_method\": \"paypal\"\n  },\n  \"transactions\": [\n  {\n    \"amount\": {\n    \"total\": \"30.11\",\n    \"currency\": \"BRL\",\n    \"details\": {\n      \"subtotal\": \"30.00\",\n      \"tax\": \"0.07\",\n      \"shipping\": \"0.03\",\n      \"handling_fee\": \"1.00\",\n      \"shipping_discount\": \"-1.00\",\n      \"insurance\": \"0.01\"\n    }\n    },\n    \"description\": \"Descrição aqui\",\n    \"custom\": \"EBAY_EMS_90048630024435\",\n    \"invoice_number\": \"48787589673\",\n    \"payment_options\": {\n      \"allowed_payment_method\": \"INSTANT_FUNDING_SOURCE\"\n    },\n    \"soft_descriptor\": \"ECHI5786786\",\n    \"item_list\": {\n    \"items\": [\n      {\n      \"name\": \"Item 1\",\n      \"description\": \"Detalhes item 1.\",\n      \"quantity\": \"5\",\n      \"price\": \"3\",\n      \"tax\": \"0.01\",\n      \"sku\": \"1\",\n      \"currency\": \"BRL\"\n      },\n      {\n      \"name\": \"Item 2\",\n      \"description\": \"Detalhes item 2.\",\n      \"quantity\": \"1\",\n      \"price\": \"15\",\n      \"tax\": \"0.02\",\n      \"sku\": \"product34\",\n      \"currency\": \"BRL\"\n      }\n    ],\n    \"shipping_address\": {\n      \"recipient_name\": \"Cliente\",\n      \"line1\": \"4 andar\",\n      \"line2\": \"Unit #34\",\n      \"city\": \"San Jose\",\n      \"country_code\": \"US\",\n      \"postal_code\": \"95131\",\n      \"phone\": \"011862212345678\",\n      \"state\": \"CA\"\n    }\n    }\n  }\n  ],\n  \"note_to_payer\": \"Entrar em contato por:.\",\n  \"redirect_urls\": {\n  \"return_url\": \"https://example.com/return\",\n  \"cancel_url\": \"https://example.com/cancel\"\n  }\n}",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Bearer A21AAFT4JVou83YMTgwvZxgptI-h-p-nTGj71LSDUXIn40Vnf9RFHievO9DkmEKfe7hnDs9YPgSowhICx1DHHVRirL8psCXBw",
		    "Cache-Control: no-cache",
		    "Content-Type: application/json",
		    "Postman-Token: 40c8ee41-409b-4a1f-8e72-c6daa12483ec"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $response;
		}

	}

	public function listar_cobrancas(){

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.sandbox.paypal.com/v1/payments/payment",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Cache-Control: no-cache",
		    "Content-Type: application/json",
		    "Postman-Token: 15f628f9-edd7-4afc-8afe-4312ec13538c"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $response;
		}

	}

	public function notificacao(){

        $dados = array(
            'notificationType' => $this->input->post('notificationType'),
            'notificationCode' => preg_replace('/[^A-Za-z0-9]/','',$this->input->post('notificationCode'))
        );
        $code = $this->input->post('notificationCode');
        $this->model_pagamento->atualizaNotificacao($dados);

        log_message('error','notificationCode '.$code);

        $this->consulta_transaction($code);


	}

	public function consulta_transaction($code) {

		$url = $this->url."/v2/transactions/notifications/{$code}".$this->autenticacao;
	 
	   	$curl = curl_init($url);
	   	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
	   	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	   	$xml_retorno = curl_exec($curl);
	   	curl_close($curl);

	   	$retorno = simplexml_load_string($xml_retorno);

	   	$assinatura = get_object_vars($retorno);

		$id_corrida = str_replace("CORRIDA_", "", $assinatura['reference']);
	   	$dados = array('status_pag_seguro' => $assinatura['status']);

	   	log_message('error','status_pag_seguro '.$dados['status_pag_seguro']);


	   	$this->model_pagamento->consultaNotificacao($dados, $id_corrida);


	}

	public function notificacao_ativacao(){

		 $dados = array(
            'notificationType' => $this->input->post('notificationType'),
            'notificationCode' => preg_replace('/[^A-Za-z0-9]/','',$this->input->post('notificationCode'))
        );
        $code = $this->input->post('notificationCode');
        $this->model_pagamento->atualizaNotificacao($dados);

        log_message('error','notificationCode '.$code);

        $this->consulta_transaction_ativacao($code);

	}

	public function consulta_transaction_ativacao($code) {

		$url = $this->url."/v2/transactions/notifications/{$code}".$this->autenticacao;
	 
	   	$curl = curl_init($url);
	   	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
	   	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	   	$xml_retorno = curl_exec($curl);
	   	curl_close($curl);

	   	$retorno = simplexml_load_string($xml_retorno);

	   	$assinatura = get_object_vars($retorno);

		$id_cartao = str_replace("PAGAMENTO_", "", $assinatura['reference']);
	   	$dados = array('status_pag_seguro' => $assinatura['status']);

	   	log_message('error','status_pag_seguro '.$dados['status_pag_seguro']);


	   	$this->model_pagamento->consultaNotificacaoAtivacao($dados, $id_cartao);


	}

}