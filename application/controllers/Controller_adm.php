<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_adm extends MY_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->model('model_adm');
    }

    public function data($data = null, $timestamp = null) {

        if (isset($data) && $data != "" && $timestamp) {

            return date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $data)));
        } else if (isset($data) && $data != "" && !$timestamp) {

            return date("Y-m-d", strtotime(str_replace('/', '-', $data)));
        } else {

            return 0;
        }
    }

    function termo(){
        $termo =  $this->input->post('termos');
        $this->model_adm->termo($termo);

        redirect('main/redirecionar/23');
    }
    function termo_app(){

        $termo['termo'] =$this->model_adm->termo_mostrar();
        $this->load->view('termo/termo_app',$termo);

    }
    function view_pagamento(){
        $dados['pagamento'] =$this->model_adm->pagamento();
        $this->load->view('pagamento/pagamento',$dados);
    }
    function cidade(){
        $cidade = $this->input->post('cidade');

        $cidade_cad = array(
          'nome_cidades_atuacao' => $cidade,
          'ativa_cidades_atuacao' => 0
        );
        $this->model_adm->cidade($cidade_cad);
        redirect('main/redirecionar/19');

    }

    function excel_corridas(){

        $dados_corrida = $this->model_adm->view_todas();

        $excel = '<table class="table table-bordered table-hover" align="center">
        <thead style="background: #f1f1f1">
        <tr>

            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">ID</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Motorista</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Passageiro</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">R$</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">% YouGo</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">R$ Motorista</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Km</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Partida</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Destino</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Data</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Status</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Justificativa</td>
            <td style="text-align: center;padding: 10px 3px 0 3px;border: none;margin-bottom: -20px;font-weight: bolder">Status PagSeguro</td>

        </tr>
        <tr>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th> </th>
            <th class="status_corrida"> </th>
            <th > </th>
            <th > </th>

        </tr>
        </thead>
        <tbody>';
    
    foreach ($dados_corrida['dados'] as $dados){ 

        $excel .='<tr>
            <td ><p style="margin-top: 15px;text-align: center">'.$dados['id_corrida'].'</p></td>
            <td ><p style="margin-top: 15px;text-align: center">'.$dados['motorista'].'</p></td>
            <td ><p style="margin-top: 15px;text-align: center">'.$dados['passageiro'].'</p></td>
            <td ><p class="mascara_monetaria" style="margin-top: 15px;text-align: center">';

            if($dados['fk_status_corrida'] == 87) {
                $excel .= 'R$ 0,00';
            } else {
                $excel .= $dados['valor_corrida'];
            } 

            $excel .=' </p></td>
            <td ><p style="margin-top: 15px;text-align: center">'.$dados['taxa_yougo'].'%</p></td>
            <td ><p class="mascara_monetaria" style="margin-top: 15px;text-align: center">'.$dados['valor_motorista'].'</p></td>
            <td ><p style="margin-top: 15px;text-align: center">'.$dados['km_corrida'].' Km &nbsp;</p></td>
            <td ><p style="margin-top: 15px;text-align: center">'.$dados['endereco_origem'].' </p></td>
            <td ><p style="margin-top: 15px;text-align: center">'.$dados['endereco_destino'].'</p></td>
            <td ><p style="margin-top: 15px;text-align: center">'.$dados['data_corrida'].'</p></td>';
            if ($dados['justificativa'] == "Tempo limite de procurar motorista.") { 
                $excel .= ' <td ><p style="margin-top: 15px;text-align: center">Sem Motorista</p></td>
                            <td ><p style="margin-top: 15px;text-align: center">Sem Motorista</p></td>';
            } else {
                $excel .= ' <td ><p style="margin-top: 15px;text-align: center">'.$dados['status'].'</p></td>
                            <td ><p style="margin-top: 15px;text-align: center">'.$dados['justificativa'].'</p></td>';
            } 
            $excel .= '<td ><p style="margin-top: 15px;text-align: center">';

                if(empty($dados['status_pagseguro'])){
                    $excel .= 'Aguardando';
                } else {
                    $excel .= $dados['status_pagseguro'];
                }

                echo '</p></td>
            </tr>';

        }
            $excel .= '</tbody>
        </table>';

            $arquivo = 'excel_corridas_'.date('d-m-Y-H:i:s').'.xls';

            // Configurações header para forçar o download
            header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
            header ("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header ("Content-Transfer-Encoding: binary");
            header ("Content-Type: application/vnd.ms-excel");
            header ("Expires: 0");
            header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
            header ("Content-Description: PHP Generated Data");

            // Envia o conteúdo do arquivo
            chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $excel);
            echo $excel;
            exit;

    }

    function atulizar_cidade(){
        $cidade = $this->input->post('cidade');
        $status = $this->input->post('status');
        $id = $this->input->post('id');

        if($status == 1){
            $status = 0;
        }
        else{
            $status = 1;
        }
        $cidade_cad = array(
            'nome_cidades_atuacao' => $cidade,
            'ativa_cidades_atuacao' => $status
        );
        $this->model_adm->cidade_atulizar($cidade_cad,$id);
        redirect('main/redirecionar/19');
    }

    function atulizar_pagamento_motorista(){
        $id = $this->input->get('id');
        $tipo = $this->input->get('tipo');
        $this->model_adm->atulizar_pagamento_motorista($id,$tipo);
        redirect('main/redirecionar/32');
    }
    function corrida_ver(){
        $id = $this->input->get('id');
        $dados = $this->model_adm->achar_dados_corrida($id);

        if(!empty($dados['dados'][0]['data_corrida'])){
        $data_corrigir = explode('-',$dados['dados'][0]['data_corrida']);
        $corrige_data_p2 = explode(' ',$data_corrigir[2]);
         $hora = explode(':',$corrige_data_p2[1]);

        $data = $corrige_data_p2[0].'/'.$data_corrigir[1].'/'.$data_corrigir[0];
        $hora = $hora[0].':'.$hora[2];


        if(!empty($dados['dados'][0]['data_fim_corrida'])){
            $data_corrigir_fim = explode('-',$dados['dados'][0]['data_fim_corrida']);
        }
        else{
            $data_corrigir_fim = '';
        }


        if(!empty($data_corrigir_fim)){
              $corrige_data_p2_fim = explode(' ',$data_corrigir_fim[2]);
              $hora_fim = explode(':',$corrige_data_p2_fim[1]);
                $data_fim = $corrige_data_p2_fim[0].'/'.$data_corrigir_fim[1].'/'.$data_corrigir_fim[0];
                $hora_fim = $hora_fim[0].':'.$hora_fim[2];
        }else{
              $data_fim = null;
              $hora_fim = null;
        }
      

      
        }


        echo'
        <head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
    
    
            #svg_linha{
                width: 100%;
            }
            @media screen and (max-width: 768px) and (min-width: 500px){
                #svg_linha{
                    width: 80%;
                    margin: 0 auto;
                    margin-left: 10%;
                    margin-top: -20px;
    
                }
            }
            @media screen and (max-width: 500px){
                #svg_linha{
                    width: 70%;
                    margin: 0 auto;
                    margin-left: 15%;
                    margin-top: -60px;
    
                }
            }
            @media screen and (max-width: 450px){
                #svg_linha{
                    width: 70%;
                    margin: 0 auto;
                    margin-left: 15%;
                    margin-top: -50px;
    
                }
            }
            @media screen and (max-width: 380px){
                #svg_linha{
                    width: 80%;
                    margin: 0 auto;
                    margin-left: 15%;
                    margin-top: -50px;
    
                }
            }
            svg path, svg rect {
                fill: transparent !important;
            }
            .logo {
                stroke-dasharray: 560;
                stroke-dashoffset: 560;
    
                animation:
                        logoStroke 2.75s linear forwards,
                        logoStrokeFill 0.75s cubic-bezier(0.445, 0.050, 0.550, 0.950) 2s forwards;
            }
    
            @keyframes logoStroke {
                to {
                    stroke-dashoffset: 0;
                }
            }
    
            @keyframes logoStrokeFill {
                to {
                    stroke:  transparent;
                    fill:  #fff;
                }
            }
    
        </style>
    
    </head>
    <body class="parallax-demo"><header>
    
    
        <main>
            <!--  Parallax Section  -->
            <div class="parallax-container">
                <div class="parallax">
    
                    <div style="position: absolute;color: #000;z-index: 999;font-size: 30px;margin-top: -5px;left: 50%;margin-left: -170px;width: 320px;fill: #fff;">
                        <div class="center-block" style="margin: 0 auto;width: 300px;z-index: 999">
                            <svg id="svg_linha" class="center-block" height="380" viewBox="0 0 1000 1000" style="padding: 15px" x="0" y="0" enable-background="new 0 0 147 147" xml:space="preserve">
                                 <path  class="logo"  fill="#ffffff" stroke="#fff"  stroke-width="3" stroke-miterlimit="10"  d="M439.5 66.6c-54.7.9-103.5 3.3-150 7.4l-23 2-4.3 4.7c-13.5 15-40.6 58-57.4 91-11.7 22.8-11.9 23.3-10.4 26.1.8 1.6 2.3 3.2 3.2 3.6.9.3 21.5.2 45.8-.4 24.2-.5 57.2-1.2 73.1-1.5 16-.2 47.9-.9 71-1.5 54.3-1.3 155.2-1.3 210.5 0 23.9.6 56.3 1.3 72 1.5 15.7.3 48.5 1 73 1.6l44.5 1 2.3-2.7c2.9-3.3 2.8-3.8-4.2-18.4-8-16.6-17.6-34.1-28.4-51.5-14-22.5-36-53.5-38-53.5-.4 0-10.6-.9-22.7-2-30.1-2.6-64.9-4.8-96.5-6-28.1-1-130.3-1.9-160.5-1.4zM121.6 148.6c-13 2-25.5 5.3-30.2 8-5.9 3.3-7.2 8.4-5 18.6 2.4 10.4 5 15.3 10.2 18.9 7.9 5.4 13.9 7.4 28.4 9.3 11.9 1.7 17.8 6.2 24.6 19.1 1 1.7 2.3 2 10.3 2.3l9.1.3-.1-20.8c0-26.7-1.3-49.3-2.8-52.2-2.2-4.1-6.8-5.1-21.8-5-7.6.1-17.8.7-22.7 1.5zM825.2 148.4c-6.1 2.8-6.4 5-7 42.8l-.5 33.8h18.2l3.2-6.1c5.2-9.7 11.7-14.1 22.7-15.5 13.5-1.6 24.9-5.8 30.6-11.1 3.8-3.5 6.3-9.1 7.7-16.7 1.4-8.5.7-14.1-2.2-17.2-2.7-2.8-13.9-6.5-27.4-9-12.8-2.3-41-2.9-45.3-1zM218 212.5c0 .9 20.1 31.6 29.3 44.8 8.4 12 16.7 19.7 21.4 19.7 3.2 0 2.9-1.9-1.6-8.6-12.9-19.3-49.1-60.5-49.1-55.9zM749.3 215c-6.4 5.8-17 17.9-30.5 34.9-12.8 16-17.8 23.4-17.8 26.1 0 2 4.8.9 8.8-2 5.6-4 11-11 25.9-33.5 6.9-10.5 13.4-20.1 14.3-21.5 4.9-7.2 4.6-8.9-.7-4zM862 292.9c-30 11.4-61.4 26.1-79.9 37.6-32.4 20-80.8 64.9-87.8 81.3-1.9 4.5-.9 5.5 4.3 4.7 7.5-1.1 36.3-11.7 56.5-20.8 32.8-14.8 47.7-21.6 57.2-26.1 48.8-23.2 63-30.7 65.6-35 4.5-7.3 7.2-26 5.1-35.9-1.2-6.1-4.2-10.8-6.8-10.6-.9.1-7.3 2.2-14.2 4.8zM107.6 293c-3.3 3.9-3.9 7.8-3.4 20.3.3 9 .8 11 3.6 17l3.3 6.7 31.7 15.6c17.4 8.5 32.5 15.9 33.4 16.4.9.5 6.5 3.1 12.5 5.8 5.9 2.8 14 6.4 17.8 8.2 12.2 5.6 14.7 6.7 28 12.5 22.4 9.9 48.4 19.5 52.6 19.5 5.5 0 2.2-7.1-10-21.3-19.2-22.2-54.7-51.9-79.1-65.9-20.8-12-79.7-37.8-86.1-37.8-.9 0-2.9 1.4-4.3 3zM291 363.9c0 .6.4 1.3 1 1.6 3.7 2.3 24.2 32.3 30.1 44.1 5.2 10.3 6 15.6 3 18.8-1.3 1.3-1.9 2.6-1.4 2.9.4.2 7.3 2.1 15.3 4.1 8 2.1 19.5 5.1 25.5 6.7 34.9 9.2 83.7 18.2 113.5 20.9 31.6 2.9 84.4-5.2 159.4-24.6 30.7-7.9 28.7-7.2 25.9-9.1-6.7-4.5 1.1-22.5 21.9-50.3 5.1-6.9 9.9-13.4 10.7-14.4 1.8-2.5 2.3-2.6-19.9 5.9-29.1 11.2-61.7 21.3-88.5 27.5-4.9 1.1-9.9 2.3-11 2.6-1.1.2-6.9 1.3-13 2.4-6 1.1-13 2.4-15.5 2.9-9.6 2-37.7 4.2-54.2 4.1-16.5 0-41.2-1.9-52.2-4.1-2.8-.6-6-1.2-7.1-1.4-31-5.2-82.9-19.9-128.6-36.6-14.6-5.3-14.9-5.4-14.9-4zM246.2 574.7l.3 5.8h513l.3-5.8.3-5.7H245.9l.3 5.7zM37.1 648.9c-1 .6 1.8 4.4 11.1 14.7 16.1 18 59.3 66.8 67.7 76.4 3.6 4.1 6.8 7.7 7.1 8 .3.3 2.2 2.4 4.3 4.8l3.7 4.2v59h53v-29.4c0-28.7 0-29.4 2.2-32.2 1.2-1.6 2.5-3.1 2.8-3.4.3-.3 2.7-3.2 5.5-6.5 2.7-3.3 5.9-7.1 7.2-8.5 18.3-20.7 72.9-86.6 72.2-87.1-.6-.3-15.3-.7-32.8-.8l-31.8-.2-15.4 19.8c-8.5 10.9-20.1 26-25.9 33.5-5.8 7.6-10.8 13.7-11.1 13.8-.5 0-24.3-28.9-37.4-45.4-1.6-2.1-3.7-4.7-4.7-5.8-.9-1-3.1-3.8-5-6-1.8-2.3-4.3-5.3-5.5-6.7l-2.1-2.6-31.9-.2c-17.5-.2-32.4.1-33.2.6zM656 648.7c-14.4 1-28.3 2.7-34.9 4.4-19.5 5-26.7 16.1-29.8 45.9-1 9.9-1 58.3 0 68.5 2.4 23 9.1 36.3 21.2 41.6 6.4 2.8 20.3 5.6 30.3 6 15.1.7 98.4.4 105.7-.3 27.2-2.8 39.2-11.5 43.4-31.8 1.1-5.5 3.1-34.1 3.1-45.8V731H685v20H772.3l-.7 10.8c-2 30.9-4.9 32.6-57.6 33.8-29.9.7-73.4-.9-82.4-3-12-2.8-15.2-7.9-17.3-27.1-.9-8.4-1-62.3-.1-68 3-19.7 6.5-23.5 23.8-26.6 10.4-1.9 96.6-2 109.5-.1 16.9 2.5 21 6.5 22.2 22.1l.6 8.2 11.5-.3 11.6-.3-.2-6.5c-.6-15.3-4-23.8-13-32.1-9.7-9-17.9-11.2-49.2-12.8-14.9-.8-65.7-1.1-75-.4z"/>
                                <path class="logo" fill="#ffffff" stroke="#fff" stroke-width="3" stroke-miterlimit="10"   d="M277.1 695.1c-5.1.4-10.9 1.1-13 1.5-19.2 4-28.6 10.9-34.2 25.1-2.1 5.3-2.3 7.8-2.7 28.7-.6 33.8 1.6 45 10.5 54 11.6 11.8 24.4 14.3 71.8 14.4 47.7.2 63.4-2.7 73.7-13.5 8.8-9.2 11.6-22.3 11.3-51.3-.4-25.4-2-33.6-8.2-42.2-6.7-9.3-20.8-15-41.3-16.7-13.1-1.2-53.8-1.2-67.9 0zm59.5 35.3c7.8 2.4 11 6.3 12.5 15 1.5 9.1.6 23.3-2 29-3.6 8.3-12.3 10.8-37.1 10.8-24.5-.1-34.6-3.5-37.1-12.5-1.5-5.4-1.5-25 0-30.4 1.6-5.9 5.3-9.9 10.9-11.7 7.1-2.3 9.2-2.4 29.2-2.1 13.7.2 19.8.7 23.6 1.9zM410.4 698.5c-1.1 2.7.3 76.6 1.5 83.4a43.8 43.8 0 0 0 11.7 22.4c10.4 10.5 25.1 14.6 52.1 14.5 28.3-.1 39.7-3.8 48.6-15.6l4.2-5.6.3 9.2.3 9.2H571l-.2-59.3-.3-59.2-21.6-.3-21.6-.2-.6 33.2c-.4 18.3-1.1 35.1-1.6 37.2-1.3 5.4-5.9 11.5-10.1 13.7-5.4 2.8-13.3 4-26.2 4-14.9.1-21.9-1.3-26.4-5.3-6.9-6.1-6.8-5.4-7.4-45.8l-.5-36.5-21.8-.3c-19-.2-21.8 0-22.3 1.3zM860.2 699c-20 1.7-29 4.9-36.6 13-6.2 6.7-9.2 13.1-11.5 25.1-3.7 19.3-1.7 44.6 4.5 57.1 6.4 12.7 17.9 19.5 36.9 21.7 11.5 1.4 57.5 1.4 68.5.1 29.4-3.6 41.5-20.6 41.5-58-.1-22.7-4.3-36.8-13.7-46.3-6.1-6.1-13.4-9.5-24.6-11.3-10.6-1.7-50.8-2.6-65-1.4zm33.1 16.1c34.9.3 44.8 5.2 48.7 24.4 1.4 6.5 1.7 26.4.6 33.5-1.2 7.7-3.7 14-6.9 17.7-7 8-17.3 10.2-47.4 10.1-45.5-.1-55.2-5.4-57.3-31.7-1.3-16.2.1-31.9 3.6-38.5 3.6-7 7.7-10.3 15.4-12.6 5.6-1.7 23.4-4.1 25.1-3.4.3.2 8.5.4 18.2.5zM223 871.2c-.1 1 0 48.1 0 51.1 0 .5 1.5.7 3.3.5l3.2-.3.3-22.8c.2-21.4.3-22.7 2.1-22.7 1 0 2.1.6 2.4 1.4.3.8 3 5.2 6 9.8 3 4.5 9.3 14.3 14 21.5 7.8 12 8.9 13.3 11.5 13.3 2.7 0 3.7-1.2 10.8-12.3 4.3-6.7 10.7-16.7 14.2-22.2 6.5-10.2 8.8-12.8 10.3-11.9.5.3.9 10.9.9 23.5v23l3.3-.3 3.2-.3v-52l-7.2-.3c-7.2-.3-7.3-.3-9.2 2.8-10.9 18-25.8 40.9-26.4 40.7-.4-.1-7-9.9-14.6-21.7l-13.8-21.5-7.2-.3c-5.3-.2-7.1 0-7.1 1zM359 871c-4.2 1-7.5 4.7-8.9 9.8-.6 2-1.1 9.4-1.1 16.4 0 14.6 1.3 19.5 6.4 23.1 3 2.1 4.1 2.2 24.9 2.4 34.1.4 35.2-.4 35.2-26.2 0-13-.3-15.5-2-19-3.3-6.5-5.1-7-29.5-7.2-11.8-.1-23.1.2-25 .7zm45.2 6.8c1.4.6 2.9 1.6 3.3 2.3.4.6.7 7.6.7 15.6-.1 17.1-.8 18.8-7.4 19.8-2.4.4-12.3.5-22.1.3-17-.3-18-.4-19.7-2.5-3.7-4.6-3.7-29 0-33.6 1.3-1.6 3.4-2.3 7.6-2.8 8.2-1 34.3-.3 37.6.9zM456 896.5v26.6l25.1-.1c33.5-.2 35.9-1.2 35.9-15 0-6.9-1.3-10.4-4.6-11.9l-2.4-1 2.5-2.7c2.1-2.3 2.5-3.7 2.5-8.8 0-6.9-1.4-9.7-6-12.1-2.3-1.2-7.7-1.5-28-1.5h-25v26.5zm50.1-18.8c.6.6 1.4 3.1 1.6 5.6.4 3.6.1 4.9-1.6 6.6-2 2-3.1 2.1-22.6 2.1H463v-16.2l21 .4c14.1.2 21.3.7 22.1 1.5zm-.6 22.3c3.2 1.2 4.8 5.3 4 10.2-1 5.9-2.3 6.2-25.5 6.3h-20.5l-.3-8.8-.3-8.7h20c11 0 21.1.4 22.6 1zM558 896.5v26.6l3.3-.3 3.2-.3v-52l-3.2-.3-3.3-.3v26.6zM606 896.4V923h50v-3c0-1.7-.5-3-1.2-3s-10.2-.2-21.1-.3l-19.9-.2-.1-23-.2-23-3.7-.3-3.8-.3v26.5zM693 896.5V923h52v-6l-22.2-.2-22.3-.3-.3-8.7-.3-8.8 21.3-.2 21.3-.3.3-3.3.3-3.2H700v-16h45v-6h-52v26.5z"/>
                             </svg>
                        </div>
                    </div>
                    <img src="'.base_url().'style/img/bk_tudo.jpg"></div>
            </div>
            <div class="section white" style="width: 100%;background: #fff;padding: 10px 0 0 0 !important;box-shadow: 0px -24px  360px 80px #000;margin-top: -110px;height: 1100px;">
                <div class="row container" style="width: 80%;margin: 0 auto;">
                    <p  style="text-align: center;font-family: Arial;color: #666;font-size: 25px">Descrição da corrida.</p>

                    <p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Motorista:</span> '.$dados['dados'][0]['motorista'].'.</p>

                    <p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Passageiro:</span> '.$dados['dados'][0]['passageiro'].'.</p>
                    <p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Valor original da corrida: </span>'.$dados['dados'][0]['valor_corrida'].'.</p>';


                    if($dados['dados'][0]['fk_status_corrida'] == 87){
                        echo '<p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Valor final da corrida: </span>R$ 0,00.</p>';
                    } else {
                        echo '<p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Valor final da corrida: </span>R$ '.$dados['dados'][0]['valor_corrida_final'].'.</p>';
                    }

                   if(isset($dados['dados'][0]['tipo_desconto_cupom']) && $dados['dados'][0]['tipo_desconto_cupom'] == 0){ 
                        
                        echo ' <p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Valor desconto do cupom: </span>';
                        echo 'R$'.$dados['dados'][0]['valor_desconto_cupom'].'</p>
                        <p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Uso do cupom: </span>'.$dados['dados'][0]['codigo_cupom'].'.</p>';

                    } else if(isset($dados['dados'][0]['tipo_desconto_cupom'])) {
                        
                        echo ' <p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Valor desconto do cupom: </span>';
                        echo $dados['dados'][0]['valor_desconto_cupom'].'%</p>
                        <p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Uso do cupom: </span>'.$dados['dados'][0]['codigo_cupom'].'.</p>';

                    }
                        echo '<p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Km:</span> '.$dados['dados'][0]['km_corrida'].' Km.</p>
                    <p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Endereço de origem:</span> '.$dados['dados'][0]['endereco_origem'].'.</p>
                    <p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Destino: </span>'.$dados['dados'][0]['endereco_destino'].'.</p>
                    <p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Status atual:</span> '.$dados['dados'][0]['status'].'.</p>
                    <p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Quantidade de malas:</span> '.$dados['dados'][0]['qtd_malas'].'.</p>
                    <p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Quantidade de passageiros:</span> '.$dados['dados'][0]['qtd_passageiros'].'.</p>
                    <p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Data da corrida:</span> '.$data.'.</p>
                    <p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Hora da corrida:</span> '.$hora.'.</p>
                    <p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Data final da corrida:</span> '.$data_fim.'.</p>
                    <p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Hora final da corrida:</span> '.$hora_fim.'.</p>
                    <p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Taxa YouGo:</span> '.$dados['dados'][0]['taxa_yougo'].'%.</p>
                    <p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Taxa de cancelamento:</span> R$ '.$dados['dados'][0]['taxa_cancelamento'].'.</p>
                    ';
                   if(isset($dados['dados'][0]['justificativa'])){

                       echo  '  <p style="margin-top: -15px"><span style="font-weight: bolder;font-size: 16px">Justificativa:</span> '.$dados['dados'][0]['justificativa'].'.</p>';
                   }
                   echo '
                  
                  <div align="center">
                     <iframe src="'.base_url().'controller_webservice/comprovante_corrida?fk_corrida='.$id.'&usuario=1" style="width:100%;height:500px;">
                  </div>
                    
                </div>
    
            </div>

    
            <div style="z-index: 999;position: relative;margin-top: -30px;height: 60px;background: #fff">&nbsp;</div>
            <div class="parallax-container" style="height: 0px">
                <!--
                <div class="parallax"><img src="parallax2.png"></div>
                -->
            </div>
    
            <!--  Scripts-->
            <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
            <script>if (!window.jQuery) { document.write(\'<script src="bin/jquery-3.2.1.min.js"><\/script>\'); }
            </script>
    
            <script src="'.base_url().'style/estilo_parallax/css/js/materi.js"></script>
            <script src="'.base_url().'style/estilo_parallax/css/js/init.js"></script>
            <!-- Twitter Button -->
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\'://platform.twitter.com/widgets.js\';fjs.parentNode.insertBefore(js,fjs);}}(document, \'script\', \'twitter-wjs\');</script>
    
            <!-- Google Plus Button-->
            <script src="https://apis.google.com/js/platform.js" async defer></script>
    
            <!--  Google Analytics  -->
            <script>
                (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');
    
                ga(\'create\', \'UA-56218128-1\', \'auto\');
                ga(\'require\', \'displayfeatures\');
                ga(\'send\', \'pageview\');
            </script>

        </body>
        ';
    }




    function ativar_passageiro(){
        $id = $this->input->get('id');      
        $email = $this->model_adm->ativar_passageiro($id);

        $server             = $_SERVER['SERVER_NAME'];
        $this->email->from('yougomobile@yougomobile.com.br', 'You GO | Perfil ativado - Parabéns você foi aprovado');
        $this->email->to($email);
        $this->email->subject('You GO | Perfil ativado - Parabéns você foi aprovado');
        $this->email->message('
        <body style="padding: 0;margin: 0"><div style="text-align: center;width: 100%;background: #f8d509;height: 200px;margin: 0;padding: 0">
                </div>
                    <div style="width: 80%;display: table;background: #fff;margin: 0 auto;text-align: center;margin-top: -100px;border-radius: 8px;box-shadow: 0 0 2px #ccc;font-family: Arial; ">
                    <center><img src="http://'.$server.base_url().'/style/img/favicon.png" style="width: 200px;"></center>
                        <h4 style="padding: 10px">Parabéns você foi ativado, já pode começar a usar seu aplicativo.</h4>

                </div>
                <br>
                <br>
                </body>
        ');

        // Enviar...
        if ($this->email->send()) {

            redirect('/main/redirecionar/27');

        } else {
            redirect('/main/redirecionar/27');
        }





        redirect('main/redirecionar/27');       
    }
    function desativar_passageiro(){
        
        $id = $this->input->post('id');
        $motivo = $this->input->post('motivo');
        $bloquear = $this->input->post('bloquear');
        $email = $this->model_adm->desativar_passageiro($id,$motivo,$bloquear);
        if(empty($motivo)){
            $motivo = 'Não especificado';
        }

        $server = $_SERVER['SERVER_NAME'];

        // TODO: Envio de E-mail, avisando que foi desativado
        $this->email->from('yougomobile@yougomobile.com.br', 'You GO | Perfil Desativado');
        $this->email->to($email);
        $this->email->subject('You GO | Perfil Desativado ');
        $this->email->message('
        <body style="padding: 0;margin: 0"><div style="text-align: center;width: 100%;background: #f8d509;height: 200px;margin: 0;padding: 0">
                </div>
                    <div style="width: 80%;display: table;background: #fff;margin: 0 auto;text-align: center;margin-top: -100px;border-radius: 8px;box-shadow: 0 0 2px #ccc;font-family: Arial; ">
                    <center><img src="http://'.$server.base_url().'/style/img/favicon.png" style="width: 200px;"></center>
                    <h4 style="padding: 10px">Você foi desativado pela equipe de gerenciamento do YouGo</h4>
                    <h4 style="padding: 10px">Motivo: '.$motivo.'</h4>
                     
            

                </div>
                <br>
                <br>
                </body>
        ');

        // Enviar...
        if ($this->email->send()) {

            redirect('/main/redirecionar/27');

        } else {
            redirect('/main/redirecionar/27');
        }

        redirect('main/redirecionar/27');       
    }


    public function excel_motorista_relatorio(){


        $dados = $this->model_adm->view_motorista_relatorio();



        $excel =  '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />';
        $excel .= '<table class="table table-bordered table-hover" style="border: 1px solid #000;padding: 10px;" align="center">
                <thead style="background: #f1f1f1">
                <tr >
                    <th  style="background: #f8d509 !important;">ID</th>
                    <th  style="background: #f8d509 !important;">Nome</th>
                    <th  style="background: #f8d509 !important;">E-mail</th>
                    <th  style="background: #f8d509 !important;">Telefone</th>
                    <th  style="background: #f8d509 !important;">CPF</th>
                    <th  style="background: #f8d509 !important;">Status</th>
                    <th  style="background: #f8d509 !important;">Total de corridas</th>
                    <th  style="background: #f8d509 !important;">Corridas canceladas</th>
                    <th  style="background: #f8d509 !important;">Custo médio de corridas</th>
                    <th  style="background: #f8d509 !important;">Média de avaliação</th>
                    <th  style="background: #f8d509 !important;">Cidade mais chamada</th>

                </tr>
                </thead>
                <tbody>';

                foreach($dados as $mot) {

                         if($mot['ativo_usuario'] == 1){
                            $ativo = "<span style=\"color: #7da957\">Ativado</span>";
                         }else{
                            $ativo = "<span style=\"color: #ff0000\">Inativo</span>";
                         }


                    $excel .= '<tr>';
                        $excel .= '<th style="text-align: center">'.$mot['id_usuario'].'</th>';
                        $excel .= '<th style="text-align: center">'.$mot['nome_usuario'].'</th>';
                        $excel .= '<th style="text-align: center">'.$mot['email_usuario'].'</th>';
                        $excel .= '<td class="mascara_cel" style="text-align: center;padding-top: 20px;white-space: nowrap">'.$mot['celular_usuario'].'</td>';
                        $excel .= '<td class="mascara_cpf" style="text-align: center;padding-top: 20px;white-space: nowrap">'.$mot['cpf_usuario'].'</td>';
                        $excel .= '<th style="text-align: center"><p style="margin-top: 13px;text-align: center">'.$ativo.'</p></th>';
                        $excel .= '<th style="text-align: center">'.$mot['total_corridas'].'</th>';
                        $excel .= '<th style="text-align: center">'.$mot['total_corridas_canceladas'].'</th>';
                        $excel .= '<th style="text-align: center">R$ '.$mot['custo_medio_corridas'].'</th>';
                        $excel .= '<th style="text-align: center">'.$mot['media_avaliacoes'].'</th>';
                        $excel .= '<th style="text-align: center">'.$mot['cidade_mais_chamada'].'</th>';
                    $excel .= '</tr>';

                }

                $excel .= '</tbody>
            </table>';

            $arquivo = 'excel_motoristas_'.date('d-m-Y-H:i:s').'.xls';

            // Configurações header para forçar o download
            header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
            header ("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header ("Content-Transfer-Encoding: binary");
            header ("Content-Type: application/vnd.ms-excel");
            header ("Expires: 0");
            header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
            header ("Content-Description: PHP Generated Data");

            // Envia o conteúdo do arquivo
            chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $excel);
            echo $excel;
            exit;


    }
    public function excel_passageiro_relatorio(){
        $dados = $this->model_adm->view_passageiro_relatorio();
         $excel =  '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />';

            $excel .=  '<table class="table table-bordered table-hover" style="border: 1px solid #000;padding: 10px;" align="center">
                <thead style="background: #f1f1f1">
                <tr>
                    <th style="background: #f8d509 !important;">ID</th>
                    <th style="background: #f8d509 !important;">Nome</th>
                    <th style="background: #f8d509 !important;">E-mail</th>
                    <th style="background: #f8d509 !important;">Telefone</th>
                    <th style="background: #f8d509 !important;">CPF</th>
                    <th style="background: #f8d509 !important;">Status</th>
                    <th style="background: #f8d509 !important;">Total de corridas</th>
                    <th style="background: #f8d509 !important;">Corridas canceladas</th>
                    <th style="background: #f8d509 !important;">Custo médio de corridas</th>
                    <th style="background: #f8d509 !important;">Média de avaliação</th>
                    <th style="background: #f8d509 !important;">Cidade mais chamada</th>
                </tr>
                </thead>
                <tbody>';

                foreach($dados as $pas) {


                    if($pas['ativo_usuario'] == 1){
                        $ativo = "<span style=\"color: #7da957\">Ativado</span>";
                    }else{
                        $ativo = "<span style=\"color: #ff0000\">Inativo</span>";
                    }


                    $excel .= '<tr>';
                    $excel .= '<th style="text-align: center">'.$pas['id_usuario'].'</th>';
                    $excel .= '<th style="text-align: center">'.$pas['nome_usuario'].'</th>';
                    $excel .= '<th style="text-align: center">'.$pas['email_usuario'].'</th>';
                    $excel .= '<td class="mascara_cel" style="text-align: center;padding-top: 20px;white-space: nowrap">'.$pas['celular_usuario'].'</td>';
                    $excel .= '<td class="mascara_cpf" style="text-align: center;padding-top: 20px;white-space: nowrap">'.$pas['cpf_usuario'].'</td>';
                    $excel .= '<th style="text-align: center"><p style="margin-top: 13px;text-align: center">'.$ativo.'</p></th>';
                    $excel .= '<th style="text-align: center">'.$pas['total_corridas'].'</th>';
                    $excel .= '<th style="text-align: center">'.$pas['total_corridas_canceladas'].'</th>';
                    $excel .= '<th style="text-align: center">R$ '.$pas['custo_medio_corridas'].'</th>';
                    $excel .= '<th style="text-align: center">'.$pas['media_avaliacoes'].'</th>';
                    $excel .= '<th style="text-align: center">'.$pas['cidade_mais_chamada'].'</th>';
                    $excel .= '</tr>';

                }


                $excel .= '</tbody>
            </table>';

            $arquivo = 'excel_passageiro_'.date('d-m-Y-H:i:s').'.xls';

            // Configurações header para forçar o download
            header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
            header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
            header ("Content-Transfer-Encoding: binary"); 
            header ("Content-Type: application/vnd.ms-excel"); 
            header ("Expires: 0"); 
            header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
            header ("Content-Description: PHP Generated Data");

            // Envia o conteúdo do arquivo
            chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $excel); 
            echo $excel;
            exit;
    }


    public function excel_calendario_relatorio(){

        $dados = $this->model_adm->view_calendario();

        $excel =  '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />';
        $excel .= '<table class="table table-bordered table-hover" style="border: 1px solid #000;padding: 10px;" align="center">
                    <thead style="background: #f1f1f1">
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Celular</th>
                        <th>CPF</th>
                        <th>Status</th>
                        <th>A receber de Hoje</th>
                        <th>A receber últimos 7 dias</th>
                        <th>A receber últimos 15 dias</th>
                        <th>A receber últimos 30 dias</th>
                        <th>Recebido nos últimos 30 dias</th>
                    </tr>
                    </thead>
                    <tbody>
                    ';
                    foreach($dados as $dado) {


                        if($dado['ativo_usuario'] == 1){
                            $ativo = "<span style=\"color: #7da957\">Ativado</span>";
                        }else{
                            $ativo = "<span style=\"color: #ff0000\">Inativo</span>";
                        }

                        $excel .= '<tr>';
                        $excel .= '<th style="text-align: center;padding-top: 20px;">'.$dado['id_usuario'].'</th>';
                        $excel .= '<th style="text-align: center;padding-top: 20px;">'.$dado['nome_usuario'].'</th>';
                        $excel .= '<th style="text-align: center;padding-top: 20px;">'.$dado['email_usuario'].'</th>';
                        $excel .= '<th class="mascara_cel" style="text-align: center;padding-top: 20px;">'.$dado['celular_usuario'].'</th>';
                        $excel .= '<th class="mascara_cpf" style="text-align: center;padding-top: 20px;">'.$dado['cpf_usuario'].'</th>';
                        $excel .= '<th style="text-align: center"><p style="margin-top: 13px;text-align: center">'.$ativo.'</p></th>';
                        $excel .= '<th style="text-align: center;padding-top: 20px;">'.$dado['dia'].'</th>';
                        $excel .= '<th style="text-align: center;padding-top: 20px;">'.$dado['semana'].'</th>';
                        $excel .= '<th style="text-align: center;padding-top: 20px;">'.$dado['quinzena'].'</th>';
                        $excel .= '<th style="text-align: center;padding-top: 20px;">'.$dado['mes'].'</th>';
                        $excel .= '<th style="text-align: center;padding-top: 20px;">'.$dado['recebido'].'</th>';
                        $excel .= '</tr>';

                     }
                echo '
                </tbody>
                </table>';




        $excel .= '</tbody>
            </table>';

        $arquivo = 'excel_calendario_'.date('d-m-Y-H:i:s').'.xls';

        // Configurações header para forçar o download
        header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header ("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header ("Content-Transfer-Encoding: binary");
        header ("Content-Type: application/vnd.ms-excel");
        header ("Expires: 0");
        header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
        header ("Content-Description: PHP Generated Data");

        // Envia o conteúdo do arquivo
        chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $excel);
        echo $excel;
        exit;
    }














    public function excel_cidades_relatorio(){

        $dados = $this->model_adm->cidades();

        $excel =  '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />';
        $excel .= '<table style="border: 1px solid #000;padding: 10px;" align="center">
                    <thead style="background: #f1f1f1">
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Corridas realizadas</th>
                        <th>Corridas canceladas</th>
                        <th>Valor médio</th>
                    </tr>
                    </thead>
                    <tbody>
                    ';
                    $cont = 0;
                    foreach($dados as $dado) {

                        $dado['relizadas'.$cont] =  $dado['numero_corridas']-$dado['canceladas']; 

                        $excel .= '<tr>';
                        $excel .= '<td style="text-align: center">'.$dado['fk_cidade_origem_certo'].'</td>';
                        $excel .= '<td style="text-align: center">'.$dado['fk_cidade_origem_nome'].'</td>';
                        $excel .= '<td style="text-align: center">'.$dado['relizadas'.$cont].'</td>';
                        $excel .= '<td style="text-align: center">'.$dado['canceladas'].'</td>';
                        $excel .= '<td style="text-align: center">R$ '.$dado['valor_medio'].'</td>';
                        $excel .= '</tr>';
                         $cont++;   
                     }




        $excel .= '</tbody>
            </table>';

        $arquivo = 'excel_cidades_'.date('d-m-Y-H:i:s').'.xls';

        // Configurações header para forçar o download
        header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header ("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header ("Content-Transfer-Encoding: binary");
        header ("Content-Type: application/vnd.ms-excel");
        header ("Expires: 0");
        header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
        header ("Content-Description: PHP Generated Data");

        // Envia o conteúdo do arquivo
        chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $excel);
        echo $excel;
        exit;
    }









    function atualizar_contato(){
        $dados = $this->input->post();
        $this->model_adm->atualizar_contato($dados);
        redirect('main/redirecionar/36');
    }



    function faq_motorista(){
        $link['links'] = $this->model_adm->buscar_links();
        $this->load->view('faq/motorista',$link);
    }
    function faq_passageiro(){
        $link['links'] = $this->model_adm->buscar_links();
        $this->load->view('faq/passageiro',$link);
    }



}