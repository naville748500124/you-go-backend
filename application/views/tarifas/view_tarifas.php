<head>
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/cupons.css" >
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/tarifas.css" >
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/theme-animate.css" >
    <link type="text/css" href="<?php echo base_url(); ?>style/css/bootstrap.min.css" rel="stylesheet" />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
    <meta name="theme-color" content="#292929">


</head>

<div>
    <h1 class="fade_1"><i class="fas fa-2x fa-ticket-alt lado_topo" data-wow-duration="2s" data-wow-delay="0.0s" > </i>  Parâmetros Corrida</h1>
    <div class="lado_direito" style="width: 100%;height: 2px;background: #f8d509;margin-top: 30px;overflow-x: hidden"></div>
</div>

<div class="lado_direito" style="width: 100%;height: 2px;background: transparent;margin-top: 40px;margin-bottom: 10px"></div>
<style>
    tr{
        background: transparent !important;
    }
    .form-control_label{
        visibility: visible;
        animation-duration: 1s;
        animation-delay: 0s;
        animation-name: fadeInDown;
    }
    .form-control{
        margin-top: -10px;
        background: transparent;
        box-shadow: none;
        border: none;
        border-bottom: 1px solid #000;
        border-radius: 0;

        visibility: visible;
        animation-duration: 1s;
        animation-delay: 0s;
        animation-name: fadeInDown;
    }

    .form-control:focus{
        border: none;
        border-bottom: 1px solid #ccc;
        box-shadow: none;
        visibility: visible;
        animation-duration: 0.3s;
        animation-delay: 0s;
        animation-name: fadeInUp;
    }

</style>


<script>
    function sair_modal(){
        $('#preto').css('display', 'none');
        $('#preto').css('opacity', '0');
        $('#ver_edit').css('display', 'none');


    }
    function abrir_modal(){
        $('#preto').animate({'opacity':'.60'}, 100, 'linear');
        $('#preto').css('display', 'block');
        $('#ver_edit').css('display', 'table');
    }
</script>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">&nbsp;</div>

<div id="ver_edit" style="position: absolute;width: 1150px;z-index: 9999;margin-top: -80px;display: none;box-shadow: 0 0 2px #292929">

    <div class="col-lg-7" style="height: 700px;background: #fff;border-right: 6px solid #ccc;overflow-y: scroll">
        <style>
            ::-webkit-scrollbar-track {
                background-color: transparent;
            }
            ::-webkit-scrollbar {
                width: 8px;
                background: transparent;
            }
            ::-webkit-scrollbar-thumb  {
                border-radius: 8px;
                background: #f8d509;
            }
        </style>
        <h3 class="fade_1" style="margin-top: 40px">  <i  style="margin-left: 20px" class="fas fa-2x fa-ticket-alt lado_topo" data-wow-duration="2s" data-wow-delay="0.9s" > </i>   &nbsp;Taxas já cadastradas</h3>


      <?php foreach ($dados_iniciais['tarifa_cidade'] as $cidades):
            extract($cidades);

            ?>


            <div class="col-lg-3 col-md-2 col-sm-3 col-xs-12">
                <form action="<?php echo base_url(); ?>controller_tarifas/atualizar_taxa" method="post">
                <select class="form-control obrigatorio" name="caidade_atualizar" id="">
                    <?php
                    foreach ($dados_iniciais['cidade_certa'] as $cidades_atuacao):
                    extract($cidades_atuacao);
                    if($id_cidades_atuacao == $fk_cidade){
                        ?>
                        <option class="opcoes_modelo" style="background: #292929;color: #fff" value="<?php echo $id_cidades_atuacao; ?>" selected> <?php echo $nome_cidades_atuacao; ?></option>
                        <?php
                    }
                    else{
                        ?>
                        <option class="opcoes_modelo" style="background: #292929;color: #fff" value="<?php echo $id_cidades_atuacao; ?>"> <?php echo $nome_cidades_atuacao; ?></option>
                        <?php
                    }
                    endforeach;
                    ?>

                </select>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="display: none"><input class="form-control obrigatorio" value="<?= $id_tarifa_cidade ?>" type="hidden" name="id_tarifa_atualizar" placeholder="Nome" ></div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"><input class="form-control obrigatorio" value="<?= $nome_tarifa ?>" type="text" name="nome_tarifa_atualizar" placeholder="Nome" ></div>
           <?php if($valor_fixo == '0'){ ?>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"><input class="form-control obrigatorio" value="<?php $valor_tarifa_t = explode('.',$valor_tarifa); echo $valor_tarifa_t[0]; ?>"  type="text" name="valor_tarifa_atualizar" placeholder="Valor" ></div>
            <?php }else{ ?>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"><input class="form-control obrigatorio mascara_monetaria" value="<?= $valor_tarifa ?>"  type="text" name="valor_tarifa_atualizar" placeholder="Valor" ></div>
            <?php } ?>


            <?php if($status_tarifa == '1'){ ?>
                <label class="usar col-lg-2" style="font-size: 16px;font-weight: bolder"> Ativo
                    <input type="checkbox" name="status_atualizar" value="on" checked>
                    <span class="checkmark" style="margin: 0 auto"></span>
                </label>
            <?php }else{ ?>
                <label class="usar col-lg-2" style="font-size: 16px;font-weight: bolder">Ativo
                    <input type="checkbox" name="status_atualizar">
                    <span class="checkmark" style="margin: 0 auto"></span>
                </label>
            <?php } ?>


            <?php if($valor_fixo == '0'){ ?>
                  <label class="usar col-lg-1" style="font-size: 16px;font-weight: bolder"> %
                      <input type="checkbox" name="valor_fixo_atulizar" value="on" checked>
                      <span class="checkmark" style="margin: 0 auto"></span>
                      <input type="hidden" value="on" name="valor_fixo_atulizar">
                  </label>
            <?php }else{ ?>
                <label class="usar col-lg-1" style="font-size: 16px;font-weight: bolder"> %
                    <input type="checkbox" name="valor_fixo_atulizar">
                    <span class="checkmark" style="margin: 0 auto"></span>
                </label>
            <?php } ?>

          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"><button class="botao_salvar">Salvar</button></div>


            <div class="col-lg-12">&nbsp;</div>
          </form>
        <?php endforeach; ?>
    </div>




    <div class="col-lg-5" style="height: 700px;background: #666">
        <form action="<?php echo base_url(); ?>controller_tarifas/nova_taxa" method="post">
            <div>
                <h1 class="fade_1" style="margin-top: 40px;margin-left: 20px;font-size: 25px;color: #fff"> <i class="fas fa-1x  fa-building lado_topo" data-wow-duration="2s" data-wow-delay="0.3s" > </i>  <i  style="margin-left: -20px" class="fas fa-2x  fa-building lado_topo" data-wow-duration="2s" data-wow-delay="0.9s" > </i>  <i class="fas fa-1x  fa-building lado_topo" style="margin-left: -20px" data-wow-duration="2s" data-wow-delay="0.6s" > </i> Nova taxa por Cidades</h1>
                <p>&nbsp;</p>
                <!--<p><span class="fa fa-1x fa-plus cad botao_adicionar" style="margin-top: 20px;margin-bottom: -20px;cursor: pointer;color: #fff"></span>&nbsp; Adicionar tarifa a uma cidade</p>-->
                <div class="col-lg-12">
                    <select style="border: none;border-bottom: 1px solid #fff;margin-top: 10px" class="inp_form_cadastro" name="fk_cidade" id="cidade_trabalho">

                        <?php foreach ($dados_iniciais['cidade_certa'] as $key => $cidades) { ?>
                            <option class="opcoes_modelo" style="background: #292929;color: #fff" value="<?php echo $cidades['id_cidades_atuacao']; ?>"> <?php echo $cidades['nome_cidades_atuacao']; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-lg-12">&nbsp;</div>
                <div id="origem" align="center" style="padding-left: 0" class="col-lg-12">
                    <div class="col-lg-6"><input type="text" class="form-control" style="color: #fff"  name="nome_tarifa"  placeholder="Nome" /></div>
                    <div class="col-lg-6"><input type="text" class="form-control" style="color: #fff" name="valor_tarifa" placeholder="Valor"  /></div>
                </div>
                <div id="destino">
                </div>
            </div>
            <label class="usar col-lg-2" style="font-size: 16px;font-weight: bolder;color: #fff;margin-top: 30px;margin-left: 20px"> %
                <input type="checkbox" name="valor_fixo[]">
                <span class="checkmark" style="margin: 0 auto"></span>
            </label>
            <label class="usar col-lg-3" style="font-size: 16px;font-weight: bolder;color: #fff;margin-top: 30px"> Usar
                <input type="checkbox" name="status_tarifa[]">
                <span class="checkmark" style="margin: 0 auto"></span>
            </label>
            <div class="col-lg-6 col-md-2 col-sm-2 col-xs-6" style="margin-top: 0px;"><button  type="submit"  style="padding: 12px 63px;"  class="botao lado_baixo botao_envia_pagamento"  ><span> Salvar</span></button></div>
        </form>
    </div>
</div>






<!--para cima modal, para baixo tela inicial de tarifcas-->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px">
        <h4 style="font-weight: bolder;padding: 8px 0;color: #292929;margin-top: 20px;margin-bottom: -20px">
                <i class="fa fa-1x fa-taxi lado_topo" > </i>
                Tarifa 1 (Manhã e tarde)
            </h4>
    </div>


    <form action="<?php echo base_url(); ?>controller_tarifas/gerais" method="post">

        <?php
        //faz com que o array seja fragmentado
        $tarifa1 = $dados_iniciais['tarifa'][0];
        $tarifa2 = $dados_iniciais['tarifa'][1];

        ?>




        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">    <label class="form-control_label" style="font-size: 12px;font-weight: bolder">Valor por Km</label>          <input class="form-control obrigatorio mascara_monetaria"   type="text" placeholder="Valor cobrado em KM"  value="<?php echo $tarifa1['valor_tarifa']; ?>" aviso="Valor cobrado em KM (Tarifa 1)" name="tarifa1_valor"></div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">    <label class="form-control_label" style="font-size: 12px;font-weight: bolder">Horario de início</label>     <input  maxlength="8" onchange="horario_inicio1()" id="hora_inicio_1" class="form-control obrigatorio mascara_hora"    type="text" placeholder="Horario de início"    value="<?php echo $tarifa1['hora_inicio']; ?>" aviso="Horario de início (Tarifa 1)" name="tarifa1_inicio"></div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">    <label class="form-control_label" style="font-size: 12px;font-weight: bolder">Horario de termino</label>    <input maxlength="8"  onchange="horario_termino1()" id="hora_termino_1" class="form-control obrigatorio mascara_hora" type="text" placeholder="Horario de Termino"   value="<?php echo $tarifa1['hora_fim']; ?>" aviso="Horario de término (Tarifa 1)" name="tarifa1_fim"></div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">    <label class="form-control_label" style="font-size: 12px;font-weight: bolder">Tarifa mínima </label>        <input class="form-control obrigatorio mascara_monetaria" placeholder="Tarifa mínima"   value="<?php echo $tarifa1['valor_tarifa_minima']; ?>" aviso="Tarifa mínima (Tarifa 1)" name="tarifa1_min"></div>     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" style="margin-top: 15px;"> <span>Tarifa mínima</span>
            <div class="switch__container">
                <?php
                if($tarifa1['usar_tarifa_minima'] == 1){
                    echo ' <input id="switch-shadow" class="switch switch--shadow" name="usar_tarifa[]"  type="checkbox" checked>
                    <label for="switch-shadow"></label> ';
                }
                else{
                    echo ' <input id="switch-shadow" class="switch switch--shadow" name="usar_tarifa[]"  type="checkbox">
                    <label for="switch-shadow"></label> ';
                }
                ?>
            </div>
        </div>


        <script>
            function horario_inicio1() {
                var valor = document.getElementById('hora_inicio_1').value;

                var inicio_1 = valor.split(':');

                if(inicio_1[1] == 0){
                    if(inicio_1[0] != 0){
                        inicio_1[0] = inicio_1[0]-1;
                    }
                   var valor_certo = 59;
                }
                else{
                    valor_certo = inicio_1[1] - 1;
                    if(valor_certo < 10){
                        valor_certo = '0'+valor_certo;
                    }
                }

                if(inicio_1[0] == 0){
                    
                    if(inicio_1[1] != 0){
                        inicio_1[1] = inicio_1[1]-1;
                    }else{
                        inicio_1[0] = '23';
                    }

                }else if(inicio_1[0] == 59){
                    inicio_1[0] = inicio_1[0]-1;
                }

                valor = inicio_1[0]+':'+valor_certo;

                if(moment($('#hora_inicio_1').val(),'HH:mm').isValid()){
                 $("#hora_termino_2").val(valor);
                }
                console.log(valor);
            }





            function horario_termino1() {
                var valor = document.getElementById('hora_termino_1').value;
                var termino_1 = valor.split(':');

                if(termino_1[1] == 0){
                    if(termino_1[0] != 0){
                        termino_1[0] = termino_1[0]-1;
                    }
                   var valor_certo = 59;
                }
                else{
                    valor_certo = termino_1[1] - 1;
                    if(valor_certo < 10){
                        valor_certo = '0'+valor_certo;
                    }
                }

                if(termino_1[0] == 0){
                    
                    if(termino_1[1] != 0){
                        termino_1[1] = termino_1[1]-1;
                    }else{
                        termino_1[0] = '23';
                    }

                }else if(termino_1[0] == 59){
                    termino_1[0] = termino_1[0]-1;
                }



                valor = termino_1[0]+':'+valor_certo;
                if(moment($('#hora_inicio_1').val(),'HH:mm').isValid()){
                    $("#hora_inicio_2").val(valor);
                }
                console.log(valor);
            }
        </script>


    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
        <h4 style="font-weight: bolder;padding: 8px 0;color: #292929;margin-top: 20px;margin-bottom: -20px">
                <i class="fa fa-1x fa-taxi lado_topo" > </i>
                Tarifa 2 (Noturno)
         </h4>
    </div>


        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> <label class="form-control_label" style="font-size: 12px;font-weight: bolder">Valor por Km</label>       <input class="form-control obrigatorio mascara_monetaria"  type="text" placeholder="Valor cobrado em KM" value="<?php echo $tarifa2['valor_tarifa']; ?>"  aviso="Valor cobrado em KM  (Tarifa 2)" name="tarifa2_valor"></div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"> <label class="form-control_label" style="font-size: 12px;font-weight: bolder">Horario de início</label>  <input  readonly style="background: transparent;opacity: .60" maxlength="8"  onkeyup="horario_inicio2()" id="hora_inicio_2"  class="form-control obrigatorio mascara_hora"   type="text" placeholder="Horario de início"   value="<?php echo $tarifa2['hora_inicio']; ?>"  aviso="Horario de início (Tarifa 2)" name="tarifa2_inicio"></div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"> <label class="form-control_label" style="font-size: 12px;font-weight: bolder">Horario de termino</label> <input  readonly style="background: transparent;opacity: .60"  maxlength="8"  onkeyup="horario_termino2()" id="hora_termino_2" class="form-control obrigatorio mascara_hora" type="text" placeholder="Horario de Termino"  value="<?php echo $tarifa2['hora_fim']; ?>"  aviso="Horario de término (Tarifa 2)" name="tarifa2_fim"></div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"> <label class="form-control_label" style="font-size: 12px;font-weight: bolder">Tarifa mínima </label>     <input class="form-control obrigatorio mascara_monetaria" value="<?php echo $tarifa2['valor_tarifa_minima']; ?>"  placeholder="Tarifa mínima"   aviso="Tarifa mínima (Tarifa 2)" name="tarifa2_min"></div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" style="margin-top: 15px;">
        <span>Tarifa mínima</span>
        <div class="switch__container_2">
            <?php
            if($tarifa2['usar_tarifa_minima'] == 1){
                echo ' <input id="switch-shadow_2" class="switch switch--shadow_2" name="usar_tarifa2[]"  type="checkbox" checked>
                    <label for="switch-shadow_2"></label> ';
            }
            else{
                echo ' <input id="switch-shadow_2" class="switch switch--shadow" name="usar_tarifa2[]"  type="checkbox">
                    <label for="switch-shadow_2"></label> ';
            }
            ?>
        </div>
    </div>




    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px">
        <h4 style="font-weight: bolder;padding: 8px 0;color: #292929;margin-top: 20px;margin-bottom: -20px">
                <i class="fa fa-1x fa-cog lado_topo" > </i>
                Geral
         </h4>
    </div>



    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">      <label class="form-control_label" style="font-size: 12px;font-weight: bolder">Tempo de espera</label>           <input class="form-control obrigatorio"                   id="tempo_espera"    type="text" placeholder="Tempo de Espera"                  value="<?php echo $dados_iniciais['config']['tempo_espera'] ;?>"          aviso="Tempo de Espera" name="tempo_espera"></div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">      <label class="form-control_label" style="font-size: 12px;font-weight: bolder">Km maximo</label>                 <input class="form-control obrigatorio"                   id="km_max"          type="text" placeholder="Quilometragem máxima"             value="<?php echo $dados_iniciais['config']['km_maximo'] ;?>"             aviso="Quilometragem máxima (Geral)" name="km_max"></div>
    <div class="col-lg-3 col-md-2 col-sm-2 col-xs-12">      <label class="form-control_label" style="font-size: 12px;font-weight: bolder">Taxa de cancelamento</label><br>  <input class="form-control obrigatorio mascara_monetaria"                                         type="text" placeholder="cancelamento"                     value="<?php echo $dados_iniciais['config']['taxa_cancelamento'];?>"      aviso="Taxas de cancelamento (Geral)" name="cancelamento"  style="width: 80%;float: left"></div>
    <div class="col-lg-3 col-md-2 col-sm-2 col-xs-12">   <label class="form-control_label" style="font-size: 12px;font-weight: bolder"> Taxa You GO</label> <br>            <input class="form-control obrigatorio "                    id="taxa_yougo"    type="text" placeholder="You GO" name="yougo"              value="<?php echo $dados_iniciais['config']['taxa_yougo'];?>"             aviso="You GO (Geral)" style="width: 30%;float: left"><span style="width: 10%;font-weight: bolder;font-size: 20px;float: left">%</span></div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> &nbsp;  </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">      <label class="form-control_label" style="font-size: 12px;font-weight: bolder">Tempo para cancelar</label>        <input class="form-control obrigatorio"                   id="tempo_cancelar" type="text" placeholder="Tempo de cancelamento"           value="<?php echo $dados_iniciais['config']['tempo_cancelamento'] ;?>"          aviso="Tempo de cancelamento" name="tempo_cancelamento"></div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">      <label class="form-control_label" style="font-size: 12px;font-weight: bolder">Tempo de busca</label>             <input class="form-control obrigatorio"                   id="tempo_busca"    type="text" placeholder="Tempo de busca"                  value="<?php echo $dados_iniciais['config']['tempo_busca'] ;?>"          aviso="Tempo de busca" name="tempo_busca"></div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> &nbsp;  </div>
        <!--botao de enviar-->
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6" style="margin-top: 20px;"><button  type="submit"  style="padding: 12px 63px;"    class="botao lado_baixo botao_envia_pagamento"  ><span> Salvar</span></button></div>
    </form>

    <a href="<?php echo base_url(); ?>"><div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"  style="margin-top: 20px;"><button   style="padding: 12px 60px;"   class="botao_preto lado_baixo botao_envia_pagamento"  ><span> Cancelar</span></button></div></a>
    <div class="col-lg-4 col-md-2 col-sm-2 col-xs-6"  style="margin-top: 20px;"><button  onclick="abrir_modal()"  style="padding: 12px 60px;"   class="botao_preto lado_baixo botao_envia_pagamento"  ><span> Tarifas por cidade</span></button></div>




<div class="col-lg-12"></div>

        <script>
            $(document).ready(function () {

                $('.botao_adicionar').click(function () {
                    var clone = '<div id="origem" class="col-lg-12" style="padding-left: 0" align="center"><br><div class="col-lg-3"><input type="text" class="form-control"  name="nome[]"  placeholder="Nome" /></div><div class="col-lg-3"><input type="text" class="form-control"  name="valor[]" placeholder="Valor"  /></div><span class="fa fa-1x fa-times col-lg-1 cad remover" style="cursor: pointer;margin-left: -30px;margin-top: 20px"></span></div>';
                    $('#origem').append(clone);

                });

                $(document).on('click','.remover',function () {
                        $(this).parent('div').remove();
                });


                var mask = "HH:MM",
                    pattern = {
                        'translation': {
                            'H': {
                                pattern: /[0-59678]/
                            },
                            'M': {
                                pattern: /[0-59678]/
                            }
                        }
                    };

                $('.mascara_hora').mask(mask, pattern);

                moment.suppressDeprecationWarnings = true;

                $('.mascara_hora').change(function(){
                    console.log('Mudou a hora: '+$(this).val());
                    console.log(moment($(this).val()).subtract("1", "minutes"));

                    if(moment($(this).val(),'HH:mm').isValid()){
                        console.log("Hora válida");
                    } else {
                        console.log("Hora inválida");
                        $(this).val("00:00");
                    }


                });


            })

        </script>

        <style>
            .select2-container--default .select2-selection--single .select2-selection__rendered{
                color : #000;
                font-weight: bolder;
            }
            .select2-container--default .select2-selection--single{
                background: transparent !important;
                background-color: transparent !important;
                border: none;
                border-bottom: 1px solid #000;
                border-radius: 0;
                color: #000;
                z-index: 99999;
            }
            input[type='search'] {
                padding: 2px 10px;
                border-radius: 0px;
                background: 0;
                border: none;
                color: #fff;
                border-bottom: 1px solid #292929;
                visibility: visible;
                animation-duration: 0.5s;
                animation-delay: 0s;
                animation-name: fadeInUp;
                margin-bottom: 10px;
                z-index: 99999;
            }
            .select2-dropdown {
                background-color: #292929;
                color: #fff;
                border: 1px solid #000;
                border-radius: 4px;
                box-sizing: border-box;
                display: block;
                position: absolute;
                left: -100000px;
                width: 100%;
                z-index: 1051;
                z-index: 99999;
            }
            .select2-container--default .select2-results__option--highlighted[aria-selected] {
                background-color: #f8d509;
                color: #000;
            }
            .select2-container--default .select2-results__option[aria-selected=true] {
                background-color: #171515;
                color: #f8d509 ;
                z-index: 99999;
            }
        </style>





        <style>

            .botao_alt {
                opacity: .80;
                background: transparent;
                border: 2px solid #f8d509;
                margin-top: 20px;
                padding: 8px 45px;
                color: #f8d509;
                position: relative;
                overflow: hidden;
                cursor: pointer;
            }
            .botao_alt:hover {
                color: #000;
                opacity: 1;
                transition-duration: 0.3s;
                transition-timing-function: ease-in;
                transition-property: all;
            }
            .botao_alt span {
                position: relative;
                z-index: 100;
                font-size: 16px;
            }
            .botao_alt:before, .botao_alt:after {
                content: '';
                position: absolute;
                display: block;
                height: 100%;
                width: 100%;
                top: 0;
                left: 0;
            }
            .botao_alt:before {
                -webkit-transform: translate3d(-100%, 0, 0);
                transform: translate3d(-100%, 0 , 0);
                background-color: #f8d509;
                color: #000;
                border: 1px solid #f8d509;
                -webkit-transition: -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
                transition: -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
                transition: transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
                transition: transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19), -webkit-transform 300ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
            }
            .botao_alt:after {
                background-color: #f8d509;
                color: #000;
                border: 1px solid #f8d509;
                -webkit-transform: translate3d(100%, 0, 0);
                transform: translate3d(100%, 0, 0);
                -webkit-transition: -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
                transition: -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
                transition: transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
                transition: transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62), -webkit-transform 300ms 300ms cubic-bezier(0.16, 0.73, 0.58, 0.62);
            }
            .botao_alt:hover:before {
                -webkit-transform: translate3d(0, 0, 0);
                transform: translate3d(0, 0, 0);
            }
            .botao_alt:hover:after {
                -webkit-transform: translate3d(0, 0, 0);
                transform: translate3d(0, 0, 0);
            }






            /* The container */
            .usar {
                display: block;
                position: relative;
                padding-left: 35px;
                margin-bottom: 12px;
                cursor: pointer;
                font-size: 22px;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            /* Hide the browser's default checkbox */
            .usar input {
                position: absolute;
                opacity: 0;
                cursor: pointer;
            }

            /* Create a custom checkbox */
            .checkmark {
                position: absolute;
                top: 0;
                left: 0;
                height: 25px;
                width: 25px;
                background-color: #eee;
            }

            /* On mouse-over, add a grey background color */
            .usar:hover input ~ .checkmark {
                background-color: #ccc;
            }

            /* When the checkbox is checked, add a blue background */
            .usar input:checked ~ .checkmark {
                background-color: #f8d509;
            }

            /* Create the checkmark/indicator (hidden when not checked) */
            .checkmark:after {
                content: "";
                position: absolute;
                display: none;
            }

            /* Show the checkmark when checked */
            .usar input:checked ~ .checkmark:after {
                display: block;
            }

            /* Style the checkmark/indicator */
            .usar .checkmark:after {
                left: 9px;
                top: 5px;
                width: 5px;
                height: 10px;
                border: solid white;
                border-width: 0 3px 3px 0;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }




            .botao_salvar{
                padding: 8px 13px;
                border: 4px solid transparent;
                background: transparent;
                margin-top: -9px;
                visibility: visible;
                animation-duration: 0.3s;
                animation-delay: 0s;
                animation-name: fadeInUp;
            }
            .botao_salvar:hover{
                cursor: pointer;
                background: #666;
                color: #fff;
                border: 4px solid #fff;
                box-shadow: 0 0 4px #ccc;
                transition-duration: 0.3s;
                transition-timing-function: ease-in;
                transition-property: all;
                visibility: visible;
                animation-duration: 0.3s;
                animation-delay: 0s;
                animation-name: fadeIn;
            }


        </style>





<script type="text/javascript">

    function mascara(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascara()",1)
    }
    function execmascara(){
        v_obj.value=v_fun(v_obj.value)
    }
    function remove_char(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        return v;
    }
    function id( el ){
        return document.getElementById( el );
    }
    window.onload = function(){
        id('tempo_espera').onkeyup = function(){
            mascara( this, remove_char );
        }
        id('km_max').onkeyup = function(){
            mascara( this, remove_char );
        }
        id('taxa_yougo').onkeyup = function(){
            mascara( this, remove_char );
        }










    }
</script>
