<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_adm extends MY_Model {

		public function get_menus($where = "") {

			return $this->db->query('select titulo_menu as titulo, id_menu as id, menu_acima as anterior from seg_menu where menu_acima '.$where.' order by posicao_menu')->result();

		}

		public function get_lista_aplicacoes($id = null,$grupo = null){

			return $this->db->query('select sa.* from seg_aplicacao sa
									inner join seg_aplicacoes_menu as sam on sam.fk_aplicacao = id_aplicacao
									inner join seg_menu on fk_menu = id_menu
									inner join seg_aplicacoes_grupos as sag on sag.fk_aplicacao = id_aplicacao
									where menu_acima = '.$id.' and fk_grupo = '.$grupo)->result();

		}

		public function get_link($id = null,$grupo = null) { //VALIDAR ACESSO DO USUÁRIO********

			$consulta = $this->db->query('select 
											m.titulo_menu,
											a.link_aplicacao,
											a.id_aplicacao
											from seg_aplicacoes_menu am
											inner join seg_menu m on m.id_menu = am.fk_menu
											inner join seg_aplicacao a on a.id_aplicacao = am.fk_aplicacao
											inner join seg_aplicacoes_grupos as sag on sag.fk_aplicacao = id_aplicacao
											where fk_menu = '.$id.' and fk_grupo = '.$grupo.'
											group by titulo_menu, link_aplicacao, id_aplicacao');

			if($consulta->num_rows() > 0) { // não tem sub-menu, é um link direto

				return $consulta;

			} else { //Tem sub-menu, deve chamar o else e iniciar a recursividade, se necessário.

				return false;

			}

		}

		/*Criar*/

		public function listar(){

			return array('models' => $this->db->get('seg_models')->result(),
						 'controllers' => $this->db->get('seg_controllers')->result(),
						 'menus' => $this->db->get('seg_menu')->result());

		}

		public function criarMc($model,$controller){

			$this->db->insert('seg_models',$model);
			echo $this->db->last_query().';<br>';

			$controller['fk_model'] = $this->db->insert_id();

			$this->db->insert('seg_controllers',$controller);
			echo $this->db->last_query().';<br>';

		}

		public function criarMenu($menu){

			$menu['id_menu'] = $this->db->query('select max(id_menu)+1 as proximo from seg_menu where id_menu < 1000')->row()->proximo;

			$this->db->insert('seg_menu',$menu);
			echo $this->db->last_query().';<br>';

			return true;

		}

		public function criarAplicacao($aplicacao,$menu){

			$this->db->insert('seg_aplicacao',$aplicacao);
			$id = $this->db->insert_id();

			echo $this->db->last_query().';<br>';

			if ($menu != "") {
				$this->db->query('insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (
																'.$id.',
																'.$menu.');');
				echo $this->db->last_query().'<br>';
			}

			$this->db->query('insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(
																'.$id.',1);');

			echo $this->db->last_query().'<br>';

			return true;

		}
        public function termo($termo){
		    echo $termo;
		    $termo_array = array (
		    'termo' => $termo
		    );
            $this->db->update('termos',$termo_array);
        }
        public function termo_mostrar(){
            return $this->db->get('cad_configuracoes','termo')->row_array();
        }

        public function pagamento(/*$valor*/){
            $valor = 4;
            $dado['dados'] = $this->db->get_where('seg_usuarios',array('id_usuario' => $valor))->row_array();
            /*print_r($dado);*/
            return $dado;

        }

        public function view_todas($inicio = null,$fim = null){

            if($inicio != null && $fim != null) {
              $this->db->where("data_corrida BETWEEN '{$inicio}' AND '{$fim}'");
            } else {
              $this->db->where("data_corrida > (DATE_SUB(CURDATE(), INTERVAL 1 MONTH))");
            }
           
            $dado['dados'] = $this->db->select("cad_corridas.*,  id_corrida,date_format(data_corrida,'%d/%m/%Y %H:%i') as data_corrida,

              ifnull((SELECT round(valor_corrida,2) FROM view_custo_final_motorista vcfm where vcfm.id_corrida = cad_corridas.id_corrida and cad_corridas.fk_motorista is not null),0) as valor_motorista,

            	(select valor_corrida from view_custo_final vcf where vcf.id_corrida = cad_corridas.id_corrida) 
            	as valor_corrida, (select nome_usuario from seg_usuarios where id_usuario = fk_passageiro) as passageiro,(select nome_usuario from seg_usuarios where id_usuario = fk_motorista) as motorista, nome_item_grupo as status,
            	status_pagseguro")
            							->from('cad_corridas')
            							->join('cad_item_grupo','id_item_grupo = fk_status_corrida','inner')
            							->join('pag_status','id_status = status_pag_seguro','left')
            							->order_by('id_corrida','desc')
            							->get()
                          //echo $this->db->last_query();
                          //die();
            						  ->result_array();
            return $dado;

        }


        public function cidade($cidade_cad){
            return $this->db->insert('cad_cidades_atuacao',$cidade_cad);
        }

        public function cidade_atulizar($cidade_cad,$id){
            $this->db->where('id_cidades_atuacao',$id);
            return $this->db->update('cad_cidades_atuacao',$cidade_cad);
        }

        public function view_cupons(){
            $dado['dados'] = $this->db->get('cad_cupons')->result_array();
            return $dado;
        }
        public function pagamento_motorista(){
          

               $dados['dados'] = $this->db->query("SELECT agencia_motorista, 
                                                   conta_motorista, 
                                                   digito_motorista, 
                                                   id_corrida, 
                                                   cad_corridas.fk_motorista, 
                                                   (SELECT REPLACE(Round(valor_corrida, 2), '.', ',') 
                                                    FROM   view_custo_final_motorista cvfm 
                                                    WHERE  cvfm.id_corrida = cad_corridas.id_corrida) AS valor_corrida, 
                                                   pagamento_motorista, 
                                                   nome_item_grupo, 
                                                   nome_usuario, 
                                                   tipo_conta,
                                                   pagamento_motorista, 
                                                   
                                                   ifnull((SELECT Datediff(CURRENT_DATE, Date(pagamento_motorista_data))
                                                    FROM   cad_corridas 
                                                    WHERE  pagamento_motorista = 1 
                                                           AND fk_motorista = id_usuario 
                                                    ORDER  BY id_corrida DESC 
                                                    LIMIT  1),'Nenhum pagamento nos últimos 30 dias ')  AS dias_receber 
                                                                                                                                                        
                                                    
                                            FROM   cad_corridas 
                                                   INNER JOIN cad_detalhes_motorista 
                                                           ON cad_corridas.fk_motorista = cad_detalhes_motorista.fk_usuario 
                                                   INNER JOIN seg_usuarios 
                                                           ON seg_usuarios.id_usuario = cad_corridas.fk_motorista 
                                                   INNER JOIN cad_item_grupo 
                                                           ON cad_detalhes_motorista.fk_banco_motorista = 
                                                              cad_item_grupo.id_item_grupo 
                                            WHERE  fk_status_corrida = 86 
                                            ORDER  BY id_corrida DESC ")->result_array();

           return $dados;


        }
        public function atulizar_pagamento_motorista($id,$tipo){

            if($tipo){
                $dados = array('pagamento_motorista' => 1);
            } else {
                $dados = array('pagamento_motorista' => 0);
            }

        	$this->db->set('pagamento_motorista_data', 'NOW()', FALSE);
            $this->db->where('id_corrida',$id);
            return $this->db->update('cad_corridas',$dados);
        }

        public function achar_dados_corrida($id){
            $dado['dados'] = $this->db->select("cad_corridas.*,
					       codigo_cupom,
					       valor_desconto_cupom,
					       tipo_desconto_cupom,
					       replace(round(taxa_cancelamento, 2), '.', ', ') AS taxa_cancelamento,
					       concat('R$ ', replace(round(valor_corrida, 2), '.', ', ')) AS valor_corrida,
					       replace(round((CASE
					                         WHEN fk_cupom IS NULL THEN ROUND(valor_corrida, 2)
					                         ELSE (CASE
					                                          (SELECT tipo_desconto_cupom
					                                           FROM cad_cupons
					                                           WHERE id_cupom = fk_cupom)
					                                   WHEN 0 THEN ROUND(valor_corrida -
					                                                       (SELECT valor_desconto_cupom
					                                                        FROM cad_cupons
					                                                        WHERE id_cupom = fk_cupom), 2)
					                                   ELSE ROUND(valor_corrida - (valor_corrida *
					                                                                 (SELECT valor_desconto_cupom
					                                                                  FROM cad_cupons
					                                                                  WHERE id_cupom = fk_cupom)/100), 2)
					                               END)
					                     END), 2),
					       '.',
					       ', ') AS valor_corrida_final,

					  (SELECT nome_usuario
					   FROM seg_usuarios
					   WHERE id_usuario = fk_passageiro) AS passageiro,
					            ifnull(
					                     (SELECT nome_usuario
					                      FROM seg_usuarios
					                      WHERE id_usuario = fk_motorista), '-') AS motorista,
					            nome_item_grupo AS status")
                ->from('cad_corridas')
                ->join('cad_item_grupo','id_item_grupo = fk_status_corrida','inner')
                ->join('cad_cupons','id_cupom = fk_cupom','left')
                ->where('id_corrida',$id)
                ->get()
                ->result_array();
            return $dado;
        }

        public function view_passageiro_relatorio(){
            return $this->db->query('select * from view_estatisticas_passageiros')->result_array();

        }

        public function view_motorista_relatorio(){
            return $this->db->query('select * from view_estatisticas_motorista')->result_array();

        }

        public function view_calendario(){
            return  $this->db->query("
		    					SELECT
		    						id_usuario,nome_usuario,email_usuario,nome_usuario,celular_usuario,cpf_usuario,ativo_usuario,
		    						(SELECT Concat('R$ ', Replace(Round(ifnull(Sum((SELECT valor_corrida 
                                        FROM   view_custo_final_motorista cvfm 
                                        WHERE  cvfm.id_corrida = 
                                               cad_corridas.id_corrida)),0), 2), 
					                      '.', ',')) AS valor_corrida 
										FROM   cad_corridas 
										WHERE  fk_status_corrida = 86 
                                                AND pagamento_motorista is null
										       AND fk_motorista = id_usuario 
										       AND data_corrida >= DATE_SUB(curdate(), interval 1 day)) as dia,




									(SELECT Concat('R$ ', Replace(Round(ifnull(Sum((SELECT valor_corrida 
                                        FROM   view_custo_final_motorista cvfm 
                                        WHERE  cvfm.id_corrida = 
                                               cad_corridas.id_corrida)),0), 2), 
					                      '.', ',')) AS valor_corrida 
										FROM   cad_corridas 
										WHERE  fk_status_corrida = 86 
                                                AND pagamento_motorista is null
										       AND fk_motorista = id_usuario 
										       AND data_corrida >= DATE_SUB(curdate(), interval 1 week)) as semana,
										       
										     
										     (SELECT Concat('R$ ', Replace(Round(ifnull(Sum((SELECT valor_corrida 
                                        FROM   view_custo_final_motorista cvfm 
                                        WHERE  cvfm.id_corrida = 
                                               cad_corridas.id_corrida)),0), 2), 
                                '.', ',')) AS valor_corrida 
                    FROM   cad_corridas 
                    WHERE  fk_status_corrida = 86 
                                                AND pagamento_motorista is null
                           AND fk_motorista = id_usuario 
                           AND data_corrida >= DATE_SUB(curdate(), interval 15 day)) as quinzena,
										       
										       


                                    (SELECT Concat('R$ ', Replace(Round(ifnull(Sum((SELECT valor_corrida 
                                        FROM   view_custo_final_motorista cvfm 
                                        WHERE  cvfm.id_corrida = 
                                               cad_corridas.id_corrida)),0), 2), 
                                          '.', ',')) AS valor_corrida 
                                        FROM   cad_corridas 
                                        WHERE  fk_status_corrida = 86 
                                                AND pagamento_motorista = 1
                                               AND fk_motorista = id_usuario 
                                               AND data_corrida >= DATE_SUB(curdate(), interval 1 month)) as recebido,
                                               
                                             
                                             




									(SELECT Concat('R$ ', Replace(Round(ifnull(Sum((SELECT valor_corrida 
                                        FROM   view_custo_final_motorista cvfm 
                                        WHERE  cvfm.id_corrida = 
                                               cad_corridas.id_corrida)),0), 2), 
					                      '.', ',')) AS valor_corrida 
										FROM   cad_corridas 
										WHERE  fk_status_corrida = 86 
                                               AND pagamento_motorista is null
										       AND fk_motorista = id_usuario 
										       AND data_corrida >= DATE_SUB(curdate(), interval 1 month)) as mes

										       from seg_usuarios
										       where fk_grupo_usuario = 2
										       group by id_usuario,nome_usuario,email_usuario,nome_usuario,telefone_usuario,cpf_usuario,ativo_usuario,dia,semana,mes

                                               ")
		    							->result_array();
        }

        public function ativar_passageiro($id){
			    $this->db->where('id_usuario',$id);
        	$this->db->update('seg_usuarios',array('ativo_usuario' => 1, 'motivo_inativacao' => 'Reativado em '.date('d/m/Y H:i:s')));
        	return $this->db->select('email_usuario')->get_where('seg_usuarios',array('id_usuario' => $id))->row()->email_usuario;
        }
        public function desativar_passageiro($id,$motivo,$bloqueado){
			    
          $this->db->where('id_usuario',$id);
          if($bloqueado == 2){
            $this->db->update('seg_usuarios',array('ativo_usuario' => 0,'bloqueado' => 1, 'motivo_inativacao' => $motivo.' Bloqueado em '.date('d/m/Y H:i:s')));
          } else {
            $this->db->update('seg_usuarios',array('ativo_usuario' => 0, 'motivo_inativacao' => $motivo.' Inativado em '.date('d/m/Y H:i:s')));
          }

        	return $this->db->select('email_usuario')->get_where('seg_usuarios',array('id_usuario' => $id))->row()->email_usuario;

        }
        public function view_faq(){
            return $this->db->get('cad_dados_empresa')->row_array();
            /*echo $this->db->last_query();
            die();*/
        }
        function atualizar_contato($dados){
            $this->db->update('cad_dados_empresa',$dados);
        }
        function cidades(){
                return $this->db->query('
                                                                               
                                        SELECT 
                                        
                                        fk_cidade_origem as fk_cidade_origem_certo,
                                        (select nome_cidades_atuacao from cad_cidades_atuacao where id_cidades_atuacao = fk_cidade_origem) as fk_cidade_origem_nome,
                                        count(case fk_cidade_origem when fk_cidade_origem then 1 else null end) as numero_corridas,
                                        count(case fk_status_corrida when 87 then 1  when 88 then 1 else null end) as canceladas,
                                        replace(round((select AVG(valor_corrida) from cad_corridas where fk_cidade_origem = fk_cidade_origem_certo),2),\'.\',\',\') as valor_medio
                                        
                                        
                                        FROM cad_corridas
                                        WHERE fk_cidade_origem = fk_cidade_origem
                                        GROUP BY fk_cidade_origem

                                
                                ')->result_array();
        }




        function buscar_links(){
            return $this->db->query('select link_youtube_motorista,link_youtube from cad_dados_empresa')->row_array();

        }
    }