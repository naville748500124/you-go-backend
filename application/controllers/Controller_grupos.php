<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_grupos extends MY_Controller {

	function __construct() {

	    parent::__construct();
	    $this->load->model('model_grupos');
		    
	}

	public function criar_grupo(){

		$this->form_validation->set_rules('nome_grupo','Nome do Grupo','required|is_unique[seg_grupos.nome_grupo]');
		$this->form_validation->set_rules('descricao_grupo','Descrição do Grupo','required');

		$dados = array (
					'usuario_criou_grupo' => $this->session->userdata('usuario'),
					'nome_grupo' => $this->input->post('nome_grupo'),
					'ativo_grupo' => $this->input->post('ativo_grupo'),
					'descricao_grupo' => $this->input->post('descricao_grupo')
				);

		if ($this->form_validation->run()) {

			$this->model_grupos->start();
			$id = $this->model_grupos->create($dados);
			$commit = $this->model_grupos->commit();
			
			if ($commit['status']) {
				$this->aviso('Registro Criado','Grupo criado com sucesso!, defina agora as telas que ele tem acesso','success',false);

				redirect('main/redirecionar/4/'.$id);
			} else {

				$this->aviso('Falha ao criar','Erro(s) ao inserir dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);

				$this->session->set_flashdata($dados);
				redirect('main/redirecionar/6');
			}

		} else {

			$this->aviso('Falha ao criar','Erro(s) no formulário: '.validation_errors(),'error',true);

			$this->session->set_flashdata($dados);
			redirect('main/redirecionar/6');

		}

	}

	public function editar_grupo(){

		if($this->input->post('nome_grupo') != $this->input->post('grupo_inicial')){
			$this->form_validation->set_rules('nome_grupo','Nome do Grupo','required|is_unique[seg_grupos.nome_grupo]');
		}
		$this->form_validation->set_rules('descricao_grupo','Descrição do Grupo','required');

		$dados = array (
					'nome_grupo' => $this->input->post('nome_grupo'),
					'descricao_grupo' => $this->input->post('descricao_grupo'),
					'ativo_grupo' => $this->input->post('ativo_grupo'),
					'id_grupo' => $this->input->post('id_grupo')
				);

		if ($this->form_validation->run()) {
			
			$this->model_grupos->start();
			$this->model_grupos->update($dados);
			$this->model_grupos->aplicacoes_grupos($this->input->post('id_grupo'),$this->input->post('aplicacao'));
			
			$commit = $this->model_grupos->commit();
			
			if ($commit['status']) {
				$this->aviso('Registro Editado','Grupo atualizado com sucesso!.','success',false);
			} else {
				$this->aviso('Falha ao editar','Erro(s) ao atualizar dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);

			}

			redirect('main/redirecionar/4/'.$this->input->post('id_grupo'));

		} else {

			$this->aviso('Falha ao editar','Erro(s) no formulário: '.validation_errors(),'error',true);
			redirect('main/redirecionar/4/'.$this->input->post('id_grupo'));

		}

	}

	public function filtro_ajax(){

		$this->load->model("model_relatorios");

		$parametros = array(); //Recebe os valores com nomes certos do filtro

		$parametros['tabela']           = $this->input->get("tabela");
		$parametros['filtro_campo']     = $this->input->get("filtro_campo");
		$parametros['filtro_ordenacao'] = $this->input->get("filtro_ordenacao");
		$parametros['filtro_ordem']     = $this->input->get("filtro_ordem");
		$parametros['filtro_limite']    = $this->input->get("filtro_limite");

		$this->model_relatorios->start();

		//Carregando os campos dinamicos que irei receber via get, que são todos do filtro.
		$campos = $this->model_relatorios->listarCampos($parametros['tabela'],$parametros['filtro_campo']);

		$filtro = array();
		foreach ($campos as $key => $campo) {
			if ($campo['descricao_campo'] != "") {
				$campo_valor = $this->input->get($campo['nome_campo']);
				if (isset($campo_valor) && $campo_valor != "") {
					$campo['valor'] = $campo_valor;
					$filtro[$campo['nome_campo']] = $campo;
				}
			} 
		}

		$resultados = $this->model_relatorios->filtroAjax($parametros,$filtro);

		if ($this->model_relatorios->commit()) {
			echo '<table class="table table-bordered table-hover" align="center">
				<thead align="center">';
				
					foreach ($campos as $chave => $campo) {
						if ($campo['selecionado']) {
							if ($campo['descricao_campo'] == "ID") 
								echo '<th style="width: 60px;" align="center" class="no-filter">Editar</th>';
							echo  "<th>{$campo['descricao_campo']}: </th>";
						}
					}

				echo '</thead>
				<tbody align="center">';	
					
					foreach ($resultados as $resultado) {

						echo "<tr>";
						foreach ($parametros['filtro_campo'] as $select_) {
							if ($select_ == "id")
							echo '<td><a href="'.base_url().'main/redirecionar/4/'.$resultado[$select_].'"><button class="btn btn-info"> <i class="glyphicon glyphicon-edit"> </i> Editar</button></a></td>';
							echo "<td>".$resultado[$select_]."</td>";
						}
						echo "</tr>";

					}
					
				echo '</tbody>
			</table>';

		}

	}

	public function aviso($titulo,$aviso,$tipo,$fixo){

		//Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
			$aviso_ = str_replace('
', '', $aviso);

		$aviso = str_replace('\'', '"', $aviso_);

		$this->session->set_flashdata('titulo_alerta',$titulo);
		$this->session->set_flashdata('mensagem_alerta',$aviso);
		$this->session->set_flashdata('tipo_alerta',$tipo);
		$this->session->set_flashdata('mensagem_fixa',$fixo);

	}

}