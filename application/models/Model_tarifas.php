<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_tarifas extends MY_Model {

		public function get_menus($where = "") {

			return $this->db->query('select titulo_menu as titulo, id_menu as id, menu_acima as anterior from seg_menu where menu_acima '.$where.' order by posicao_menu')->result();

		}

		public function get_lista_aplicacoes($id = null,$grupo = null){

			return $this->db->query('select sa.* from seg_aplicacao sa
									inner join seg_aplicacoes_menu as sam on sam.fk_aplicacao = id_aplicacao
									inner join seg_menu on fk_menu = id_menu
									inner join seg_aplicacoes_grupos as sag on sag.fk_aplicacao = id_aplicacao
									where menu_acima = '.$id.' and fk_grupo = '.$grupo)->result();

		}

		public function get_link($id = null,$grupo = null) { //VALIDAR ACESSO DO USUÁRIO********

			$consulta = $this->db->query('select 
											m.titulo_menu,
											a.link_aplicacao,
											a.id_aplicacao
											from seg_aplicacoes_menu am
											inner join seg_menu m on m.id_menu = am.fk_menu
											inner join seg_aplicacao a on a.id_aplicacao = am.fk_aplicacao
											inner join seg_aplicacoes_grupos as sag on sag.fk_aplicacao = id_aplicacao
											where fk_menu = '.$id.' and fk_grupo = '.$grupo.'
											group by titulo_menu, link_aplicacao, id_aplicacao');

			if($consulta->num_rows() > 0) { // não tem sub-menu, é um link direto

				return $consulta;

			} else { //Tem sub-menu, deve chamar o else e iniciar a recursividade, se necessário.

				return false;

			}

		}

		/*Criar*/

		public function listar(){

			return array('models' => $this->db->get('seg_models')->result(),
						 'controllers' => $this->db->get('seg_controllers')->result(),
						 'menus' => $this->db->get('seg_menu')->result());

		}

		public function criarMc($model,$controller){

			$this->db->insert('seg_models',$model);
			echo $this->db->last_query().';<br>';

			$controller['fk_model'] = $this->db->insert_id();

			$this->db->insert('seg_controllers',$controller);
			echo $this->db->last_query().';<br>';

		}

		public function criarMenu($menu){

			$menu['id_menu'] = $this->db->query('select max(id_menu)+1 as proximo from seg_menu where id_menu < 1000')->row()->proximo;

			$this->db->insert('seg_menu',$menu);
			echo $this->db->last_query().';<br>';

			return true;

		}

		public function criarAplicacao($aplicacao,$menu){

			$this->db->insert('seg_aplicacao',$aplicacao);
			$id = $this->db->insert_id();

			echo $this->db->last_query().';<br>';

			if ($menu != "") {
				$this->db->query('insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (
																'.$id.',
																'.$menu.');');
				echo $this->db->last_query().'<br>';
			}

			$this->db->query('insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(
																'.$id.',1);');

			echo $this->db->last_query().'<br>';

			return true;

		}







		 /*###########
		 Parte do jean
		 ############*/

        public function view_tarifas(){
            $dados['tarifa'] = $this->db->select('*, round(valor_tarifa,2) as valor_tarifa, round(valor_tarifa_minima,2) as valor_tarifa_minima')->get('cad_tarifa_corrida')->result_array();
            $dados['config'] = $this->db->select('*, round(taxa_cancelamento,2) as taxa_cancelamento')->get('cad_configuracoes')->row_array();
            /*$dados['config'] = $this->db->select('*, round(taxa_yougo,2) as taxa_yougo')->get('cad_configuracoes')->row_array();*/

            /*$dados['tarifa_cidade'] = $this->db->get('cad_tarifa_cidade')->result_array();*/
            $dados['tarifa_cidade'] = $this->db->query('select id_tarifa_cidade,fk_cidade,nome_tarifa,round(valor_tarifa,2) as valor_tarifa,valor_fixo,status_tarifa from cad_tarifa_cidade ')->result_array();
            $dados['cidade_certa'] = $this->db->get('cad_cidades_atuacao')->result_array();
            return $dados;

        }
        public function atualizar_tarifa1($dados_tarifa1){
            $this->db->where('id_tarifas_corrida','1');
            return $this->db->update('cad_tarifa_corrida',$dados_tarifa1);
            /*echo $this->db->last_query();
            echo '<br>';*/
        }
        public function atualizar_tarifa2($dados_tarifa2){
            $this->db->where('id_tarifas_corrida','2');
            return $this->db->update('cad_tarifa_corrida',$dados_tarifa2);
            /*echo $this->db->last_query();*/
        }
        public function configuracoes($config){
            $this->db->where('id_configuracoes','1');
            $this->db->update('cad_configuracoes',$config);
            /*echo $this->db->last_query();
            die();*/
        }

        public function tarifa_cidade($nova){
            $this->db->insert('cad_tarifa_cidade',$nova);
        }
        public function atulizar_tarifa_cidades($atulizar_tarifa_cidades,$id_taxa){
            $this->db->where('id_tarifa_cidade',$id_taxa);
            $this->db->update('cad_tarifa_cidade',$atulizar_tarifa_cidades);
        }


    }