<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Controller_webservice extends REST_Controller  {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['perfil_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['perfileditar_put']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('model_webservice');
        $this->load->model('model_usuarios');
        $this->load->model('model_motorista');
        $this->load->model('model_passageiro');
    }

    ######################################################
    /*Garante que quem está usando o sistema possúi credenciais de acesso.
    * @Param $facebook_usuario String opcional, É passado o ID do facebook no lugar do e-mail e senha
    * @Param $tipo avisa qual o perfil de acesso e aparelho usado
    */
    ######################################################
    public function autenticacao($facebook_usuario = null,$tipo = null){

        if(is_null($facebook_usuario)){

            $PHP_AUTH_USER  = isset($_SERVER['PHP_AUTH_USER'])  ? $_SERVER['PHP_AUTH_USER'] : null;
            $PHP_AUTH_PW    = isset($_SERVER['PHP_AUTH_PW'])    ? $_SERVER['PHP_AUTH_PW']   : null;

            $valores = array(
                'email_usuario' => $PHP_AUTH_USER,
                'senha_usuario' => $PHP_AUTH_PW
            );

            $this->form_validation->set_data($valores);
            $this->form_validation->set_rules('email_usuario','E-mail do Usuário','required');
            $this->form_validation->set_rules('senha_usuario','Senha do Usuário (Criptografada)','required');

        } else {

            $valores = array(
                'facebook_usuario' => $facebook_usuario
            );

            $this->form_validation->set_data($valores);
            $this->form_validation->set_rules('facebook_usuario','ID Facebook','required');

        }

        if ($this->form_validation->run()) {

            if($tipo == MOTORISTA_ANDROID || $tipo == MOTORISTA_IOS){
                $valores['fk_grupo_usuario'] = 2;
            } else {
                $valores['fk_grupo_usuario'] = 3;
            }

            if(is_null($facebook_usuario)){
                $autenticacao = $this->model_webservice->validar_login($valores);
            } else {
                $autenticacao = $this->model_webservice->validar_login_facebook($valores);
            }

            if($autenticacao) {

                if(!$autenticacao['ativado_sms']){ //Ativação via SMS
                    $this->response(array('status' => STATUS_FALHA_SMS, 'resultado' => 'Perfil pendente de ativação por SMS!', 'usuario' => array('celular_usuario' => $autenticacao['celular_usuario'],'id_usuario' => $autenticacao['id_usuario'])),Self::HTTP_OK); //403

                } else if(!$autenticacao['ativado_email']){ //Ativação via E-mail

                    $email              = $autenticacao['email_usuario'];
                    $fk_grupo_usuario   = $autenticacao['fk_grupo_usuario'];
                    $server             = $_SERVER['SERVER_NAME'];

                    $this->email->from('yougomobile@yougomobile.com.br', 'You GO | Confirmação de cadastro');
                    $this->email->to($email);
                    $this->email->subject('You GO | Confirmação de cadastro');
                    $this->email->message('
                        <body style="padding: 0;margin: 0"><div style="text-align: center;width: 100%;background: #f8d509;height: 200px;margin: 0;padding: 0">
                                </div>
                                    <div style="width: 80%;display: table;background: #fff;margin: 0 auto;text-align: center;margin-top: -100px;border-radius: 8px;box-shadow: 0 0 2px #ccc;font-family: Arial; ">
                                    <center><img src="http://'.$server.base_url().'/style/img/favicon.png" style="width: 200px;"></center>
                                        <h4 style="padding: 10px">Seu cadastro ainda precisa de uma confirmação, <br>confirme seu cadastro e sejá um usuário You GO.</h4>
                                <a href="http://'.$server.base_url().'/controller_webservice/ativar_email?email_confirmacao='.base64_encode($email).'&grupo='.$fk_grupo_usuario.'"><button style="background: #f8d509;border: 1px solid #f8d509;padding: 10px 15px;color: #292929;cursor: pointer;border-radius: 5px;cursor: pointer">Confirmar</button></a><br><br>

                                <div style="margin-top: -10px;font-size: 14px">Em: '.date('d/m/Y H:i:s').'</div><br>

                                </div>
                                <br>
                                <br>
                                </body>');

                    // Enviar...
                    if ($this->email->send()) {

                        $this->response(array('status' => STATUS_FALHA_EMAIL, 'resultado' => 'Perfil pendente de ativação por E-Mail!'),Self::HTTP_OK); //403

                    } else {
                        $this->response(array('status' => STATUS_FALHA_EMAIL, 'resultado' => 'Perfil pendente de ativação por E-Mail!, falha no envio'),Self::HTTP_OK); //403
                    }


                } else if(!$autenticacao['ativo_usuario']){ //Usuário Inativo
                    $this->response(array('status' => PERFIL_INATIVO, 'resultado' => 'Perfil pendente de ativação ou inativo'),Self::HTTP_OK); //403
                } else {
                    return $autenticacao;
                }
            } else {
                $this->response(array('status' => STATUS_FALHA, 'resultado' => 'Dados incorretos ou perfil inativo!'),Self::HTTP_OK); //404
            }

        } else { //Campos em branco.

            $erros = strip_tags(validation_errors());
            $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

        }

    }

    ######################################################
    // Autenticação via token
    ######################################################
    public function autenticacao_token($perfil = null){

        $token = $this->getAuthorizationHeader();
        if(!is_null($token)){
            //Buscando dados do usuário.
            $dados = $this->model_webservice->validar_token($token);

            if(isset($dados)) {

                $dados = $dados->row_array();
                if(!$dados['ativo']) {
                    $this->response(array('status' => STATUS_FALHA, 'resultado' => 'Perfil Inativo'),Self::HTTP_OK); //404
                } else if(!$dados['token_ativo']) {
                    $this->response(array('status' => TOKEN_EXPIROU, 'resultado' => 'Token expirou'),Self::HTTP_OK); //404
                } else {

                    if(!is_null($perfil) && $perfil != $dados['fk_grupo_usuario']) {
                        $this->response(array('status' => STATUS_PERFIL, 'resultado' => 'Perfil Incorreto para essa chamada'),Self::HTTP_OK); //404
                    } else {
                        return $dados;
                    }

                }

            } else {
                $this->response(array('status' => STATUS_FALHA, 'resultado' => 'Token incorreto!'),Self::HTTP_OK); //404
            }


        } else {
            $this->response(array('status' => STATUS_FALHA, 'resultado' => 'Token Bearer necessário!'),Self::HTTP_OK); //404
        }

    }

    public function validar_token_get(){
     
        $autenticacao = $this->autenticacao_token();
        if($autenticacao){ 
            $this->response(array('status' => STATUS_OK,
                                  'resultado' => "Login Ativo"
                                 ),Self::HTTP_OK); //200
        }


    }   

    ######################################################
    //WS1 Login
    ######################################################
    public function login_usuario_get(){

        $tipo   = $this->get('tipo_acesso');
        $token  = $this->get('token'); //Push

        $autenticacao = $this->autenticacao($this->get('facebook_usuario'),$tipo);
        if($autenticacao){



            $usuario = array (

                "id_usuario"         => $autenticacao['id_usuario'],
                "nome_usuario"       => $autenticacao['nome_usuario'],
                "email_usuario"      => $autenticacao['email_usuario'],
                "senha_usuario"      => $autenticacao['senha_usuario'],
                "token_acesso"       => $autenticacao['token_acesso'],
                "ultima_corrida"     => $autenticacao['ultima_corrida']

            );

            //Token para push
            $this->model_webservice->atualizarInserirToken($autenticacao['id_usuario'],$tipo,$token);

            if($autenticacao['fk_grupo_usuario'] == MOTORISTA) {

                if($tipo == MOTORISTA_ANDROID || $tipo == MOTORISTA_IOS){
                    $this->response(array('status' => STATUS_OK, 'resultado' => "Login realizado com sucesso", 'usuario' => $usuario, 'ajuda' => $this->model_webservice->dados_empresa(),"dados_bancarios" => $this->model_webservice->dados_bancarios($autenticacao['id_usuario'])),Self::HTTP_OK); //200
                } else {
                    $this->response(array('status' => STATUS_PERFIL,
                                          'resultado' => "Baixe o APP do Passageiro."
                                         ),Self::HTTP_OK); //200
                }


            } else {

                if($tipo == PASSAGEIRO_ANDROID || $tipo == PASSAGEIRO_IOS){
                    $this->response(array('status' => STATUS_OK, 'resultado' => "Login realizado com sucesso", 'usuario' => $usuario, 'ajuda' => $this->model_webservice->dados_empresa()),Self::HTTP_OK); //200
                } else {
                    $this->response(array('status' => STATUS_PERFIL,
                                          'resultado' => "Baixe o APP do Passageiro."
                                         ),Self::HTTP_OK); //200
                }

            }
        }

    }

    ######################################################
    //WS1.1 Ativação SMS
    ######################################################
    public function ativar_sms_post(){

        if($this->model_webservice->ativarSms($this->post('id_usuario'))) {

            $this->response(array(
                'status' => STATUS_OK,
                'resultado' => "Ativação realizada com sucesso."),Self::HTTP_OK); //200

        } else {

            $this->response(array(
                'status' => STATUS_FALHA,
                'resultado' => "Ativação falhou."),Self::HTTP_OK); //200

        }

    }


    ######################################################
    //WS1.2 Ativação SMS
    ######################################################
    public function ativar_email_get(){

        $email              = $this->get('email_confirmacao');
        $fk_grupo_usuario   = $this->get('fk_grupo_usuario');
        $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
        $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
        $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
        $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
        $webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

        if(isset($email) && $this->model_webservice->ativarEmail(base64_decode($email))) {

            if($iPod || $iPhone || $iPad){
                if ($fk_grupo_usuario == MOTORISTA) {
                    header("Location: https://www.apple.com/br"); //@TODO Atualizar link
                } else {
                    header("Location: https://www.apple.com/br"); //@TODO Atualizar link
                }
                die();
            } else if($Android){

                if ($fk_grupo_usuario == MOTORISTA) {
                    header("Location: https://www.android.com/"); //@TODO Atualizar link
                } else {
                    header("Location: https://www.android.com/"); //@TODO Atualizar link
                }
                die();
            } else { //Caso abra pelo Navegador de MAC / PC / Linux

                // @jean Deixar layout bonito.
                echo "E-mail validado com sucesso. <br> Baixe sua versão para a <a href=\"https://www.android.com/\">Android</a> ou <a href=\"https://www.apple.com/br/\">iOS</a>";
            }

        } else {
            // @jean Deixar layout bonito.
            if($iPod || $iPhone || $iPad){
                echo "Ativação do e-mail falhou!";
                die();
            } else if($Android){
                echo "Ativação do e-mail falhou!";
                die();
            } else { //Caso abra pelo Navegador de MAC / PC / Linux
                echo "Ativação do e-mail falhou!";
            }

        }

    }

    ######################################################
    //WS2 Recuperar Senha
    ######################################################
    public function esqueci_senha_get(){

        $valores = array('email_usuario' => $this->get('email_usuario'));

        $this->form_validation->set_data($valores);
        $this->form_validation->set_rules('email_usuario','E-mail do Usuário','required');

        if ($this->form_validation->run()) {

            $this->load->helper('string');
            $senha = random_string('alnum',10);

            $email = $this->model_usuarios->senha_Email(sha1($senha),$this->get('email_usuario'));

            //Usuário inativo ou desativado
            if($email == ""){

                $this->response(array('status' => STATUS_FALHA, 'resultado' => 'Dados incorretos ou perfil inativo!'),Self::HTTP_OK); //404

            } else { //Senha enviada para o E-mail.

                // Detalhes do Email.
                $this->email->from('suporte@yougo.com', 'YouGO | Troca de senha');
                $this->email->to($email);
                $this->email->subject('YouGO | Troca de senha');
                $this->email->message('<h1>
                                            <a href="http://'.$_SERVER['HTTP_HOST'].base_url().'">
                                                YouGO
                                            </a>
                                           </h1>
                                    Recebemos sua solicitação de nova senha. <br>
                                    Sua nova senha agora é: <strong>'.$senha.'</strong><br>
                                    <img src="http://'.$_SERVER['HTTP_HOST'].base_url().'/style/img/rodape.png" alt="Rodapé">');

                // Enviar...
                if ($this->email->send()) {

                    $this->response(array('status' => STATUS_OK, 'resultado' => "Nova Senha enviada para o E-mail: (".$email.")"),Self::HTTP_OK); //200

                } else {

                    $this->response(array('status' => STATUS_FALHA, 'resultado' => "Erro ao enviar senha: ".$this->email->print_errorger()),Self::HTTP_OK); //400

                }

            }


        } else { //Campos Preenchidos

            $erros = strip_tags(validation_errors());
            $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

        }

    }

    ######################################################
    //WS3 Busca CEP
    ######################################################
    public function buscar_cep_get(){

        $valores = array('cep'  => $this->get('cep'));

        $this->form_validation->set_data($valores);
        $this->form_validation->set_rules('cep','CEP','required');

        if ($this->form_validation->run()) {

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://viacep.com.br/ws/{$valores['cep']}/json/unicode",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "Cache-Control: no-cache",
                    "Content-Type: Application/json"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            $dado = json_decode($response);
            $dados['cep']         = !isset($dado->cep)         ? "" : $dado->cep;
            $dados['logradouro']  = !isset($dado->logradouro)  ? "" : $dado->logradouro;
            $dados['complemento'] = !isset($dado->complemento) ? "" : $dado->complemento;
            $dados['bairro']      = !isset($dado->bairro)      ? "" : $dado->bairro;
            $dados['localidade']  = !isset($dado->localidade)  ? "" : $dado->localidade;
            $dados['uf']          = !isset($dado->uf)          ? "" : $dado->uf;
            $dados['uf_id']          = !isset($dado->uf)       ? "" : $this->model_webservice->filtroUf($dado->uf);

            if ($err) {

                $this->response(array('status' => STATUS_FALHA, 'resultado' => $err),Self::HTTP_OK); //400

            } else {

                $this->response(array('status' => STATUS_OK, 'resultado' => "Retorno do via cep.", "endereco" => $dados),Self::HTTP_OK); //200

            }

        } else {

            $erros = strip_tags(validation_errors());
            $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

        }

    }

    ######################################################
    //WS4 Lista Modelos
    ######################################################
    public function busca_modelo_get(){

        $valores = array(
            'id_montadora' => $this->get('id_montadora')
        );

        $this->form_validation->set_data($valores);
        $this->form_validation->set_rules('id_montadora','ID do Montadora','required');

        if ($this->form_validation->run()) {

            $modelos = $this->model_webservice->buscarModelo($valores['id_montadora'],$this->get('nome'));

            $this->response(array('status' => STATUS_OK, 'resultado' => "Lista dos modelos:", 'modelos' => $modelos),Self::HTTP_OK); //200

        } else { //Campos Preenchidos

            $erros = strip_tags(validation_errors());
            $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

        }

    }

    ######################################################
    //WS5 Pré Cadastro Motorista
    ######################################################
    public function pre_cadastro_motorista_get(){

        $dados = $this->model_webservice->listarPreCadastroMotorista();
        $this->response(array('status'    => STATUS_OK,
            'resultado' => "Listas para pré cadastro do motorista:",
            'generos'   => $dados['generos'],
            'cidades'   => $dados['cidades'],
            'marcas'    => $dados['marcas']),Self::HTTP_OK); //200


    }

    ######################################################
    //WS6 Cadastro Motorista
    ######################################################
    public function cadastro_motorista_post(){

        $regras = $this->model_motorista->regras();
        $nomes  = $this->model_motorista->nomes();

        $dados = $_POST;
        $dados['ativo_usuario']    = 0; //Inicia inativo
        $dados['fk_grupo_usuario'] = 2; //Motorista

        $dados['login_usuario']    = $dados['email_usuario'];
        $dados['telefone_usuario']  = $this->removerMascara($dados['telefone_usuario']);
        $dados['rg_usuario']        = $this->removerMascara($dados['rg_usuario']);
        $dados['cpf_usuario']       = $this->removerMascara($dados['cpf_usuario']);
        $dados['celular_usuario']   = $this->removerMascara($dados['celular_usuario']);
        $dados['cep_usuario']       = $this->removerMascara($dados['cep_usuario']);

        if ($this->model_webservice->validar_email_unico($dados['email_usuario'],$dados['fk_grupo_usuario'])->num_rows() > 0) {

                $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "O E-mail deve ser único por perfil."),
                        Self::HTTP_OK); //400

        } else if ($this->model_webservice->validar_cpf_unico($dados['cpf_usuario'],$dados['fk_grupo_usuario'])->num_rows() > 0) {

                $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "O CPF deve ser único por perfil."),
                        Self::HTTP_OK); //400

        } else if ($this->validarCampos($dados,$regras,$nomes)) {

            $this->model_webservice->start();
            $id = $this->model_webservice->cadastroMotorista($dados);

            //inserindo as imagens
            if ($id == 0) {

                $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "Falha no cadastro do motorista id 0.",
                        'campos' => $_POST),Self::HTTP_OK); //200

            } else if(  //Sem erros no formulário.
                $this->base64ToImage($id['id'],$this->post('motorista'),                "motorista","motorista") &&
                $this->base64ToImage($id['id'],$this->post('cnh'),                      "cnh","motorista") &&
                $this->base64ToImage($id['id'],$this->post('seguro'),                   "seguro","motorista") &&
                $this->base64ToImage($id['id'],$this->post('documento'),                "documento","motorista") &&
                $this->base64ToImage($id['id'],$this->post('veiculo_1'),                "veiculo_1","motorista") &&
                $this->base64ToImage($id['id'],$this->post('veiculo_2'),                "veiculo_2","motorista") &&
                $this->base64ToImage($id['id'],$this->post('veiculo_3'),                "veiculo_3","motorista") &&
                $this->base64ToImage($id['id'],$this->post('rg'),                       "rg","motorista") &&
                $this->base64ToImage($id['id'],$this->post('antecedentes'),             "antecedentes","motorista") &&
                //$this->base64ToImage($id['id_passageiro'],$this->post('motorista'),     "passageiro","passageiro") &&
                $this->base64ToImage($id['id'],$this->post('comprovante'),              "comprovante","motorista")) {

                $commit = $this->model_webservice->commit();

                if ($commit['status']) {

                    $this->response(array('status' => STATUS_OK, 'resultado' => "Motorista cadastrado com sucesso.",'id' => $id['id']),Self::HTTP_OK); //200

                } else {

                    $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "Falha no cadastro do motorista commit.",
                        'log' => $commit),Self::HTTP_OK); //200
                }


            } else {
                $commit = $this->model_usuarios->rollback();
                $this->response(array('status' => STATUS_FALHA, 'resultado' => "Falha no upload das imagens",'campos' => $_POST),Self::HTTP_OK); //200
            }

        }

    }

    ######################################################
    //WS7 Pré Edição Motorista
    ######################################################
    public function pre_editar_motorista_get(){

        $autenticacao = $this->autenticacao_token(MOTORISTA);
        if($autenticacao){
            $dados = $this->model_webservice->preEditarMotorista($autenticacao['id_usuario']);

            $this->response(array('status'      => STATUS_OK,
                'resultado'      => "Listas para pré edição do motorista:",
                'generos'        => $dados['generos'],
                'cidades'        => $dados['cidades'],
                'marcas'         => $dados['marcas'],
                'motorista'      => $dados['motorista'],
                'veiculo'        => $dados['veiculo']),Self::HTTP_OK); //200
        }

    }

    ######################################################
    //WS8 Editar Cadastro Motorista
    ######################################################
    public function editar_cadastro_motorista_post(){

        $dados = $_POST;

        if(isset($dados['dashboard'])){

            $autenticacao['email_usuario']  = $this->post('email_usuario_original');
            $autenticacao['cpf_usuario']    = $this->post('cpf_usuario_original');
            $autenticacao['id_usuario']     = $this->post('id_usuario');

            unset($dados['dashboard']);
            unset($dados['cpf_usuario_original']);
            unset($dados['email_usuario_original']);

        } else {
            $autenticacao = $this->autenticacao_token(MOTORISTA);
        }

        if($autenticacao){

            $regras = $this->model_motorista->regras();
            $nomes = $this->model_motorista->nomes();

            unset($dados['tipo']);

            //removendo mascaras, se for o caso.
            if(isset($dados['telefone_usuario'])) {
                $dados['telefone_usuario'] = $this->removerMascara($dados['telefone_usuario']);
            }
            if(isset($dados['rg_usuario'])) {
                $dados['rg_usuario'] = $this->removerMascara($dados['rg_usuario']);
            }
            if(isset($dados['cpf_usuario'])) {
                    $dados['cpf_usuario'] = $this->removerMascara($dados['cpf_usuario']);
            }
            if(isset($dados['celular_usuario'])) {
                $dados['celular_usuario'] = $this->removerMascara($dados['celular_usuario']);
            }
            if(isset($dados['cep_usuario'])) {
                $dados['cep_usuario'] = $this->removerMascara($dados['cep_usuario']);
            }


            if(isset($dados['email_usuario']) && $dados['email_usuario'] == $autenticacao['email_usuario']) {
                unset($dados['email_usuario']); //E-mail não foi alterado
            } else {
                if(isset($dados['email_usuario'])){
                    $dados['login_usuario']    = $dados['email_usuario'];

                    if ($this->model_webservice->validar_email_unico($dados['email_usuario'],2)->num_rows() > 0) {

                            $this->response(array('status' => STATUS_FALHA,
                                    'resultado' => "O E-mail deve ser único por perfil."),
                                    Self::HTTP_OK); //200
                            //die();

                    }
                }
            }

            if(isset($dados['cpf_usuario']) && $dados['cpf_usuario'] == $autenticacao['cpf_usuario']) {
                unset($dados['cpf_usuario']); //CPF não foi alterado
            } else if (isset($dados['cpf_usuario']) && $this->model_webservice->validar_cpf_unico($dados['cpf_usuario'],2)->num_rows() > 0) {

                    $this->response(array('status' => STATUS_FALHA,
                            'resultado' => "O CPF deve ser único por perfil."),
                            Self::HTTP_OK); //200
                    //die();

            }

            if(isset($dados['senha_usuario']) && $dados['senha_usuario'] == "") {
                unset($dados['senha_usuario']);
            }

            if($this->validarCamposEditar($dados,$regras,$nomes)) {
                $tipo_edicao = $this->post('tipo');
                if ($tipo_edicao == 1) { //Edição do usuário

                    $dados['id_usuario'] = $autenticacao['id_usuario'];

                } else if ($tipo_edicao == 2) { //Edição da tabela do motorista.
                    unset($dados['id_usuario']);
                    $dados['fk_usuario'] = $autenticacao['id_usuario'];

                }

                $this->model_webservice->start();
                $this->model_webservice->editarMotorista($dados,$tipo_edicao);
                $commit = $this->model_webservice->commit();

                if ($commit['status']) {

                  $this->email->from('yougomobile@yougomobile.com.br', 'You GO | Perfil Editado - Cadastro Voltou para aprovação');
                  $this->email->to($autenticacao['email_usuario']);
                  $this->email->subject('You GO | Perfil Editado - Cadastro Voltou para aprovação');
                  $this->email->message('');

                  // Enviar...
                  if ($this->email->send()) {

                    $this->response(array(  'status' => STATUS_OK,
                        'resultado' => "Motorista editado com succeso."),Self::HTTP_OK); //200

                  } else {
                    $this->response(array(  'status' => STATUS_OK,
                        'resultado' => "Motorista editado com succeso, falhou o envio de E-mail"),Self::HTTP_OK); //200
                  }


                } else {

                    $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "Falha na edição.",
                        'log' => $commit),Self::HTTP_OK); //200
                }

            }

        }

    }

    ######################################################
    //WS8.1 Editar Foto motorista
    ######################################################
    public function editar_foto_motorista_post(){

        $autenticacao = $this->autenticacao_token(MOTORISTA);
        if($autenticacao){

            $id = $autenticacao['id_usuario'];

            if( $this->base64ToImage($id,$this->post('motorista'),  "motorista","motorista") &&
                $this->base64ToImage($id,$this->post('cnh'),           "cnh",   "motorista") &&
                $this->base64ToImage($id,$this->post('seguro'),     "seguro",   "motorista") &&
                $this->base64ToImage($id,$this->post('documento'),  "documento","motorista") &&
                $this->base64ToImage($id,$this->post('veiculo_1'),  "veiculo_1","motorista") &&
                $this->base64ToImage($id,$this->post('veiculo_2'),  "veiculo_2","motorista") &&
                $this->base64ToImage($id,$this->post('veiculo_3'),  "veiculo_3","motorista") &&
                $this->base64ToImage($id,$this->post('rg'),         "rg",       "motorista") &&
                $this->base64ToImage($id,$this->post('antecedentes'),"antecedentes","motorista") &&
                $this->base64ToImage($id,$this->post('motorista'),  "passageiro","passageiro") &&
                $this->base64ToImage($id,$this->post('comprovante'),"comprovante","motorista")) {


                $this->model_webservice->start();
                $this->model_webservice->editarMotorista(array('id_usuario' => $id));
                $commit = $this->model_webservice->commit();

                if ($commit['status']) {

                    $server = $_SERVER['SERVER_NAME'];

                  $this->email->from('yougomobile@yougomobile.com.br', 'You GO | Perfil Editado - Cadastro Voltou para aprovação');
                  $this->email->to($autenticacao['email_usuario']);
                  $this->email->subject('You GO | Perfil Editado - Cadastro Voltou para aprovação');
                  $this->email->message('
                  <body style="padding: 0;margin: 0"><div style="text-align: center;width: 100%;background: #f8d509;height: 200px;margin: 0;padding: 0">
                          </div>
                              <div style="width: 80%;display: table;background: #fff;margin: 0 auto;text-align: center;margin-top: -100px;border-radius: 8px;box-shadow: 0 0 2px #ccc;font-family: Arial; ">
                              <center><img src="http://'.$server.base_url().'/style/img/favicon.png" style="width: 200px;"></center>
                                  <h4 style="padding: 10px">Você foi desativado por alguma edição realizada, aguarde a nova avaliação do YouGo</h4>
                          </div>
                          <br>
                          <br>
                          </body>');

                  // Enviar...
                  if ($this->email->send()) {

                    $this->response(array('status' => STATUS_OK, 'resultado' => "Foto(s) do motorista editada(s) com succeso."),Self::HTTP_OK); //200

                  } else {
                    $this->response(array('status' => STATUS_OK, 'resultado' => "Foto(s) do motorista editada(s) com succeso, falha no envio do E-mail"),Self::HTTP_OK); //200
                  }

                } else {

                    $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "Falha na edição.",
                        'log' => $commit),Self::HTTP_OK); //200
                }


            } else {

                $this->response(array('status' => STATUS_FALHA, 'resultado' => "Falha ao editar"),Self::HTTP_OK); //200

            }


        }

    }

    ######################################################
    //WS9 Ficar online
    ######################################################
    public function motorista_disponivel_post(){

        $autenticacao = $this->autenticacao_token(MOTORISTA);
        if($autenticacao){

            $status = $this->post('online');
            if(isset($status)){

                $this->model_webservice->ficarOnline($status,$autenticacao['id_usuario']);

                if($status){

                    $this->response(array('status' => STATUS_OK, 'resultado' => "Sucesso na mudança de status para online"),Self::HTTP_OK); //200

                } else {

                    $this->response(array('status' => STATUS_OK, 'resultado' => "Sucesso na mudança de status para offline"),Self::HTTP_OK); //200

                }

            } else {
                $this->response(array('status' => STATUS_FALHA, 'resultado' => "Campo status é obrigatório"),Self::HTTP_OK); //200
            }

        }

    }

    ######################################################
    //WS10 Lista bancos
    ######################################################
    public function listar_bancos_get(){

        $autenticacao = $this->autenticacao_token(MOTORISTA);
        if($autenticacao){
            $bancos = $this->model_webservice->buscarBancos();
            $this->response(array('status' => STATUS_OK, 'resultado' => "Lista dos bancos:", 'bancos' => $bancos),Self::HTTP_OK); //200
        }

    }

    ######################################################
    //WS10.1 Cadastrar / Editar Dados bancários
    ######################################################
    public function motorista_dados_bancarios_post(){

        $autenticacao = $this->autenticacao_token(MOTORISTA);
        if($autenticacao){
            $regras = $this->model_motorista->regras_bancos();
            $nomes = $this->model_motorista->nomes_bancos();
            //fk_usuario
            $dados = $_POST;
            $dados['fk_usuario'] = $autenticacao['id_usuario'];

            if($this->validarCampos($dados,$regras,$nomes)) {

                $this->model_webservice->start();
                $this->model_webservice->atualizarDadosBancarios($dados);

                $commit = $this->model_webservice->commit();

                if ($commit['status']) {

                    $this->response(array('status' => STATUS_OK, 'resultado' => "Dados bancários inseridos com sucesso!"),Self::HTTP_OK); //200

                } else {

                    $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "Falha no cadastro / edição.",
                        'log' => $commit),Self::HTTP_OK); //200
                }

            }

        }

    }

    ######################################################
    //WS11 Cadastro Cliente
    ######################################################
    public function cadastro_passageiro_post(){

        $regras = $this->model_passageiro->regras();
        $nomes = $this->model_passageiro->nomes();

        $dados = $_POST;
        unset($dados['passageiro']);
        $dados['ativo_usuario']    = 1;
        $dados['ativado_email']    = 1; //Não precisa mais ativação pelo E-mail.
        $dados['fk_grupo_usuario'] = 3; //Passageiro
        $dados['login_usuario']    = $dados['email_usuario'];

        $dados['telefone_usuario']  = $this->removerMascara($dados['telefone_usuario']);
        $dados['rg_usuario']        = $this->removerMascara($dados['rg_usuario']);
        $dados['cpf_usuario']       = $this->removerMascara($dados['cpf_usuario']);
        $dados['celular_usuario']   = $this->removerMascara($dados['celular_usuario']);
        $dados['cep_usuario']       = $this->removerMascara($dados['cep_usuario']);

        if ($this->model_webservice->validar_email_unico($dados['email_usuario'],$dados['fk_grupo_usuario'])->num_rows() > 0) {

                $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "O E-mail deve ser único por perfil."),
                        Self::HTTP_OK); //200

        } else if ($this->model_webservice->validar_cpf_unico($dados['cpf_usuario'],$dados['fk_grupo_usuario'])->num_rows() > 0) {

                $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "O CPF deve ser único por perfil."),
                        Self::HTTP_OK); //200

        } else if($this->validarCampos($dados,$regras,$nomes)) {

            $this->model_webservice->start();

            $id = $this->model_webservice->cadastroPassageiro($dados);

            //inserindo foto do perfil
            if( $id > 0 && //Sem erros no formulário.
                $this->base64ToImage($id,$this->post('passageiro'),  "passageiro","passageiro")) {

                $commit = $this->model_webservice->commit();

                if ($commit['status']) {

                    $this->response(array('status' => STATUS_OK, 'resultado' => "Passageiro cadastrado com sucesso.",'id' => $id),Self::HTTP_OK); //200

                } else {

                    $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "Falha no cadastro do passageiro.",
                        'log' => $commit),Self::HTTP_OK); //200
                }


            } else {
                $commit = $this->model_usuarios->rollback();
                $this->response(array('status' => STATUS_FALHA, 'resultado' => "Falha no upload da imagem"),Self::HTTP_OK); //200
            }

        }

    }

    ######################################################
    //WS12 Pré Edição cliente
    ######################################################
    public function pre_editar_cliente_get(){

        $autenticacao = $this->autenticacao_token(PASSAGEIRO);
        if($autenticacao){
            $dados = $this->model_webservice->preEditarCliente($autenticacao['id_usuario']);

            $this->response(array('status'    => STATUS_OK,
                'resultado' => "Listas para pré edição do cliente:",
                'cliente' => $dados),Self::HTTP_OK); //200
        }

    }

    ######################################################
    //WS13 Edição Cliente
    ######################################################
    public function editar_passageiro_post(){

        $autenticacao = $this->autenticacao_token(PASSAGEIRO);
        if($autenticacao){

            $regras = $this->model_passageiro->regras();
            $nomes  = $this->model_passageiro->nomes();

            $dados = $_POST;
            unset($dados['passageiro']);

            $campos_array = array('telefone_usuario','rg_usuario','cpf_usuario','celular_usuario','cep_usuario');
            foreach ($campos_array as $key => $campo) {
                if (isset($dados[$campo])) {
                    $dados[$campo]  = $this->removerMascara($dados[$campo]);
                }
            }

            if(isset($dados['email_usuario'])) {
                $dados['login_usuario'] = $dados['email_usuario'];
            }

            //MANDOU UM E-MAIL NOVO E JÁ UTLIZADO
            if (isset($dados['email_usuario']) &&
                $dados['email_usuario'] != $autenticacao['email_usuario'] && 
                $this->model_webservice->validar_email_unico($dados['email_usuario'],$autenticacao['fk_grupo_usuario'])->num_rows() > 0) {

                    $this->response(array('status' => STATUS_FALHA,
                            'resultado' => "O E-mail deve ser único por perfil."),
                            Self::HTTP_OK); //200

            } else if (isset($dados['cpf_usuario']) && 
                            $dados['cpf_usuario'] != $autenticacao['cpf_usuario'] && 
                            $this->model_webservice->validar_cpf_unico($dados['cpf_usuario'],$autenticacao['fk_grupo_usuario'])->num_rows() > 0) {

                        $this->response(array('status' => STATUS_FALHA,
                                'resultado' => "O CPF deve ser único por perfil."),
                                Self::HTTP_OK); //200

                } else if($this->validarCamposEditar($dados,$regras,$nomes)) {

                    $this->model_webservice->start();
                    $dados['id_usuario'] = $autenticacao['id_usuario'];
                    $this->model_webservice->editarPassageiro($dados);

                    //inserindo foto do perfil
                    if($this->base64ToImage($autenticacao['id_usuario'],$this->post('passageiro'),  "passageiro","passageiro")) {

                        $commit = $this->model_webservice->commit();

                        if ($commit['status']) {

                            $this->response(array('status' => STATUS_OK, 'resultado' => "Passageiro editado com sucesso."),Self::HTTP_OK); //200

                        } else {

                            $this->response(array('status' => STATUS_FALHA,
                                'resultado' => "Falha na edição do passageiro.",
                                'log' => $commit),Self::HTTP_OK); //200
                        }


                    } else {
                        $commit = $this->model_usuarios->rollback();
                        $this->response(array('status' => STATUS_FALHA, 'resultado' => "Falha no upload da imagem"),Self::HTTP_OK); //200
                    }

                }

            }

        }

    ######################################################
    //WS14 Cadastro forma de Pagamento
    ######################################################
    public function nova_forma_pagamento_post() {

        $autenticacao = $this->autenticacao_token(PASSAGEIRO);
        if($autenticacao){

            $valores = array(
                'nome_cartao'               => $this->post('nome_cartao'),
                'numero_cartao'             => $this->post('numero_cartao'),
                'data_vencimento_cartao'    => $this->post('data_vencimento_cartao'),
                'cvv_cartao'                => $this->post('cvv_cartao'),
                'data_nascimento'           => $this->post('data_nascimento'),
                'cartao_ativo'              => 1,
                'status_pag_seguro'         => 0
            );

            $this->form_validation->set_data($valores);
            $this->form_validation->set_rules('nome_cartao',            'Nome cartão','required');
            $this->form_validation->set_rules('numero_cartao',          'Número cartão','required|callback_validar_num_cartao['.$autenticacao['id_usuario'].'|0]');
            $this->form_validation->set_rules('data_vencimento_cartao', 'Data vencimento','required');
            $this->form_validation->set_rules('cvv_cartao',             'CVV cartão','required');
            $this->form_validation->set_rules('data_nascimento',        'Data nascimento','required');

            $valores['fk_usuario'] = $autenticacao['id_usuario'];

            if ($this->form_validation->run()) {

                $this->model_webservice->start();
                $id = $this->model_webservice->novaFormaPagamento($valores);
                $commit = $this->model_webservice->commit();

                if ($commit['status']) {

                    $this->response(array('status' => STATUS_OK,
                        'resultado' => "Dados cadastrados com sucesso",
                        'id' => $id
                    ),Self::HTTP_OK); //200

                } else {

                    $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "Falha ao inserir.",
                        'log' => $commit),Self::HTTP_OK);
                }

            } else { //Campos Preenchidos

                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

            }

        }

    }

    public function validar_num_cartao($cartao,$detalhes){

        $dados = explode('|', $detalhes);

        $consulta = $this->model_webservice->validarNumCartao($cartao,$dados[0],$dados[1]);

        if (isset($consulta) && $consulta->num_rows() == 0) {
            return TRUE;
        } else {

            if($consulta->row()->cartao_ativo) {
                $this->form_validation->set_message('validar_num_cartao', 'O cartão deve ser único por usuário');
                return FALSE;
            } else {
                $this->model_webservice->reativarFormaPagamento($consulta->row()->id_pagamento);
                $this->form_validation->set_message('validar_num_cartao', 'Cartão reativado com succeso');
                return FALSE;
            }
        }

    }

    ######################################################
    //WS15 Listar Formas de pagamento
    ######################################################
    public function listar_formas_pagamento_get(){

        $autenticacao = $this->autenticacao_token(PASSAGEIRO);
        if($autenticacao){

            $dados = $this->model_webservice->listarFormasPagamentos($autenticacao['id_usuario']);
            $this->response(array('status'    => STATUS_OK,
                'resultado' => "Listas das formas de pagamento:",
                'formas' => $dados),Self::HTTP_OK); //200
        }

    }

    ######################################################
    //WS15.1 Remover Formas de pagamento
    ######################################################
    public function remover_forma_pagamento_get(){

        $autenticacao = $this->autenticacao_token(PASSAGEIRO);
        if($autenticacao){

            $valores = array('id_pagamento' => $this->get('id_pagamento'));
            $this->form_validation->set_data($valores);
            $this->form_validation->set_rules('id_pagamento', 'ID da forma de pagamento','required');

            if ($this->form_validation->run()) {

                $this->model_webservice->removerFormaPagamento($valores['id_pagamento']);
                $this->response(array('status' => STATUS_OK,
                                      'resultado' => "Forma de pagamento removida")
                                ,Self::HTTP_OK); //200

            } else { //Campos Preenchidos

                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

            }

        }

    }

    ######################################################
    //WS16 Editar forma pagamento
    ######################################################
    public function editar_forma_pagamento_post() {

        $autenticacao = $this->autenticacao_token(PASSAGEIRO);
        if($autenticacao){

            $valores = array(
                'id_pagamento'              => $this->post('id_pagamento'),
                'nome_cartao'               => $this->post('nome_cartao'),
                'numero_cartao'             => $this->post('numero_cartao'),
                'data_vencimento_cartao'    => $this->post('data_vencimento_cartao'),
                'cvv_cartao'                => $this->post('cvv_cartao'),
                'data_nascimento'           => $this->post('data_nascimento'),
                'status_pag_seguro'         => 0
            );

            $this->form_validation->set_data($valores);
            $this->form_validation->set_rules('id_pagamento',          'ID da forma de pagamento','required');
            $this->form_validation->set_rules('nome_cartao',           'Nome cartão','required');
            $this->form_validation->set_rules('numero_cartao',         'Número cartão','required|callback_validar_num_cartao['.$autenticacao['id_usuario'].'|'.$valores['id_pagamento'].']');
            $this->form_validation->set_rules('data_vencimento_cartao','Data vencimento','required');
            $this->form_validation->set_rules('cvv_cartao',            'CVV cartão','required');
            $this->form_validation->set_rules('data_nascimento',        'Data nascimento','required');

            $valores['fk_usuario'] = $autenticacao['id_usuario'];

            if ($this->form_validation->run()) {

                $this->model_webservice->start();
                $this->model_webservice->editarFormaPagamento($valores);
                $commit = $this->model_webservice->commit();

                if ($commit['status']) {

                    $this->response(array('status' => STATUS_OK,
                        'resultado' => "Dados editados com sucesso", 'id' => $valores['id_pagamento']
                    ),Self::HTTP_OK); //200

                } else {

                    $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "Falha ao editar.",
                        'log' => $commit),Self::HTTP_OK);
                }

            } else { //Campos Preenchidos

                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

            }

        }

    }

    public function distancia_tempo_google($percurso){

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://maps.googleapis.com/maps/api/directions/json?origin={$percurso['latitude_origem']},{$percurso['longitude_origem']}&destination={$percurso['latitude_destino']},{$percurso['longitude_destino']}&sensor=false&mode=driving",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {

            $response = json_decode($response);
            if(isset($response) && count($response->routes) > 0){
              $km     = round($response->routes[0]->legs[0]->distance->value/1000);
              $time   = round($response->routes[0]->legs[0]->duration->value/60);

              return array('km' => $km,'time' => $time);
            } else {

              return array('km' => -1,'time' => -1);
            }


        }

    }

    ######################################################
    //WS17 Custo corrida
    ######################################################
    public function custo_corrida_get(){

        $autenticacao = $this->autenticacao_token(PASSAGEIRO);
        if($autenticacao){
            $dados = array(
                'fk_cidade_origem'  => $this->get('cidade_origem'),
                'latitude_origem'   => $this->get('latitude_origem'),
                'longitude_origem'  => $this->get('longitude_origem'),
                'latitude_destino'  => $this->get('latitude_destino'),
                'longitude_destino' => $this->get('longitude_destino'),
            );

            $this->form_validation->set_data($dados);
            $this->form_validation->set_rules('fk_cidade_origem',   'Cidade origem',    'required');
            $this->form_validation->set_rules('latitude_origem',    'Latitude Origem',  'required');
            $this->form_validation->set_rules('longitude_origem',   'Longitude Origem', 'required');
            $this->form_validation->set_rules('latitude_destino',   'Latitude Destino', 'required');
            $this->form_validation->set_rules('longitude_destino',  'Longitude Destino','required');

            if ($this->form_validation->run()) {
                //Custo Corrida + Validação de viabilidade
                $distancia_tempo_google = $this->distancia_tempo_google($dados);

                $validar = $this->model_webservice->validarCorrida($distancia_tempo_google['km'],$dados['fk_cidade_origem']);

                if(!$validar) {
                  $this->response(array('status' => STATUS_FALHA,
                      'resultado' => "Cidade fora da lista"),Self::HTTP_OK);
                } else if(!$validar['geral']->km_valido) {

                    $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "KM acima do permitido (Limite ".$validar['geral']->km_maximo.")."),Self::HTTP_OK);

                } else {

                    $cupom = $this->get('cupom');
                    if(isset($cupom)){

                        $dados_cupons = $this->model_webservice->dados_cupom($cupom);

                        if(isset($dados_cupons) && $dados_cupons->num_rows() > 0) {

                            $dados_cupons = $dados_cupons->row();

                            //Calculando o valor da corrida com o cupom.
                            if($dados_cupons->tipo_desconto_cupom == 0){
                                $valor = $validar['geral']->valor_corrida - $dados_cupons->valor_desconto_cupom;
                            } else {
                                $valor = $validar['geral']->valor_corrida - (($validar['geral']->valor_corrida * $dados_cupons->valor_desconto_cupom)/100);
                            }

                            if($valor < 0){
                                $valor = 0;
                            }



                            $this->response(array('status' => STATUS_OK,
                                          'resultado' => "Custo da corrida",
                                          'valor' => $valor,
                                          'cupom' => $dados_cupons,
                                          'km' => $distancia_tempo_google['km'],
                                          'tempo' => $distancia_tempo_google['time']),Self::HTTP_OK);

                        } else {

                            $this->response(array('status' => STATUS_OK,
                                          'resultado' => "Custo da corrida, Cupom inválido.",
                                          'valor' => $validar['geral']->valor_corrida,
                                          'cupom' => null,
                                          'km' => $distancia_tempo_google['km'],
                                          'tempo' => $distancia_tempo_google['time']),Self::HTTP_OK);

                        }




                    } else {
                        $this->response(array('status' => STATUS_OK,
                                          'resultado' => "Custo da corrida",
                                          'valor' => $validar['geral']->valor_corrida,
                                          'km' => $distancia_tempo_google['km'],
                                          'tempo' => $distancia_tempo_google['time']),Self::HTTP_OK);
                    }

                }

            } else { //Campos Preenchidos

                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

            }

        }

    }

    ######################################################
    //WS17.1 Solicitar Corrida
    ######################################################
    public function solicitar_corrida_post(){

        $autenticacao = $this->autenticacao_token(PASSAGEIRO);
        if($autenticacao){

            $dados = array(
                'fk_passageiro'         => $autenticacao['id_usuario'],
                'km_corrida'            => $this->post('km_corrida'),
                'qtd_malas'             => $this->post('qtd_malas'),
                'qtd_passageiros'       => $this->post('qtd_passageiros'),
                'latitude_origem'       => $this->post('latitude_origem'),
                'longitude_origem'      => $this->post('longitude_origem'),
                'endereco_origem'       => $this->post('endereco_origem'),
                'latitude_destino'      => $this->post('latitude_destino'),
                'longitude_destino'     => $this->post('longitude_destino'),
                'endereco_destino'      => $this->post('endereco_destino'),
                'fk_cidade_origem'      => $this->post('cidade_origem'),
                'fk_forma_pagamento'    => $this->post('fk_forma_pagamento'),
                'fk_status_corrida'     => BUSCANDO_MOTORISTA
            );

            $this->form_validation->set_data($dados);
            $this->form_validation->set_rules('fk_passageiro','ID do passageiro','required');
            $this->form_validation->set_rules('km_corrida','KM da corrida','required');
            $this->form_validation->set_rules('qtd_malas','Quantidade de malas','required');
            $this->form_validation->set_rules('qtd_passageiros','Quantidade de passageiros','required');
            $this->form_validation->set_rules('latitude_origem','Latitude origem','required');
            $this->form_validation->set_rules('longitude_origem','Longitude origem','required');
            $this->form_validation->set_rules('endereco_origem','Endereço origem','required');
            $this->form_validation->set_rules('latitude_destino','Latitude destino','required');
            $this->form_validation->set_rules('longitude_destino','Longitude destino','required');
            $this->form_validation->set_rules('fk_cidade_origem','Cidade origem','required');
            $this->form_validation->set_rules('fk_forma_pagamento','Forma de pagamento','required');
            $this->form_validation->set_rules('endereco_destino','Endereço destino','required');

            if ($this->form_validation->run()) {
                //Custo Corrida + Validação de viabilidade
                $validar = $this->model_webservice->validarCorrida($dados['km_corrida'],$dados['fk_cidade_origem']);

                if(!isset($validar['geral'])) {

                    $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "Cidade '{$this->post('cidade_origem')}' fora da lista"),Self::HTTP_OK);

                } else if(isset($validar['geral']->km_valido) && !$validar['geral']->km_valido) {

                    $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "KM acima do permitido (Limite ".$validar['geral']->km_maximo.")."),Self::HTTP_OK);

                } else if (!isset($validar['geral']->fk_cidade)) {

                    $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "Cidade '{$this->post('cidade_origem')}' fora da lista"),Self::HTTP_OK);

                } else {

                    //Copiando dados únicos para a corrida
                    $dados['fk_cidade_origem']      = $validar['geral']->fk_cidade;
                    $dados['valor_corrida']         = $validar['geral']->valor_corrida;
                    $dados['taxa_cancelamento']     = $validar['geral']->taxa_cancelamento;
                    $dados['taxa_yougo']            = $validar['geral']->taxa_yougo;
                    $dados['valor_tarifa']          = $validar['geral']->valor_tarifa;
                    $dados['valor_tarifa_minima']   = $validar['geral']->valor_tarifa_minima;
                    $fk_cupom                       = $this->post('fk_cupom');

                    if(isset($fk_cupom) && $fk_cupom > 0){
                        $dados['fk_cupom'] = $this->model_webservice->validar_cupom($fk_cupom);
                    }

                    $this->model_webservice->start();
                    $id = $this->model_webservice->solicitarCorrida($dados);
                    $commit = $this->model_webservice->commit();

                    if ($commit['status']) {

                        $this->solicitar_motorista($id);
                        //Copiando n taxas
                        $this->model_webservice->historicoTaxasCorrida($validar['taxas'],$id);

                        $this->response(array('status' => STATUS_OK,
                            'resultado' => "Dados inseridos com sucesso",
                            'id_corrida' => $id
                        ),Self::HTTP_OK); //200

                    } else {

                        $this->response(array('status' => STATUS_FALHA,
                            'resultado' => "Falha ao cadastrar.",
                            'log' => $commit),Self::HTTP_OK);
                    }

                }


            } else { //Campos Preenchidos

                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

            }

        }

    }

    public function comprovante_corrida_get() {

        $fk_corrida = $this->get('fk_corrida');
        $usuario    = $this->get('usuario');
        $dados = array('fk_corrida' => $fk_corrida,'usuario' => $usuario);

        $this->form_validation->set_data($dados);
        $this->form_validation->set_rules('fk_corrida'  ,'ID da corrida','required');
        $this->form_validation->set_rules('usuario'     ,'Tipo de usuário','required');

        if ($this->form_validation->run()) {

            $dados = $this->model_webservice->comprovanteCorrida($fk_corrida);

            $taxa_yougo = $dados['geral']->taxa_yougo;
            if($dados['geral']->fk_status_corrida == CANCELADO_PASSAGEIRO) {
                $corrida = $dados['geral']->taxa_cancelamento;
            } else {
                $corrida = $dados['geral']->valor_corrida;
            }

            $lucro_yougo = (($corrida*$taxa_yougo)/100);

            $descontos = $lucro_yougo;

            echo '<html lang="pt-br">
                <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
                        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
                    </head>

                <body style="padding:0;margin:0;font-family: Arial;text-align: center">
                <div style="width: 100%;height: 250px;background: url(\'http://'.$_SERVER['SERVER_NAME'].base_url().'style/img/bk_tudo.jpg\') center center no-repeat;background-size: cover"></div>
                <div style="width: 95%;background: #fff;height: 300px;border-radius: 8px;box-shadow: 0 0 4px #ccc;margin: 0 auto;margin-top: -180px;display: table;padding-bottom: 10px;">
                    <br>
                    <p>Custo da corrida </p>';

                    if($dados['geral']->fk_status_corrida == CANCELADO_PASSAGEIRO) {
                        echo '<h1 style="margin-top: 5px;"><span style="color: red">'.$this->formatMoeda($dados['geral']->taxa_cancelamento).'</span> </h1>
                        <small>Valor caso não tivesse cancelado: '.$this->formatMoeda($corrida).'</small> <br>';
                    } else if($dados['geral']->fk_status_corrida == CANCELADO_MOTORISTA) {
                        echo '<h1 style="margin-top: 5px;"><span style="color: green">R$ 0,00</span> </h1>
                        <small>Valor seria de: '.$this->formatMoeda($corrida).'</small> <br>';
                    } else {

                        if($usuario == MOTORISTA){

                            echo '<h1 style="margin-top: 5px;"><span style="font-size: 12px;font-weight: normal"></span>'.$dados['geral']->valor_corrida_original.'</h1>';

                        } else {

                            echo '<h1 style="margin-top: 5px;"><span style="font-size: 12px;font-weight: normal"></span>'.$dados['geral']->custo_final.'</h1>';

                        }

                        
                    }

                    if(!is_null($dados['geral']->cupom)){
                        echo '<small>Cupom usado: '.$dados['geral']->cupom.' de desconto.</small> <br>';
                        echo '<small>Valor sem cupom: '.$this->formatMoeda($dados['geral']->valor_corrida).'.</small> <br>';
                    }

                    echo '
                    <small>Valor mínimo: '.$this->formatMoeda($dados['geral']->valor_tarifa_minima).'</small>
                    <hr style="width: 80%;margin: 0 auto">
                    <br>
                    <div style="width: 95%;margin: 0 auto">
                    ';

            echo'
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-size: 12px;text-align: left">YouGO <small>('.$taxa_yougo.'%)</small></div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-size: 12px;">'.$this->formatMoeda($lucro_yougo).'</div>



                    ';
            foreach ($dados['taxas'] as $chave => $taxa) {
                if($taxa->valor_fixo) {
                    echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"style="font-size: 12px;text-align: left">'.$taxa->nome_tarifa.':</div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-size: 12px;">'.$this->formatMoeda($taxa->valor_tarifa).'</div>';
                    $descontos += $taxa->valor_tarifa;
                } else {

                    $calculo_taxa = (($corrida*$taxa->valor_tarifa)/100);

                    echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-size: 12px;text-align: left">'.$taxa->nome_tarifa.': <small>('.$taxa->valor_tarifa.'%)<small></div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-size: 12px;">'.$this->formatMoeda($calculo_taxa).'
                            </div>';
                    $descontos += $calculo_taxa;
                }
            }
            echo'

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: left">&nbsp;</div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8" style="font-size: 12px;text-align: left">Valor Total descontado:</div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-4" style="font-size: 12px;">'.$this->formatMoeda($descontos).'</div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8" style="font-size: 12px;text-align: left">Valor Final Motorista:</div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-4" style="font-size: 12px;">'.$this->formatMoeda(($corrida - $descontos)).'</div>';

                    if($dados['geral']->fk_status_corrida != CANCELADO_PASSAGEIRO) {
                        echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: left">&nbsp;</div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: left;color: red"><small style="font-size: 13px">Tarifa caso tivesse cancelado: '.$this->formatMoeda($dados['geral']->taxa_cancelamento).'</small></div>';
                    }

                    echo '</div>
                </div>
                </body>

                ';

        } else { //Campos Preenchidos

            $erros = strip_tags(validation_errors());
            $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

        }

    }

    ######################################################
    //WS18 Deslicamento Motorista
    ######################################################
    public function deslocamento_motorista_post(){

        $autenticacao = $this->autenticacao_token(MOTORISTA);
        if($autenticacao){

            $dados = array(
                'fk_usuario'                    => $autenticacao['id_usuario'],
                'latitude_atual'                => $this->post('latitude_atual'),
                'longitude_atual'               => $this->post('longitude_atual')
            );

            $this->form_validation->set_data($dados);
            $this->form_validation->set_rules('fk_usuario',     'ID do passageiro','required');
            $this->form_validation->set_rules('latitude_atual', 'Latitude','required');
            $this->form_validation->set_rules('longitude_atual','Longitude','required');

            if ($this->form_validation->run()) {

                $this->model_webservice->start();
                $this->model_webservice->deslocamentoMotorista($dados);

                $commit = $this->model_webservice->commit();

                if ($commit['status']) {

                    $id_corrida = $this->post('id_corrida');

                    $corridaEmEspera = $this->model_webservice->corridaEmEspera($autenticacao['id_usuario']);
                    if(isset($corridaEmEspera) && !is_null($corridaEmEspera)){
                        $this->notificando_get( "Nova corrida",
                                                "Chegou uma nova corrida YouGo",
                                                $autenticacao['id_usuario']);
                    }

                    if(isset($id_corrida)) {

                        // Verifica a distancia do motorista para a origem / destino, caso atenda o mínimo irá notificar o passageiro
                        $distancia_minima = $this->model_webservice->distanciaMinima($id_corrida,$dados['latitude_atual'],$dados['longitude_atual']);

                        if(isset($distancia_minima) && !is_null($distancia_minima)) {

                            if($distancia_minima->avisar_origem) {

                                $this->notificando_get("Motorista próximo","Seu Yougo está chegando.",$this->model_webservice->descobrirPassageiro($id_corrida));

                            } else if ($distancia_minima->avisar_destino) {

                                $this->notificando_get("Destino próximo","Seu destino está próximo.",$this->model_webservice->descobrirPassageiro($id_corrida));

                            }

                        }

                        $this->response(array('status' => STATUS_OK,
                            'resultado'         => "Dados atualizados com sucesso",
                            'motorista_online'  => $autenticacao['motorista_online'],
                            'dados_corrida'     => $corridaEmEspera,
                            'status_corrida'    => $this->model_webservice->statusCorrida($id_corrida)
                        ),Self::HTTP_OK); //200

                    } else {
                        $this->response(array('status' => STATUS_OK,
                            'resultado' => "Dados atualizados com sucesso",
                            'motorista_online'  => $autenticacao['motorista_online'],
                            'dados_corrida' => $corridaEmEspera
                        ),Self::HTTP_OK); //200

                    }


                } else {

                    $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "Falha ao cadastrar.",
                        'log' => $commit),Self::HTTP_OK);
                }


            } else { //Campos Preenchidos

                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

            }

        }

    }

    ######################################################
    //WS19 Cancelar Corrida
    ######################################################
    public function cancelar_corrida_post(){

        $autenticacao = $this->autenticacao_token();
        if($autenticacao){

            $id_corrida     = $this->post('id_corrida');
            $justificativa  = $this->post('justificativa');
            
            $gratis         = $this->post('gratis'); //Caso seja cancelada a tempo ou não exista motorista

            if(isset($id_corrida) && isset($justificativa)){

                if($autenticacao['fk_grupo_usuario'] == MOTORISTA){ // Motorista

                    $this->model_webservice->cancelarCorrida($id_corrida,$justificativa,$autenticacao['id_usuario'],CANCELADO_MOTORISTA,null);

                    //Notificar passageiro do cancelamento:
                    $this->notificando_get("Corrida Cancelada!","Motorista cancelou a corrida",$this->model_webservice->descobrirPassageiro($id_corrida));

                    $this->response(array('status' => STATUS_OK, 'resultado' => "Corrida cancelada pelo motorista"),Self::HTTP_OK); //200

                } else {

                    $this->model_webservice->cancelarCorrida($id_corrida,$justificativa,$autenticacao['id_usuario'],CANCELADO_PASSAGEIRO,$gratis);

                    //Notificar motorista do cancelamento:
                    $this->notificando_get("Corrida Cancelada!","Passageiro cancelou a corrida",$this->model_webservice->descobrirMotorista($id_corrida));

                    $this->response(array('status' => STATUS_OK, 'resultado' => "Corrida cancelada pelo cliente"),Self::HTTP_OK); //200

                }

            } else {
                $this->response(array('status' => STATUS_FALHA, 'resultado' => "Campo ID corrida e justificativa são obrigatórios"),Self::HTTP_OK); //200
            }

        }

    }
    ######################################################
    //WS20 Acompanhar Corrida
    ######################################################
    public function acompanhar_corrida_post(){

        $autenticacao = $this->autenticacao_token();
        if($autenticacao){

            $dados = array(
                'fk_corrida' => $this->post('id_corrida')
            );

            $this->form_validation->set_data($dados);
            $this->form_validation->set_rules('fk_corrida','ID da corrida','required');


            if ($this->form_validation->run()) {

                $corrida = $this->model_webservice->acompanharCorrida($autenticacao['fk_grupo_usuario'],$autenticacao['id_usuario'],$dados['fk_corrida']);
                $this->solicitar_motorista($dados['fk_corrida']);

                $this->response(array('status' => STATUS_OK,
                    'resultado' => "Dados sobre a corrida",
                    'dados'   => $corrida->row()
                ),Self::HTTP_OK); //200


            } else { //Campos Preenchidos

                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

            }

        }

    }
    ######################################################
    //WS21 Avaliação
    ######################################################
    public function avaliar_corrida_post(){

        $autenticacao = $this->autenticacao_token();
        if($autenticacao){

            if($autenticacao['fk_grupo_usuario'] == MOTORISTA){ // Motorista

                $dados = array(
                    'fk_corrida'                => $this->post('id_corrida'),
                    'pontualidade_passageiro' => $this->post('pontualidade_passageiro'),
                    'simpatia_passageiro'       => $this->post('simpatia_passageiro')
                );

                $this->form_validation->set_data($dados);
                $this->form_validation->set_rules('fk_corrida',             'ID da corrida','required');
                $this->form_validation->set_rules('pontualidade_passageiro','Pontualidade', 'required');
                $this->form_validation->set_rules('simpatia_passageiro',    'Simpatia',     'required');

            } else {

                $dados = array(
                    'fk_corrida'                => $this->post('id_corrida'),
                    'pontualidade_motorista'    => $this->post('pontualidade_motorista'),
                    'simpatia_motorista'        => $this->post('simpatia_motorista'),
                    'carro_motorista'           => $this->post('carro_motorista'),
                    'servico_motorista'         => $this->post('servico_motorista')
                );

                $this->form_validation->set_data($dados);
                $this->form_validation->set_rules('fk_corrida',             'ID da corrida','required');
                $this->form_validation->set_rules('pontualidade_motorista', 'Pontualidade', 'required');
                $this->form_validation->set_rules('simpatia_motorista',     'Simpatia',     'required');
                $this->form_validation->set_rules('carro_motorista',        'Carro',        'required');
                $this->form_validation->set_rules('servico_motorista',      'Serviço',      'required');

            }

            if ($this->form_validation->run()) {

                $this->model_webservice->start();
                $this->model_webservice->avaliarCorrida($dados,$autenticacao['fk_grupo_usuario']);
                $commit = $this->model_webservice->commit();

                if ($commit['status']) {

                    if($autenticacao['fk_grupo_usuario'] == MOTORISTA){
                        $this->response(array('status' => STATUS_OK,
                            'resultado' => "Avaliação inserida com sucesso para o passageiro"
                        ),Self::HTTP_OK); //200
                    } else {
                        $this->response(array('status' => STATUS_OK,
                            'resultado' => "Avaliação inserida com sucesso para o motorista"
                        ),Self::HTTP_OK); //200
                    }

                } else {

                    $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "Falha ao editar.",
                        'log' => $commit),Self::HTTP_OK);
                }


            } else { //Campos Preenchidos

                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

            }

        }

    }
    ######################################################
    //WS22 Motorista Aceita / Rejeita Corrida
    ######################################################
    public function responder_corrida_post(){

        $autenticacao = $this->autenticacao_token(MOTORISTA);
        if($autenticacao){

            $dados = array(
                'id_solicitacao_corrida'    => $this->post('id_solicitacao_corrida'),
                'id_corrida'                => $this->post('id_corrida'),
                'fk_status_corrida'         => $this->post('resposta')
            );

            $this->form_validation->set_data($dados);
            $this->form_validation->set_rules('id_solicitacao_corrida',  'ID da solicitação',   'required');
            $this->form_validation->set_rules('id_corrida',              'ID da corrida',       'required');
            $this->form_validation->set_rules('fk_status_corrida',       'Resposta',            'required');


            if ($this->form_validation->run()) {

                $this->model_webservice->start();
                $status = $this->model_webservice->responderCorrida($dados);
                $commit = $this->model_webservice->commit();

                if ($commit['status']) {

                    if($status == 1){

                        if($dados['fk_status_corrida'] == BUSCANDO_MOTORISTA){
                            //busca um novo motorista
                            $this->solicitar_motorista($dados['id_corrida']);
                            $this->response(array('status' => STATUS_OK,
                                'resultado' => "Corrida Recusada com sucesso"
                            ),Self::HTTP_OK); //200
                        } else {

                            $motorista = $this->model_webservice->detalhesMotorista($dados['id_corrida']);

                            if(isset($motorista) && !is_null($motorista)){

                                //Notificar passageiro do cancelamento:
                                $this->notificando_get(
                                    "Motorista '{$motorista->motorista}' à caminho",
                                    "Carro: {$motorista->modelo} Placa: {$motorista->placa}",
                                    $this->model_webservice->descobrirPassageiro($dados['id_corrida'])
                                );

                                $this->response(array('status' => STATUS_OK,
                                    'resultado' => "Corrida Aceita com sucesso"
                                ),Self::HTTP_OK); //200

                            }

                        }

                    } else if ($status == -1){

                        $this->response(array('status' => STATUS_FALHA,
                            'resultado' => "Não respondido a tempo"),Self::HTTP_OK);

                    } else if ($status == -2){

                        $this->response(array('status' => STATUS_FALHA,
                            'resultado' => "Corrida já foi aceita"),Self::HTTP_OK);

                    } else if ($status == -3){

                        $this->response(array('status' => STATUS_FALHA,
                            'resultado' => "Corrida já foi recusada"),Self::HTTP_OK);

                    } else {

                        $this->response(array('status' => STATUS_FALHA,
                            'resultado' => "Falha ao aceitar / recusar corrida"),Self::HTTP_OK);

                    }

                } else {

                    $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "Falha na resposta.",
                        'log' => $commit),Self::HTTP_OK);
                }


            } else { //Campos Preenchidos

                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

            }

        }

    }
    ######################################################
    //WS23 Iniciar Corrida
    ######################################################
    public function iniciar_corrida_post(){

        $autenticacao = $this->autenticacao_token(MOTORISTA);
        if($autenticacao){

            $atualizar = array(
                'id_corrida'                 => $this->post('id_corrida'),
                'latitude_inicio_corrida'    => $this->post('latitude_atual'),
                'longitude_inicio_corrida'   => $this->post('longitude_atual'),
                'fk_status_corrida'          => EM_ANDAMENTO
            );

            $this->form_validation->set_data($atualizar);
            $this->form_validation->set_rules('id_corrida',                 'ID da corrida',    'required');
            $this->form_validation->set_rules('latitude_inicio_corrida',    'Latitude',         'required');
            $this->form_validation->set_rules('longitude_inicio_corrida',   'Longitude',        'required');

            if ($this->form_validation->run()) {

                $this->model_webservice->start();

                $this->model_webservice->iniciarCorrida($atualizar,$autenticacao['id_usuario']);
                $commit = $this->model_webservice->commit();

                if ($commit['status']) {

                    //Notificar passageiro que o motorista iniciou a corrida:
                    $this->notificando_get("Corrida Iniciada!","Motorista iniciou a corrida",$this->model_webservice->descobrirPassageiro($atualizar['id_corrida']));

                    $this->response(array('status' => STATUS_OK,
                        'resultado' => "Corrida Iniciada com sucesso"
                    ),Self::HTTP_OK); //200

                } else {

                    $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "Falha na resposta.",
                        'log' => $commit),Self::HTTP_OK);
                }


            } else { //Campos Preenchidos

                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

            }

        }

    }
    ######################################################
    //WS24 Finalizar Corrida
    ######################################################
    public function finalizar_corrida_post(){

        $autenticacao = $this->autenticacao_token(MOTORISTA);
        if($autenticacao){

            $atualizar = array(
                'id_corrida'             => $this->post('id_corrida'),
                'latitude_fim_corrida'   => $this->post('latitude_atual'),
                'longitude_fim_corrida'  => $this->post('longitude_atual'),
                'fk_status_corrida'      => CORRIDA_FINALIZADA
            );

            $this->form_validation->set_data($atualizar);
            $this->form_validation->set_rules('id_corrida',             'ID da corrida',    'required');
            $this->form_validation->set_rules('latitude_fim_corrida',   'Latitude',         'required');
            $this->form_validation->set_rules('longitude_fim_corrida',  'Longitude',        'required');


            if ($this->form_validation->run()) {

                $this->model_webservice->start();

                $this->model_webservice->finalizarCorrida($atualizar,$autenticacao['id_usuario']);
                $commit = $this->model_webservice->commit();

                if ($commit['status']) {

                    //Notificar passageiro que o motorista iniciou a corrida:
                    $this->notificando_get("Corrida Finalizada!","Motorista finalizou a corrida",$this->model_webservice->descobrirPassageiro($atualizar['id_corrida']));

                    $this->response(array('status' => STATUS_OK,
                        'resultado' => "Corrida Finalizada com sucesso"
                    ),Self::HTTP_OK); //200

                } else {

                    $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "Falha na resposta.",
                        'log' => $commit),Self::HTTP_OK);
                }


            } else { //Campos Preenchidos

                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

            }

        }

    }

    ######################################################
    //WS25 Viagens
    ######################################################
    public function listar_corridas_get(){

        $autenticacao = $this->autenticacao_token();
        if($autenticacao){
            $dados = $this->model_webservice->listarCorridas($autenticacao['id_usuario'],$autenticacao['fk_grupo_usuario']);

            $this->response(array(  'status'      => STATUS_OK,
                                    'resultado'  => "Listas de corridas",
                                    'corridas'   => $dados),Self::HTTP_OK); //200
        }

    }

    ######################################################
    //26 Média Avaliações
    ######################################################
    public function media_avaliacoes_get(){

        $autenticacao = $this->autenticacao_token();
        if($autenticacao){
            $avaliacoes = $this->model_webservice->mediaAvaliacoes($autenticacao['id_usuario'],$autenticacao['fk_grupo_usuario']);

            $this->response(array(  'status'      => STATUS_OK,
                                    'resultado'  => "Listas de avaliações",
                                    'avaliacoes'   => $avaliacoes),Self::HTTP_OK); //200
        }

    }

    ######################################################
    //27 Pagamentos
    ######################################################
    public function hist_pagamentos_get(){

        $autenticacao = $this->autenticacao_token(MOTORISTA);
        if($autenticacao){

            $data_corrida       = $this->input->get('data_corrida');
            $data_fim_corrida   = $this->input->get('data_fim_corrida');
            $limit              = $this->input->get('quantidade_desejada');
            $offset             = $this->input->get('quantidade_atual');
            $pago               = $this->input->get('pago');

            $ganhos = $this->model_webservice->histPagamentos($autenticacao['id_usuario'],$data_corrida, $data_fim_corrida,$limit, $offset,$pago);

            $this->response(array(  'status'      => STATUS_OK,
                                    'resultado'  => "Listas de pagamentos",
                                    'ganhos'   => $ganhos),Self::HTTP_OK); //200
        }

    }












    ######################################################
    //Formatação monetária
    ######################################################
    public function formatMoeda($valor){
        return 'R$ '.number_format($valor, 2, ',', '.');
    }
    ######################################################
    //Criar solicitação
    ######################################################
    public function solicitar_motorista($id_corrida){

        //Verficiar se existe uma solicitação em aberto para a corrida
        $solicitacao_anterior = $this->model_webservice->existeSolicitacao($id_corrida);

        if(is_null($solicitacao_anterior)) {

            log_message('error', 'Primeira chamada');

            $this->model_webservice->criarSolicitacoes($id_corrida);
        } else if (!is_null($solicitacao_anterior->solicitacao_aceita) && !$solicitacao_anterior->solicitacao_aceita) {

            log_message('error', 'Solicitação rejeitada.');

            $this->model_webservice->cancelarSolicitacoes($solicitacao_anterior->id_solicitacao_corrida);
            $this->model_webservice->criarSolicitacoes($id_corrida);


        } else if(!$solicitacao_anterior->em_tempo && is_null($solicitacao_anterior->solicitacao_aceita)){//Existe uma solicitação vencida ou recusada.

            log_message('error', 'Passou do tempo e não aceitou nem rejeitou');

            $this->model_webservice->cancelarSolicitacoes($solicitacao_anterior->id_solicitacao_corrida);
            $this->model_webservice->criarSolicitacoes($id_corrida);
        } else if (!is_null($solicitacao_anterior->solicitacao_aceita) && $solicitacao_anterior->solicitacao_aceita) { //Retorna os detalhes do motorista que aceitou.
            log_message('error', 'corrida aceita');
            return $solicitacao_anterior;

        }

    }
    ######################################################
    //Remove mascaras, deixando somente números
    ######################################################
    public function removerMascara($valor) {
        if(isset($valor)){
            return preg_replace('/[^0-9a-zA-Z]/', '', $valor);
        } else {
            return null;
        }
    }
    ######################################################
    //Validando campos
    ######################################################
    public function validarCampos($campos,$regras,$nomes){

        $this->form_validation->set_data($campos);
        foreach ($regras as $nome_campo => $regra) {
            $this->form_validation->set_rules($nome_campo,$nomes[$nome_campo],$regra);
        }

        if (!$this->form_validation->run()) {

            $erros = strip_tags(validation_errors());
            $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

        } else {
            return true;
        }

    }
    ######################################################
    //Validando campos Ediçao, podendo enviar somente o campo que deseja
    ######################################################
    public function validarCamposEditar($campos,$regras,$nomes){

        $this->form_validation->set_data($campos);
        foreach ($regras as $nome_campo => $regra) {
            if(isset($campos[$nome_campo])){
                $this->form_validation->set_rules($nome_campo,$nomes[$nome_campo],$regra);
            }
        }

        if (!$this->form_validation->run()) {

            $erros = strip_tags(validation_errors());
            $this->response(array('status' => STATUS_FALHA,
                'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

        } else {

            return true;

        }

    }
    ######################################################
    //Upload de imagens em Base64 para .PNG
    ######################################################
    function base64ToImage($id,$imagem,$nome = '',$perfil){

        if (!is_null($imagem) && $imagem != '') {

            $imagem = str_replace("\n", "", $imagem);
            $imagem = str_replace("data:image/png;base64,", "", $imagem);

            $imagem = str_replace("\r", "", $imagem);

            $imagem = str_replace("\\n", "", $imagem);

            $imagem = str_replace("\\r", "", $imagem);

            $imagem = str_replace(" ", "+", $imagem);

            $fileName = $nome.'.png';
            $imagem = base64_decode($imagem);
            $endereco = $_SERVER['DOCUMENT_ROOT'].base_url().'upload/'.$perfil.'s/'.$perfil.'_'.$id;

            if (!file_exists($endereco)) {
                mkdir($endereco, 0777, true);
            }

            if(file_exists($endereco.'/'.$fileName)) {
                chmod($endereco,0777);
                unlink($endereco.'/'.$fileName);
            }

            return file_put_contents($endereco.'/'.$fileName, $imagem);
        }

        return true;
    }


    ######################################################
    //Nofitiações PUSH
    ######################################################
    public function notificando_get($titulo = null,$mensagem = null,$id_usuario = null) {

        if(!is_null($id_usuario)) {

            $tokens = $this->model_webservice->carregarFirebaseTokens($id_usuario);

            $devicesToken = array();

            $resultadoHTML = "";

            $prontos    = 0; //Garante que passou por todos token
            $titulo     = isset($titulo)    ? $titulo   : strtoupper($this->input->get("titulo"));
            $mensagem   = isset($mensagem)  ? $mensagem : strtoupper($this->input->get("mensagem"));

            while ($prontos < $tokens->num_rows()) {

                $devicesToken[] = $tokens->row($prontos)->token;
                //echo $tokens->row($prontos)->token;

                $prontos += 1;
                if(count($devicesToken) == 999) { //Garante que não passará o limite de envio
                    $gcpm = new FCMPushMessage();
                    $gcpm->setDevices($devicesToken);

                    $response = $gcpm->send(array('mensagem' => $mensagem,'titulo' => $titulo));
                    //Atualizar token ou remover, caso necessário.
                    $resultadoHTML .= $this->atualizarBD($response, $devicesToken);

                    $resultadoHTML .= "<br><hr><br>";

                    $devicesToken = array(); //zera o array, e fica pronto para mais 999 tokens

                }

            }

            if(count($devicesToken) > 0) { //Garante que se não entrar no if do while, enviara os tokens existentes no array.
                $gcpm = new FCMPushMessage();
                $gcpm->setDevices($devicesToken);

                $response = $gcpm->send(array('mensagem' => $mensagem,'titulo' => $titulo));

                $resultadoHTML .= $this->atualizarBD($response, $devicesToken);

                $resultadoHTML .= "<br><hr><br>";

            }
            //Conferir envio das notificações.
            //echo $resultadoHTML;
            //echo $tokens->row(0)->token;
            $this->response(array('status' => STATUS_OK, 'resultado' => "Nova publicação enviada com sucesso! notificado a {$prontos} aparelhos. {$titulo} / {$mensagem}"),Self::HTTP_OK); //400

        }


    }

    public function atualizarBD($response = null, $devicesToken = null) {
        // RESULT JSON
                $html = '';
                $resultJson = json_decode($response);
                foreach($resultJson as $key=>$value){
                    if(is_array($value)){
                        $html .= $key.'=>{<br/>';
                        $i = 0;

                        foreach($value as $k=>$v){
                            $html .= '&nbsp;&nbsp;&nbsp;&nbsp;{&nbsp;';
                            foreach($v as $kObj=>$vObj){
                                $html .= $kObj.'=>'.$vObj;

                                // UPDATE REG ID
                                    if(strcasecmp($kObj, 'registration_id') == 0 && strlen( trim($vObj) ) > 0){

                                        $novo = trim($vObj);

                                        if($this->model_webservice->atualizarToken($novo,$devicesToken[$i])) {
                                        $html .= " Token : {$devicesToken[$i]} Atualizado com sucesso para: {$novo}";
                                        } else {
                                            $html .= " Falha na atualização.";
                                        }

                                    }
                                // DELETE REG ID
                                    else if(strcasecmp($kObj, 'error') == 0 && strcasecmp($vObj, 'NotRegistered') == 0){

                                        if($this->model_webservice->deletarToken($devicesToken[$i])) {
                                            $html .= " Token: {$devicesToken[$i]} Deletado com sucesso. NotRegistered";
                                        } else {
                                            $html .= " Token: {$devicesToken[$i]} Falhou ao deletar. NotRegistered";
                                        }

                                    }
                                    else if(strcasecmp($kObj, 'error') == 0 && strcasecmp($vObj, 'MismatchSenderId') == 0){

                                        if($this->model_webservice->deletarToken($devicesToken[$i])) {
                                            $html .= " Token: {$devicesToken[$i]} Deletado com sucesso. MismatchSenderId";
                                        } else {
                                            $html .= " Token: {$devicesToken[$i]} Falhou ao deletar. MismatchSenderId";
                                        }

                                    } else if(strcasecmp($kObj, 'error') == 0 && strcasecmp($vObj, 'InvalidRegistration') == 0){

                                        if($this->model_webservice->deletarToken($devicesToken[$i])) {
                                            $html .= " Token: {$devicesToken[$i]} Deletado com sucesso. InvalidRegistration";
                                        } else {
                                            $html .= " Token: {$devicesToken[$i]} Falhou ao deletar. InvalidRegistration";
                                        }

                                    } else {

                                        $html .= " Token {$devicesToken[$i]} OK";

                                    }

                                $html .= '<br />';
                            }
                            $html = rtrim($html, '<br />');
                            $html .= '&nbsp;}<br />';
                            $i++;
                        }
                        $html .= '}<br />';
                    }
                    else{
                        $html .= $key.'=>'.$value.'<br />';
                    }
                }

                return $html; // PRINT RESULT

    }

    public function adicionar_Token(){

        $token['token'] = $this->input->get('token');
        $token['fk_usuario'] = $this->input->get('fk_usuario');
        $token['aparelho'] = $this->input->get('aparelho');

        if(!$this->model_webservice->novo_Token($token)){

            $this->response(array('status' => STATUS_FALHA, 'resultado' => "Sucesso"),Self::HTTP_OK); //400

        } else {

            $this->response(array('status' => STATUS_OK, 'resultado' => "Sucesso"),Self::HTTP_OK); //400

        }


    }

} // FIM Controller_webservices

class FCMPushMessage {

    var $url = 'https://fcm.googleapis.com/fcm/send';
    //Chave do Firebase
    var $serverApiKey = "AAAAKZ4qMv4:APA91bHaFwuoq8dF_PkRRQuzauNm8TIBzIPcHxXYLD5Tpwq2oxbqjJ3V7L865t6IT2AzM7uraiQMvvn4ogeX1tQqo2gznSKtXvckjfnHcVpTfRCU1euB7QACPC_s6MAwWaFPaimrCV45";
    var $devices = array();


    function setDevices($deviceIds){

        if(is_array($deviceIds)){
            $this->devices = $deviceIds;
        } else {
            $this->devices = array($deviceIds);
        }

    }

    function send($data = null){

        if(!is_array($this->devices) || count($this->devices) == 0){
            $this->error("Nenhum Aparelho informado");
        }

        if(strlen($this->serverApiKey) < 8){
            $this->error("API Key não informada");
        }

        $notificacao = array
        (
            "body"      => $data['mensagem'],
            "title"     => $data['titulo'],
            "badge"     => 1,
            'vibrate'   => 1,
            'sound'     => 1
        );

        $campos = array(
            'registration_ids'  => $this->devices,
            'content_available' => true,
            'notification'      => $notificacao,
            'priority'          => 'high',
            'data'              => $data
        );


        $headers = array(
            'Authorization: key=' . $this->serverApiKey,
            'Content-Type: application/json'
        );

        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, $this->url );

        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $campos ) );

        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }

    function error($msg){
        echo "Falha ao enviar:";
        echo "\t" . $msg;
        exit(1);
    }

}


?>
