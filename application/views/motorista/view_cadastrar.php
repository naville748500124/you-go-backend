<head>
    <meta name="viewport" content="width=device-width, user-scalable=no">

    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/cad_motorista.css" >
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/login_yougo.css" >
    <link rel="stylesheet"  href="<?php echo base_url(); ?>style/css/theme-animate.css" >
    <link type="text/css" href="<?php echo base_url(); ?>style/css/bootstrap.min.css" rel="stylesheet" />
    <script src="<?php echo base_url(); ?>style/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>style/js/script_efeito.js"></script>
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">


    <script>
        $(document).ready(function(){

            var mask = "LLL-NNNN",
                pattern = {
                    'translation': {
                        'L': {
                            pattern: /[A-Za-z]/
                        },
                        'N': {
                            pattern: /[0-9]/
                        }
                    }
                };

            $('#placa_veiculo').mask(mask, pattern);

            var url_modelo = "<?php echo base_url() ?>controller_webservice/busca_modelo?id_montadora=";
            var montadora = "25";
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": url_modelo+montadora,
                "method": "GET",
                "headers": {
                    "cache-control": "no-cache",
                    "postman-token": "b4f801dd-1000-c5aa-1315-5c6b90644816"
                }
            }
            $.ajax(settings).done(function (response) {
                $("#modelo_veiculo").html('');
                for(const i in response.modelos){
                    var clone = '<option class="opcoes_modelo" style="background: #292929;color: #fff" value="'+ response.modelos[i].id_modelo +'"> '+ response.modelos[i].modelo +'</option>';
                    $('#modelo_veiculo').append(clone);
                }

            });



            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "<?php echo base_url() ?>controller_webservice/pre_cadastro_motorista",
                "method": "GET",
                "headers": {
                    "cache-control": "no-cache",
                    "postman-token": "ea37fa41-27ad-d7a4-0305-705935aafce9"
                }
            }

            $.ajax(settings).done(function (response) {
                $("#marca_veiculo").html('');
                for(const x in response.marcas){
                    var clone = '<option class="opcoes_modelo" style="background: #292929;color: #fff" value="'+ response.marcas[x].id +'"> '+ response.marcas[x].nome +'</option>';
                    $('#marca_veiculo').append(clone);
                };


                $("#cidade_trabalho").html('');
                for(const c in response.cidades){
                    var clone = '<option class="opcoes_modelo" style="background: #292929;color: #fff" value="'+ response.cidades[c].id +'"> '+ response.cidades[c].nome +'</option>';
                    $('#cidade_trabalho').append(clone);
                };
            });




            $('#marca_veiculo').change(function () {
                var url_modelo = "<?php echo base_url() ?>controller_webservice/busca_modelo?id_montadora=";
                var montadora = document.getElementById('marca_veiculo').value;
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": url_modelo+montadora,
                    "method": "GET",
                    "headers": {
                        "cache-control": "no-cache",
                        "postman-token": "b4f801dd-1000-c5aa-1315-5c6b90644816"
                    }
                }
                $.ajax(settings).done(function (response) {
                    $("#modelo_veiculo").html('');
                    for(const i in response.modelos){
                        var clone = '<option class="opcoes_modelo" style="background: #292929;color: #fff" value="'+ response.modelos[i].id_modelo +'"> '+ response.modelos[i].modelo +'</option>';
                        $('#modelo_veiculo').append(clone);
                    }

                });
            });
        });


    </script>
    <script>

        var nome = null;
        var sexo = null;
        var rg = null;
        var cpf = null;
        var email = null;
        var confirmaremail = null;
        var senha = null;
        var confirmarsenha = null;
        var telefone = null;
        var celular = null;
        var cep = null;
        var logradouro = null;
        var complemento = null;
        var bairro = null;
        var cidade = null;
        var numero = null;
        var uf = null;
        var cidade_trabalho = null;
        var marca_veiculo = null;
        var modelo_veiculo = null;
        var placa_veiculo = null;
        var cor_veiculo = null;
        //Imagens
        var pessoal_valor = null;
        var cnh_valor = null;
        var seguro_valor = null;
        var carro1_valor = null;
        var carro2_valor = null;
        var carro3_valor = null;
        var documento_valor = null;
        var comprovante_valor = null;
        var antecedentes_valor = null;
        var rg_valor = null;



        /*recebe all do formulario */
        function consultar(){
            // Guarda o valor do campo NOME na variável nome.
            nome                = document.getElementById('nome').value;
            sexo                = document.getElementById('sexo').value;
            rg                  = document.getElementById('rg').value;
            cpf                 = document.getElementById('cpf').value;
            email               = document.getElementById('email').value;
            confirmaremail      = document.getElementById('confirmaremail').value;
            senha               = document.getElementById('senha').value;
            confirmarsenha      = document.getElementById('confirmarsenha').value;
            telefone            = document.getElementById('telefone').value;
            celular             = document.getElementById('celular').value;
            cep                 = document.getElementById('cep').value;
            logradouro          = document.getElementById('rua').value;
            complemento          = document.getElementById('complemento').value;
            bairro              = document.getElementById('bairro').value;
            cidade              = document.getElementById('cidade').value;
            numero              = document.getElementById('numero').value;
            uf                  = document.getElementById('uf').value;
            cidade_trabalho     = document.getElementById('cidade_trabalho').value;
            marca_veiculo       = document.getElementById('marca_veiculo').value;
            modelo_veiculo       = document.getElementById('modelo_veiculo').value;
            placa_veiculo       = document.getElementById('placa_veiculo').value;
            cor_veiculo         = document.getElementById('cor_veiculo').value;



            if(nome == ''){verfificar_campos();return false;}
            if(sexo == ''){verfificar_campos();return false;}
            if(rg == ''){verfificar_campos();return false;}
            if(cpf == ''){verfificar_campos();return false;}
            if(email == ''){verfificar_campos();return false;}
            if(confirmaremail == ''){verfificar_campos();return false;}
            if(senha == ''){verfificar_campos();return false;}
            if(confirmarsenha == ''){verfificar_campos();return false;}
            if(telefone == ''){verfificar_campos();return false;}
            if(celular == ''){verfificar_campos();return false;}
            if(cep == ''){verfificar_campos();return false;}
            if(logradouro == ''){verfificar_campos();return false;}
            if(bairro == ''){verfificar_campos();return false;}
            if(cidade == ''){verfificar_campos();return false;}
            if(numero == ''){verfificar_campos();return false;}
            if(uf == ''){verfificar_campos();return false;}
            if(cidade_trabalho == ''){verfificar_campos();return false;}
            if(marca_veiculo == ''){verfificar_campos();return false;}
            if(modelo_veiculo == ''){verfificar_campos();return false;}
            if(placa_veiculo == ''){verfificar_campos();return false;}
            if(cor_veiculo == ''){verfificar_campos();return false;}

            function verfificar_campos(){
                $.toast().reset('all');
                $.toast({
                    heading: ' ',
                    text: 'Preencha todos os dados',
                    hideAfter : 3000,
                    position: 'top-right',
                    icon: 'warning'
                });
            }

            /*
            ############################################
            Edu caso de algum problema remova essa parte
            ############################################
            */
            if(senha == confirmarsenha){
            }else{
                $.toast().reset('all');
                $.toast({
                    heading: ' ',
                    text: 'Senhas diferentes',
                    hideAfter : 3000,
                    position: 'top-right',
                    icon: 'success'
                });
                return false;
            }

            /*
            ############################################
                        até aqui
            ############################################
            */

            var carregou = 0;


            /*pessaol*/
            var filesSelected = document.getElementById("pessoal").files;
            if(filesSelected.length == 0){verificarBase64();return false;}
            var fileToLoad = filesSelected[0];
            var fileReader = new FileReader();
            fileReader.onload = function() {
                pessoal_valor = fileReader['result'];
                carregou++;
                if(carregou == 10){
                    enviar();
                }
            };
            fileReader.readAsDataURL(fileToLoad);

            /*CNH*/
            var filesSelected_cnh = document.getElementById("cnh").files;
            if(filesSelected.length == 0){verificarBase64();return false;}
            var fileToLoad_cnh = filesSelected_cnh[0];
            var fileReader_cnh = new FileReader();
            fileReader_cnh.onload = function() {
                cnh_valor = fileReader_cnh['result'];
                carregou++;
                if(carregou == 10){
                    enviar();
                }


            };
            fileReader_cnh.readAsDataURL(fileToLoad_cnh);


            /*seguro*/
            var filesSelected_seguro = document.getElementById("seguro").files;
            if(filesSelected.length == 0){verificarBase64();return false;}
            var fileToLoad_seguro = filesSelected_seguro[0];
            var fileReader_seguro = new FileReader();
            fileReader_seguro.onload = function() {
                seguro_valor = fileReader_seguro['result'];
                carregou++;
                if(carregou == 10){
                    enviar();
                }
            };
            fileReader_seguro.readAsDataURL(fileToLoad_seguro);

            /*carro*/
            var filesSelected_carro = document.getElementById("carro").files;
            if(filesSelected.length == 0){verificarBase64();return false;}
            var fileToLoad_carro = filesSelected_carro[0];
            var fileReader_carro = new FileReader();
            fileReader_carro.onload = function() {
                carro1_valor = fileReader_carro['result'];
                carregou++;
                if(carregou == 10){
                    enviar();
                }
            };
            fileReader_carro.readAsDataURL(fileToLoad_carro);

            /*carro _img2*/
            var filesSelected_carro_img2 = document.getElementById("carro_img2").files;
            if(filesSelected.length == 0){verificarBase64();return false;}
            var fileToLoad_carro_img2 = filesSelected_carro_img2[0];
            var fileReader_carro_img2 = new FileReader();
            fileReader_carro_img2.onload = function() {
                carro2_valor = fileReader_carro_img2['result'];
                carregou++;
                if(carregou == 10){
                    enviar();
                }
            };
            fileReader_carro_img2.readAsDataURL(fileToLoad_carro_img2);


            /*carro _img3*/
            var filesSelected_carro_img3 = document.getElementById("carro_img3").files;
            if(filesSelected.length == 0){verificarBase64();return false;}
            var fileToLoad_carro_img3 = filesSelected_carro_img3[0];
            var fileReader_carro_img3 = new FileReader();
            fileReader_carro_img3.onload = function() {
                carro3_valor = fileReader_carro_img3['result'];
                carregou++;
                if(carregou == 10){
                    enviar();
                }
            };
            fileReader_carro_img3.readAsDataURL(fileToLoad_carro_img3);

            /*documento*/
            var filesSelected_documento = document.getElementById("documento").files;
            if(filesSelected.length == 0){verificarBase64();return false;}
            var fileToLoad_documento = filesSelected_documento[0];
            var fileReader_documento = new FileReader();
            fileReader_documento.onload = function() {
                documento_valor = fileReader_documento['result'];
                carregou++;
                if(carregou == 10){
                    enviar();
                }
            };
            fileReader_documento.readAsDataURL(fileToLoad_documento);


            /*comprovante*/
            var filesSelected_comprovante = document.getElementById("comprovante").files;
            if(filesSelected.length == 0){verificarBase64();return false;}
            var fileToLoad_comprovante = filesSelected_comprovante[0];
            var fileReader_comprovante = new FileReader();
            fileReader_comprovante.onload = function() {
                comprovante_valor = fileReader_comprovante['result'];
                carregou++;
                if(carregou == 10){
                    enviar();
                }
            };
            fileReader_comprovante.readAsDataURL(fileToLoad_comprovante);




            /*rg*/
            var filesSelected_rg = document.getElementById("rg_inp").files;
            if(filesSelected.length == 0){verificarBase64();return false;}
            var fileToLoad_rg = filesSelected_rg[0];
            var fileReader_rg = new FileReader();
            fileReader_rg.onload = function() {
                rg_valor = fileReader_rg['result'];
                carregou++;
                if(carregou == 10){
                    enviar();
                }
            };
            fileReader_rg.readAsDataURL(fileToLoad_rg);



            /*antecedentes*/
            var filesSelected_antecedentes = document.getElementById("antecedentes").files;
            if(filesSelected.length == 0){verificarBase64();return false;}
            var fileToLoad_antecedentes = filesSelected_antecedentes[0];
            var fileReader_antecedentes = new FileReader();
            fileReader_antecedentes.onload = function() {
                antecedentes_valor = fileReader_antecedentes['result'];
                carregou++;
                if(carregou == 10){
                    enviar();
                }
            };
            fileReader_antecedentes.readAsDataURL(fileToLoad_antecedentes);

        }


        function verificarBase64(){
            if(pessoal_valor != '' &&
                cnh_valor != '' &&
                seguro_valor != '' &&
                carro1_valor != '' &&
                carro2_valor != '' &&
                carro3_valor != '' &&
                documento_valor != '' &&
                rg_valor != '' &&
                antecedentes_valor != '' &&
                comprovante_valor != '') {
                enviar();
            }
            else{
                $.toast().reset('all');
                $.toast({
                    heading: ' ',
                    text: 'Falha no cadastro de imagens, você deve inserir todas as imagens solicitadas',
                    hideAfter : 3000,
                    position: 'top-right',
                    icon: 'warning'
                });
            }
        }

        function enviar(){

            var settings_sha1 = {
                "async": true,
                "crossDomain": true,
                "url": "<?php  echo base_url();?>/controller_motorista/sha1",
                "method": "POST",
                "data": {
                    "senha_usuario": senha
                }
            }
            $.ajax(settings_sha1).done(function (response) {
                senha = response;


                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "<?php echo base_url() ?>controller_webservice/cadastro_motorista",
                    "method": "POST",
                    "headers": {
                        "content-type": "application/x-www-form-urlencoded",
                        "cache-control": "no-cache"
                    },
                    "data": {
                        "nome_usuario": nome,
                        "email_usuario":   email,
                        "telefone_usuario": telefone,
                        "senha_usuario": senha,
                        "facebook_usuario": "null",
                        "rg_usuario": rg,
                        "cpf_usuario": cpf,
                        "celular_usuario": celular,
                        "logradouro_usuario": logradouro,
                        "cidade_usuario": cidade,
                        "fk_uf_usuario": "1",
                        "bairro_usuario": bairro,
                        "cep_usuario": cep,
                        "complemento_usuario": complemento,
                        "num_residencia_usuario": numero,
                        "fk_genero": sexo,
                        "fk_cidade_ativa_motorista": cidade_trabalho,
                        "fk_modelo_carro_motorista": modelo_veiculo,
                        "cor_carro_motorista": cor_veiculo,
                        "placa_carro_motorista": placa_veiculo,
                        "motorista": pessoal_valor,
                        "cnh": cnh_valor,
                        "rg": rg_valor,
                        "antecedentes": antecedentes_valor,
                        "seguro":  seguro_valor,
                        "documento": documento_valor,
                        "veiculo_1": carro1_valor,
                        "veiculo_2": carro2_valor,
                        "veiculo_3": carro3_valor,
                        "comprovante": comprovante_valor
                    }
                }




                console.log(nome);
                console.log(email);
                console.log(telefone);
                console.log(senha);
                console.log(rg);
                console.log(cpf);
                console.log(celular);
                console.log(logradouro);
                console.log(cidade);
                console.log(bairro);
                console.log(cep);
                console.log(complemento);
                console.log(numero);
                console.log(sexo);
                console.log(cidade_trabalho);
                console.log(modelo_veiculo);
                console.log(cor_veiculo);
                console.log(placa_veiculo);
                console.log(pessoal_valor);
                console.log(cnh_valor);
                console.log(rg_valor);
                console.log(antecedentes_valor);
                console.log(seguro_valor);
                console.log(documento_valor);
                console.log(carro1_valor);
                console.log(carro2_valor);
                console.log(carro3_valor);
                console.log(comprovante_valor);


                $.ajax(settings).done(function (response) {
                    $.toast().reset('all');
                    $.toast({
                        heading: ' ',
                        text: 'Cadastro feito com sucesso',
                        hideAfter : 3000,
                        position: 'top-right',
                        icon: 'success'
                    });
                    window.setTimeout(function(){
                        location.reload();
                    }, 3000);

                });
                $('.app__logout').click();
            });

        }

    </script>



</head>


<style>


    .select2-container--default .select2-selection--single{
        background: transparent !important;
        background-color: transparent !important;
        border: none;
        border-bottom: 1px solid #ccc;
        border-radius: 0;
        margin-top: 10px;
    }
    input[type='search'] {
        padding: 2px 10px;
        border-radius: 0px;
        background: 0;
        border: none;
        color: #fff !important;
        border-bottom: 1px solid #292929;
        visibility: visible;
        animation-duration: 0.5s;
        animation-delay: 0s;
        animation-name: fadeInUp;
        margin-bottom: 10px;
    }
    .select2-dropdown {
        background-color: #292929;
        color: #fff;
        border: 1px solid #000;
        border-radius: 4px;
        box-sizing: border-box;
        display: block;
        position: absolute;
        left: -100000px;
        width: 100%;
        z-index: 1051;
    }
    .select2-container--default .select2-results__option--highlighted[aria-selected] {
        background-color: #f8d509;
        color: #fff;
    }
    .select2-container--default .select2-results__option[aria-selected=true] {
        background-color: #171515;
        color: #f8d509;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        color: #000 !important;
    }




    ::-webkit-input-placeholder {
        color: #999;
    }

    :-moz-placeholder { /* Firefox 18- */
        color: #999;
    }

    ::-moz-placeholder {  /* Firefox 19+ */
        color: #999;
    }

    :-ms-input-placeholder {
        color: #999;
    }



    .inp_classe{
        width: 100%;
        margin: 0 auto;
        margin-top: 9px !important;
        padding-left: 10px;
        background: transparent;
        color: #000;
        border: none;
        border-bottom: 1px solid #ccc;
    }
    .inp_classe:focus{
        border: none;
        border-bottom: 1px solid #000;
        box-shadow: none;
    }

    textarea:focus, input:focus, select:focus {
        border-bottom: 1px solid #000;
        outline: 1px;
        color: #000;
    }
    ::-webkit-scrollbar-track {
        background-color: transparent;
    }
    ::-webkit-scrollbar {
        width: 5px !important;;
        background: transparent;
    }
    ::-webkit-scrollbar-thumb  {
        border-radius: 15px;
        background: #f8d509;
    }

</style>








<div id="tudo" style="margin-top: 120px">
    <div class="col-lg-7" style="border-right: 8px solid #ccc;height: 750px;background: linear-gradient(#fff,#fff);display: table">
        <div style="margin-top: 20px;">
            <a href="<?php echo base_url();?>">
                <div class="voltar">
                    <i class="fas fa-2x fa-arrow-left" style="margin-top: 15px"></i>
                </div>
            </a>

            <div style="margin: 0 auto;display: table;width: 80%">
                <form>
                    Nome
                    <input placeholder="Nome" type="text"  class="inp_classe" id="nome" name="nome">
                    Gênero
                    <select  style="border: none;border-bottom: 1px solid #fff;margin-top: 10px" class="inp_classe" id="sexo">
                        <option value="82" style="background: #292929;color: #fff"> Masculino</option>
                        <option value="81" style="background: #292929;color: #fff"> Feminino</option>
                    </select>
                    RG
                    <input placeholder="RG" type="tel" class="inp_classe" id="rg" maxlength="12">
                    CPF
                    <input placeholder="CPF" type="tel" class="inp_classe" id="cpf" maxlength="14">
                    E-mail
                    <input placeholder="E-mail" type="email" class="inp_classe" id="email">
                    Confirmar E-mail
                    <input placeholder="Confirmar E-mail" type="email" class="inp_classe" id="confirmaremail">
                    Senha
                    <input placeholder="Senha" type="password" class="inp_classe" id="senha">
                    Confirmar Senha
                    <input placeholder="Confirmar senha" class="inp_classe" type="password" id="confirmarsenha">
                    Telefone
                    <input placeholder="Telefone" type="tel" class="inp_classe" style="width: 49%" id="telefone" maxlength="14">
                    Celular
                    <input placeholder="Celular" type="tel" class="inp_classe" style="width: 49%" id="celular" maxlength="15">
                    CEP
                    <input placeholder="CEP" type="tel" class="inp_classe" id="cep"  onblur="pesquisacep(this.value);" maxlength="9" >
                    Logradouro
                    <input placeholder="Logradouro" type="text" class="inp_classe" id="rua" style="width: 79%">
                    Número
                    <input placeholder="N°" type="tel" class="inp_classe" id="numero" style="width: 19%">
                    Bairro
                    <input placeholder="Bairro" type="text" class="inp_classe" id="bairro">
                    Cidade
                    <input placeholder="Cidade" type="text" class="inp_classe" style="width: 79%" id="cidade"><!--
                    <input placeholder="UF" type="text" class="inp_classe" style="width: 19%" id="uf" maxlength="2">-->
                    UF
                    <div style="width: 18%;margin-right: 1%;margin-top: -7px;float: right">
                        <select name="" id="uf"  >
                            <option value="1" selected>AC</option><option value="2">AL</option><option value="3">AP</option><option value="4">AM</option><option value="5">BA</option><option value="6">CE</option><option value="7">DF</option><option value="8">ES</option><option value="9">GO</option><option value="10">MA</option><option value="11">MT</option><option value="12">MS</option><option value="13">MG</option><option value="14">PA</option><option value="15">PB</option><option value="16">PR</option><option value="17">PE</option><option value="18">PI</option><option value="19">RJ</option><option value="20">RN</option><option value="21">RS</option><option value="22">RO</option><option value="23">RR</option><option value="24">SC</option><option value="25">SP</option><option value="26">SE</option><option value="27">TO</option>
                        </select>
                    </div>
                    Complemento
                    <input placeholder="Complemento" type="text" class="inp_classe" id="complemento">
                    <p>&nbsp;</p>
                    Cidade
                    <select  style="border: none;border-bottom: 1px solid #fff;margin-top: 10px"  class="inp_classe" id="cidade_trabalho">
                    </select>
                    Marca
                    <select  style="border: none;border-bottom: 1px solid #fff;margin-top: 10px" class="inp_classe" id="marca_veiculo">
                    </select>
                    Modelo
                    <select  style="border: none;border-bottom: 1px solid #fff;margin-top: 10px" class="inp_classe" id="modelo_veiculo" >
                    </select>
                    Placa
                    <input placeholder="Placa do veículo" style="text-transform:uppercase" type="text" class="inp_classe" id="placa_veiculo" maxlength="8">
                    Cor
                    <input placeholder="Cor do veículo"   type="text" class="inp_classe" id="cor_veiculo">



            </div>

        </div>
    </div>



    <div class="col-lg-5" style="overflow-y: scroll;height: 750px;background: #666;padding: 0; margin: 0;color: #fff;text-align: center">
        <div  class="tm30 wow fadeIn" data-wow-duration="1s">
            <div id="usuario"  >

                <h3 style="color: #fff">Anexos de fotos</h3>

                <div class="btn_cadastro" onclick="pessoal()" id="pessoal_abrir"><span>Pessoal</span></div>
                <div class="btn_cadastro" onclick="pessoal_fechar()" style="display: none;background: #292929" id="pessoal_sair"><span>Pessoal</span></div>
                <label for="pessoal" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="pessoal_icon"><i class="fa fa-2x fa-image"> </i> &nbsp;<img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="pessoal_aparecer_img" style="display: none" /> </label>
                <input type="file"  style="display: none" class="cadastro_imagens" id="pessoal" accept="image/*" onchange="pessoal_apareer_img(event)">
                <script>
                    function pessoal_apareer_img(event){
                        var aparecer = document.getElementById('pessoal_aparecer_img');
                        aparecer.src = URL.createObjectURL(event.target.files[0]);
                        $('#pessoal_aparecer_img').css('display','inline-block');
                    }

                    function  pessoal() {
                        $('#pessoal_icon').css('display','block');
                        $('#pessoal_sair').css('display','block');
                        $('#pessoal_abrir').css('display','none');
                    }
                    function  pessoal_fechar() {
                        $('#pessoal_icon').css('display','none');
                        $('#pessoal_sair').css('display','none');
                        $('#pessoal_abrir').css('display','block');
                    }
                </script>


                <!--Upload Rg-->
                <div class="btn_cadastro" onclick="rg_abrir()" id="rg_abrir"><span>RG</span></div>
                <div class="btn_cadastro" onclick="rg_fechar()" style="display: none;background: #292929" id="rg_sair"><span>RG</span></div>
                <label for="rg_inp" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="rg_icon"><i class="fa fa-2x fa-image"></i> &nbsp;<img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="rg_aparecer_img" style="display: none" />  </label>
                <input type="file"  style="display: none" class="cadastro_imagens tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="rg_inp"  accept="image/*" onchange="rg_apareer_img(event)">
                <script>
                    function rg_apareer_img(event){
                        var aparecer = document.getElementById('rg_aparecer_img');
                        aparecer.src = URL.createObjectURL(event.target.files[0]);
                        $('#rg_aparecer_img').css('display','inline-block');
                    }
                    function  rg_abrir() {
                        $('#rg_icon').css('display','block');
                        $('#rg_sair').css('display','block');
                        $('#rg_abrir').css('display','none');
                    }
                    function  rg_fechar() {
                        $('#rg_icon').css('display','none');
                        $('#rg_sair').css('display','none');
                        $('#rg_abrir').css('display','block');
                    }
                </script>


                <!--Upload Antecedentes-->
                <div class="btn_cadastro" onclick="antecedentes()" id="antecedentes_abrir"><span>Antecedentes</span></div>
                <div class="btn_cadastro" onclick="antecedentes_fechar()" style="display: none;background: #292929" id="antecedentes_sair"><span>Antecedentes</span></div>
                <label for="antecedentes" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="antecedentes_icon"><i class="fa fa-2x fa-image"></i> &nbsp;<img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="antecedentes_aparecer_img" style="display: none" />  </label>
                <input type="file"  style="display: none" class="cadastro_imagens tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="antecedentes"  accept="image/*" onchange="antecedentes_apareer_img(event)">
                <script>
                    function antecedentes_apareer_img(event){
                        var aparecer = document.getElementById('antecedentes_aparecer_img');
                        aparecer.src = URL.createObjectURL(event.target.files[0]);
                        $('#antecedentes_aparecer_img').css('display','inline-block');
                    }
                    function  antecedentes() {
                        $('#antecedentes_icon').css('display','block');
                        $('#antecedentes_sair').css('display','block');
                        $('#antecedentes_abrir').css('display','none');
                    }
                    function  antecedentes_fechar() {
                        $('#antecedentes_icon').css('display','none');
                        $('#antecedentes_sair').css('display','none');
                        $('#antecedentes_abrir').css('display','block');
                    }
                </script>

                <!--Upload CNH-->
                <div class="btn_cadastro" onclick="cnh()" id="cnh_abrir"><span>CNH</span></div>
                <div class="btn_cadastro" onclick="cnh_fechar()" style="display: none;background: #292929" id="cnh_sair"><span>CNH</span></div>
                <label for="cnh" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="cnh_icon"><i class="fa fa-2x fa-image"></i> &nbsp;<img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="cnh_aparecer_img" style="display: none" /> </label>
                <input type="file"  style="display: none" class="cadastro_imagens tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="cnh" accept="image/*" onchange="cnh_apareer_img(event)">
                <script>
                    function cnh_apareer_img(event){
                        var aparecer = document.getElementById('cnh_aparecer_img');
                        aparecer.src = URL.createObjectURL(event.target.files[0]);
                        $('#cnh_aparecer_img').css('display','inline-block');
                    }
                    function  cnh() {
                        $('#cnh_icon').css('display','block');
                        $('#cnh_sair').css('display','block');
                        $('#cnh_abrir').css('display','none');
                    }
                    function  cnh_fechar() {
                        $('#cnh_icon').css('display','none');
                        $('#cnh_sair').css('display','none');
                        $('#cnh_abrir').css('display','block');
                    }
                </script>

                <!--Upload seguro-->
                <div class="btn_cadastro" onclick="seguro()" id="seguro_abrir"><span>Seguro</span></div>
                <div class="btn_cadastro" onclick="seguro_fechar()" style="display: none;background: #292929" id="seguro_sair"><span>Seguro</span></div>
                <label for="seguro" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="seguro_icon"><i class="fa fa-2x fa-image"></i> &nbsp;<img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="seguro_aparecer_img" style="display: none" />  </label>
                <input type="file"  style="display: none" class="cadastro_imagens tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="seguro" accept="image/*" onchange="seguro_apareer_img(event)">
                <script>
                    function seguro_apareer_img(event){
                        var aparecer = document.getElementById('seguro_aparecer_img');
                        aparecer.src = URL.createObjectURL(event.target.files[0]);
                        $('#seguro_aparecer_img').css('display','inline-block');
                    }
                    function  seguro() {
                        $('#seguro_icon').css('display','block');
                        $('#seguro_sair').css('display','block');
                        $('#seguro_abrir').css('display','none');
                    }
                    function  seguro_fechar() {
                        $('#seguro_icon').css('display','none');
                        $('#seguro_sair').css('display','none');
                        $('#seguro_abrir').css('display','block');
                    }
                </script>


                <!--Upload carro-->
                <div class="btn_cadastro" onclick="carro()" id="carro_abrir"><span>Carro</span></div>
                <div class="btn_cadastro" onclick="carro_fechar()" style="display: none;background: #292929" id="carro_sair"><span>Carro</span></div>
                <!--img 1-->
                <label for="carro" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="carro_icon"><i class="fa fa-2x fa-image"></i> &nbsp;<img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="carro_aparecer_img" style="display: none" />  </label>
                <input type="file"  style="display: none" class="cadastro_imagens tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="carro" accept="image/*" onchange="carro_apareer_img(event)">

                <!--img 2-->
                <label for="carro_img2" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" id="carro_icon_img2"><i class="fa fa-2x fa-image"></i> &nbsp;<img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="carro_aparecer_img_img2" style="display: none" />  </label>
                <input type="file"  style="display: none" class="cadastro_imagens tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="carro_img2" accept="image/*" onchange="carro_apareer_img_img2(event)">
                <!--img 3-->
                <label for="carro_img3" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s" id="carro_icon_img3"><i class="fa fa-2x fa-image"></i> &nbsp;<img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="carro_aparecer_img_img3" style="display: none" />  </label>
                <input type="file"  style="display: none" class="cadastro_imagens tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="carro_img3" accept="image/*" onchange="carro_apareer_img_img3(event)">

                <script>
                    function carro_apareer_img(event){
                        var aparecer = document.getElementById('carro_aparecer_img');
                        aparecer.src = URL.createObjectURL(event.target.files[0]);
                        $('#carro_aparecer_img').css('display','inline-block');
                    }
                    function carro_apareer_img_img2(event){
                        var aparecer = document.getElementById('carro_aparecer_img_img2');
                        aparecer.src = URL.createObjectURL(event.target.files[0]);
                        $('#carro_aparecer_img_img2').css('display','inline-block');
                    }
                    function carro_apareer_img_img3(event){
                        var aparecer = document.getElementById('carro_aparecer_img_img3');
                        aparecer.src = URL.createObjectURL(event.target.files[0]);
                        $('#carro_aparecer_img_img3').css('display','inline-block');
                    }
                    function  carro() {
                        $('#carro_icon').css('display','block');
                        $('#carro_icon_img2').css('display','block');
                        $('#carro_icon_img3').css('display','block');
                        $('#carro_sair').css('display','block');
                        $('#carro_abrir').css('display','none');
                    }
                    function  carro_fechar() {
                        $('#carro_icon').css('display','none');
                        $('#carro_icon_img2').css('display','none');
                        $('#carro_icon_img3').css('display','none');
                        $('#carro_sair').css('display','none');
                        $('#carro_abrir').css('display','block');
                    }
                </script>


                <!--Upload documento-->
                <div class="btn_cadastro" onclick="documento()" id="documento_abrir"><span>Documentos do carro</span></div>
                <div class="btn_cadastro" onclick="documento_fechar()" style="display: none;background: #292929" id="documento_sair"><span>Documentos do carro</span></div>
                <label for="documento" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="documento_icon"><i class="fa fa-2x fa-image"></i>  <img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="documento_aparecer_img" style="display: none" />  </label>
                <input type="file"  style="display: none" class="cadastro_imagens tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="documento" accept="image/*" onchange="documento_apareer_img(event)" >
                <script>
                    function documento_apareer_img(event){
                        var aparecer = document.getElementById('documento_aparecer_img');
                        aparecer.src = URL.createObjectURL(event.target.files[0]);
                        $('#documento_aparecer_img').css('display','inline-block');
                    }
                    function  documento() {
                        $('#documento_icon').css('display','block');
                        $('#documento_sair').css('display','block');
                        $('#documento_abrir').css('display','none');
                    }
                    function  documento_fechar() {
                        $('#documento_icon').css('display','none');
                        $('#documento_sair').css('display','none');
                        $('#documento_abrir').css('display','block');
                    }
                </script>

                <!--Upload comprovante-->
                <div class="btn_cadastro" onclick="comprovante()" id="comprovante_abrir"><span>Comprovante de residência</span></div>
                <div class="btn_cadastro" onclick="comprovante_fechar()" style="display: none;background: #292929" id="comprovante_sair"><span>Comprovante de residência</span></div>
                <label for="comprovante" style="display: none" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="comprovante_icon"><i class="fa fa-2x fa-image"></i> &nbsp;<img class="tm30 wow fadeInUp img_lado_file" data-wow-duration="0.8s" data-wow-delay="0.0s" id="comprovante_aparecer_img" style="display: none" />  </label>
                <input type="file"  style="display: none" class="cadastro_imagens tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" id="comprovante"  accept="image/*" onchange="comprovante_apareer_img(event)">
                <script>
                    function comprovante_apareer_img(event){
                        var aparecer = document.getElementById('comprovante_aparecer_img');
                        aparecer.src = URL.createObjectURL(event.target.files[0]);
                        $('#comprovante_aparecer_img').css('display','inline-block');
                    }
                    function  comprovante() {
                        $('#comprovante_icon').css('display','block');
                        $('#comprovante_sair').css('display','block');
                        $('#comprovante_abrir').css('display','none');
                    }
                    function  comprovante_fechar() {
                        $('#comprovante_icon').css('display','none');
                        $('#comprovante_sair').css('display','none');
                        $('#comprovante_abrir').css('display','block');
                    }
                </script>

                </form>
                <p style="margin-top: 20px"></p>

                <button style="background: transparent;margin-top: 30px;" onclick="consultar()">

                    <div class="svg-wrapper" style="cursor: pointer">
                        <svg height="60" width="280" xmlns="http://www.w3.org/2000/svg">
                            <rect class="shape" height="60" width="312" />
                        </svg>
                        <div class="text" >Cadastrar</div>
                    </div>

                </button>

            </div>
        </div>
        <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tm30 wow fadeInUp"></div>

    </div>




</div>




<script type="text/javascript">

    function meu_callback(conteudo) {
        if (!("erro" in conteudo)) {
            //Atualiza os campos com os valores.
            document.getElementById('rua').value=(conteudo.endereco.logradouro);
            document.getElementById('bairro').value=(conteudo.endereco.bairro);
            document.getElementById('cidade').value=(conteudo.endereco.localidade);
            $('#uf').val(conteudo.endereco.uf_id).trigger('change');

        } else {
            //CEP não Encontrado.
            limpa_formulário_cep();
            alert("CEP não encontrado.");
        }
    }

    function limpa_formulário_cep() {
        //Limpa valores do formulário de cep.
        document.getElementById('rua').value=("");
        document.getElementById('bairro').value=("");
        document.getElementById('cidade').value=("");
        document.getElementById('uf').value=("");
        document.getElementById('ibge').value=("");
    }

    function pesquisacep(valor) {

        //Nova variável "cep" somente com dígitos.
        var cep = valor.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                document.getElementById('rua').value="...";
                document.getElementById('bairro').value="...";
                document.getElementById('uf').value="UF";
                document.getElementById('cidade').value="...";

                //Cria um elemento javascript.
                var script = document.createElement('script');

                //Sincroniza com o callback.
                script.src = '<?php echo base_url() ?>Controller_webservice/buscar_cep?cep='+ cep +'&callback=meu_callback';

                //Insere script no documento e carrega o conteúdo.
                document.body.appendChild(script);

            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    };





</script>








<script type="text/javascript">

    function mascara(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascara()",1)
    }
    function execmascara(){
        v_obj.value=v_fun(v_obj.value)
    }
    function mtel(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
        v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
        return v;
    }
    function mtel2(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        v=v.replace(/^(\d{5})(\d)/g,"$1-$2"); //Coloca parênteses em volta dos dois primeiros dígitos
        return v;
    }
    function mtel3(cpf){
        cpf=cpf.replace(/\D/g,"");
        cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2");
        cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2");
        cpf=cpf.replace(/(\d{3})(\d{1,2})$/,"$1-$2");

        return cpf;
    }
    function mtel4(rg){
        rg=rg.replace(/\D/g,"");
        rg=rg.replace(/(\d{2})(\d)/,"$1.$2");
        rg=rg.replace(/(\d{3})(\d)/,"$1.$2");
        rg=rg.replace(/(\d{3})(\d{1,2})$/,"$1-$2");

        return rg;
    }/*
    function mtel5(v){
        v=v.replace(/^(\d{4})(\d)/,"$1-$2");
        return v;
    }*/
    function id( el ){
        return document.getElementById( el );
    }
    window.onload = function(){
        id('telefone').onkeyup = function(){
            mascara( this, mtel );
        }
        id('celular').onkeyup = function(){
            mascara( this, mtel );
        }
        id('cep').onkeyup = function(){
            mascara( this, mtel2 );
        }
        id('cpf').onkeyup = function(){
            mascara( this, mtel3 );
        }
        id('rg').onkeyup = function(){
            mascara( this, mtel4 );
        }
        /*id('placa_veiculo').onkeyup = function(){
            mascara( this, mtel5 );
        }*/
    }
</script>
